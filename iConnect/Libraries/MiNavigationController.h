//
//  MiNavigationController.h
//  iConnect
//
//  Created by Amit Priyadarshi on 04/05/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MiNavigationController : UINavigationController
- (void)updateVehicleList;
@end
