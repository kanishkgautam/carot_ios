//
//  MIBaseViewController.h
//  iConnect
//
//  Created by Amit Priyadarshi on 29/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MIBaseViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource> {
    UIView *pickerBase;
    UIPickerView *picker;

}
-(void)menuBtnTapped:(UIButton*)btn;
-(void)logoBtnTapped:(UIButton*)btn;
-(void)deviceSelectBtnTapped:(UIButton*)btn;
-(void)notificationsBtnTapped:(UIButton*)btn;
@end
