//
//  MICollectionVC.h
//  iConnect
//
//  Created by Aditya Srivastava on 14/12/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIBaseViewController.h"
@interface MICollectionVC : MIBaseViewController <UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *collectionView;
}
@end
