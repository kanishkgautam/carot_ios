//
//  MiNavigationController.m
//  iConnect
//
//  Created by Amit Priyadarshi on 04/05/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "MiNavigationController.h"

@interface MiNavigationController ()

@end

@implementation MiNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationBar setBarTintColor:[UIColor colorWithRed:0.949 green:0.949 blue:0.925 alpha:0.920]];
    [self.navigationBar setTranslucent:YES];
//    [self.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
    NSDictionary* dict = @{NSForegroundColorAttributeName:[UIColor blackColor]};
    [self.navigationBar setTitleTextAttributes:dict];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateVehicleList {
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
