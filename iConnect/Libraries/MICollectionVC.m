//
//  MICollectionVC.m
//  iConnect
//
//  Created by Aditya Srivastava on 14/12/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import "MICollectionVC.h"

@interface MICollectionVC ()
{
    NSArray *arr;
}
@end

@implementation MICollectionVC

static NSString * const reuseIdentifier = @"Cell";
- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [flowLayout setItemSize:[self setFlowItemSize]];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(X(self.view) +5 , Y(self.view)+70+30, W(self.view), H_2(self.view)/2) collectionViewLayout:flowLayout];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.backgroundColor = [UIColor whiteColor];
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    [self.view addSubview:collectionView];
    self.automaticallyAdjustsScrollViewInsets = NO;

    arr = @[@"Dash Board",@"Trips",@"FindMyCar",@"Safety"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionViewDataSource cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionViewDataSource dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    
    
    cell.backgroundColor = [UIColor brownColor];
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, W(cell)-10, 40)];
    title.text = [arr objectAtIndex:indexPath.row];
    title.font = kLabelNormalFont;
    title.tag = indexPath.row;
    [cell.contentView addSubview:title];
    // Configure the cell
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10; // This is the minimum inter item spacing, can be more
}
-(CGSize) setFlowItemSize{
    float width;
    
    if (IS_GreaterThan_Iphone6) {
        // no. of collection view cell visible =4
        width  = (W(self.view) - 4*10)/4;
    }
    else
    {
        width = (W(self.view) - 3*10)/3;
    }
    return CGSizeMake(width, 110);
}

@end
