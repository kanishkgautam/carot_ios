//
//  MICLocationManager.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 24/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import CoreLocation


typealias CompletionBlock = (_ locationObj: CLLocation? ,_ isSuccessful:Bool) -> Void

class MICLocationManager: NSObject, CLLocationManagerDelegate {
    
    private var locationManager:CLLocationManager?
    var delegate:UIViewController?
    
    private var completionReturnBlock:CompletionBlock?
    
    private var lastLocation: CLLocation?
    
    private override init(){
        super.init()
        
    }
    
    static let sharedManager = MICLocationManager()
    
    func getCurrentLocationWithDelegate(delegateObj:UIViewController,  completion:@escaping CompletionBlock) {
        
        if locationManager == nil{
            locationManager = CLLocationManager()
            locationManager?.delegate = self
        }
        
        completionReturnBlock = completion
        self.delegate = delegateObj
        
        self.getCurrentLocation()
    }
    
    
    private func getCurrentLocation(){
        
        if CLLocationManager.locationServicesEnabled() == true{
            // location is enabled, get updated location now after checking authorisation status
            if CLLocationManager.authorizationStatus() == .notDetermined {
                // ask for permission
                locationManager?.requestWhenInUseAuthorization()
                //                locationManager?.startUpdatingLocation()
                
            }else if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied{
                // ask user to switch on permission from app settings menu
                self.askUserToSwitchOnSytemLocation()
            }else if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
                // start monitoring location
                locationManager?.startUpdatingLocation()
            }
            
        }else{
            self.askUserToSwitchOnSytemLocation()
            print("ask user to switch on location service")
            
        }
    }
    
    
    
    internal func askUserToSwitchOnSytemLocation() {
        
        let locationAlert = UIAlertController(title: "Location Services are Off", message: "Turn on location services to use map based services such as trips, find my car, real time tracking, etc.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action ) in
            
            if let settingsURL = NSURL(string: UIApplication.openSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsURL as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(settingsURL as URL)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
            //            let alert = UIAlertController(title: "Location error", message: "Please enable location services to use this feature", preferredStyle: .alert)
            
            self.delegate?.view.showIndicatorWithError("Please enable location services to use this feature")
            
            self.completionReturnBlock!(nil,false)
        }
        
        
        locationAlert.addAction(okAction)
        locationAlert.addAction(cancelAction)
        self.delegate?.present(locationAlert, animated: true, completion: nil)
    }
    
    
    internal func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse || status == .authorizedAlways || status == .notDetermined {
            self.getCurrentLocation()
        }else{
            self.askUserToSwitchOnSytemLocation()
        }
        
    }
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        self.completionReturnBlock!(nil,false)
        locationManager?.stopUpdatingLocation()
        
    }
    
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.delegate != nil {
            self.completionReturnBlock!(locations.last!,true)
            locationManager?.stopUpdatingLocation()
            
            self.delegate = nil
            
        }
    }
    
    
}
