//
//  MIBaseViewController.m
//  iConnect
//
//  Created by Amit Priyadarshi on 29/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "MIBaseViewController.h"

@interface MIBaseViewController () 

@end

@implementation MIBaseViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.view.bounds;
//    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor colorWithRed:0.941 green:0.922 blue:0.878 alpha:1.000] CGColor], nil];
//    [self.view.layer insertSublayer:gradient atIndex:0];

//    [self.view setBackgroundColor:[UIColor colorWithWhite:0.97 alpha:1.000]];
    NSDictionary* dict = @{NSForegroundColorAttributeName:[UIColor blackColor]};
    [self.navigationController.navigationBar setTitleTextAttributes:dict];

    UIColor* color = [UIColor colorWithRed:133.0f/255.0f green:24.0f/255.0f blue:70.0f/255.0f alpha:1.0f];
    
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(X(self.view)+5, Y(self.view)+70, 50, 30);
    [menuBtn addTarget:self action:@selector(menuBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setTitleColor:color forState:UIControlStateNormal];
    menuBtn.titleLabel.font = kButtonNormalFont;
    [menuBtn setTitle:@"Menu" forState:normal];
    [self.view addSubview:menuBtn];
    
    UIButton *logo = [UIButton buttonWithType:UIButtonTypeCustom];
    logo.frame = CGRectMake(X(menuBtn)+ W(menuBtn) +5, Y(self.view)+70, 50, 30);
    [logo addTarget:self action:@selector(logoBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [logo setTitleColor:color forState:UIControlStateNormal];
    logo.titleLabel.font = kButtonNormalFont;
    [logo setTitle:@"Logo" forState:normal];
    [self.view addSubview:logo];
    
    UIButton *notifications = [UIButton buttonWithType:UIButtonTypeCustom];
    notifications.frame = CGRectMake(W(self.view) - 100 -10, Y(self.view)+70, 100, 30);
    [notifications addTarget:self action:@selector(notificationsBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [notifications setTitleColor:color forState:UIControlStateNormal];
    notifications.titleLabel.font = kButtonNormalFont;
    [notifications setTitle:@"Notifications" forState:normal];
    [self.view addSubview:notifications];
    
    UIButton *deviceSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    deviceSelect.frame = CGRectMake(X(notifications) - 90 -5,Y(self.view)+70, 90, 30);
    [deviceSelect addTarget:self action:@selector(deviceSelectBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    deviceSelect.titleLabel.font = kButtonNormalFont;
    [deviceSelect setTitleColor:color forState:UIControlStateNormal];
    [deviceSelect setTitle:@"Device Select" forState:normal];
    [self.view addSubview:deviceSelect];
    
    
    
//    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(W_2(self.view)-100, H_2(self.view)-100, 200, 200)];
//    [view setBackgroundColor:[UIColor redColor]];
//    [self.view addSubview:view];
//    
//    UIInterpolatingMotionEffect *verticalMotionEffect =
//    [[UIInterpolatingMotionEffect alloc]
//     initWithKeyPath:@"center.y"
//     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
//    verticalMotionEffect.minimumRelativeValue = @(-40);
//    verticalMotionEffect.maximumRelativeValue = @(40);
//    
//    // Set horizontal effect
//    UIInterpolatingMotionEffect *horizontalMotionEffect =
//    [[UIInterpolatingMotionEffect alloc]
//     initWithKeyPath:@"height"
//     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
//    horizontalMotionEffect.minimumRelativeValue = @(-40);
//    horizontalMotionEffect.maximumRelativeValue = @(40);
//    
//    // Create group to combine both
//    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
//    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
//    
//    // Add both effects to your view
//    [view addMotionEffect:group];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor  = [UIColor whiteColor];

    [[UITabBar appearance] setBackgroundColor:AppColor(TabBGColor)];
    if (self.navigationController.viewControllers.count>1) {
        UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"appLogoBack"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTap:)];
        back.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
        self.navigationItem.leftBarButtonItems = @[back];
    }
    else {
        UIBarButtonItem* logo = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"appLogo"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.leftBarButtonItem = logo;        
    }
}
- (void)backButtonTap:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)menuBtnTapped:(UIButton*)btn{
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:kCATransition];

}
-(void)logoBtnTapped:(UIButton*)btn{
    NSLog(@"logoBtn");

}
-(void)deviceSelectBtnTapped:(UIButton*)btn{
    
    if(!pickerBase){
        pickerBase = [[UIView alloc] initWithFrame:CGRectMake(0, H([APPDelegate window]), W(self.view), 220)];
        [pickerBase setBackgroundColor:[UIColor colorWithWhite:0.98f alpha:0.90f]];
        pickerBase.alpha=0.0;
        [self.view addSubview:pickerBase];
        
        UIView* buttonBase = [[UIView alloc] initWithFrame:CGRectMake(0, 0, W(pickerBase), 40)];
        [buttonBase setBackgroundColor:[UIColor colorWithRed:80/255.0f green:77/255.0f blue:69/255.0f alpha:0.7]];
        [pickerBase addSubview:buttonBase];
        
        UIButton* doneButton = [SI miButtonOfType:MIButtonTypeDone andTitle:@"Ok"];
        [doneButton setFrame:CGRectMake(W(buttonBase)-kToolBarButtonWidth-2.5, 2.5, kToolBarButtonWidth, H(buttonBase)-5)];
        [doneButton addTarget:self action:@selector(doneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [buttonBase addSubview:doneButton];
        
        UIButton* cancelButton = [SI miButtonOfType:MIButtonTypeCancel andTitle:@"Cancel"];
        [cancelButton setFrame:CGRectMake(2.5, 2.5, kToolBarButtonWidth, H(buttonBase)-5)];
        [cancelButton addTarget:self action:@selector(cancelButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [buttonBase addSubview:cancelButton];
        
//        lblOfPickerBase = [[UILabel alloc]initWithFrame:CGRectMake(W(pickerBase)/2-50, 0, 100, 40)];
//        [pickerBase addSubview:lblOfPickerBase];
        
        
        picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, W(self.view), 180)];
        picker.delegate = self;
        picker.dataSource = self;
        [pickerBase addSubview:picker];
        
        [UIView animateWithDuration:0.3 animations:^{
            pickerBase.alpha=1.0;
        }];
    }


    [pickerBase setFrame:CGRectMake(0, H([APPDelegate window])-220, W([APPDelegate window]), 220)];
    
    [APPDelegate presentPopUpView:pickerBase cancelAction:^{
        
    }];
}

-(void)notificationsBtnTapped:(UIButton*)btn{
    NSLog(@"notificationBtn");

}

#pragma mark Picker Methods
- (void) cancelButtonTapped {
    [APPDelegate dismissPicker];
}

- (void) doneButtonTapped {
    [APPDelegate dismissPicker];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 2;
}
@end
