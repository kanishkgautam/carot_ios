//
//  BarData.swift
//  BarGraphs
//
//  Created by Vaibhav Gautam on 28/03/17.
//  Copyright © 2017 Vaibhav Gautam. All rights reserved.
//

import UIKit

class BarData: NSObject {
    
    var bottomText:String = ""
    var firstValue:Double = 0
    var secondValue:Double = 0
    var thirdValue:Double = 0
    var firstText:String = ""
    var secondText:String = ""
    var thirdText:String = ""
    var topValue:Double = 0

}
