//
//  BarCollectionCell.swift
//  BarGraphs
//
//  Created by Vaibhav Gautam on 28/03/17.
//  Copyright © 2017 Vaibhav Gautam. All rights reserved.
//

import UIKit

class BarCollectionCell: UICollectionViewCell {

    
    private let firstColor:UIColor = UIColor(red:0.52, green:0.52, blue:0.52, alpha:1.00)
    private let secondColor:UIColor = UIColor(red:0.61, green:0.65, blue:0.65, alpha:1.00)
    private let thirdColor:UIColor = UIColor(red:0.32, green:0.35, blue:0.41, alpha:1.00)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataForBar(barData:BarData){
        
        for viewObj in self.subviews {
            viewObj.removeFromSuperview()
        }
        
        var firstBarHeight:Double = 0
        var secondBarHeight:Double = 0
        var thirdBarHeight:Double = 0
        
        let percentileDelta:Double = barData.topValue - ((barData.firstValue * 0.20) + (barData.secondValue * 0.32) + (barData.thirdValue * 0.48))
        
        firstBarHeight = barData.firstValue
        firstBarHeight = ((firstBarHeight * 0.20) + (percentileDelta * 0.20)) * 2
        
        secondBarHeight = barData.secondValue
        secondBarHeight = ((secondBarHeight * 0.32) + (percentileDelta * 0.32)) * 2
        
        thirdBarHeight = barData.thirdValue
        thirdBarHeight = ((thirdBarHeight * 0.48) + (percentileDelta * 0.48)) * 2
        
        let firstBar:UIView = UIView.init(frame: CGRect(x: 5, y: (205-Int(firstBarHeight)), width: 25, height: Int(firstBarHeight)))
        firstBar.backgroundColor = firstColor
        addSubview(firstBar)
        
        let firstText:UILabel = UILabel(frame: CGRect(x: 0, y: ((firstBar.frame.size.height/2)-15), width: 25, height: 30))
        firstText.text = barData.firstText
        firstText.font = UIFont(name: "Helvetica", size: 8)
        firstText.textColor = UIColor.white
        firstText.textAlignment = .center
        firstBar.addSubview(firstText)
        
        let secondBar:UIView = UIView.init(frame: CGRect(x: 5, y: (205-Int(secondBarHeight)-Int(firstBar.frame.size.height)), width: 25, height: Int(secondBarHeight)))
        secondBar.backgroundColor = secondColor
        addSubview(secondBar)
        
        let secondText:UILabel = UILabel(frame: CGRect(x: 0, y: ((secondBar.frame.size.height/2)-15), width: 25, height: 30))
        secondText.text = barData.secondText
        secondText.font = UIFont(name: "Helvetica", size: 8)
        secondText.textColor = UIColor.white
        secondText.textAlignment = .center
        secondBar.addSubview(secondText)
        
        let thirdBar:UIView = UIView.init(frame: CGRect(x: 5, y: (205-Int(thirdBarHeight)-Int(firstBar.frame.size.height)-Int(secondBar.frame.size.height)), width: 25, height: Int(thirdBarHeight)))
        thirdBar.backgroundColor = thirdColor
        addSubview(thirdBar)
        
        let thirdText:UILabel = UILabel(frame: CGRect(x: 0, y: ((thirdBar.frame.size.height/2)-15), width: 25, height: 30))
        thirdText.text = barData.thirdText
        thirdText.font = UIFont(name: "Helvetica", size: 8)
        thirdText.textColor = UIColor.white
        thirdText.textAlignment = .center
        thirdBar.addSubview(thirdText)
        
        
        let topText:UILabel = UILabel(frame: (CGRect(x: 0, y: (thirdBar.frame.origin.y - 15), width: 35, height: 15)))
        topText.text = "\(UInt(barData.topValue))"
        topText.font = UIFont(name: "Helvetica", size: 8)
        topText.textColor = UIColor(red:0.96, green:0.55, blue:0.00, alpha:1.0)
        topText.textAlignment = .center
        addSubview(topText)
        
        let bottomText:UILabel = UILabel(frame: CGRect(x: 0, y:200, width: 35, height: 30))
        bottomText.text = barData.bottomText
        bottomText.font = UIFont(name: "Helvetica", size: 10)
        bottomText.textColor = UIColor(red:0.25, green:0.30, blue:0.33, alpha:1.0)
        bottomText.textAlignment = .center
        self.addSubview(bottomText)
        
        
        
    }
    

}
