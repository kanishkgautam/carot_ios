//
//  BarGraphView.swift
//  BarGraphs
//
//  Created by Vaibhav Gautam on 28/03/17.
//  Copyright © 2017 Vaibhav Gautam. All rights reserved.
//

import UIKit



class BarGraphView: UIView, UICollectionViewDataSource {


    @IBOutlet weak var selectedFilterTypeLabel: UILabel!
    @IBOutlet weak var percentileLabel: UILabel!
    @IBOutlet weak var barsCollectionView: UICollectionView!
    
    private var barsDataArray:[BarData] = []
    
    @IBOutlet weak var noDataLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let radians = -CGFloat(90 * Double.pi / 180)
        let rotation = CATransform3DMakeRotation(radians, 0, 0, 1.0)
        
        self.percentileLabel.layer.transform = CATransform3DTranslate(rotation, 0, 0, 0)
        self.percentileLabel.frame.origin.x = 5
        
    }
    
    func setDataForBars(dataArray:[BarData]) {
        
        self.barsDataArray = dataArray
        //let cellNib = UINib(nibName: "BarCollectionCell", bundle:nil)
        //barsCollectionView.register(cellNib, forCellWithReuseIdentifier: "BarCollectionCell")
        barsCollectionView.register(BarCollectionCell.self, forCellWithReuseIdentifier: "BarCollectionCell")
        barsCollectionView.reloadData()
        
        if self.barsDataArray.count < 1 {
            noDataLabel.isHidden = false
        }else{
            noDataLabel.isHidden = true
        }
        
    }
    
    func setBottomTextForbar(selectedType: String) -> Void {
        let bottomStr: String?
        
        switch selectedType {
        case "Daily","last 7 days":
            bottomStr = "( Day of month )"
            
        case "Weekly":
            bottomStr = "( Week of Year )"
        case "Monthly":
            bottomStr = "( Month of Year )"
        case "Yearly":
            bottomStr = "( Year )"

        default:
            bottomStr = ""
        }
        selectedFilterTypeLabel.text = bottomStr
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.barsDataArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let barData:BarData = barsDataArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BarCollectionCell", for: indexPath) as! BarCollectionCell
        cell.setDataForBar(barData: barData)
        return cell
    }

}
