//
//  DriverScoreOverlay.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 28/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DriverScoreOverlay: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    @IBOutlet weak var centreContentArea: UIView!
    
    @IBOutlet weak var fromButton: UIButton!
    @IBOutlet weak var toButton: UIButton!
    @IBOutlet weak var fromCalenderButton: UIButton!
    @IBOutlet weak var toCalenderButton: UIButton!
    
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var applyButton: UIButton!
    
    //DatePicker outlets
    @IBOutlet weak var datePickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterAndDatesConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var datePickerView: UIView!
    
    @IBOutlet weak var filterButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickerView: UIDatePicker!
    
    @IBOutlet weak var closeButton: UIButton!
    
    var selectedButtonTag = 0
    
    lazy  var fromDate = AppUtility.sharedUtility.getStartOfDay(startDateObj: Date())
    lazy var toDate = AppUtility.sharedUtility.getEndOfDay(endDateObj: Date())
    
    var filterTypeArray: [String]?
    
    var customPickerView:CustomPickerView?
    
    var barGraphView:BarGraphView?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //fetchAndDisplayData()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //fetchAndDisplayData()
    }
    override func awakeFromNib() {
        
        self.setDataforLastSevenDays()
        
        self.pickerView.maximumDate = toDate
        
        self.filterTypeArray = ["last 7 days","Daily","Weekly","Monthly","Yearly"]
        self.filterButton.setTitle(self.filterTypeArray![0] + " ▼", for: .normal)
        
        fetchAndDisplayData()
        updateViews()
        udateFrameOfFilterButton()
    }
    
    func udateFrameOfFilterButton() {
        let screenHeight:CGFloat = UIScreen.main.bounds.height
        
        if screenHeight == 480 {
            filterButtonBottomConstraint.constant = 15
            filterAndDatesConstraint.constant = 10
        }else if screenHeight == 568 {
            filterButtonBottomConstraint.constant = 50
            filterAndDatesConstraint.constant = 50
        }else{
            filterButtonBottomConstraint.constant = 80
            filterAndDatesConstraint.constant = 80
        }
    }
    
    func setDataforLastSevenDays() -> Void {
        self.fromDate = Calendar.current.date(byAdding: .day, value: -8, to: Date())! // 7days before yesterday
        self.toDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())! //yesterday
    }
    
    func isLastSevenDaysSelected() -> Bool {
        
        if self.fromDate == Calendar.current.date(byAdding: .day, value: -8, to: Date())! && self.toDate == Calendar.current.date(byAdding: .day, value: -1, to: Date())! {
            return true
            
        }
        else {
            return false
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func filterTapped(_ sender: Any) {
        
    }
    
    func setCustomPickerViewHidden(hidden: Bool) -> Void {
        if customPickerView == nil {
            return
        }
        
        if hidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.customPickerView?.view.frame.origin.y = self.bounds.height
                self.layoutIfNeeded()
                
            })
            
        }
        else {
            UIView.animate(withDuration: 0.25, animations: {
                self.customPickerView?.view.frame.origin.y = self.bounds.height - 262
                self.layoutIfNeeded()
                
            })
            
        }
        
    }
    
    func hideCustomPicker() -> Void {
        setCustomPickerViewHidden(hidden: true)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        
        
    }
    
    func updateViews() -> Void {
        
        self.toButton.setTitle(self.stringFromDate(date: self.toDate), for: .normal)
        self.fromButton.setTitle(self.stringFromDate(date: self.fromDate), for: .normal)
        
    }
    
    
    
    
    @IBAction func fromTapped(_ sender: Any) {
        //        self.setCustomPickerViewHidden(hidden: true)
        //
        //        selectedButtonTag = 0
        //        self.titleLabel.text = "Select From date"
        //        self.pickerView.minimumDate = nil
        //        self.pickerView.date = self.fromDate
        //
        //        setDatePickerHidden(hidden: false)
    }
    
    @IBAction func toTapped(_ sender: Any?) {
        
        //        self.setCustomPickerViewHidden(hidden: true)
        //
        //        selectedButtonTag = 1
        //        self.titleLabel.text = "Select To date"
        //
        //        self.pickerView.minimumDate = self.fromDate
        //        self.pickerView.date = self.toDate
        //
        //        setDatePickerHidden(hidden: false)
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        setDatePickerHidden(hidden: true)
    }
    
    @IBAction func applyTapped(_ sender: Any) {
        
        fetchAndDisplayData()
        
    }
    
    
    func setDatePickerHidden(hidden: Bool) -> Void {
        
        if hidden {
            UIView.animate(withDuration: 1, animations: {
                self.layoutIfNeeded()
                
                self.datePickerBottomConstraint.constant = -self.datePickerView.frame.height
                
            })
        }
        else {
            UIView.animate(withDuration: 1, animations: {
                self.layoutIfNeeded()
                
                self.datePickerBottomConstraint.constant = 0
            })
            
        }
    }
    
    func stringFromDate(date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY"
        
        return formatter.string(from: date as Date)
        
    }
    
    func compareDates(date1: Date, date2: Date) -> ComparisonResult {
        return Calendar.current.compare(date1, to: date2, toGranularity: .day)
        
    }
    
    func fetchAndDisplayData() {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }

        var data:[BarData] = []
        CommonData.sharedData.getSelectedVehicle { (selectedVehicle) in
            if selectedVehicle != nil{
                
                let endDate:Int64 = Int64(AppUtility.sharedUtility.getEndOfDay(endDateObj: self.toDate).timeIntervalSince1970 * 1000)
                let startDate:Int64 = Int64(AppUtility.sharedUtility.getStartOfDay(startDateObj: self.fromDate).timeIntervalSince1970 * 1000)
                
                let filterType = self.getSelectedFilter()
                self.showIndicatorWithProgress(message: "")
                CTripRequest.sharedInstance().getTripScore(forVehicle: Int32(selectedVehicle!.vehicleId), selectedGroupingType: filterType, start: startDate, end:endDate , response: { (responseObj, isSuccessful, error) in
                    self.dismissProgress()
                    
                    if let scores:[CDriveScore] = responseObj as? [CDriveScore] {
                        
                        if self.barGraphView == nil {
                            self.barGraphView = Bundle.main.loadNibNamed("BarGraphView", owner: nil, options: nil)?.first as? BarGraphView
                            self.barGraphView?.frame = CGRect(x: 0, y: (self.frame.size.height - 260), width: self.frame.size.width, height: 240)
                            self.centreContentArea.addSubview(self.barGraphView!)

                        }
                        for score in scores{
                            let bar:BarData = BarData()
                            self.setBarBottomTextForDriveScore(score: score, barData: bar)

//                            bar.firstValue = UInt(Double(score.pFuelScore) * 0.2)
//                            bar.secondValue = UInt(Double(score.pTripScore) * 0.32)
//                            bar.thirdValue = UInt(Double(score.pSafetyScore) * 0.48)
                            bar.firstValue = Double(score.pFuelScore)
                            bar.secondValue = Double(score.pTripScore)
                            bar.thirdValue = Double(score.pSafetyScore)
                            
                            bar.firstText = "\(score.pFuelScore!)"
                            bar.secondText = "\(score.pTripScore!)"
                            bar.thirdText = "\(score.pSafetyScore!)"
                            bar.topValue = Double(score.pScore!)
                            
                            data.append(bar)
                        }
                        self.barGraphView?.setDataForBars(dataArray: data)
                        
                    }
                })
            }
        }
    }
    
    func setBarBottomTextForDriveScore(score: CDriveScore, barData:BarData) -> Void {
        
        let title = self.filterButton.titleLabel!.text!.replacingOccurrences(of: " ▼", with: "")
        
        if title == self.filterTypeArray![0] {
            barData.bottomText = "\(score.day!)"
            
        }else if title == self.filterTypeArray![1] {
            barData.bottomText = "\(score.day!)"
            
        }else if title == self.filterTypeArray![2] {
            barData.bottomText = "\(score.week!)"
        }else if title == self.filterTypeArray![3] {
            barData.bottomText = "\(score.month!)"
        }else if title == self.filterTypeArray![4] {
            barData.bottomText = "\(score.year!)"
        }
    }
    
    func getSelectedFilter() -> TimeGrouping {
        let title = self.filterButton.titleLabel!.text!.replacingOccurrences(of: " ▼", with: "")
        
        if title == self.filterTypeArray![0] {
            return TimeGrouping.lastSevenDays
            
        }else if title == self.filterTypeArray![1] {
            return TimeGrouping.daily
            
        }else if title == self.filterTypeArray![2] {
            return TimeGrouping.weekly
        }else if title == self.filterTypeArray![3] {
            return TimeGrouping.monthly
        }else if title == self.filterTypeArray![4] {
            return TimeGrouping.yearly
        }
        
        return TimeGrouping.daily
    }
}
