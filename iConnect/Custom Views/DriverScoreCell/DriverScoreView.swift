//
//  DriverScoreView.swift
//  iConnect
//
//  Created by Vivek Joshi on 27/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DriverScoreView: UIViewController {
    
    @IBOutlet weak var safetyBar: UIView!
    @IBOutlet weak var efficeincyBar: UIView!
    @IBOutlet weak var fuelEconomyBar: UIView!
    
    @IBOutlet weak var driverScoreLeftView: UIView!
    @IBOutlet weak var roundedBackgroundView: UIView!
    @IBOutlet weak var driverScore: UILabel!
    
    let driverScoreLoader = LoaderView(frame: CGRect.zero)
    
    @IBOutlet weak var progressBackgroundView: LoaderView!
    @IBOutlet weak var progressView: LoaderView!
    
    
    @IBOutlet weak var safetyBarView: UIView!
    
    @IBOutlet weak var safetyLabel: UILabel!
    @IBOutlet weak var efficiencyLabel: UILabel!
    @IBOutlet weak var economyLabel: UILabel!
    
    
    @IBOutlet weak var driverScoreWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var driverScoreHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var safetyBarWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var efficiencyBarWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var economyBarWidthConstraint: NSLayoutConstraint!
    
    var driveScore: CDriveScore?
    
    var tripScore : Int?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Initialization code
        self.beautifyView()
    }
    
    func beautifyView() {
        
        //        if driverScoreLeftView.frame.size.width > driverScoreLeftView.frame.size.height {
        //            driverScoreWidthConstraint.priority = 900
        //            driverScoreHeightConstraint.priority = 999
        //        }else{
        //            driverScoreWidthConstraint.priority = 999
        //            driverScoreHeightConstraint.priority = 900
        //        }
        
        progressBackgroundView.loaderColor = UIColor(red:0.89, green:0.91, blue:0.92, alpha:1.0).cgColor
        progressBackgroundView.progress = 1.0
        
        
        progressView.loaderColor = UIColor(red:0.97, green:0.58, blue:0.11, alpha:1.0).cgColor
        progressView.progress = 0.0
        
        
        roundedBackgroundView.layer.cornerRadius = 3.0
        // setDriverScore(score: 0.5)
        // drop shadow
        roundedBackgroundView.layer.shadowColor = UIColor.black.cgColor
        roundedBackgroundView.layer.shadowOpacity = 0.2
        roundedBackgroundView.layer.shadowRadius = 1.0
        roundedBackgroundView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        
        setDriverScore(score: self.tripScore)
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func setDriverScore(score:Int?) {
        self.driverScore.text = String.init(format: "%d %", score!)
//
        
        if driveScore != nil {
            self.economyLabel.text = "\(driveScore!.pFuelScore.intValue)%"
            self.safetyLabel.text = "\(driveScore!.pSafetyScore.intValue)%"
            self.efficiencyLabel.text = "\(driveScore!.pTripScore.intValue)%"
            
            
            
            UIView .animate(withDuration: 1.5) {
                
                self.efficiencyBarWidthConstraint.constant = self.getBarWidthFromScore(value: self.driveScore?.pTripScore as! CGFloat)
                self.safetyBarWidthConstraint.constant = self.getBarWidthFromScore(value: self.driveScore!.pSafetyScore as! CGFloat)
                self.economyBarWidthConstraint.constant = self.getBarWidthFromScore(value: self.driveScore!.pFuelScore as! CGFloat)
                
                self.view.layoutIfNeeded()
            }
            
        }

        
        UIView .animate(withDuration: 2) {
            self.progressView.progress = CGFloat(score!)/100
            self.view.layoutIfNeeded()
        }
    }
    
    func getBarWidthFromScore(value: CGFloat) -> CGFloat {
        
        let totalWidth = self.safetyBarView.frame.width
        
        let trailingValue = totalWidth - (totalWidth*value/100)
        
        return trailingValue
    }
}
