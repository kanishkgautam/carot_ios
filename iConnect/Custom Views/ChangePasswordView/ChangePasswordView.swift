//
//  ChangePasswordView.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 20/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class ChangePasswordView: UIView, UITextFieldDelegate {
    
    
    @IBOutlet weak var oldPasswordTextView: UITextField!
    @IBOutlet weak var newPasswordTextView: UITextField!
    @IBOutlet weak var confirmPasswordTextView: UITextField!
    
    
    @IBOutlet weak var centreView: UIView!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    @IBAction func crossButtonTapped(_ sender: Any) {
        MICSnackbar.sharedSnackBar.superView = nil

        removeFromSuperview()
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        MICSnackbar.sharedSnackBar.superView = self
        
        if AppUtility.sharedUtility.trimWhiteSpaces(inString: oldPasswordTextView.text!).count < 6 {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: NSLocalizedString("Please provide valid password", comment: ""))
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidOldPassword", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if AppUtility.sharedUtility.trimWhiteSpaces(inString: newPasswordTextView.text!).count < 6 {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: NSLocalizedString("Password must have at least 6 characters", comment: ""))
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidNewPassword", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if AppUtility.sharedUtility.trimWhiteSpaces(inString: oldPasswordTextView.text!) == AppUtility.sharedUtility.trimWhiteSpaces(inString: newPasswordTextView.text!) {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: NSLocalizedString("New password and old password cannot be same.", comment: ""))
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidConfirmPassword", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if AppUtility.sharedUtility.trimWhiteSpaces(inString: newPasswordTextView.text!) != AppUtility.sharedUtility.trimWhiteSpaces(inString: confirmPasswordTextView.text!) {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: NSLocalizedString("Passwords do not match", comment: ""))
            AppUtility.sharedUtility.sendGAEvent(withEventName: "PasswordMismatch", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        self.showIndicatorWithProgress(message: "Updating password")
        
        CUserRequest.sharedInstance().changePassword(fromOldPassword: AppUtility.sharedUtility.trimWhiteSpaces(inString: oldPasswordTextView.text!), toNewPassword: AppUtility.sharedUtility.trimWhiteSpaces(inString: newPasswordTextView.text!)) { (responseObj, isSuccessful, error) in
            self.dismissProgress()
            
            if(error == nil){
                self.showIndicatorWithInfo("Record updated sucessfully")
                MICSnackbar.sharedSnackBar.superView = nil

                self.removeFromSuperview()
            }else{
                if responseObj != nil {
                    MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObj as! NSDictionary))
                }else{
                    self.showIndicatorWithError(error?.localizedDescription)
                    
                }
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case oldPasswordTextView:
            newPasswordTextView.becomeFirstResponder()
        case newPasswordTextView:
            confirmPasswordTextView.becomeFirstResponder()
  
        default:
            self.endEditing(true)
        }
        
        
        return true
    }
    
}
