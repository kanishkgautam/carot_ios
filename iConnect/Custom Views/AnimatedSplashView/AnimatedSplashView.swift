//
//  AnimatedSplashView.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 02/04/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class AnimatedSplashView: UIView {
    
    @IBOutlet weak var carBackgroundView: UIView!
    @IBOutlet weak var personBackgroundView: UIView!
    @IBOutlet weak var leftConnectionIcon: UIImageView!
    @IBOutlet weak var connectionCarotIcon: UIImageView!
    @IBOutlet weak var rightConnectionIcon: UIImageView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var backgroundBuildingView: UIImageView!
    @IBOutlet weak var bottomRoadImage: UIImageView!
    
    private var animationPerformed:Bool = false
    
    private var needToStopAnimations:Bool = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //        self.fetchData()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        self.fetchData()
    }
    
    override func awakeFromNib() {
        self.fetchData()
        self.placeViews()
    }
    
    
    func placeViews() {
        self.needToStopAnimations = false
        animationPerformed = false
        backgroundImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let buildingWidth:CGFloat = UIScreen.main.bounds.width
        let buildingHeight:CGFloat = UIScreen.main.bounds.width * 0.5546
        
        backgroundBuildingView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height-buildingHeight, width: buildingWidth, height: buildingHeight)
        
        bottomRoadImage.frame = CGRect(x: 0, y: (UIScreen.main.bounds.height-5), width: UIScreen.main.bounds.width, height: 7)
        personBackgroundView.frame = CGRect(x: (UIScreen.main.bounds.width-90), y: (UIScreen.main.bounds.height - 202), width: 60, height: 200)
        
        carBackgroundView.frame = CGRect(x: -220, y: (UIScreen.main.bounds.height-107), width: 220, height: 100)
        
        personBackgroundView.alpha = 0.0
        rightConnectionIcon.alpha = 0.0
        leftConnectionIcon.alpha = 0.0
        connectionCarotIcon.alpha = 0.0
        
        UIView.animate(withDuration: 1.0, animations: {
            
            self.carBackgroundView.frame = CGRect(x: 0, y: (UIScreen.main.bounds.height-107), width: 220, height: 100)
        }) { (carMoveAnimation) in
            print("🔫🔫🔫🔫🔫 CAR ANIMATION")
            UIView.animate(withDuration: 0.3, animations: {
                
                self.personBackgroundView.alpha = 1.0
                
            }, completion: { (personBackgroundAnimation) in
                print("😝😝😝😝😝 PERSON ANIMATION")
                //                    if personBackgroundAnimation == true {
                self.connectionCarotIcon.alpha = 1.0
                self.showRightConnectionIcon()
                self.showLeftConnectionIcon()
//                self.showHideLeftConnectionIcon()
                
            })
            //            }
        }
        
    }
    
    func showHideLeftConnectionIcon() {
        UIView.animateKeyframes(withDuration: 0.6, delay: 0, options: .repeat , animations: {
            self.leftConnectionIcon.alpha = 1.0
        }) { (isCompleted) in
            self.leftConnectionIcon.alpha = 0.0
            self.layer.removeAllAnimations()
        }
    }
    
    
    func hideRightConnectionIcon() {
        UIView.animate(withDuration: 0.3, animations: {
            self.rightConnectionIcon.alpha = 0.0
        }) { (completion) in
            if self.needToStopAnimations == false {
                if self.needToStopAnimations == false {
                    self.showRightConnectionIcon()
                }
            }
            
            self.animationPerformed = true
            self.checkIfViewNeedsToBeRemoved()
        }
    }
    
    func showRightConnectionIcon() {
        UIView.animate(withDuration: 0.3, animations: {
            self.rightConnectionIcon.alpha = 1.0
        }) { (completion) in
            if self.needToStopAnimations == false {
                self.hideRightConnectionIcon()
            }
        }
    }
    
    func hideLeftConnectionIcon() {
        UIView.animate(withDuration: 0.3, animations: {
            self.leftConnectionIcon.alpha = 0.0
        }) { (completion) in
            if self.needToStopAnimations == false {
                self.showLeftConnectionIcon()
            }
        }
    }
    
    func showLeftConnectionIcon() {
        UIView.animate(withDuration: 0.3, animations: {
            self.leftConnectionIcon.alpha = 1.0
        }) { (completion) in
            if self.needToStopAnimations == false {
                self.hideLeftConnectionIcon()
            }
        }
    }
    
    
    func fetchData() {
        
        CommonData.sharedData.fetchAllPrerequisitData { (dataFetchedSuccessfully) in
            if dataFetchedSuccessfully {
                self.checkIfViewNeedsToBeRemoved()
            }else{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: { 
                    self.fetchData()
                })
//                
//                self.perform(#selector(self.fetchData), with: nil, afterDelay: 3)
//                self.checkIfViewNeedsToBeRemoved()
            }
        }
        
    }
    
    
    func checkIfViewNeedsToBeRemoved() {
        if CommonData.sharedData.isDataPrefetched == true && animationPerformed == true{
            
            let notificationName = Notification.Name("UpdateDataObDashboard")
            NotificationCenter.default.post(name: notificationName, object: nil)
            self.needToStopAnimations = true
            self.removeFromSuperview()
        }
    }
    
    
}
