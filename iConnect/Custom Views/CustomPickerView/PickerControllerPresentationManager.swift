//
//  PickerControllerPresentationManager.swift
//  MedalCount
//
//  Created by Vaibhav Gautam on 02/02/17.
//  Copyright © 2017 Ron Kliffer. All rights reserved.
//



/* For using it do it like this
 
 lazy var pickerTransitionDelegate = PickerControllerPresentationManager()
 
 
 at the time of presenting just before perorming segue or presenting view controller
 
 controller.transitioningDelegate = pickerTransitionDelegate
 controller.modalPresentationStyle = .custom
 
 */

import UIKit

extension PickerControllerPresentationManager: UIViewControllerTransitioningDelegate {
  
}

class PickerControllerPresentationManager: NSObject {
  var shoulAlignViewInCenter = false
    
  func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
    let presentationController = PickerPresentationController(presentedViewController: presented, presenting: presenting)
    presentationController.shoulAlignViewInCenter = self.shoulAlignViewInCenter
    
    return presentationController
  }
  
}
