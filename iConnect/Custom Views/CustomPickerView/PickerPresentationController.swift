//
//  PickerPresentationController.swift
//  MedalCount
//
//  Created by Vaibhav Gautam on 02/02/17.
//  Copyright © 2017 Ron Kliffer. All rights reserved.
//

import UIKit

private extension PickerPresentationController {
  func setupDimmingView() {
    dimmingView = UIView()
    dimmingView.translatesAutoresizingMaskIntoConstraints = false
    dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
    dimmingView.alpha = 0.0
    
    let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
    dimmingView.addGestureRecognizer(recognizer)
    
  }
}

class PickerPresentationController: UIPresentationController {
    var shoulAlignViewInCenter = false
    
  fileprivate var dimmingView: UIView!
  
  override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
    
    super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    setupDimmingView()
    
  }
  
  /// This method added for making sure that controller dismisses when user tapped on the transparent area
  ///
  /// - Parameter recognizer: gesture performed on vide
    @objc dynamic func handleTap(recognizer: UITapGestureRecognizer) {
    presentingViewController.dismiss(animated: true)
  }
  
  
  /// this method will be called just before presenting the view to determine the annimation
  override func presentationTransitionWillBegin() {
    containerView?.insertSubview(dimmingView, at: 0)
    
    NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
    NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView]))
    
    guard let coordinator = presentedViewController.transitionCoordinator else {
      dimmingView.alpha = 1.0
      return
    }
    
    coordinator.animate(alongsideTransition: { _ in
      self.dimmingView.alpha = 1.0
    })
  }
  
  
  
  /// this method will be called just before dismissing the view to determine the annimation
  override func dismissalTransitionWillBegin() {
    guard let coordinator = presentedViewController.transitionCoordinator else {
      dimmingView.alpha = 0.0
      return
    }
    
    coordinator.animate(alongsideTransition: { _ in
      self.dimmingView.alpha = 0.0
    })
  }
  
  
  
  override func containerViewWillLayoutSubviews() {
    presentedView?.frame = frameOfPresentedViewInContainerView
    
    if self.shoulAlignViewInCenter {
        presentedView?.center = (containerView?.center)!
    }
  }
  
  
  override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
    return CGSize(width: parentSize.width, height: 260)
  }
  
  override var frameOfPresentedViewInContainerView: CGRect{
    
    var frame:CGRect = .zero
    frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView!.bounds.size)
    
    frame.origin.y = containerView!.frame.height-260
//    parentSize.height*(2.0/3.0))
    
    return frame
  }
  
}
