//
//  CustomPickerView.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 02/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit


typealias SelectionCallback = (_ selectedIndex: Int,_ selectedComponent:Int) -> Void


class CustomPickerView: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    var arrayData : NSArray = []
    var pickerTitle:String?
    
    var isShowingHeader = false
    
    var pickerSelectionCallbackObj:SelectionCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.modalPresentationStyle = .custom
        
        if let title = self.pickerTitle {
            self.titleLabel.text = AppUtility.sharedUtility.decodeString(stringToDecode: title)
        }
        else {
            self.titleLabel.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        self.returnPickerSelection()
    }
    
    private func returnPickerSelection(){
        if !isShowingHeader {
            dismiss(animated: true, completion: nil)
        }

        if pickerSelectionCallbackObj != nil {
            pickerSelectionCallbackObj!(pickerView.selectedRow(inComponent: 0), 0)
        }
    }
    
    func doneButtonCallback(selectionCallback:@escaping SelectionCallback) {
        
        
        self.pickerSelectionCallbackObj = selectionCallback
        
    }
    

    // MARK:- Pickerview methods
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return AppUtility.sharedUtility.decodeString(stringToDecode:arrayData[row] as! String)
    }
    
}
