//
//  CustomDatePicker.swift
//  iConnect
//
//  Created by Vivek Joshi on 14/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit


typealias DatePickerCallback = (_ selectedDate: Date) -> Void

class CustomDatePicker: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickerView: UIDatePicker!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
  //  var arrayData : NSArray = []
    var pickerTitle:String?
    
    var minimumDate: Date?
    var maximumDate: Date?

    var datePickerCallbackObj:DatePickerCallback?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.modalPresentationStyle = .custom
        
        if let title = self.pickerTitle {
            self.titleLabel.text = AppUtility.sharedUtility.decodeString(stringToDecode: title)
        }
        else {
            self.titleLabel.text = ""
        }
        
        if (minimumDate != nil) {
                self.pickerView.minimumDate = minimumDate
        }
        
        if maximumDate != nil {
            self.pickerView.maximumDate = maximumDate

        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
       @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        self.returnPickerSelection()

    }
    
    
    private func returnPickerSelection(){
        
        if datePickerCallbackObj != nil {
            datePickerCallbackObj!(pickerView.date)
        }
    }
    
    func doneButtonCallback(selectionCallback:@escaping DatePickerCallback) {
        
        self.datePickerCallbackObj = selectionCallback
    }
    
}
