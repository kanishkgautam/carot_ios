//
//  MICSnackbar.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 23/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

@objc enum SnackBarAnimationLength:Int {
    case short
    case long
}

class MICSnackbar: NSObject {
    
    var snackbarHeight: CGFloat     = 66
    var backgroundColor: UIColor    = .darkGray
    var textColor: UIColor          = .white
    var buttonColor:UIColor         = .cyan
    var buttonColorPressed:UIColor  = .gray
    var sbLenght: SnackBarAnimationLength = .short
    var currentVisibleText = ""
    var isVisible:Bool = false
    
    var superView: UIView?
    
    private let window = UIApplication.shared.keyWindow!
    private let snackbarView = UIView(frame: .zero)
    private let snackBarLabel: UILabel = UILabel()
    
    
    private override init(){
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(rotate), name: UIDevice.orientationDidChangeNotification, object: nil)
    }

    @objc static let sharedSnackBar = MICSnackbar()
    
    /// This method will re calculate the frames in the even of device rotation if supported
    @objc private func rotate(){
        self.snackbarView.frame = CGRect(x: 0, y: 0, width: self.window.frame.width, height: self.snackbarHeight)
    }
    
    private func setupSnackbarView(){
        if (superView != nil) {
            superView!.addSubview(snackbarView)

        }
        else {
            window.addSubview(snackbarView)

        }
        snackbarView.frame = CGRect(x: 0, y: (0-self.snackbarHeight), width: window.frame.width, height: snackbarHeight)
        snackbarView.backgroundColor = self.backgroundColor
    }
    
    

    /// This method can be used for showing s snackbar from top of screen for pre-configured short duration
    ///
    /// - Parameter stringToBeDisplayed: text which has to be displayed in snackbar
    @objc func showWithText(stringToBeDisplayed:String){
        self.showWithText(stringToBeDisplayed: stringToBeDisplayed, duration: .short)
    }
    
    
    /// This method can be used for showing snackbar from top of screen for 2 durations, short or long
    ///
    /// - Parameters:
    ///   - stringToBeDisplayed: text which has to be displayed in snackbar
    ///   - duration: duration value from the enum
    @objc func showWithText(stringToBeDisplayed:String, duration:SnackBarAnimationLength){
        if isVisible {
            self.hideWithoutAnimation()
        }

        setupSnackbarView()
        
        snackBarLabel.text = stringToBeDisplayed
        snackBarLabel.textColor = textColor
        snackBarLabel.font = UIFont(name: "Helvetica", size: 14.0)
        snackBarLabel.frame = CGRect(x: 20, y: 0, width: (window.frame.width - 40), height: snackbarHeight)
        snackBarLabel.numberOfLines = 2
        snackbarView.addSubview(snackBarLabel)
        
        self.isVisible = true
        self.currentVisibleText = stringToBeDisplayed
        
        self.animateBar(animationLength: duration)
        
    }
    
    private func animateBar(animationLength:SnackBarAnimationLength){
        
        UIView.animate(withDuration: 0.4, animations: {
            self.snackbarView.frame = CGRect(x: 0, y: 0, width: self.window.frame.width, height: self.snackbarHeight)
            
            Timer.scheduledTimer(timeInterval: TimeInterval(((animationLength == .short) ? 2 : 3)), target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
        })
    }
    
    @objc func hideWithoutAnimation(){
        self.snackbarView.frame = CGRect(x: 0, y: (0-self.snackbarHeight), width: self.window.frame.width, height: self.snackbarHeight)
        self.currentVisibleText = ""
        self.snackbarView.removeFromSuperview()
        self.isVisible = false

    }
    
    @objc func hide(){
        
        if !isVisible {
            return
        }
        UIView.animate(withDuration: 0.4, animations: { 
            
            self.snackbarView.frame = CGRect(x: 0, y: (0-self.snackbarHeight), width: self.window.frame.width, height: self.snackbarHeight)
            
        }) { (completion:Bool) in
            self.isVisible = false
            self.currentVisibleText = ""
            self.snackbarView.removeFromSuperview()
        }
    }

}
