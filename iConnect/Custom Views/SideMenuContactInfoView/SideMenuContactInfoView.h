//
//  SideMenuContactInfoView.h
//  iConnect
//
//  Created by Vaibhav Gautam on 01/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuContactInfoView : UIView<UITextViewDelegate>{
    
}

- (IBAction)sideInfoButtonTapped:(id)sender;
- (IBAction)emailButtonTapped:(id)sender;
- (IBAction)phoneButtonTapped:(id)sender;
@end
