//
//  SideMenuContactInfoView.m
//  iConnect
//
//  Created by Vaibhav Gautam on 01/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

#import "SideMenuContactInfoView.h"

@implementation SideMenuContactInfoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)sideInfoButtonTapped:(id)sender {
    [APPDelegate toggleDrawer];
}

- (IBAction)emailButtonTapped:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        [APPDelegate toggleDrawer];
        MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
        [composeViewController setMailComposeDelegate:[kAppDelegate getCentreController]];
        [composeViewController setToRecipients:@[@"help@carot.com"]];
        [composeViewController setSubject:@"example subject"];
        [[kAppDelegate getCentreController] presentViewController:composeViewController animated:YES completion:nil];
        
    }
}


- (IBAction)phoneButtonTapped:(id)sender {
    
    NSString *phoneNumber = @"tel://+91-11-3959595";
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]]) {
        [APPDelegate toggleDrawer];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
}
@end
