//
//  HeaderPickerArea.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 08/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

typealias vehicleSelectionCallback = (_ selectedIndex: Int) -> Void

class HeaderPickerArea: UIView {
    
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var pointingArrow: UILabel!
    
    private var selectedVehicleIndex:Int = -1
    
    var callback:vehicleSelectionCallback?
    
    var vehicles:Array<Any>?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    
    @IBAction func headerButonTapped(_ sender: Any) {
        if callback != nil {
            callback!(selectedVehicleIndex)
        }
    }
    
    
    // TODO: we need to pass proper arrays and selected data also
    func setVehiclesDataOnHeaderWith(vehiclesArray:Array<Any>, selectionCallback: @escaping vehicleSelectionCallback) {
        self.callback = selectionCallback
        vehicles = vehiclesArray
    }
    
    
    

}
