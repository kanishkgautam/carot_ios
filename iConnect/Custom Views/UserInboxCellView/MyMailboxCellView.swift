//
//  MyMailboxCellView.swift
//  iConnect
//
//  Created by Vivek Joshi on 23/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MyMailboxCellView: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.dateLabel.isHidden = true
        self.descriptionLabel.numberOfLines = 2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        if selected {
//            self.descriptionLabel.numberOfLines = 0
//            self.layoutIfNeeded()
//
//        }else {
//            self.descriptionLabel.numberOfLines = 2
//            self.layoutIfNeeded()
//
//        }
        // Configure the view for the selected state
    }
    
    func setCellData(inboxItem: CUserInboxItem) -> Void {

//        if inboxItem.expiryDate < NSDate() as Date {
//            self.updateExpiryStatus(isExpired: true)
//            
//        } else {
//            self.updateExpiryStatus(isExpired: false)
//        }
        
        if inboxItem.isRead.boolValue {
            self.titleLabel.textColor = UIColor(red:0.61, green:0.72, blue:0.80, alpha:1.0)
            self.descriptionLabel.textColor = UIColor(red:0.61, green:0.72, blue:0.80, alpha:1.0)
            //self.dateLabel.textColor =  UIColor(red:0.61, green:0.72, blue:0.80, alpha:1.0)
        }
        else {
            self.titleLabel.textColor = UIColor(red: 247/255, green: 148/255, blue: 29/255, alpha: 1)
            self.descriptionLabel.textColor = UIColor(red: 64/255, green: 76/255, blue: 85/255, alpha: 1)
            
        }
        
        self.titleLabel.text = inboxItem.title
        self.descriptionLabel.text = inboxItem.message
        //self.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: inboxItem.expiryDate)

    }
    
//    func updateExpiryStatus(isExpired: Bool) -> Void {
//        
//        if isExpired {
//            self.titleLabel.textColor = UIColor(red:0.61, green:0.72, blue:0.80, alpha:1.0)
//            self.descriptionLabel.textColor = UIColor(red:0.61, green:0.72, blue:0.80, alpha:1.0)
//            //self.dateLabel.textColor =  UIColor(red:0.61, green:0.72, blue:0.80, alpha:1.0)
//        }
//        else {
//            self.titleLabel.textColor = UIColor(red: 247/255, green: 148/255, blue: 29/255, alpha: 1)
//            self.descriptionLabel.textColor = UIColor(red: 64/255, green: 76/255, blue: 85/255, alpha: 1)
//
//        }
//        
//    }
    
}
