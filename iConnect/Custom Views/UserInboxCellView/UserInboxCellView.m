//
//  UserInboxCellView.m
//  iConnect
//
//  Created by Amit Priyadarshi on 07/10/15.
//  Copyright © 2015 Amit Priyadarshi. All rights reserved.
//

#import "UserInboxCellView.h"

@implementation UserInboxCellView

- (void)awakeFromNib {
    // Initialization code
    self.lblTitle.numberOfLines = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
}

- (void)setUIforMessageTypeRead:(BOOL)isRead {
    if (isRead) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        self.lblTitle.font = kNormalAppFont(UnivVal(12, 14, 14));
    }
    else {//
        [self setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:233.0f/255.0f blue:189.0f/255.0f alpha:1.0f]];
        [self.contentView setBackgroundColor:[UIColor colorWithRed:250.0f/255.0f green:233.0f/255.0f blue:189.0f/255.0f alpha:1.0f]];
        self.lblTitle.font = kBoldAppFont(UnivVal(12, 14, 14));
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
}
@end
