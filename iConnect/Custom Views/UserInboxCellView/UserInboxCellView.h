//
//  UserInboxCellView.h
//  iConnect
//
//  Created by Amit Priyadarshi on 07/10/15.
//  Copyright © 2015 Amit Priyadarshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInboxCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIButton *imgCheck;
@property (weak, nonatomic) IBOutlet UIImageView *activeIndicator;
@property (assign) BOOL isSelected;

- (void)setUIforMessageTypeRead:(BOOL)isRead;
@end
