//
//  AppDelegate.h
//  iConnect
//
//  Created by Administrator on 11/17/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "CAROT-Swift.h"
@class CentreViewController;
@import Firebase;

#define APPDelegate (AppDelegate*)[[UIApplication sharedApplication] delegate]

typedef enum {
    MenuItemProfile,
    MenuItemChangePassword,
    MenuItemManageAccount,
    MenuItemManageAlerts,
    MenuItemEmergencyContact,
    MenuItemUserInbox,
    MenuItemDocumentWallet,
    MenuItemLogout,
    MenuItemHome,
    MenuItemConfirmMail,
    MenuItemAddDevice,
    MenuItemBuySubscription,
    MenuItemBuySubscriptionFromPush,
    MenuItemBuyUpdateApplication,
    MenuItemFindMyCar,
    MenuItemLogIn,
    
    
}MenuItem;
@class SideMenuContactInfoView;

typedef void (^PickerCancel) ();
//NSString *emailSignUp;
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
//     PKRevealController *revealController;
    UIView* _blockerView;
    PickerCancel _pickerCancel;

    CentreViewController *centreController;
    SideMenuContactInfoView *contactInfo;
    MFSideMenuContainerViewController *container;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString* deviceToken;
@property (nonatomic, strong) UIView* blockerView;

- (void)addBackButtonOnViewController:(UIViewController *)vc;
- (void)navigateToScreenForSelectedMenu:(MenuItem)item;
- (void)presentPopUpView:(UIView*)pickerView cancelAction:(PickerCancel)cancelBlock ;
- (void)toggleDrawer;
- (void)dismissPicker;
- (CentreViewController *)getCentreController;
- (AppDelegate *) app;
@end
