//
//  Preferences.swift
//  iConnect
//
//  Created by Vivek Joshi on 06/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class Preferences: NSObject {
    
    var serialNumber: String?
    var defaultValue: String?
    var defaultValueMeasuringUnit: String?
    var editable: Bool?
    var enabled: Bool?
    var preferenceId: NSNumber?
    var preferenceName : String?
    var showDefaultValue: Bool?
    var vehicleAlarmType: String?
    var vehicleEventType: String?
    var value: String?
    var criticalEvent: String?
    
    func setPropertiesFromDictionary(dict: NSDictionary) {
        print(dict)
        
        self.defaultValue =  (dict["defaultValue"] as? String) ?? ""
        self.editable =  ((dict["editable"] as? NSNumber) ?? 0).boolValue
        self.enabled = ((dict["enabled"] as? NSNumber) ?? 0).boolValue
        self.showDefaultValue =  ((dict["showDefaultValue"] as? NSNumber) ?? 0).boolValue
        self.defaultValueMeasuringUnit = (dict["defaultValueMeasuringUnit"] as? String) ?? ""
        self.preferenceId = (dict["id"] as? NSNumber) ?? 0
        self.preferenceName =  (dict["preferenceName"] as? String) ?? ""
        self.vehicleAlarmType = (dict["vehicleAlarmType"] as? String) ?? ""
        self.vehicleEventType = (dict["vehicleEventType"] as? String) ?? ""
        
        self.value = (dict["value"] as? String) ?? ""
        self.criticalEvent = (dict["criticalEvent"] as? String) ?? ""
        
    }
    
    
//    + (EventPreferences*)eventPreferenceWithId:(NSInteger)epid {
//    NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"EventPreferences"];
//    NSEntityDescription* entity = [NSEntityDescription entityForName:@"EventPreferences" inManagedObjectContext:MOC];
//    [request setEntity:entity];
//    
//    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"preferenceId=%ld",epid];
//    [request setPredicate:predicate];
//    
//    NSError *error = nil;
//    NSArray *result = [MOC executeFetchRequest:request error:&error];
//    if (error) {
//    NSLog(@"%s Unable to execute fetch request.",__FUNCTION__);
//    NSLog(@"%@, %@", error, error.localizedDescription);
//    return nil;
//    } else {
//    if (result.count) {
//    return result.lastObject;
//    }
//    return nil;
//    }
//    return nil;
//    }
//    + (EventPreferences*)newSafeEventPreferencesFromDictionary:(NSDictionary*)dict isNew:(BOOL*)isNew{
//    
//    if (!dict) return nil;
//    EventPreferences* oldPreference = [EventPreferences eventPreferenceWithId:[NonNullObject(dict, @"id") integerValue]];
//    if (oldPreference) {
//    [oldPreference setPropertiesFromDictionary:dict];
//    *isNew = NO;
//    return oldPreference;
//    }
//    else {
//    EventPreferences* newObj = [NSEntityDescription insertNewObjectForEntityForName:@"EventPreferences" inManagedObjectContext:MOC];
//    [newObj setPropertiesFromDictionary:dict];
//    *isNew = YES;
//    return newObj;
//    }
//    }
//    + (NSArray*)allEventPrefrences
//    
    
    
    
    
}
