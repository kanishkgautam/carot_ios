//
//  AppDelegate.m
//  iConnect
//
//  Created by Administrator on 11/17/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "CAROT-Swift.h"
#import "SideMenuContactInfoView.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LoginVc.h"
@import GooglePlaces;
@import GoogleMaps;

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
@import GoogleMaps;

NSString *googleApiKey = @"AIzaSyDeDarI_BW90aUeFulaGmU05IAV1Lkui5M";

#endif

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
#endif

//#import "CentreViewController.h"

@class CentreViewController;
@class LoginVc;

//#import "PKRevealController.h"
@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

NSInteger minimumAppBuildNumber = 80;

@synthesize deviceToken;
@synthesize  blockerView=alertBlockerView;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    @try {
        [SICUserRequest getUserReadableStrings:^(id response, BOOL isSuccessful, NSError *error) {
            
            if(isSuccessful)
                NSLog(@"User Readable strings saved");
            else
                NSLog(@"User Readable strings ERROR");
        }];
    }
    @catch (NSException *exception)
    {
        NSLog( @"NSException caught");
        NSLog( @"Name: %@", exception.name);
        NSLog( @"Reason: %@", exception.reason );
    }
    [self initialiseCarotSDK];
    //Initialize Here map SDK
    [NMAApplicationContext setAppId:kHelloMapAppID appCode:kHelloMapAppCode licenseKey:kHelloMapAppLicense];
    
    [FIRApp configure];
    [GMSServices provideAPIKey:googleApiKey];
    [GMSPlacesClient provideAPIKey:googleApiKey];

    // [START set_messaging_delegate]
    [FIRMessaging messaging].delegate = self;
    // [END set_messaging_delegate]
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    /*
     Commented by vaibhav for FCM
     if (OSVersionGreterOrEqual(8.0)) {
     UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
     [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
     }else {
     [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
     
     }
     */
    [[CommonData sharedData] fetchErrorStrings];
    
    [self setupSideMenu];
    
    NSDictionary *userInfo = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
    [self checkIfUserLoggedInWithNotification:apsInfo];
    
    //Initialize Fabric
    [Fabric with:@[[Crashlytics class]]];
    //Fabric initialization done
    
    // Initialize google SDK
    // Configure tracker from GoogleService-Info.plist.
    //    NSError *configureError;
    //    [[GGLContext sharedInstance] configureWithError:&configureError];
    //    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
//    [[GAI sharedInstance] setTrackUncaughtExceptions:YES];
    // google SDK initialisation ends here
    
    
    return YES;
}

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler();
}

#endif




-(void)initialiseCarotSDK{
    /*
     honda raw =sdk_ios:$4a$54$iujoiujXiHONDwr/KiIU90e33TioIKioiIrImrOxTHVGWuSswGuW9
     carot raw = iconnect_ios:$2a$11$gxpnezmYfNJRYnw/EpIK5Oe08TlwZDmcmUeKkrGcSGGHXvWaxUwQ2
     */
    [[Carot sharedInstance] initialiseSDKWithPlatformKey:@"iconnect_ios:" clientRawKey:@"aWNvbm5lY3RfYW5kcm9pZDokMmEkMTEkZ3hwbmV6bVlmTkpSWW53L0VwSUs1T2UwOFRsd1pEbWNtVWVLa3JHY1NHR0hYdldheFV3UTI=" signUpSource:@"" clientHereMapsID:@"aB7olMEXCKtkqgwAbs47" clientHereMapsAppCode:@"udRH-A7OAODFmuCaPq2Y5Q" hereMapsRestAppID:@"8dzXrQZeCsDfYK0aWqKy" hereMapsRestAppCode:@"hLv4ERBl1OuLQFqvMIfXayLxlNS7XeGXqbEOA7SGd38"];
    
}

-(void)setupSideMenu{
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    container = (MFSideMenuContainerViewController *)self.window.rootViewController;
    [container setShadow:nil];
    if (!centreController) {
        centreController = [storyboard instantiateViewControllerWithIdentifier:@"CentreViewController"];
    }
    centreController.selectedIndex = 0;
    
    UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
    
    [container setCenterViewController:centreController];
    [container setLeftMenuWidth:150];
    [container setLeftMenuViewController:leftSideMenuViewController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateChanged:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleDrawer) name:@"toggleLeftMenu" object:nil];
    
}

-(void)menuStateChanged:(NSNotification *)notification{
    
    MFSideMenuStateEvent eventObj = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    if (eventObj == MFSideMenuStateEventMenuDidOpen) {
        if (!contactInfo) {
            contactInfo = [[NSBundle mainBundle] loadNibNamed:@"SideMenuContactInfoView" owner:nil options:nil].firstObject;
            [contactInfo setFrame:CGRectMake(150, 0, [UIScreen mainScreen].bounds.size.width-150, [UIScreen mainScreen].bounds.size.height)];
        }
        [[[[UIApplication sharedApplication] windows] lastObject] addSubview:contactInfo];
    }
}

-(void)toggleDrawer{
    
    [contactInfo removeFromSuperview];
    [container toggleLeftSideMenuCompletion:^{
        if (container.menuState == MFSideMenuStateClosed){
            container.panMode = MFSideMenuPanModeDefault;
        }else{
            container.panMode = MFSideMenuPanModeNone;
        }
    }];
    //    [container setMenuState:MFSideMenuStateClosed completion:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [SI postDataUsageOnFlurry];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // [Flurry logEvent:@"AppEnterForeground"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateManageAlertUI" object:nil];
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // [Flurry logEvent:@"AppBecomeActive"];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    [[MICSnackbar sharedSnackBar] hide];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // [Flurry logEvent:@"AppWillTerminate"];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)dToken {
    
    [FIRMessaging messaging].APNSToken = dToken;
    NSString *token = [FIRMessaging messaging].FCMToken;

    [self updateDeviceToken:token];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"error is ==== >>> %@",error.debugDescription);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //[Flurry logEvent:@"AppNotification"];
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    NSString* values = [userInfo objectForKey:@"tag"];
    if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
        // take user to specific screen
        [self handleRemoteNotificationWithInfo:values];
    }else{
        // show data in alert
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    NSString* values = [userInfo objectForKey:@"tag"];
    if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
        // take user to specific screen
        [self handleRemoteNotificationWithInfo:values];
    }else{
        // show data in alert
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)handleRemoteNotificationWithInfo:(NSString *)tag {
    
    if (!tag) {
        return;
    }
    
    //return;
    
    if ([tag isEqualToString:@"events"]) {
        [self showEventsScreen];
    }
    else if ([tag isEqualToString:@"renew_subscription"]) {
        [self showManageDevicesScreen];
    }
    else if ([tag isEqualToString:@"theftalarm"]) {
        
        //            NSString *playSoundOnAlert = @"redAlert.wav";
        //            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] resourcePath],playSoundOnAlert]];
        //            NSError *error;
        //
        //            if (_audioPlayer) {
        //                _audioPlayer = nil;
        //            }
        //            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        //            _audioPlayer.numberOfLoops = 0;
        //            [_audioPlayer play];
    }
    else if ([tag isEqualToString:@"terminated"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSubscription" object:nil];
        
        [self showManageDevicesScreen];
        
    }//megha
    else if ([tag isEqualToString:@"promotion"]) {
        [self showMyMailboxScreen];
    }
    else if ([tag isEqualToString:@"home"]) {
        // [self navigateToScreenForSelectedMenu:MenuItemHome];
        
    }
    else if ([tag isEqualToString:@"subscription_activated"]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSubscription" object:nil];
        
    }
}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    
    [self updateDeviceToken:fcmToken];
    
}

- (void)updateDeviceToken:(NSString*)newtoken {
    
    if (!newtoken) {
        return;
    }
    
    BOOL checkUser = [SICUserRequest isUserLoggedIn];
    
    if (checkUser == NO) {
        [[NSUserDefaults standardUserDefaults] setObject:newtoken forKey:@"deviceToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"✅ First Token %@",newtoken);
        
        return;
    }
    
    NSString *token = newtoken;
    NSString *oldToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    NSLog(@"✅✅✅✅✅ refresh %@",token);
    if ((oldToken == nil || oldToken == NULL || oldToken != token) && token != nil ) {
        [[CUserRequest sharedInstance] getLoginHistoryDetailsWithToken:token response:^(id response, BOOL isSuccessful, NSError *error) {
            
            if (isSuccessful == YES) {
                [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"deviceToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else {
                NSLog(@"Unable to get login history for REFRESH TOKEN ");
                
            }
            
        }];
    }
}

- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"Received data message: %@", remoteMessage.appData);
}

- (void)showEventsScreen {
    
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertsNavController"];
    viewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [centreController presentViewController:viewController animated:true completion:nil];
    
}

- (void)showMyProfileScreen {
    
    [centreController performSegueWithIdentifier:kCentreToMyProfileSegue sender:nil];
}

- (void)showMyMailboxScreen {
    
    [centreController performSegueWithIdentifier:kCentreToMailboxSegue sender:nil];
}

- (void)showManageDevicesScreen {
    //SideMenuViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
    
    [centreController performSegueWithIdentifier:kCentreToDeviceManagementSegue sender:nil];
    
}


- (void)addBackButtonOnViewController:(UIViewController *)vc {
    //    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"backbtn"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapedFromInnerViews:)];
    //    back.width = 25.0f;
    //
    //    UIBarButtonItem* logo = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"navMindLogo"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:nil action:nil];
    //    logo.width = 25.0f;
    //    logo.imageInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    
    //
    //    UIBarButtonItem *negativeSpacer1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
    //                                                                                     target:nil action:nil];
    //    negativeSpacer1.width = -15.0f;
    //    [vc.navigationItem setLeftBarButtonItems:nil animated:NO];
    //    vc.navigationItem.leftBarButtonItems = @[negativeSpacer1, back,negativeSpacer1,logo];
    UIBarButtonItem* back = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"backbtn.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapedFromInnerViews:)];
    back.imageInsets = UIEdgeInsetsMake(0, -15, 0, 0);
    vc.navigationItem.leftBarButtonItems = @[back];
}


- (void)backButtonTapedFromInnerViews:(id)sender {
    //[Flurry logEvent:@"AppDelegete-BackFromInnerView"];
    [self navigateToScreenForSelectedMenu:MenuItemHome];
}

- (void)navigateToScreenForSelectedMenu:(MenuItem)item {
    
    switch (item) {
            
        case MenuItemHome:
        {
            [UIView transitionWithView:self.window
                              duration:0.6f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^(void) {
                                BOOL oldState = [UIView areAnimationsEnabled];
                                [UIView setAnimationsEnabled:NO];
                                //                            [revealController setFrontViewController:mainTabBar];
                                [UIView setAnimationsEnabled:oldState];
                            }
                            completion:nil];
            
        }break;
    }
}


- (void)presentPopUpView:(UIView*)pickerView cancelAction:(PickerCancel)cancelBlock {
    CGRect frame = pickerView.frame;
    CGRect backupFrame = pickerView.frame;
    [_blockerView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView* view = (UIView*)obj;
        [view removeFromSuperview];
    }];
    _pickerCancel = cancelBlock;
    _blockerView = [[UIView alloc] initWithFrame:self.window.bounds];
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(presentationCancled:)];
    [_blockerView addGestureRecognizer:tapGesture];
    [_blockerView setFrame:self.window.bounds];
    [self.window addSubview:_blockerView];
    
    [_blockerView addSubview:pickerView];
    
    frame.origin = CGPointMake(frame.origin.x, H(_blockerView));
    [pickerView setFrame:frame];
    
    frame.origin = CGPointMake(frame.origin.x, H(_blockerView)-frame.size.height);
    [_blockerView setBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.750]];
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [pickerView setFrame:backupFrame];
        [_blockerView setBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.850]];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.05f animations:^{
            [_blockerView setBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.920]];
        }];
    }];
}

- (void)presentationCancled:(id)sender {
    //  [Flurry logEvent:@"AppDelegete-PopUpTouchCancel"];
    [self dismissPicker];
    _pickerCancel();
}

- (void)dismissPicker {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2f animations:^{
            [[_blockerView subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                UIView* popup = obj;
                popup.frame = CGRectMake(X(popup), H(_blockerView), W(popup), H(popup));
            }];
            [_blockerView setAlpha:0.3f];
        } completion:^(BOOL finished) {
            [_blockerView removeFromSuperview];
        }];
    });
}

-(void)checkIfUserLoggedInWithNotification:(NSDictionary*)dict{
    
//    NSInteger upgradedBuildNumber = [[NSUserDefaults standardUserDefaults] integerForKey:@"upgradedBuildNumber"];
//
//    if (upgradedBuildNumber < minimumAppBuildNumber) {
//
//        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
//        NSString *buildNumber = [info objectForKey:@"CFBundleVersion"];
//
//        [[NSUserDefaults standardUserDefaults] setInteger:buildNumber.integerValue forKey:@"upgradedBuildNumber"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        [self updatePushToken];
//        return;
//
//    }
    
    //    return;
    BOOL checkUser = [SICUserRequest isUserLoggedIn];
    NSLog(@"%d",checkUser);
    
    
    
    if(!checkUser){
        [self performSelector:@selector(showLoginScreen) withObject:nil afterDelay:0];
    }else {
        // [centreController showAnimationViewAndUpdateDataInBackground];

        if (dict) {
            NSString* values = [dict objectForKey:@"tag"];
            [self performSelector:@selector(handleRemoteNotificationWithInfo:) withObject:values afterDelay:2.0f];
            
            //            [self handleRemoteNotificationWithInfo:dict];
        } else {
            [centreController showAnimationViewAndUpdateDataInBackground];
        }
        
        [self getUserDevices];
        [[CommonData sharedData] getAllFuelTypesWithFuels:^(NSArray<CFuelType *>  * _Nonnull fuelName) {
            NSLog(@"data fetched for fuels");
        }];
        
    }
    // [self showEventsScreen];
    //Check if user has any device: if No then logout and present Login screen again
}

//- (void)updatePushToken {
//
//    [SICUserRequest logoutUser];
//    [[CommonData sharedData] clearDataForLogout];
//
//    [self performSelector:@selector(showLoginScreen) withObject:nil afterDelay:0];
//    [[UIApplication sharedApplication] registerForRemoteNotifications];
//}

-(void)getUserDevices
{
    [SICVehicleRequest getUserDevicesResponse:^(id response, BOOL isSuccessful, NSError *error) {
        if(isSuccessful){
            NSArray *arrayResponse = response;
            __block int flag = 0;
            
            if(arrayResponse.count == 0){
                [SICUserRequest logoutUser];
                [[CommonData sharedData] clearDataForLogout];
                
                [self performSelector:@selector(showLoginScreen) withObject:nil afterDelay:1.0f];
                
            }
            else {
                [arrayResponse enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    CDevice *deviceObj = (CDevice*)obj;
                    if([deviceObj.status isEqualToString:kDeviceStatusSubscriptionPending] || [deviceObj.status isEqualToString:kDeviceStatusActivationInProgress]){
                        flag++;
                    }
                }];
                
                if(arrayResponse.count == flag){
                    
                    [self showLoginScreen];
                    
                    //                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    //                    LoginVc *loginVC = (LoginVc *)[storyboard instantiateViewControllerWithIdentifier:@"loginNavController"];
                    //
                    //                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginVC];
                    //                    navController.navigationBar.translucent = false;
                    //                    [self.window setRootViewController:navController];
                    
                    
                    
                    //[SICUserRequest logoutUser];
                    //[self performSelector:@selector(showLoginScreen) withObject:nil afterDelay:1.0f];
                    //                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    //                    DeviceManagementViewController *deviceManagementViewController = (DeviceManagementViewController *)[storyboard instantiateViewControllerWithIdentifier:@"DeviceManagementViewController"];
                    //                    deviceManagementViewController.hideBackButton = true;
                    //                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:deviceManagementViewController];
                    //                    navController.navigationBar.translucent = false;
                    //                    [self.window setRootViewController:navController];
                    //                    [self.window presentViewController:navController animated:YES completion:^{
                    //                        // nothing to do here for now
                    //                    }];
                }
                
            }
        }
        else{
            
            //            [SICUserRequest logoutUser];
            //            [[CommonData sharedData] clearDataForLogout];
            //            [self performSelector:@selector(showLoginScreen) withObject:nil afterDelay:1.0f];
        }
        
    }];
}

-(void)showLoginScreen{
    
    UINavigationController *loginScreenController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"loginNavController"];
    
    if (loginScreenController) {
        
       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
       LoginVc *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVc"];
       loginVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [centreController presentViewController:loginVC animated:YES completion:^{
            
        }];
        
//        [centreController performSegueWithIdentifier:@"CentreToLoginScreen" sender:nil];
    }
}

- (CentreViewController *)getCentreController{
    if (centreController) {
        return centreController;
    }
    return nil;
}

-(AppDelegate *) app
{
    return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}
@end
