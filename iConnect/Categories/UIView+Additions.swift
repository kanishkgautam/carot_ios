//
//  UIView+Additions.swift
//  iConnect
//
//  Created by Vivek Joshi on 01/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import FTIndicator

extension UIView {
    
    @objc func showIndicatorWithError(_ errorMessage: String?) -> Void {
        
        
        if let error = errorMessage {
            DispatchQueue.main.async {
                FTIndicator.setIndicatorStyle(.dark)
                FTIndicator.showError(withMessage: NSLocalizedString(error, comment: ""))
                
            }
        }
    }
    
    @objc func showIndicatorWithInfo(_ info: String) -> Void {
        DispatchQueue.main.async {

        FTIndicator.setIndicatorStyle(.dark)
        FTIndicator.showInfo(withMessage: NSLocalizedString(info, comment: ""))
        }
    }
    
    @objc func showIndicatorWithProgress(message: String) -> Void {
        DispatchQueue.main.async {
            FTIndicator.setIndicatorStyle(.dark)
            FTIndicator.showProgress(withMessage: NSLocalizedString(message, comment: ""), userInteractionEnable: false)
            
        }
    }
    
    @objc func showIndicatorWithSuccess(_ message: String) -> Void {
        DispatchQueue.main.async {

        FTIndicator.setIndicatorStyle(.dark)
        FTIndicator.showSuccess(withMessage: NSLocalizedString(message, comment: ""))
        }
    }
    
    @objc func dismissProgress() -> Void {
        DispatchQueue.main.async {
            FTIndicator.dismissProgress()
        }
    }
}
