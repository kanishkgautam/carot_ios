//
//  Singleton.h
//  iConnect
//
//  Created by Aditya Srivastava on 07/12/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import <UIKit/UIKit.h>

@interface Singleton : NSObject{
NSMutableDictionary* dataUsagePerSession;

}
typedef enum {
    MIButtonTypeNormal,
    MIButtonTypeDone,
    MIButtonTypeCancel,
}MIButtonType;

typedef enum {
    MiUserNotificationTypeInfo,
    MiUserNotificationTypeError,
    MiUserNotificationTypeUpdate,
}MiUserNotificationType;

typedef enum {
    SOSButtonStateNormal,
    SOSButtonStateActive,
}SOSButtonState;


enum {
    OrangeColor,
    LightGreenColor,
    MagentaColor,
    DarkGreenColor,
    BrownColor,
    DarkBlueColor,
    RedColor,
    DurtColor,
    GrayColor,
    LightTextColor,
    LightDurtColor,
    DarkDurtColor,
    MapLineColor,
    TabBGColor,
};

typedef enum {
    UserDataRequestNone,
    UserDataRequestPrevious,
    UserDataRequestNext,
}UserDataRequest;

typedef enum {
    CarotRequestTypeVehicleCurrentState,
    CarotRequestTypeCurrentTripLocations,
    CarotRequestTypeTripList,
    CarotRequestTypeTripStaticData,
    CarotRequestTypeNewEventCount,
    CarotRequestTypeEvents,
    CarotRequestTypePlaceApi,
    //    CarotRequestTypeGeoCode,
    CarotRequestTypeImage,
}CarotRequestType;




#define SI [Singleton sharedInstance]
#define AppColor(color) [[[Singleton sharedInstance] appColors] objectAtIndex:color]
@property(nonatomic, strong) NSArray* eventIconMap;
@property(nonatomic, strong) NSArray* appColors;
@property(nonatomic, strong) NSArray* requestStringMap;
@property(nonatomic, strong) NSString* screenName;
@property(nonatomic, strong) NSString* dataSize;
@property(nonatomic, strong) NSOperationQueue* sharedQueue;
@property(nonatomic, strong) NSMutableArray* networkRequests;
//-(UIViewController*)shareFunctionality:(NSArray*)items;
+(Singleton *)sharedInstance ;
-(BOOL)isInternetReachable ;
- (UILabel *)systemDefaultLabel ;
- (void)postDataUsageOnFlurry;
- (void)addDataLength:(double)length forRequestType:(CarotRequestType)type ;
- (void)resetDataUsage;
- (void)addMotionEffectOnView:(UIView *)view ;
- (UIButton *)miButtonOfType:(MIButtonType)buttonType andTitle:(NSString*)title;
//- (BOOL)validateText:(NSString*)text forFieldType:(TextFieldType)type;
- (void)setRightNavigationTitle:(NSString*)title onViewController:(UIViewController*)vc ;
@end
