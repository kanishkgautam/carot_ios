//
//  Constants.h
//  iConnect
//
//  Created by Amit Priyadarshi on 30/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#ifndef iConnect_Constants_h
#define iConnect_Constants_h
#define kFlurryApiKey @"2PMJ36S9VZ9F3K2DQ5DD"


#define kMapZoomLevel 16
#define kMapZoomLevelTripDetails 40
//Here map app ID,code,license
#define kHelloMapAppID @"aB7olMEXCKtkqgwAbs47"
#define kHelloMapAppCode @"udRH-A7OAODFmuCaPq2Y5Q"
//#define kHelloMapAppLicense @"cVDUODM35kgEZCESrd1W6dX7omoTAl/XZ1m1eIrd+GmBLG6b0V4CJIqp5NFcqIpTWJmi+9PmmmLZVbjdsLNBg2BX1eQi/oThCh0BQdk7KLrXSQB24DxnLrmjmRygV8oIdiBevCzKbykyciP2jX63tlhSP5I/A/UWNxqI3sN97gZzNPtz/CC8auXETYzq6zIzlyW63WjQ8u2a6TlCivzObW7x3u4v3QeJ/n0evxkDWArEw5QUU4NvZneQkGwuh1nMxTEcSS67Vtp0KNBIRjWP6mD//Uqx4qLWEgbu/C1RrJ3q6PoYp4rM7K8yHd5qkavLIeaqhIdxPX24qeU3rFAHk0kp0RVpb+D9mMCQ7HZt2B26qNAFzAlHpwP3FLAW8KwlzRgHTjrLqooa/wpQeocFRNiDJjNAXZ4YYd0+YRfrGqkQcWuNmUI6MLP6PvygvRIFPkgpCrJcV2hqoqUwINCTp3p4kZtnh7LkBnxvOb/NYDeJjj3w3wWIM3qaaJrlaEFmYVXeZO2H3baPJ07yRQl49GiwxYPpx95TvQMBBgyEWpU1S+bEpMIWnq7ThMunZhhsiBoi0K7Ix7qAfTGA/2x0ISCnTTqpkmZbgiB9yUuA0pX16ZN/SFZC8j+bDx3t2FcvVEXidkEC8/fkzpSE5kRi6hTiG66cigFn6/JnBuIKOIs="

#define kHelloMapAppLicense @"GDgi/ppxXXYY2/PUUJiWlH0HPhccOQxydet/0kPzot2+pYc2zH+xlHRZ1fkVEsagJesAHSmmspNC55Fyp4fCSrXHP9VnmP7TmU6FnlCoXr1hWNSSVnVzutm3isp+BoOi08wZm3gDbVkC7cYwCrVwvaZV+U4OvlRAZGWvf1Zaa0srgujCBckGYtDCZ7XS2nn0lM3MRU3fOnygizTcmThUZMewzsOJ1H1AKu8uMl0cwmD914dEeA2VEsatRtK3Bkwx4jy5ErxuGQAzP0B0ic7MZQLZB+t0sdGPX5Reyqer16tYXft59XsXMNmeG/RU3dwoHOIgzerrsNz9g/mP1xjhN++o2Ajpg2spHg39LuzOcayqdbY7cQc50CF+pwpXYfqzmXbReSjK+AqDOH84DB6F/mOJ227UNaPgQqHmy7bskciqT3yH4XhWJoqZ8Z0QCioglcdW2eMcp9EvMLJW9e/4Y7ZDl7BsIRey1T4hFIG7kAnvvzEySMKdsj1KGdSFn6V2EQzBVf+gP5iibekfUwvgpFU1TAgRoOCeoanhzZhH6zzJD+Us0vqxczyeK0y0CcOX9Y3wLpzXYcnqtJDI2tsf/VJSjeHHZNtlObXXy888s2IExEy5xivGbPPJaeBLivMVCqbhTjDvt42tMikUkpVBpoo5ZFEBhoy27ky933y4Tc4="


//Device status
#define kDeviceStatusSubscriptionPending @"Subscription Pending"
#define kDeviceStatusActivationInProgress @"Activation in Progress"
#define kDeviceStatusTerminated @"Terminated"
#define kDeviceStatusExpired @"Expired"

#define kUnableToGetLocation @"Unable to get location"
#define kParametersError @"Check error details other parameters"

//Vehicle Live tracking interval
#define kVehicleLocationUpdateIntervalTenSec 10 //10 seconds
#define kVehicleLocationUpdateIntervalThirtyMin 1800 //30 min

//Share Location Time refresh
#define krefreshSessionInterval 0.1
//Locate my car data update
#define kLocateMyCarUpdateInterval 10


#define kIsReachable [[AFNetworkReachabilityManager sharedManager] isReachable]
#define kAppUtility [AppUtility sharedUtility]
#define kAppDelegate (AppDelegate*)[[UIApplication sharedApplication] delegate]

#define kpickerVehicleMakeTag 200
#define kpickerVehicleModelTag 201
#define kpickervehicleYearTag 202
#define kpickervehicleGearTypeTag 203
#define kpickerVehicleFuelTypeTag 204

#define kDocTypeService @"SERVICE DUE"
#define kFirstName          @"fName"
#define kLastName           @"lName"
#define kEmailId            @"email"
#define kPassword           @"password"
#define kConfirmPassword    @"cPassword"
#define kOTP                @"otp"

#define kUserActive         @"active"
#define kUserAddressLine1   @"addressLine1"
#define kUserAddressLine2   @"addressLine2"
#define kUserCity           @"city"
#define kUserEmail          @"email"
#define kUserEmailConfirmed @"emailConfirmed"
#define kUserFirstName      @"firstName"
#define kUserLastName       @"lastName"
#define kUserPhoneNumber    @"phoneNumber"
#define kUserContactNumber  @"contactNumber"
#define kUserGender         @"gender"
#define kUserUserId         @"id"
#define kUserState          @"state"
#define kUserUserName       @"userName"
#define kUserZipPostalCode  @"zipPostalCode"
#define kUserEContactPreference  @"preference"

#define kVehicleSelected @"vehicleSelected"

#define kEventCategoryLogin @"Login"
#define kEventCategoryRegister @"Register"
#define kEventCategoryNavigation @"AppNavigation"
#define kEventCategoryDashboard @"Dashboard"
#define kEventCategoryMailbox @"Mailbox"
#define kEventCategoryDeviceManagement @"Device"
#define kEventCategorySOS @"SOS"
#define kEventCategoryTrips @"Trips"
#define kEventCategoryDriveScore @"DriveScore"
#define kEventCategoryHealth @"CarHealth"
#define kEventCategoryMyProfile @"MyProfile"



#define kToolBarButtonWidth 80

#define DOC_DIR [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define StringForKey(o) (([[SiUController keyMap] objectForKey:o])?[[SiUController keyMap] objectForKey:o]:o)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

//Tags for red alert on false validation
#define kBaseViewofLabels 500
#define kemailIDLabelTag 501
#define kPasswordLabelTag 502
#define kConfirmPasswordLabelTag 503
#define kFirstNameLabelTag 504
#define kLastNameLabelTag 505
#define kMobileNumberLabelTag 506
#define kOTPLabelTag 507

//tags for red alert while adding device
#define kdeviceIdTag 508
#define kdeviceNameTag 509
#define kvehicleMake 510
#define kvehicleModel 511
#define kvehicleYear 512
#define kvehicleGearType 513
#define kvehicleFuelType 514


#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_GreaterThan_Iphone6 (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
#define iPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define yShift(yOffset, fromView) (fromView.frame.origin.y + fromView.frame.size.height + yOffset)
#define xShift(xOffset, fromView) (fromView.frame.origin.x + fromView.frame.size.width + xOffset)

#define UnivVal(siPhone, biPhone, valIpad) UniversalValue(UniviPhoneValue(siPhone, biPhone), valIpad)
#define UniversalValue(valIphone, valIpad) ((iPhone)?valIphone:valIpad)
#define UniviPhoneValue(siPhone, biPhone) ((IS_IPHONE_4_OR_LESS)?siPhone:biPhone)

#define OSVersionGreterThan(x) ([[[UIDevice currentDevice] systemVersion] floatValue] > x)
#define OSVersionGreterOrEqual(x) ([[[UIDevice currentDevice] systemVersion] floatValue] >= x)
#define OSVersionLessThan(x) ([[[UIDevice currentDevice] systemVersion] floatValue] < x)
#define OSVersionLessOrEqual(x) ([[[UIDevice currentDevice] systemVersion] floatValue] <= x)

#define W(view) view.frame.size.width
#define W_2(view) (view.frame.size.width/2.0f)
#define H(view) view.frame.size.height
#define H_2(view) (view.frame.size.height/2.0f)
#define X(view) view.frame.origin.x
#define Y(view) view.frame.origin.y

#define RGBColor(r, g, b, a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define LocString(key) NSLocalizedString(key, nil)
#define klightGrayColor "lightGrayColor"
#define kOrangeColor "orangeColor"

#define DOC_DIR [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define kUserId @"userId"
#define kPassword @"password"
#define kTextfieldNormalFont [UIFont fontWithName:@"LaoUI" size:15]
#define kButtonNormalFont [UIFont fontWithName:@"LaoUI-Bold" size:14]
#define kLabelNormalFont [UIFont fontWithName:@"LaoUI" size:UniversalValue(UniviPhoneValue(10,12), 14)]
#define kNormalAppFont(s) [UIFont fontWithName:@"LaoUI" size:s]
#define kBoldAppFont(s) [UIFont fontWithName:@"LaoUI-Bold" size:s]
#define TrimmedText(text) [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]

//Colors
#define kTextUnderlineGrayColorObjectivceC [UIColor colorWithRed:155/255.0 green:183/255.0 blue:205/255.0 alpha:1.0f]
#define kTextUnderlineGrayColorSwift UIColor.init(colorLiteralRed: 155/255.0, green: 183/255.0, blue: 205/255.0, alpha: 1.0)

#define kMainGrayColorObjectiveC [UIColor colorWithRed:0.14 green:0.16 blue:0.20 alpha:1.0f]
#define kMainGrayColorSwift UIColor.init(colorLiteralRed: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)

// Segues
#define kLoginToCreateAcountSegue @"LoginToCreateAccountSegue"
#define kLoginToForgotPasswordSegue @"LoginToForgotPasswordSegue"
#define kLoginToOTPSegue @"LoginToOTPSegue"
#define kPasswordStepOneToTwoSegue @"PasswordStepOneToTwoSegue"
#define kSignUpOTPSegue @"SignUpOTPSegue"
#define kRegisterToOTPSegue @"RegisterToOTPSegue"

#define kCentreToMyProfileSegue @"CentreToMyProfileSegue"
#define kCentreToDeviceManagementSegue @"CentreToDeviceManagementSegue"
#define kCentreToHelpSegue @"CentreToHelpSegue"
#define kCentreToMailboxSegue @"CentreToMailboxSegue"
#define kCentreToSettingsSegue @"CentreToSettingsSegue"
#define kCentreToNearbySegue @"CentreToNearbySegue"
#define kCentreToAlertsScreenSegue @"CentreToAlertsScreenSegue"
#define kDashboardToShareSegue @"DashboardToShareSegue"
#define kCentreToAddDeviceSegue @"AddDeviceControllerID"
#define kCentreToManagePreferenceSegue @"ManagePreferencesSegue"
#define kCentreToSOSSegue @"CentreToSOS"
#define kManageDeviceToSebViewSegue @"devicesToWebViewSegue"
#define kManageDeviceToBuySubscriptionSegue @"devicesToBuySubscriptionSegue"
#define kDashboardToDriverScoreSegue @"DashboardToDriverScoreSegue"

#endif


//Trips
#define kTripStaticImageWidth 400
#define kTripStaticImageHeight 400


//Notifications
#define kVehicleChangedNotification @"VehicleChangedNotification"

//Selected vehicle

#define kSelectedVehicleNameKey @"SelectedVehicleName"
#define kSelectedVehicleIDKey @"SelectedVehicleId"


//Error messages
#define kGenericErrorMessageKey @"APIErrorMessage"


