//
//  CommonData.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 20/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

typealias GetFuelBlock = (_ fuels: [CFuelType]) -> Void
typealias GetVehiclesBlock = (_ vehicles: [CVehicle], _ isSuccess: Bool) -> Void
typealias GetDevicesBlock = (_ devices: [CDevice], _ isSuccess: Bool) -> Void

typealias GetSelectedVehicleBlock = (_ selectedVehicle: CVehicle?) -> Void

typealias PrefetchCompletedCallback = (_ completed: Bool) -> Void

typealias MakeModelCallback = (_ maker:CVehicleMake?, _ model:CVehicleModel?) -> Void

class CommonData: NSObject {
    
    private var fuelCallback:GetFuelBlock?
    private var vehicleReferenceObj:CVehicleReference? = nil
    
    
    private var allVehiclesCallback:GetVehiclesBlock?
    private var allVehiclesArray:[CVehicle]?
    
    private var allDevicesCallback:GetDevicesBlock?
    private var allDevicesArray:[CDevice]?
    
    var isDataPrefetched:Bool = false
    
    var lastEventTimeStamp = ""
    
    @objc static let sharedData = CommonData()
    
    private var errorKeydictionary = NSMutableDictionary()
    
    
//    let appDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//    
//    let errorFileName = "ErrorList"
    
    @objc func getErrorDescriptionFromError(errorString: String) -> String {
        if self.errorKeydictionary.count == 0 {
            return ""
        }
        else {
            
            return (self.errorKeydictionary.value(forKey: errorString) as? String) ?? ""
        }
    }
    
    @objc func fetchErrorStrings() -> Void {
//        let bundle = Bundle.main
//        let path = bundle.path(forResource: self.errorFileName, ofType: "")
        
        CUserRequest.sharedInstance().getErrorStringMapResponse { (responseData, isSuccess, error) in
            
            if isSuccess {
                self.errorKeydictionary.removeAllObjects()
                
                for obj in (responseData as? NSArray)! {
                    let valueDict = obj as! NSDictionary
                    
                    self.errorKeydictionary.setValue(valueDict.value(forKey: "value"), forKey: valueDict.value(forKey: "key") as! String)
                }
              //  UserDefaults.standard.set(self.errorKeydictionary, forKey: "errorListDictionary")
//                let _ = self.storeErrorListDictionary(dictionary: self.errorKeydictionary as! Dictionary<String, String>)
                
                //                if FileManager().fileExists(atPath: path!) == true {
                //                    do {
                //                        try FileManager().removeItem(atPath: path!)
                //                }
                //                catch {
                //
                //                }
                //                let data : Data = NSKeyedArchiver.archivedData(withRootObject: self
                //                .errorKeydictionary) as Data
                //
                //               // FileManager().createFile(atPath: path!, contents: data, attributes: nil)
                //                self.errorKeydictionary.write(toFile: path!, atomically: true)
                //
            } else {
                
//                if path != nil {
//                    if FileManager.default.fileExists(atPath: path!) {
//                        self.errorKeydictionary = NSDictionary(contentsOfFile: path!) as! NSMutableDictionary
//                    }
//                }
            }
        }
    }
    
    @objc func fetchAllPrerequisitData(prefetchCallback:@escaping PrefetchCompletedCallback) {
        self.fetchErrorStrings()
        //        let nsDocumentDirectory = FileManager.SearchPathDirectory.applicationDirectory
        //        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        //        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        //        if let dirPath          = paths.first {
        //            let errorFileURL = URL(fileURLWithPath: dirPath).appendingPathComponent("sysKeys")
        //
        //            if FileManager().fileExists(atPath: errorFileURL.absoluteString) {
        //                self.errorKeydictionary = NSDictionary(contentsOfFile: errorFileURL.absoluteString)
        //            }
        //        }
        
        //        if self.errorKeydictionary == nil {
        //            let bundle = Bundle.main
        //            let path = bundle.path(forResource: "sysKeys", ofType: "")
        //
        //            if FileManager().fileExists(atPath: path!) {
        //                self.errorKeydictionary = NSDictionary(contentsOfFile: path!) as! NSMutableDictionary
        //            }
        //        }
        //        let filePath =    + "/sysKeys"
        
        
        if let timestamp = UserDefaults.standard.object(forKey: "lastEventTimeStamp") {
            self.lastEventTimeStamp = timestamp as! String
        }
        
        var dataReturned:Bool = false
        self.getAllDevices { (devices, isSuccess) in
            
            if !isSuccess {
                prefetchCallback(false)
                return
                
            }
            
    //        if devices.count > 0  {
                
                self.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                    
                    if !isSuccess {
                        prefetchCallback(false)
                        return

                    }
                    
              //      if vehicles.count > 0 {
                        
                        self.getAllFuelTypes(fuels: { (fuels) in
                            
                            if fuels.count > 0 {
                                
                                self.isDataPrefetched = true
                                if !dataReturned {
                                    dataReturned = !dataReturned
                                    prefetchCallback(true)
                                    return
                                }
                                return
                                
                            }else{
                                prefetchCallback(false)
                                return
                            }
                        })
//                    }else{
//                        prefetchCallback(false)
//                        return
//                    }
                })
//            }else{
//                prefetchCallback(false)
//                return
//            }
        }
    }
    
    
    /// This method will return all the fuel types if available else it will first get them from server and then it will return the updated data
    ///
    /// - Parameter fuels: callback for arrays of fuel types.
    @objc func getAllFuelTypes(fuels:@escaping GetFuelBlock){
        
        self.fuelCallback = fuels
        
        if vehicleReferenceObj != nil {
            if self.fuelCallback != nil {
                self.fuelCallback!((self.vehicleReferenceObj?.fuelType)!)
            }
        }else{
            
            CVehicleRequest.sharedInstance().getVehicleReferenceDataResponse { (responseObject, isSuccessful, error) in
                if let vehicleObj = responseObject as? CVehicleReference{
                    self.vehicleReferenceObj = vehicleObj
                    if self.fuelCallback != nil {
                        self.fuelCallback!((self.vehicleReferenceObj?.fuelType)!)
                    }
                }
                else {
                    if self.fuelCallback != nil {
                        self.fuelCallback!([CFuelType]())
                    }
                }
            }
        }
    }
    
    
    
    /// This method will return all the vehicles available to the users
    ///
    /// - Parameter vehicles: callback for arrays of vehicles
    @objc func getAllVehicles(vehicles:@escaping GetVehiclesBlock){
        
        self.allVehiclesCallback = vehicles
        if (allVehiclesArray != nil && (allVehiclesArray?.count)! > 0) {
            if self.allVehiclesCallback != nil {
                self.allVehiclesCallback!(self.allVehiclesArray!, true)
            }
        }else{
            CVehicleRequest.sharedInstance().getUserVehiclesResponse({ (responseObject, isSuccessful, error) in
                if let vehicles = responseObject as? [CVehicle]{
                    
                    self.allVehiclesArray = vehicles.sorted { (object1, object2) -> Bool in
                        
                        return object1.name < object2.name
                        
                    }
                    if self.allVehiclesCallback != nil {
                        self.allVehiclesCallback!(self.allVehiclesArray!, true)
                    }
                }
            })
        }
    }
    
    
    /// This method will return list of devices for a user
    ///
    /// - Parameter devices: this method will return array of devices
    @objc func getAllDevices(devices:@escaping GetDevicesBlock){
        
        self.allDevicesCallback = devices
        if (allDevicesArray != nil && (allDevicesArray?.count)! > 0) {
            if self.allDevicesCallback != nil {
                self.allDevicesCallback!(self.allDevicesArray!, true)
            }
        }else{
            CVehicleRequest.sharedInstance().getUserDevicesResponse({ (responseObject, isSuccessful, error) in
                if let devices = responseObject as? Array<CDevice>{
                    self.allDevicesArray = devices
                    if self.allDevicesCallback != nil {
                        self.allDevicesCallback!(self.allDevicesArray!, true)
                    }
                    
                }
                else {
                    if self.allDevicesCallback != nil {
                        self.allDevicesCallback!(self.allDevicesArray ?? [CDevice](), false)
                    }
                }
            })
        }
    }
    
    @objc func hasFetchedVehicleData() -> Bool {
        return ((self.vehicleReferenceObj != nil) ? true : false )
    }
    
    @objc func fetchAndUpdateVehiclesFromServer(with callback:@escaping GetVehiclesBlock) {
        
        CVehicleRequest.sharedInstance().getUserVehiclesResponse({ (responseObject, isSuccessful, error) in
            if let vehicles = responseObject as? [CVehicle]{
                self.allVehiclesArray = vehicles
                callback(self.allVehiclesArray!, true)
                return
            }else{
                callback([CVehicle](), false)
            }
        })
    }
    
    
    @objc func fetchAndUpdateDevicesFromServer(with callback:@escaping GetDevicesBlock) {
        CVehicleRequest.sharedInstance().getUserDevicesResponse({ (responseObject, isSuccessful, error) in
            if let devices = responseObject as? Array<CDevice>{
                self.allDevicesArray = devices
                callback(self.allDevicesArray!, true)
            }else{
                callback([CDevice](), false)
            }
        })
    }
    
    
    /// This method will simply return name of all the makers of vehicles
    ///
    /// - Returns: String array containing names of all the makers
    @objc func getAllVehicleMakersName() -> [String]?{
        
        var namesArray:[String]? = []
        
        if self.vehicleReferenceObj != nil {
            for makeObj in (self.vehicleReferenceObj?.vehicleMakers)! {
                namesArray?.append(makeObj.makerName)
            }
        }
        
        let sortedArray = namesArray?.sorted { ( $0 < $1 ) }
        
        return sortedArray!
    }
    
    
    /// This method can be used for retreiving list of models for a particular maker on the basis of maker name
    ///
    /// - Parameter makerName: name of maker in string for which we need to find model array
    /// - Returns: array of type CVehicleModel which will be containing all the models for a particular maker
    @objc func getAllVehicleModelsForMakeString(withMakerNameInString makerName:String) -> [CVehicleModel]?{
        
        if self.vehicleReferenceObj != nil {
            for makeObj in (self.vehicleReferenceObj?.vehicleMakers)! {
                if makeObj.makerName == makerName {
                    if let models = makeObj.models as? [CVehicleModel] {
                        return models
                    }
                }
            }
        }
        return nil
    }
    
    
    /// This method can be used for retreiving maker and model for a particular model id
    ///
    /// - Parameters:
    ///   - modelID: model if for which we need to find model and maker data
    ///   - mappingCallback: callback returning make object of type CVehicleMake and model of type CVehicleModel
    @objc func getMakerAndModel(with modelID:Int , mappingCallback:MakeModelCallback?) {
        
        if self.vehicleReferenceObj != nil {
            for makeObj in (self.vehicleReferenceObj?.vehicleMakers)! {
                
                if let modelsArray = makeObj.models as? [CVehicleModel] {
                    for model in modelsArray {
                        if model.modelId == UInt(modelID) {
                            if mappingCallback != nil {
                                mappingCallback!(makeObj, model)
                            }
                            return
                        }
                    }
                }
            }
        }
        mappingCallback!(nil, nil)
    }
    
    
    @objc func setSelectedVehicle(selectedVehicle: CVehicle) -> Bool {
        
        var errorObj:NSError?
        CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(selectedVehicle.vehicleId, withError: &errorObj)
        
        if errorObj == nil {
            return true
        }else{
            return false
        }
    }
    
    @objc func getSelectedVehicle(selectedVehicleCallback:@escaping GetSelectedVehicleBlock) {
        
        if let vehicle = CVehicleRequest.sharedInstance().getSelectedVehicle() {
            selectedVehicleCallback(vehicle)
        }else{
            self.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                for singleVehicle in vehicles{
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(singleVehicle){
                        if ((device.status != kDeviceStatusSubscriptionPending) && (device.status != kDeviceStatusActivationInProgress)) {
                            if self.setSelectedVehicle(selectedVehicle: singleVehicle) {
                                selectedVehicleCallback(singleVehicle)
                                return
                            }
                            
                        }
                    }
                }
                selectedVehicleCallback(nil)
                print("Not found")
            })
        }
    }
    
    @objc func clearDataForLogout() {
        
        self.isDataPrefetched = false
        
        self.vehicleReferenceObj = nil
        self.allDevicesArray = nil
        self.allVehiclesArray = nil
        
        AppUtility.sharedUtility.allEventsRead = false
        
        UserDefaults.standard.removeObject(forKey: "lastSavedLocation")
        UserDefaults.standard.removeObject(forKey: "lastSavedLocationDate")
        UserDefaults.standard.removeObject(forKey: "lastEventTimeStamp")
        
        self.lastEventTimeStamp = ""
        
        // TODO:- Deregistering Remote notifications
        //  UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    @objc func refreshAllData() -> Void {
        self.isDataPrefetched = false
        self.fetchAllPrerequisitData { (isSuccess) in
            self.isDataPrefetched = true
            
        }
        
    }
    
    @objc func markAlertsAsRead(alertsArray: Array<CEvent>) -> Void {
        var unreadEvents = Array<CEvent>()
        
        for event in alertsArray {
            if !event.read {
                unreadEvents.append(event)
            }
        }
        
        if unreadEvents.count > 0 {
            
            CEventRequest.sharedInstance().markAsRead(forEvents: unreadEvents, response: { (responseData, isSuccessful, error) in
                
                if isSuccessful {
                    
                }
            })
            
        }
        
    }
    
//    func storeErrorListDictionary(dictionary: Dictionary<String, String>) -> Bool {
//        //let fileExtension = "plist"
//       // let directoryURL = create(directory:self.appDirectory.absoluteString)
//        
//        let directoryURL = self.appDirectory
//        
//        do {
//            let data = try PropertyListSerialization.data(fromPropertyList: dictionary, format: .xml, options: 0)
//            try data.write(to: directoryURL.appendingPathComponent(self.errorFileName))//.appendingPathExtension(fileExtension))
//            return true
//        }  catch {
//            print(error)
//            return false
//        }
//    }
    
//    func create(directory: String) -> URL {
//        let documentsDirectory = FileManager.default.urls(for: .applicationDirectory, in: .userDomainMask)[0]
//        //let directoryURL = documentsDirectory.appendingPathComponent(directory)
//        
//        do {
//            try FileManager.default.createDirectory(at: documentsDirectory, withIntermediateDirectories: true, attributes: nil)
//        } catch let error as NSError {
//            fatalError("Error creating directory: \(error.localizedDescription)")
//        }
//        return documentsDirectory
//    }
}
