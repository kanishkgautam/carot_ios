//
//  AppUtility.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 18/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Crashlytics

@objcMembers class AppUtility: NSObject {
    
    private override init() {
        // made provate to stop it from being instantiated further
    }
    
    @objc static let sharedUtility = AppUtility()
    var arrayMake:Array = [String]()
    
    var allEventsRead: Bool?
    
    /// this method will be used for validating email address weather it is of valid format or not
    ///
    /// - Parameter emailToBeValidated: email addresss which has to be validated
    /// - Returns: true if email is valid else false
    @objc func isValidEmail(emailToBeValidated:String) -> Bool{
        
        if !self.isValidString(stringToBeValidated: emailToBeValidated) {
            return false
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailToBeValidated)
        
    }
    
    /// this method will be used for validating phone number weather it is of valid phone number or not
    ///
    /// - Parameter phoneNumberToBeValidated: phone number which has to be validated
    /// - Returns: true if phone number is valid else false
    @objc func isValidPhoneNumber(phoneNumberToBeValidated:String) -> Bool{
        
        let PHONE_REGEX = "[789][0-9]{9}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: phoneNumberToBeValidated)
        
        //        if phoneTest.evaluateWithObject(value) {
        //            return (true)
        //        }
        //        return (false)
        //}
    }
    
    /// This method will validate if the string has atleast one character or not
    ///
    /// - Parameter stringToBeValidated: sting to be validated
    /// - Returns: true if string length is greater than one else false
    @objc func isValidString(stringToBeValidated: String) -> Bool {
        
        
        if self.trimWhiteSpaces(inString: stringToBeValidated).count < 1 {
            return false
        }
        return true
        
    }
    
    @objc func isDocumentExpired(documentExpiry:String?) -> Bool {
        
        if documentExpiry == nil {
            return false
        }
        let interval = TimeInterval(documentExpiry!)
        let expiry = AppUtility.sharedUtility.getStartOfDay(startDateObj: Date(timeIntervalSince1970: interval!))
        let startOftoday = AppUtility.sharedUtility.getStartOfDay(startDateObj: Date()).timeIntervalSince1970
        let startOfExpiryDay = AppUtility.sharedUtility.getStartOfDay(startDateObj: expiry).timeIntervalSince1970
        
        if startOfExpiryDay <= startOftoday {
            return true
        }
        else {
            return false
        }
    }
    
    @objc func isDocumentAboutToExpire(documentExpiry: String?) -> Bool {
        
        if documentExpiry == nil {
            return false
        }
        let interval = TimeInterval(documentExpiry!)
        let expiry = AppUtility.sharedUtility.getStartOfDay(startDateObj: Date(timeIntervalSince1970: interval!))
        let startOftoday = AppUtility.sharedUtility.getStartOfDay(startDateObj: Date()).timeIntervalSince1970
        let startOfExpiryDay = AppUtility.sharedUtility.getStartOfDay(startDateObj: expiry).timeIntervalSince1970
        
        if startOfExpiryDay <= startOftoday {
            return true
        }
        else {
            return false
        }
    }
    
    /// This method will remove blankspaces and extra lines
    ///
    /// - Parameter textToBeTrimmed: string which needs to be beautified
    /// - Returns: beautified string with no extra whitespace
    @objc func trimWhiteSpaces(inString textToBeTrimmed:String) -> String {
        return textToBeTrimmed.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    @objc func getErrorDescription(FromDictionary dataDictionary:NSDictionary) -> String {
        var errorStr = ""
        
        if let data:NSDictionary = dataDictionary.object(forKey: "data") as! NSDictionary? {
            if let errorString:NSString = data.object(forKey: "error") as! NSString?{
                
                errorStr = CommonData.sharedData.getErrorDescriptionFromError(errorString: errorString as String)// NSLocalizedString(errorString as String, comment: "")
                
                if errorStr == "" || errorStr == " " {
                    errorStr = NSLocalizedString(errorString as String, comment: "")
                    
                }
            }
        }
        
        return errorStr
    }
    
    
    /// This method will return NSAttributed string for the HTML
    ///
    /// - Parameter string: HTML string
    /// - Returns: attributed string which can be used anywhere(except web view)
    @objc func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [.documentType:NSAttributedString.DocumentType.html],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }
    //App Orange color
    @objc func appColor(color:String) -> UIColor
    {
        let borderColor : UIColor
        switch color {
        case kOrangeColor:
            borderColor = UIColor(red:255/255.0, green:128/255.0, blue:0, alpha:1)
            
        case klightGrayColor:
            borderColor = UIColor(red:170/255.0, green:170/255.0, blue:170/255.0, alpha:1)
        default:
            borderColor = UIColor.black
            
        }
        return borderColor
        
    }
    @objc func convertDateStringToDate(dateStr:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-YYYY"//"MMM d, yyyy"
        let newDate = dateFormatter.date(from: dateStr)
        // print(newDate)
        return newDate!
    }
    
    @objc func readableDateAndTimeFormat(date:Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d,YYYY h:mm a"
        let str:String = dateFormatter.string(from: date as Date)
        print(str)
        return str
    }
    @objc func stringFromDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-YYYY"//"MMM d, yyyy"
        let newDate = dateFormatter.string(from: date)
        // print(newDate)
        return newDate

        
    }
    
    @objc func TimeInIndianTimezone(date:Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "h:mm a"
        let str:String = dateFormatter.string(from: date as Date)
        // let time:Date = dateFormatter.date(from: str)!
        return str
    }
    
    @objc func DateInIndianTimezone(date:Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MMM d, YYYY"
        let str:String = dateFormatter.string(from: date as Date)
        // let dateNew:Date = dateFormatter.date(from: str)!
        return str
    }
    @objc func intervalBetweenTwoDates(date1: Date, date2: Date) -> String {
        
        let interval:TimeInterval = date2.timeIntervalSinceNow
        //print(interval)
        
        let hr = floor(interval / 3600)
        let min = floor((interval.truncatingRemainder(dividingBy: 3600)) / 60)
        let sec = floor((interval.truncatingRemainder(dividingBy: 3600)).truncatingRemainder(dividingBy: 60))
        
        let strHour:String = String(format: floor(hr) == floor(hr) ? "%.0f" : "%.1f", hr)
        let strMin:String = String(format: floor(min) == floor(min) ? "%.0f" : "%.1f", min)
        let strSec:String = String(format: floor(sec) == floor(sec) ? "%.0f" : "%.1f", sec)
        
        var str :String = "\(strHour):\(strMin):\(strSec)"
        if(hr<0 || min < 0 || sec < 0){
            str = "Expired"
        }
        return str
        
    }
    
    @objc func roundTo(places:Int, value:Double) -> Double {
        let divisor = pow(10.0, Double(places))
        return (value * divisor).rounded() / divisor
    }
    
    /* func convertEpocTimestampForDate:(Double: timestamp)
     {
     let interval:Double = dict.object(forKey: "expiresOn") as! Double
     let date = Date(timeIntervalSince1970: (interval/1000))
     let currentDate = Date()
     let formatter = DateFormatter()
     formatter.dateFormat = "MMM d,YYYY hh:mm:ss"
     let dateString = formatter.string(from: currentDate)
     let dateObj = formatter.date(from: dateString)
     
     
     }*/
    
    
    @objc func encodeString(stringToEncode: NSString) -> NSString {
        
        //        let data = stringToEncode.data(using: String.Encoding.unicode.rawValue)
        //        return NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
        
        //        let transform = "Any-Hex/Java"
        //        let input = stringToEncode
        //        let convertedString = input.mutableCopy() as! NSMutableString
        //        CFStringTransform(convertedString, nil, transform as NSString, true)
        //        return convertedString as String
        
        //  var encodeEmoji: String? {
        let encodedStr = NSString(cString: stringToEncode.cString(using: String.Encoding.nonLossyASCII.rawValue)!, encoding: String.Encoding.utf8.rawValue)
        return encodedStr!
        //}
        
        // return stringToEncode
    }
    
    
    /// This method will decode a unicode encoded string to normal string
    ///
    /// - Parameter stringToDecode: unicode encoded string
    /// - Returns: decoded string
    @objc func decodeString(stringToDecode: String) -> String {
        let transform = "Any-Hex/Java"
        let input = stringToDecode
        let convertedString = input.mutableCopy() as! NSMutableString
        CFStringTransform(convertedString, nil, transform as NSString, true)
        return convertedString as String
    }
    
    @objc func convertSecondsInHoursMinSec(timeInSeconds:TimeInterval) -> String{
        let hr = Int(timeInSeconds / 3600)
        let min = Int((timeInSeconds.truncatingRemainder(dividingBy: 3600)) / 60)
        let sec = Int((timeInSeconds.truncatingRemainder(dividingBy: 3600)).truncatingRemainder(dividingBy: 60))
        if(hr != 0){
            let timeStr = ("\(hr)"+"hr "+"\(min)"+"min "+"\(sec)"+"sec")
            return timeStr
        }
        else if(min != 0) {
            let timeStr = ("\(min)"+"min "+"\(sec)"+"sec")
            return timeStr
        }
        else {
            let timeStr = ("\(sec)"+"sec")
            return timeStr
        }
        
        
    }
    
    @objc func getStartOfDay(startDateObj:Date) -> Date {
        return Calendar.current.startOfDay(for: startDateObj)
    }
    
    @objc func getEndOfDay(endDateObj:Date) -> Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        
        return Calendar.current.date(byAdding: components, to: getStartOfDay(startDateObj: endDateObj))!
    }
    
    
    
    
    //    func getMimeType(For stringURL:String) -> String {
    //
    //    }
    //    - (NSString *)mimeTypeForPath:(NSString *)path
    //    {
    //    // get a mime type for an extension using MobileCoreServices.framework
    //
    //    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    //    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    //    assert(UTI != NULL);
    //
    //    NSString *mimetype = (__bridge NSString *)(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    //    //NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    //
    //    // NSString *mimetype = (__bridge_transfer NSString*)UTTypeCopyPreferredTagWithClass
    //    //    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    //    // ((__bridge CFStringRef)[representation UTI], kUTTagClassMIMEType);
    //
    //
    //
    //    assert(mimetype != NULL);
    //
    //    CFRelease(UTI);
    //
    //    return mimetype;
    //    }
    
    @objc func showAnimatedSplashAndUpdateDataInBackground() {
        
        let viewC:ChangePasswordView = Bundle.main.loadNibNamed("ChangePasswordView", owner: nil, options: nil)?.first as! ChangePasswordView
        viewC.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        UIApplication.shared.windows.last?.addSubview(viewC)
    }
    
    @objc func isNetworkAvailable() -> Bool {
        
        //let alert = UIAlertController(title: "Network Error", message: "The Internet connection appears to be offline", preferredStyle: .alert)
        let reachability = Reachability()
        
        if (reachability?.isReachable)! {
            return true
            
        }
        else {
            UIApplication.shared.windows.last?.showIndicatorWithError("The Internet connection appears to be offline")
            return false
        }
    }
    
    
    /// This method will send GA event data
    ///
    /// - Parameters:
    ///   - eventName: name of event to be sent
    ///   - screenName: screen name on which event is triggered
    ///   - eventLabelText: button text on which event is triggered(optional)
    ///   - value: value in case needed to be sent(optional)
    @objc func sendGAEvent(withEventName eventName:String, category eventCategory:String? = "UIEvent", andScreenName screenName:String, eventLabel eventLabelText:String? = nil, eventValue value:NSNumber? = nil) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        if tracker != nil {
            
            let build = GAIDictionaryBuilder.createEvent(withCategory: eventCategory, action: eventName, label: eventLabelText, value: value).build()! as [NSObject: AnyObject]
            
            tracker?.send(build)
        }
        
        Answers.logCustomEvent(withName: eventName,
                               customAttributes: [
                                "Event Category": eventCategory ?? "UIEvent",
                                "Screen Name": (screenName.count < 1 ? screenName : "")
            ])
        
    }
    
    @objc func trackScreenName(with screenName:String) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        let build = (GAIDictionaryBuilder.createScreenView().build() as NSDictionary) as! [AnyHashable: Any]
        
        if tracker != nil{
            tracker?.set(kGAIScreenName, value: screenName)
            tracker?.send(build)
        }
    }
    
    
    @objc func showSubscriptionSpecificPopup(forController controller:UIViewController, withSubscriptionError errorType:String){
        
        switch errorType {
        case kDeviceStatusActivationInProgress:
            
            let buySubsAlert:UIAlertController = UIAlertController(title: "Renewal in progress", message: "Your device will get activated in 48-72 hours.", preferredStyle: .alert)
            
            let okAction:UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                // take user to buy subscription
            })
            
            buySubsAlert.addAction(okAction)
            controller.present(buySubsAlert, animated: true, completion: nil)
            
        case kDeviceStatusSubscriptionPending:
            
            let buySubsAlert:UIAlertController = UIAlertController(title: "Subscription Pending", message: "Your subscription is pending", preferredStyle: .alert)
            
            let okAction:UIAlertAction = UIAlertAction(title: "Check status", style: .default, handler: { (action) in
                // take user to buy subscription
                if let centreController = controller.menuContainerViewController.centerViewController as? CentreViewController {
                    centreController.performSegue(withIdentifier: kCentreToDeviceManagementSegue, sender: nil)
                }
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
        
            })

            buySubsAlert.addAction(cancelAction)
            buySubsAlert.addAction(okAction)
            controller.present(buySubsAlert, animated: true, completion: nil)
            
        case kDeviceStatusTerminated:
            
            let buySubsAlert:UIAlertController = UIAlertController(title: "Your device is Terminated", message: "We're sorry but your subscription services seems to have terminated.\nPlease contact our helpline number 011-39595595 or mail us at help@carot.com", preferredStyle: .alert)
            
            let okAction:UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            })
            
            buySubsAlert.addAction(okAction)
            controller.present(buySubsAlert, animated: true, completion: nil)
            
        case kDeviceStatusExpired:
            let buySubsAlert:UIAlertController = UIAlertController(title: "Subscription Expired", message: "We are sorry but your subscription seems to have expired!\n Please renew your subscription to continue.", preferredStyle: .alert)
            
            let okAction:UIAlertAction = UIAlertAction(title: "Renew", style: .default, handler: { (action) in
                // take user to buy subscription
                if let centreController = controller.menuContainerViewController.centerViewController as? CentreViewController {
                    centreController.performSegue(withIdentifier: kCentreToDeviceManagementSegue, sender: nil)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
            })
            
            buySubsAlert.addAction(cancelAction)

            buySubsAlert.addAction(okAction)
            controller.present(buySubsAlert, animated: true, completion: nil)
            
        default:
            return
        }
    }
}
