//
//  Singleton.m
//  iConnect
//
//  Created by Aditya Srivastava on 07/12/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import "Singleton.h"

@interface Singleton() {

    NSCharacterSet *englishCharacterSet;
}
@end

@implementation Singleton

//-(UIViewController*)shareFunctionality:(NSArray*)items{
//
//    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
//    controller.excludedActivityTypes = @[UIActivityTypePostToFacebook,
//                                         UIActivityTypePrint,
//                                         UIActivityTypeCopyToPasteboard,
//                                         UIActivityTypeAssignToContact,
//                                         UIActivityTypeSaveToCameraRoll,
//                                         UIActivityTypeAddToReadingList,
//                                         UIActivityTypeAirDrop,
//                                         UIActivityTypeOpenInIBooks,
//                                         UIActivityTypeAssignToContact,
//                                         UIActivityTypePostToFlickr,
//                                         UIActivityTypePostToVimeo,
//                                         UIActivityTypePostToTencentWeibo,
//                                         UIActivityTypePostToWeibo
//                                         ];
//    
//    
//    
//    return controller;
//    
//}
static Singleton* sharedVariable;
+(Singleton*)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!sharedVariable) {
            sharedVariable = [[Singleton alloc] init];
            sharedVariable.eventIconMap = @[@"TOWING",//tow
                                            
                                            @"CRASH",//crash
                                            
                                            @"HIGH_ENGINE_COLLANT_TEMPERATURE",
                                            @"LOW_VOLTAGE",//dtc
                                            
                                            @"UNLOCK_ALARM",
                                            @"NO_CARD_PRESENT",
                                            @"POWERDOWN_ALARM",//device-plugged
                                            
                                            @"OVER_SPEEDING",
                                            @"HARD_ACCELERATION",
                                            @"HARD_DECELERATION",
                                            @"IDLE_ENGINE",
                                            @"HIGH_RPM",
                                            @"EXHAUST_EMISSION",
                                            @"QUICK_LANE_CHANGE",
                                            @"SHARP_TURN",
                                            @"FATIGUE_DRIVING",
                                            @"EMERGENCY",
                                            @"OBD_COMMUNICATION_ERROR",
                                            @"MIL_ALARM",
                                            @"TRAFFIC_ACCIDENT_ALARM",
                                            @"HIJACK_ALARM",
                                            @"SHOCK_ALARM",
                                            @"DOOR_OPEN_ALARM",
                                            @"HAS_RPM",//driver
                                            
                                            @"POWER_OFF",
                                            @"IGNITION_OFF",
                                            @"POWER_ON",
                                            @"IGNITION_ON",
                                            ];
            sharedVariable.appColors = @[[UIColor colorWithRed:0.952 green:0.506 blue:0.094 alpha:1.000],//243, 129, 24
                                         [UIColor colorWithRed:0.484 green:0.745 blue:0.188 alpha:1.000],
                                         [UIColor colorWithRed:0.445 green:0.027 blue:0.206 alpha:1.000],
                                         [UIColor colorWithRed:0.203 green:0.244 blue:0.064 alpha:1.000],//52, 62, 16
                                         [UIColor colorWithRed:0.279 green:0.153 blue:0.053 alpha:1.000],//71, 39, 14
                                         [UIColor colorWithRed:0.086 green:0.052 blue:0.249 alpha:1.000],
                                         [UIColor colorWithRed:0.757 green:0.208 blue:0.231 alpha:1.000],
                                         [UIColor colorWithRed:0.635 green:0.553 blue:0.333 alpha:1.000],
                                         [UIColor colorWithWhite:0.969 alpha:1.000],
                                         [UIColor colorWithRed:0.387 green:0.376 blue:0.349 alpha:1.000],
                                         [UIColor colorWithRed:0.929 green:0.918 blue:0.871 alpha:1.000],
                                         [UIColor colorWithRed:0.929 green:0.922 blue:0.631 alpha:1.000],
                                         [UIColor colorWithRed:0.118 green:0.375 blue:0.491 alpha:1.000],
                                         [UIColor colorWithRed:0.692 green:0.254 blue:0.241 alpha:0.000],
                                         ];
            sharedVariable.requestStringMap = @[@"VehicleCurrentState",
                                                @"CurrentTripLocations",
                                                @"TripList",
                                                @"TripStaticData",
                                                @"NewEventCount",
                                                @"Events",
                                                @"PlaceApi",
                                                //                                                @"GeoCode",
                                                @"Image"];
//            if (kDevLogger) {
//                [NSTimer scheduledTimerWithTimeInterval:2.0f target:sharedVariable selector:@selector(developerMachineUsageLog) userInfo:nil repeats:YES];
//                sharedVariable->developerLog = [[NSMutableString alloc] initWithString:@"Seconds, Threads, CPU, RAM, Battery, Screen, Data Size\n"];
//                sharedVariable.screenName = @"";
//                sharedVariable.dataSize = @"0";
//            }
            
//            sharedVariable.sharedQueue = [[AFHTTPRequestOperationManager manager] operationQueue];
            sharedVariable.networkRequests = [[NSMutableArray alloc] init];
            
            
            
            
            NSMutableString *asciiCharacters = [NSMutableString string];
            for (int i = 32; i < 127; i++)  {
                [asciiCharacters appendFormat:@"%c", i];
            }
            sharedVariable->englishCharacterSet = [NSCharacterSet characterSetWithCharactersInString:asciiCharacters];
            
        }
    });
    
    return sharedVariable;
}


-(BOOL)isInternetReachable {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
        return NO;
    else
        return YES;
}

- (UILabel*)systemDefaultLabel {
    UILabel* lbl = [[UILabel alloc] init];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setTextColor:[UIColor grayColor]];
    [lbl setFont:kLabelNormalFont];
    [lbl setTextAlignment:NSTextAlignmentLeft];
    return lbl;
}

- (void)postDataUsageOnFlurry {
    __block double total;
    [dataUsagePerSession enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        double value = [obj doubleValue];
        total += [obj doubleValue];
        [dataUsagePerSession setObject:@(ceil(value)) forKey:key];
    }];
    float battery = [dataUsagePerSession[@"Battery"] floatValue]-[[UIDevice currentDevice] batteryLevel]*100;
    [dataUsagePerSession setObject:@(ceil(battery)) forKey:@"Battery"];
    [dataUsagePerSession setObject:@(ceil(total)) forKey:@"Total"];
    //[Flurry logEvent:@"DataUsage" withParameters:dataUsagePerSession];
}
- (void)addDataLength:(double)length forRequestType:(CarotRequestType)type {
    if (!dataUsagePerSession) {
        dataUsagePerSession = [[NSMutableDictionary alloc] initWithDictionary:@{@"VehicleCurrentState":@(0),
                                                                                @"CurrentTripLocations":@(0),
                                                                                @"TripList":@(0),
                                                                                @"TripStaticData":@(0),
                                                                                @"NewEventCount":@(0),
                                                                                @"Events":@(0),
                                                                                @"PlaceApi":@(0),
                                                                                @"Image":@(0),
                                                                                @"Battery":@([[UIDevice currentDevice] batteryLevel]*100)}];
    }
    double previous = [dataUsagePerSession[self.requestStringMap[type]] doubleValue];
    previous += length;
    dataUsagePerSession[self.requestStringMap[type]] = @(previous);
}
- (void)resetDataUsage {
    dataUsagePerSession = [[NSMutableDictionary alloc] initWithDictionary:@{@"VehicleCurrentState":@(0),
                                                                            @"CurrentTripLocations":@(0),
                                                                            @"TripList":@(0),
                                                                            @"TripStaticData":@(0),
                                                                            @"NewEventCount":@(0),
                                                                            @"Events":@(0),
                                                                            @"PlaceApi":@(0),
                                                                            //                                                                            @"GeoCode":@(0),
                                                                            @"Image":@(0),
                                                                            @"Battery":@([[UIDevice currentDevice] batteryLevel]*100)}];
}

- (void)addMotionEffectOnView:(UIView*)view {
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-40);
    verticalMotionEffect.maximumRelativeValue = @(40);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-40);
    horizontalMotionEffect.maximumRelativeValue = @(40);
    
    UIInterpolatingMotionEffect *horizontalMotionEffect1 =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"width"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect1.minimumRelativeValue = @(-40);
    horizontalMotionEffect1.maximumRelativeValue = @(40);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, horizontalMotionEffect1, verticalMotionEffect];
    
    // Add both effects to your view
    [view addMotionEffect:group];
}

- (UIButton*)miButtonOfType:(MIButtonType)buttonType andTitle:(NSString*)title {
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //switch buttonType for backgroud Image
    switch (buttonType) {
        case MIButtonTypeCancel:
            [button setBackgroundColor:[UIColor colorWithWhite:0.765 alpha:1.000]];
            [button setTintColor:[UIColor whiteColor]];
            button.layer.cornerRadius = 2.5f;
            [button.titleLabel setFont:kNormalAppFont(UnivVal(16, 18, 20))];
            break;
        case MIButtonTypeDone:
            [button setBackgroundColor:AppColor(OrangeColor)];
            [button setTintColor:[UIColor whiteColor]];
            button.layer.cornerRadius = 2.5f;
            [button.titleLabel setFont:kNormalAppFont(UnivVal(16, 18, 20))];
            break;
        case MIButtonTypeNormal:
            button = [UIButton buttonWithType:UIButtonTypeSystem];
            [button setTintColor:[UIColor blackColor]];
            [button setTitleColor:AppColor(BrownColor) forState:UIControlStateNormal];
            [button.titleLabel setFont:kNormalAppFont(UnivVal(14, 16, 18))];
            break;
        default:
            break;
    }
    [button setTitle:title forState:UIControlStateNormal];
    return button;
}
- (void)setRightNavigationTitle:(NSString*)title onViewController:(UIViewController*)vc {
    UIBarButtonItem* pageTitle = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:nil action:nil];
    pageTitle.tintColor = AppColor(BrownColor);
    NSDictionary* dict = @{NSFontAttributeName:kNormalAppFont(UnivVal(16, 18, 20)),
                           NSForegroundColorAttributeName:AppColor(BrownColor)};
    [pageTitle setTitleTextAttributes:dict forState:UIControlStateDisabled];
    [pageTitle setTitleTextAttributes:dict forState:UIControlStateNormal];
    pageTitle.enabled = NO;
    vc.navigationItem.rightBarButtonItems = @[pageTitle];
}
/*- (BOOL)validateText:(NSString*)text forFieldType:(TextFieldType)type{
        switch (type) {
            case TextFieldTypePassword: {
                //            NSString* regEx = @"((?=.*[a-zA-Z0-9]).{6,20})";
                //            NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regEx];
                //            return [test evaluateWithObject:text];
                return [TrimmedText(text) length]>=6;
            }break;
            case TextFieldTypeEmail: {
                BOOL stricterFilter = YES;
                NSString *stricterFilterString = @"[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?";
                NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
                NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                return [emailTest evaluateWithObject:text];
            }break;
            case TextFieldTypePhonenumber: {
                NSString *regEx = @"[0-9]{10}";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regEx];
                return [emailTest evaluateWithObject:text];
            }break;
            case TextFieldTypeZIP: {
                NSString *regEx = @"[0-9]{6}";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regEx];
                return [emailTest evaluateWithObject:text];
            }break;
            case TextFieldTypeName: {
                NSString *regEx = @"[A-Za-z\\.\\s]*";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regEx];
                return [emailTest evaluateWithObject:text];
            }break;
            default:
                break;
        }
        return NO;
    
}*/

@end
