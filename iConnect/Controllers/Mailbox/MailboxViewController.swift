//
//  MailboxViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 06/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MailboxViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var mailBoxHeaderView: UIView!
    @IBOutlet weak var headerViewTitleLabel: UILabel!
    
    @IBOutlet weak var cancelbutton: UIButton!
    
    @IBOutlet weak var selectAllButton: UIButton!
    
    @IBOutlet weak var buttonOK: UIButton!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var popUpView: UIView!
    
    @IBOutlet weak var noRecordsLabel: UILabel!
    
    var isAllSelected = false
    
    let cellIdentifier = "MyMailboxCellView"
    
    var dataRecords = NSMutableArray()
    var selectedArray = NSMutableArray()
    
    var arrayInboxItems = NSMutableArray()
    
    var selectedIndex = -1
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(MailboxViewController.handleRefresh(refreshControl:)), for: UIControl.Event.valueChanged)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppUtility.sharedUtility.trackScreenName(with: "Mailbox Screen")
        let borderColor = AppUtility.sharedUtility.appColor(color: kOrangeColor)
        buttonOK.layer.borderColor = borderColor.cgColor
        
        self.view.showIndicatorWithProgress(message: "Please wait")
        getPromotions()
        
        self.initializeVC()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func closePopup(_ sender: Any) {
        popUpView.isHidden = true
    }
    
    func getPromotions() {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        let paramDict:[String:String] = [
            "page" : "0",
            "pageSize" : "20",
            ]
        CMailBoxRequest.sharedInstance().getPromotionsForInboxOptions(paramDict) { (responseObject, isSuccessful, error) in
            
            self.refreshControl.endRefreshing()
            
            if(error == nil && responseObject != nil){
                print(responseObject ?? NSArray() as Any)
                self.arrayInboxItems.removeAllObjects()
                self.arrayInboxItems = responseObject as! NSMutableArray
                self.tableView.reloadData()
                
                if self.arrayInboxItems.count == 0 {
                    self.noRecordsLabel.isHidden = false
                    
                }
                else {
                    self.noRecordsLabel.isHidden = true
                    
                }
                self.view.dismissProgress()
            }
            else{
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeVC() -> Void {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        self.noRecordsLabel.isHidden = true
        
        self.tableView.register(UINib(nibName: "MyMailboxCellView", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        self.tableView.layer.cornerRadius = 2.0
        
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.cancelbutton.layer.cornerRadius = 2.0
        self.cancelbutton.layer.borderColor = UIColor.black.cgColor
        self.cancelbutton.layer.borderWidth = 1.0
        
        self.selectAllButton.layer.cornerRadius = 2.0
        self.selectAllButton.layer.borderColor = UIColor(red: 214/255, green: 147/255, blue: 50/255, alpha: 1).cgColor
        self.selectAllButton.layer.borderWidth = 1.0
        
        self.mailBoxHeaderView.alpha = 0
        self.headerHeightConstraint.constant = 0
        
        self.deleteButton.isHidden = true
        
        addPullToRefresh()
    }
    
    func addPullToRefresh() -> Void {
        self.tableView.addSubview(self.refreshControl)
        
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        self.getPromotions()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func editingChanged() -> Void {
        if self.selectedArray.count > 0 {
            self.deleteButton.isHidden = false
            
            if self.selectedArray.count == 1 {
                self.headerViewTitleLabel.text = "\(self.selectedArray.count) item selected"
            }
            else {
                self.headerViewTitleLabel.text = "\(self.selectedArray.count) items selected"
            }
            
            UIView.animate(withDuration: 0.5, animations: {
                self.headerHeightConstraint.constant = 44
                self.mailBoxHeaderView.alpha = 1
                self.view.layoutIfNeeded()
                
            })
        }
        else {
            self.deleteButton.isHidden = true
            
            UIView.animate(withDuration: 0.5, animations: {
                self.headerHeightConstraint.constant = 0
                self.mailBoxHeaderView.alpha = 0
                self.view.layoutIfNeeded()
                
            })
        }
    }
    
    @objc func didTapCheckboxButton(sender: UIButton) -> Void {
        
        let inboxItem:CUserInboxItem = arrayInboxItems[sender.tag] as! CUserInboxItem
        
        // let mailInfo = dataRecords[sender.tag] as? MailInfo
        self.isAllSelected = false
        
        if sender.isSelected {
            sender.isSelected = false
            self.removeItemFromSelectedArray(inboxItem: inboxItem)
            
        }
        else {
            sender.isSelected = true
            self.selectedArray.add(inboxItem)
        }
        
        if self.selectedArray.count == self.arrayInboxItems.count {
            self.isAllSelected = true
        }
        self.editingChanged()
    }
    
    func removeItemFromSelectedArray(inboxItem : CUserInboxItem) -> Void {
        
        for item in self.selectedArray {
            
            if (item as! CUserInboxItem).messageId == inboxItem.messageId {
                self.selectedArray.remove(item)
                break
            }
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "DeteMessagesTapped", category: kEventCategoryMailbox, andScreenName: "Mailbox Screen", eventLabel: nil, eventValue: nil)
        if(self.isAllSelected == true){
            popUpView.isHidden = false
            //let button = UIButton(type: .system)
            //deleteSelectedMessages(button)
        }
        else {
            deleteSelectedMessages(nil)
        }
    }
    
    @IBAction func deleteSelectedMessages(_ sender: Any?) {
        popUpView.isHidden = true
        
        self.view.showIndicatorWithProgress(message: "Please wait")
        self.selectedIndex = -1
        for inboxItem in selectedArray {
            let inbox:CUserInboxItem = inboxItem as! CUserInboxItem
            CMailBoxRequest.sharedInstance().markDeleteForPromotion(withId: inbox.messageId as! UInt) { (responseObject, isSuccessful, error) in
                if(error == nil && responseObject != nil){
                    self.selectedArray.removeAllObjects()
                    self.editingChanged()
                    
                    print(responseObject ?? NSDictionary())
                    self.getPromotions()
                    
                }
                else{
                    if (responseObject != nil){
                        self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                        
                    }else{
                        self.view.showIndicatorWithError((error?.localizedDescription)!)
                    }
                    print(error?.localizedDescription ?? String())
                }
            }
        }
    }
    
    
    //MARK:- Header IBActions
    
    @IBAction func selectAllTapped(_ sender: Any) {
        self.isAllSelected = true
        selectedArray.removeAllObjects()
        selectedArray.addObjects(from: (arrayInboxItems as AnyObject) as! [Any] )
        editingChanged()
        self.tableView.reloadData()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.headerHeightConstraint.constant = 0
            self.mailBoxHeaderView.alpha = 0
            self.view.layoutIfNeeded()
            
        })
        
        self.deleteButton.isHidden = true
        self.isAllSelected = false
        self.selectedArray.removeAllObjects()
        self.tableView.reloadData()
    }
    
    
    //MARK:- UITableView Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayInboxItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let inboxItem:CUserInboxItem = arrayInboxItems[indexPath.row] as! CUserInboxItem

        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MyMailboxCellView
        if cell == nil {
            cell = MyMailboxCellView()
        }
        cell!.selectionStyle = .none
        
        cell!.setCellData(inboxItem: inboxItem)
        cell!.checkButton.addTarget(self, action: #selector(didTapCheckboxButton(sender:)), for: .touchUpInside)
        cell!.checkButton.tag = indexPath.row
        
        if isAllSelected {
            cell!.checkButton.isSelected = true
        }
        else {
            if isSelectedArrayContains(inboxItem: inboxItem) {
                cell!.checkButton.isSelected = true
                
            }
            else {
                cell!.checkButton.isSelected = false
            }
        }
        
        if indexPath.row == self.selectedIndex  {
            cell!.descriptionLabel.numberOfLines = 0
            
        }else {
            cell!.descriptionLabel.numberOfLines = 2
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let inboxItem = self.arrayInboxItems[indexPath.row] as! CUserInboxItem



        //Mark inbox as read
        if !inboxItem.isRead.boolValue {
            inboxItem.isRead = true

            if selectedIndex == -1 { //Same or no row selected prevoisly
                self.selectedIndex = indexPath.row
                
                tableView.reloadRows(at: [indexPath], with: .automatic)
                
            }else if selectedIndex == indexPath.row { //Selected row tapped again
                self.selectedIndex = -1
                tableView.reloadRows(at: [indexPath], with: .automatic)
                
            }
            else { //Row different from selected row tapped
                let previndexPath = IndexPath(row: selectedIndex, section: 0)
                self.selectedIndex = indexPath.row
                
                tableView.reloadRows(at: [indexPath,previndexPath], with: .automatic)
            }
            
            
            CMailBoxRequest.sharedInstance().markReadForPromotion(withId: inboxItem.messageId as! UInt) { (responseDict, isSuccess, error) in
                
                if isSuccess {
//                    inboxItem.isRead = true
                }
            }
        }
//        else {
//            if selectedIndex == -1 { //Same or no row selected prevoisly
//                self.selectedIndex = indexPath.row
//
//                tableView.reloadRows(at: [indexPath], with: .automatic)
//
//            }else if selectedIndex == indexPath.row { //Selected row tapped again
//                self.selectedIndex = -1
//                tableView.reloadRows(at: [indexPath], with: .automatic)
//
//            }
//            else { //Row different from selected row tapped
//                let previndexPath = IndexPath(row: selectedIndex, section: 0)
//                self.selectedIndex = indexPath.row
//
//                tableView.reloadRows(at: [indexPath,previndexPath], with: .automatic)
//            }
//        }
    }
    
    func isSelectedArrayContains(inboxItem: CUserInboxItem) -> Bool {
        
        for item in self.selectedArray {
            let selectedItem  = item as! CUserInboxItem
            
            if inboxItem.messageId == selectedItem.messageId {
                return true
            }
        }
        
        return false
    }
    
}
