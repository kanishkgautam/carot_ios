//
//  NearbyCell.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 13/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NearbyCell: UITableViewCell {

    @IBOutlet weak var nearbyTextLabel: UILabel!
    @IBOutlet weak var addressLAbel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataOnCell(with cellTitleText:String, addressText:String, distanceText:String) {
        nearbyTextLabel.attributedText = AppUtility.sharedUtility.stringFromHtml(string: cellTitleText)
        addressLAbel.attributedText = AppUtility.sharedUtility.stringFromHtml(string: addressText)
        distanceLabel.attributedText = AppUtility.sharedUtility.stringFromHtml(string: distanceText)
        
        nearbyTextLabel.font = UIFont(name: "Helvetica", size: 15)
        addressLAbel.font = UIFont(name: "Helvetica", size: 9)
        distanceLabel.font = UIFont(name: "Helvetica", size: 8)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
