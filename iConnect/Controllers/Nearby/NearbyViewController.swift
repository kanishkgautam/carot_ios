//
//  NearbyViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 10/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class NearbyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var isOpenedForServiceStation:Bool = false
    var tableDataArray:Array<Any>?
    
    var currentLocation:CLLocation? = nil
    
    @IBOutlet weak var nearbyTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Nearby Screen")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.dismissProgress()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        if isOpenedForServiceStation {
            headerTitle.text = "Service Stations"
        }else{
            headerTitle.text = "Fuel Pumps"
        }
        headerTitle.sizeToFit()
        self.view.dismissProgress()

        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
        self.getLocation()

    }
    
    func getLocation() {
        
        if CLLocationManager.locationServicesEnabled() && (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            self.view.showIndicatorWithProgress(message: "")
        }
        
       // self.view.showIndicatorWithProgress(message: "") {
            MICLocationManager.sharedManager.getCurrentLocationWithDelegate(delegateObj: self) { (locationObj:CLLocation?, success:Bool) in
                self.view.dismissProgress()
                
                if (success == true) {
                    print("success")
                    self.currentLocation = locationObj
                    if self.isOpenedForServiceStation == true{
                        self.updateViewWithServiceStationForLocation(locationObj: self.currentLocation!)
                    }else{
                        self.updateViewWithFuelStationsForLocation(locationObj: self.currentLocation!)
                    }
                }else{
                    self.view.dismissProgress()
                    let alertController = UIAlertController(title: "Allow location access", message: "Please allow location access to find nearby Fuelpumps and service stations. \nTo Enable location access go to Settings > Privacy > Location Services > Carot, and select \"While Using\"", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
     //   }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableDataArray != nil && (tableDataArray?.count)! > 0 {
            return (tableDataArray?.count)!
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NearbyCell", for: indexPath) as! NearbyCell
        if let cellDataDictionary = tableDataArray?[indexPath.row] as? Dictionary<String, Any> {
//            cell.setDataOnCell(with: cellDataDictionary["title"] as! String)
            cell.setDataOnCell(with: cellDataDictionary["title"] as! String, addressText: cellDataDictionary["vicinity"] as! String, distanceText: cellDataDictionary["distance"] as! String)
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
//        NSString *latlong = @"-56.568545,1.256281";
        if let cellDataDictionary = tableDataArray?[indexPath.row] as? Dictionary<String, Any> {
            let coordinates:CLLocation = cellDataDictionary["location"] as! CLLocation
            var locationString:String = ""
            if let startLocation = self.currentLocation {
                locationString = "https://maps.google.com/maps?saddr=\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)&daddr=\(coordinates.coordinate.latitude),\(coordinates.coordinate.longitude)"
                
                if UIApplication.shared.canOpenURL(URL(string: locationString)!) {
                    UIApplication.shared.openURL(URL(string: locationString)!)
                }
            }
        }
    }
    
    func updateViewWithFuelStationsForLocation(locationObj:CLLocation) {
        
        CUtilityRequest.sharedInstance().getNearByFuelPumps(locationObj) { (responseObj:Any?, success:Bool, error:Error?) in
            self.view.dismissProgress()
            if let serverResponse:NSDictionary = responseObj as? NSDictionary{
                print(serverResponse)
                self.tableDataArray = self.prepareDataArrayFromServerResponse(serverResponseData: ((serverResponse.object(forKey: "results")) as! NSDictionary).object(forKey: "items") as! NSArray)
            }
            
            self.nearbyTableView.reloadData()
        }
    }
    
    
    func updateViewWithServiceStationForLocation(locationObj:CLLocation) {
        
        CUtilityRequest.sharedInstance().getNearByServiceStations(locationObj) { (responseObj:Any?, success:Bool, error:Error?) in
        self.view.dismissProgress()
            if let serverResponse:NSDictionary = responseObj as? NSDictionary{
                print(serverResponse)
                self.tableDataArray = self.prepareDataArrayFromServerResponse(serverResponseData: ((serverResponse.object(forKey: "results")) as! NSDictionary).object(forKey: "items") as! NSArray)
            }
            
            self.nearbyTableView.reloadData()
        }
        
    }
    
    func prepareDataArrayFromServerResponse(serverResponseData: NSArray) -> [Dictionary<String, Any>] {
        var parsedData = [Dictionary<String, Any>]()
        
        for placeData in serverResponseData{
            var filteredData = [String: Any]()
            
            if let placeDictionary = placeData as? Dictionary<String, Any> {
                if (placeDictionary["title"] != nil) && (placeDictionary["position"] != nil){
                    filteredData["title"] = placeDictionary["title"]
                    let locationObj:CLLocation = CLLocation(latitude: (placeDictionary["position"] as! Array)[0], longitude: (placeDictionary["position"] as! Array)[1])
                    filteredData["location"] = locationObj
                    filteredData["vicinity"] = placeDictionary["vicinity"]
                    if let distance:Float = placeDictionary["distance"] as? Float {
                        if distance > 1000 {
                            filteredData["distance"] = String.localizedStringWithFormat("%.2f Kms", distance/1000)
                        }else{
                            filteredData["distance"] = String.localizedStringWithFormat("%0.f Mts", distance)
                        }
                    }
                }
                parsedData.append(filteredData)
            }
        }
        print(parsedData)
        return parsedData
    }
    
    func updateViewWithServiceStationsWithLocation(locationObj:CLLocation) {
        //        CUtilityRequest.sharedInstance()
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        self.dismiss(animated: true) {
            // nothing to do here
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
