//
//  MICSlider.swift
//  slider
//
//  Created by Vaibhav Gautam on 15/02/17.
//  Copyright © 2017 Vaibhav Gautam. All rights reserved.
//

import UIKit

typealias selectedDurationBlock = (_ locationObj: Int) -> Void

class MICSlider: UIView {

    private var itemsArray: [String] = []
    private var barWidth:CGFloat = 0
    private var draggableButton:UIButton?
    
    private var selectedIndexForDuration:selectedDurationBlock?
    private var bubbleText:UILabel?
    private var bubbleImage:UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        barWidth = frame.width-40
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    func setDataArray(titleArray:Array<String>){
        self.backgroundColor = UIColor.clear
        itemsArray = titleArray
        
        createBars()
        createBackgroundBar()
        createDraggableView()
    }
    
    func createDraggableView(){
        let dragGestureRecogniser = UIPanGestureRecognizer(target: self, action: #selector(panView(sender:)))
        
        draggableButton = UIButton(type: .custom)
        draggableButton?.frame = CGRect(x: 10, y: ((self.frame.size.height/2)-10), width: 20, height: 20)
        draggableButton?.titleLabel?.text = ""
        draggableButton?.backgroundColor = UIColor.clear
        draggableButton?.setBackgroundImage(#imageLiteral(resourceName: "ShareSliderIcon"), for: .normal)
        
        draggableButton?.isUserInteractionEnabled = true
        draggableButton?.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        dragGestureRecogniser.minimumNumberOfTouches = 1
        dragGestureRecogniser.maximumNumberOfTouches = 1
        dragGestureRecogniser.cancelsTouchesInView = false
        dragGestureRecogniser.delaysTouchesBegan = false
        dragGestureRecogniser.delaysTouchesEnded = false
        
        draggableButton?.addGestureRecognizer(dragGestureRecogniser)
        self.addSubview(draggableButton!)
        if bubbleImage == nil {
            bubbleImage = UIImageView(image: #imageLiteral(resourceName: "timeBubble"))
            bubbleImage?.frame = CGRect(x: -5, y: -27, width: 32, height: 32)
            bubbleImage?.image = #imageLiteral(resourceName: "timeBubble")
            draggableButton?.addSubview(bubbleImage!)
        }
        
        if bubbleText == nil {
            bubbleText = UILabel(frame: (bubbleImage?.frame)!)
            bubbleText?.text = "15m"
            bubbleText?.textAlignment = .center
            bubbleText?.textColor = UIColor.white
            bubbleText?.font = UIFont(name: "Helvetica", size: 9.0)
            draggableButton?.addSubview(bubbleText!)
        }
        
        
        
    }
    
    func resetBubbleText(){
        bubbleText?.text = "1km"
    }
    
    @IBAction func panView(sender: UIPanGestureRecognizer){
        
        let translation = sender.translation(in: self)
        let maxX:CGFloat = self.frame.width-20
        let minX:CGFloat = 20
        
        if ((sender.view!.center.x + translation.x) > maxX) {
            sender.view!.center = CGPoint(x: maxX, y: sender.view!.center.y)
        }else if ((sender.view!.center.x + translation.x) < minX){
            sender.view!.center = CGPoint(x: minX, y: sender.view!.center.y)
        }else{
            sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y)
        }
        
        sender.setTranslation(CGPoint.zero, in: self)
        
        if sender.state == .ended {
            adjustSliderLocation()
        }
    }
    
    func adjustSliderLocation() {
        
        let pointerLocation:CGPoint = (draggableButton?.center)!
        
        var pointsArray:[CGFloat] = []
        
        let itemsCount = self.itemsArray.count
        for index in 0..<itemsCount{
            let xPosition:CGFloat = (10 + (CGFloat(index)*(barWidth/CGFloat(itemsCount-1))))
            pointsArray.append(xPosition)
        }
        
        let distanceBetweenTwoPoints:CGFloat = self.barWidth/CGFloat(self.itemsArray.count-1)
        
        
        for index in 0..<pointsArray.count{
            if index == pointsArray.count-1 {
                if pointerLocation.x < floor(((pointsArray[index-1] + (distanceBetweenTwoPoints/2))+20)){
                    self.animateBarTo(xPosition: pointsArray[index-1],withIndex: index)
                }else{
                    self.animateBarTo(xPosition: pointsArray[index],withIndex: index)
                }
                break
            }else if(pointerLocation.x >= pointsArray[index]) && (pointerLocation.x < pointsArray[index+1]) {
                if (((pointsArray[index] + (distanceBetweenTwoPoints/2))+20) > pointerLocation.x) {
                    self.animateBarTo(xPosition: pointsArray[index],withIndex: index)
                }else{
                    self.animateBarTo(xPosition: pointsArray[index+1],withIndex: index)
                }
                break
            }
        }
    }
    
    
    func animateBarTo(xPosition xPos:CGFloat, withIndex index:Int) {
        UIView.animate(withDuration: 0.5, animations: {
            self.draggableButton?.frame = CGRect(x: xPos, y: self.draggableButton!.frame.origin.y, width: self.draggableButton!.frame.size.width, height: self.draggableButton!.frame.size.height)
        }, completion: { (completion) in
            // perfomr selector here
            if (self.selectedIndexForDuration != nil){
                self.selectedIndexForDuration!(index)
                self.bubbleText?.text = self.itemsArray[index]
            }
        })
    }
    
    func createBars() {
        
        let totalBars = self.itemsArray.count
        
        for index in 0..<totalBars{
            
            let barTitle:UILabel = UILabel(frame: CGRect.zero)
            
            barTitle.text = self.itemsArray[index]
            barTitle.font = UIFont(name: "Helvetica-Bold", size: 9)
            barTitle.frame = CGRect(x: (barTitle.frame.origin.x), y: (barTitle.frame.origin.y) - 2, width: (barTitle.frame.size.width), height: (barTitle.frame.size.height))
            barTitle.textColor = UIColor(red:0.47, green:0.49, blue:0.51, alpha:1.00)
            barTitle.sizeToFit()
            
            self.addSubview(barTitle)
            
            let xPosition:CGFloat = (20 + (CGFloat(index)*(barWidth/CGFloat(totalBars-1))))
            let yPosition:CGFloat = ((index%2 == 0) ? (self.frame.size.height/2) : (self.frame.size.height/2)-10 )
            
            let verticalBar:UIView = UIView(frame: CGRect(x: xPosition, y: yPosition, width: 1, height: 10))
            verticalBar.backgroundColor = UIColor(red:0.64, green:0.64, blue:0.53, alpha:1.00)
            self.addSubview(verticalBar)
            
            if index%2 == 0 {
                barTitle.center = CGPoint(x: verticalBar.center.x, y: (verticalBar.center.y + 15))
            }else{
                barTitle.center = CGPoint(x: verticalBar.center.x, y: (verticalBar.center.y - 15))
            }
        }
    }
    
    func scrollToIndex(index:Int, length:Int)
    {

         
            let xPosition:CGFloat = (10 + (CGFloat(index)*(barWidth/CGFloat(length-1))))

            animateBarTo(xPosition: xPosition, withIndex: index)
        
        
    }
    
    
    func setCallbackForSelectedDuration(completion:@escaping selectedDurationBlock){
       self.selectedIndexForDuration = completion
    }
    
    func createBackgroundBar(){
        
        let barView:UIView = UIView(frame: CGRect(x: 20, y: ((self.frame.size.height/2)-1), width: (self.frame.size.width-40), height: 2))
        barView.backgroundColor = UIColor(red:0.64, green:0.64, blue:0.53, alpha:1.00)
        self.addSubview(barView)
        
    }

}
