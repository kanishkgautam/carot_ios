//
//  ShareLocationController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 16/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import MessageUI

class SessionDetailsCell: UITableViewCell {
    
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var btnRevokeSession: UIButton!
    @IBOutlet weak var timeBackgroundView: UIView!
    @IBOutlet weak var expiredView: UIView!
    
    override func awakeFromNib() {
        timeBackgroundView.layer.borderWidth = 1.0
        timeBackgroundView.layer.borderWidth = 1.0
    }
}


class ShareLocationController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var sessionTableView: UITableView!
    
    var duration : Int = 15
    var arraySessions: NSMutableArray = []
    var timerSession : Timer?
    
    var slider:MICSlider?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Share Location Screen")
        // self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        //self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavBar()
        
        createSliderView()
        //        FTIndicator.setIndicatorStyle(.dark)
        //
        //        FTIndicator.showProgressWithmessage(NSLocalizedString("Fetching Sessions", comment: ""))
        
        self.view.showIndicatorWithProgress(message: "Fetching Sessions")
        self.getAllSessions()
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "SHARE VEHICLE LOCATION"
        headerTitle.sizeToFit()
        self.navigationItem.titleView = headerTitle
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func getAllSessions()
    {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            self.view.dismissProgress()
            return
        }
        
        CSessionRequest.sharedInstance().getAllActiveSessionResponse { (responseObject, isSuccessful, error) in
            if(error == nil && responseObject != nil){
                self.view.dismissProgress()
                let arr : NSArray = (responseObject as? NSArray)!
                if(arr.count > 0 ){
                    
                    self.arraySessions = arr as! NSMutableArray
                    self.sessionTableView.reloadData()
                }
                self.sessionTableView.reloadData()
            }
            else{
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
    }
    
    func createSliderView() {
        self.view.layoutSubviews()
        if slider == nil {
            slider = MICSlider(frame: CGRect(x: 10, y: 60, width: UIScreen.main.bounds.width-30, height: 80))
            let arrayDuration:  [String] = ["15m", "30m", "1hr","2hr","3hr","4hr","5hr","6hr","7hr","8hr","9hr","10hr", "11hr", "12hr"]
            
            let arrayDurationInSeconds:  [Int] = [15,30,60,120,180,240,300,360,420,480,540,600,660,720]
            
            slider?.setDataArray(titleArray: arrayDuration)
            roundedView.addSubview(slider!)
            slider?.setCallbackForSelectedDuration { (selectedIndex) in
                //print(selectedIndex)
                self.duration = arrayDurationInSeconds[selectedIndex]
                //print(self.duration )
            }
        }
    }
    
    
    @IBAction func shareWhatsappTapped(_ sender: Any) {
        
        let url  = NSURL(string: "whatsapp://")
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "WhatsappShareTapped", category: kEventCategoryDashboard, andScreenName: "Share Location Screen", eventLabel: nil, eventValue: nil)
        if UIApplication.shared.canOpenURL(url! as URL) {
            self.shareSessionTapped(sender)
        } else {
            self.view.showIndicatorWithError("Your device is not able to send WhatsApp messages.")
        }
        
    }
    
    
    @IBAction func shareMailTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "MapShareTapped", category: kEventCategoryDashboard, andScreenName: "Share Location Screen", eventLabel: nil, eventValue: nil)
        if MFMailComposeViewController.canSendMail() {
            self.shareSessionTapped(sender)
        }else{
            self.view.showIndicatorWithError("Mail not Configured")
        }
    }
    
    @IBAction func shareMessageTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "MessageShareTapped", category: kEventCategoryDashboard, andScreenName: "Share Location Screen", eventLabel: nil, eventValue: nil)
        if( MFMessageComposeViewController.canSendText()){
            self.shareSessionTapped(sender)
        }else{
            self.view.showIndicatorWithError("Your device is not able to send messages.")
        }
    }
    
    @IBAction func shareSessionTapped(_ sender: Any) {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        let btn:UIButton = sender as! UIButton
        print(Int32(self.duration))
        CSessionRequest.sharedInstance().createSession(forDuration: Int32(self.duration), response: { (responseObject, isSuccessful, error) in
            if(error == nil && responseObject != nil)
            {
                let dictSession = responseObject
                self.arraySessions.insert(dictSession ?? NSDictionary(), at: 0)
                
                switch btn.tag{
                case 1:
                    self.sendMessage(session: dictSession as! NSDictionary)
                case 2:
                    self.sendWhatsAppMessage(session: dictSession as! NSDictionary)
                case 3:
                    self.sendEmail(session: dictSession as! NSDictionary)
                default:
                    self.sendMessage(session: dictSession as! NSDictionary)
                }
            }
            else{
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        })
        //        }
        //        else {
        //            FTIndicator.showError(withMessage: "Please choose duration to share")
        //        }
        
    }
    
    
    func startTimer () {
        
        if timerSession == nil {
            timerSession =  Timer.scheduledTimer(
                timeInterval: TimeInterval(krefreshSessionInterval),
                target      : self,
                selector    : #selector(refreshSessionInterval),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopTimer() {
        if timerSession != nil {
            timerSession?.invalidate()
            timerSession = nil
        }
    }
    
    @objc func refreshSessionInterval()
    {
        sessionTableView.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.stopTimer()
    }
    
    @objc func revokeSessionTapped(_ sender: Any)
    {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "RevokeSessionTapped", category: kEventCategoryDashboard, andScreenName: "Share Location Screen", eventLabel: nil, eventValue: nil)
        let alert:UIAlertController = UIAlertController(title: "Revoke Session?", message: "Do you want to revoke active shared session?", preferredStyle: .alert)
        
        let revokeAction:UIAlertAction = UIAlertAction(title: "Revoke", style: .default) { (action) in
            self.view.showIndicatorWithProgress(message: "")
            let btn:UIButton = sender as! UIButton
            let dict:NSDictionary = self.arraySessions[btn.tag] as! NSDictionary
            let sessionCode:String = (dict.object(forKey: "sessionCode") as! String)
            
            CSessionRequest.sharedInstance().revokeSession(withSessionId: sessionCode) { (responseObject, isSuccessful, error) in
                if(error == nil && responseObject != nil){
                    
                    self.view.showIndicatorWithInfo("Session Revoked")
                    
                    self.getAllSessions()
                }
                else{
                    if (responseObject != nil){
                        self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                        
                    }else{
                        self.view.showIndicatorWithError((error?.localizedDescription)!)
                    }
                    print(error?.localizedDescription ?? String())
                }
            }
        }
        
        let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(revokeAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: Mail
    func sendEmail(session:NSDictionary) {
        
        let shareSessionString:String = session.object(forKey: "sessionUrl") as! String
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setMessageBody(("Hey buddy, you can follow my car by clicking on the given link " + shareSessionString), isHTML: true)
            present(mail, animated: true)
        } else {
            
            self.view.showIndicatorWithError("Mail not Configured")
        }
    }
    
    func sendMessage(session:NSDictionary) {
        if( MFMessageComposeViewController.canSendText()){
            var shareSessionString:String = session.object(forKey: "sessionUrl") as! String
            shareSessionString = "Hey buddy, you can follow my car by clicking on the given link " + shareSessionString
            let messageVC = MFMessageComposeViewController()
            
            messageVC.body = shareSessionString;
            // messageVC.recipients = ["Enter tel-nr"]
            messageVC.messageComposeDelegate = self;
            
            self.present(messageVC, animated: false, completion: nil)
        }
        else
        {
            self.view.showIndicatorWithError("Your device is not able to send messages.")
        }
    }
    
    func sendWhatsAppMessage(session:NSDictionary) {
        
        let shareSessionString:String = session.object(forKey: "sessionUrl") as! String
        let urlString = "Hey buddy, you can follow my car by clicking on the given link " + shareSessionString
        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)")
        
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.openURL(url! as URL)
        } else {
            self.view.showIndicatorWithError("Your device is not able to send WhatsApp messages.")
        }
        self.sessionTableView.reloadData()
        
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
        self.sessionTableView.reloadData()
        
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
        self.sessionTableView.reloadData()
        
        /*switch (result.value) {
         case MessageComposeResultCancelled.value:
         println("Message was cancelled")
         self.dismissViewControllerAnimated(true, completion: nil)
         case MessageComposeResultFailed.value:
         println("Message failed")
         self.dismissViewControllerAnimated(true, completion: nil)
         case MessageComposeResultSent.value:
         println("Message was sent")
         self.dismissViewControllerAnimated(true, completion: nil)
         default:
         break;
         }*/
    }
    //MARK:Table View
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arraySessions.count > 0){ self.startTimer()}
        else { stopTimer()}
        return arraySessions.count
        
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"SessionCellIdentifier", for: indexPath) as! SessionDetailsCell
        
        cell.selectionStyle = .none
        cell.btnRevokeSession.addTarget(self, action:#selector(revokeSessionTapped(_:)), for: .touchUpInside)
        cell.btnRevokeSession.tag = indexPath.row
        let dict:NSDictionary = arraySessions[indexPath.row] as! NSDictionary
        var dur:Int =  dict.object(forKey: "duration") as! Int
        dur = (dur/60)
        let str:String = (dur == 0) ? "\(dict.object(forKey: "duration") as! Int) min" : "\(dur) hr"
        cell.labelDuration.text = str
        
        let interval:Double = dict.object(forKey: "expiresOn") as! Double
        let date = Date(timeIntervalSince1970: (interval/1000))
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d,YYYY hh:mm:ss"
        let dateString = formatter.string(from: currentDate)
        let dateObj = formatter.date(from: dateString)
        
        
        let activeState =  (dict.object(forKey: "active"))
        let isSessionActive:Bool = (activeState as! Bool?)!
        cell.expiredView.layer.borderColor = UIColor(red:0.33, green:0.37, blue:0.40, alpha:1.00).cgColor
        cell.expiredView.layer.borderWidth = 1.0
        
        if(isSessionActive == true){
            //            cell.btnRevokeSession.titleLabel?.textColor = UIColor.orange
            cell.btnRevokeSession.isUserInteractionEnabled = true
            cell.timeBackgroundView.layer.borderColor = UIColor(red:0.91, green:0.56, blue:0.20, alpha:1.00).cgColor
            
            let result = AppUtility.sharedUtility.intervalBetweenTwoDates(date1: dateObj! , date2: date )
            cell.labelTime.text = result
            cell.expiredView.isHidden = true
            
        }
        else{
            cell.timeBackgroundView.layer.borderColor = UIColor(red:0.40, green:0.40, blue:0.40, alpha:1.0).cgColor
            //            cell.btnRevokeSession.titleLabel?.textColor = UIColor.lightGray
            cell.btnRevokeSession.isUserInteractionEnabled = false
            cell.labelTime.text = "Expired"
            cell.expiredView.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

