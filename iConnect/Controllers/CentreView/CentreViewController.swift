//
//  CentreViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 06/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import MFSideMenu
import MessageUI

@objc class CentreViewController: UITabBarController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kCentreToNearbySegue {
            
            if let screenType = sender as? String{
                
                let navController:UINavigationController = segue.destination as! UINavigationController
                if !navController.children.isEmpty {
                    if let nearbyObj:NearbyViewController = navController.children[0] as? NearbyViewController {
                        if screenType == "FUEL" {
                            nearbyObj.isOpenedForServiceStation = false
                            AppUtility.sharedUtility.sendGAEvent(withEventName: "FuelStationsTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard Screen", eventLabel: nil, eventValue: nil)
                        }else {
                            nearbyObj.isOpenedForServiceStation = true
                            AppUtility.sharedUtility.sendGAEvent(withEventName: "ServiceStationsTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard Screen", eventLabel: nil, eventValue: nil)
                        }
                    }
                    
                }
            }
        } 
    }
    
    @objc func showAnimationViewAndUpdateDataInBackground() {
        
        let animationScreen:AnimatedSplashView = Bundle.main.loadNibNamed("AnimatedSplashView", owner: nil, options: nil)?.first as! AnimatedSplashView
        animationScreen.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        self.view.addSubview(animationScreen)
        self.view.bringSubviewToFront(animationScreen)
    }
    
    @objc func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        self .dismiss(animated: true) { 
            
        }
    }
    
}
