//
//  SignUpOTPViewController.m
//  iConnect
//
//  Created by Administrator on 1/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

#import "SignUpOTPViewController.h"
#import "CAROT-Swift.h"
@interface SignUpOTPViewController ()
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *resendButton;

@end

@implementation SignUpOTPViewController
@synthesize emailConfirmString;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[AppUtility sharedUtility] trackScreenNameWith:@"Signup OTP Screen"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resendButtonTapped:(id)sender {
    
    [self.submitButton setUserInteractionEnabled:NO];
    [self.resendButton setUserInteractionEnabled:NO];

    [[AppUtility sharedUtility] sendGAEventWithEventName:@"OTPResendTapped" category:kEventCategoryRegister andScreenName:@"Signup OTP Screen" eventLabel:NULL eventValue:NULL];
    [SICUserRequest sendOtpForEmail:self.emailConfirmString forPurpose:COTPTypeConfirmEmail response:^(id response, BOOL isSuccessful, NSError *error) {
        [self.submitButton setUserInteractionEnabled:YES];
        [self.resendButton setUserInteractionEnabled:YES];
        
        if (!isSuccessful) {
            if (response) {
                [self.view showIndicatorWithError:[[AppUtility sharedUtility] getErrorDescriptionFromDictionary:response]];
            }else{
                [self.view showIndicatorWithError:error.localizedDescription];
            }
        }
        else{
            [self.view showIndicatorWithError:NSLocalizedString(@"Please check your email inbox! We’ve resent you an OTP Code for verifying your account", nil)];
        }
    }];
}

- (IBAction)submitButtonTapped:(id)sender
{
    txtFieldInValid = nil;
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:otpTextField.text].length < 6) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"otp.invalid", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kOTPLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = otpTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidOTP" category:kEventCategoryRegister andScreenName:@"Signup OTP Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    [self.submitButton setUserInteractionEnabled:NO];
    [self.resendButton setUserInteractionEnabled:NO];

    [[AppUtility sharedUtility] sendGAEventWithEventName:@"OTPSubmitTapped" category:kEventCategoryRegister andScreenName:@"Signup OTP Screen" eventLabel:NULL eventValue:NULL];
    [self.view showIndicatorWithProgressWithMessage:@""];
    
    [SICUserRequest confirmEmail:self.emailConfirmString withOtp:[[[AppUtility sharedUtility] trimWhiteSpacesInString:otpTextField.text] stringByReplacingOccurrencesOfString:@" " withString:@""] response:^(id response, BOOL isSuccessful, NSError *error) {
        
        [self.submitButton setUserInteractionEnabled:YES];
        [self.resendButton setUserInteractionEnabled:YES];
        [self.view dismissProgress];

        if(isSuccessful){
            [self.view showIndicatorWithInfo:@"OTP Verified.Please login to continue using the app."];
            [[CommonData sharedData] getAllFuelTypesWithFuels:^(NSArray<CFuelType *> * _Nonnull allFuels) {
                NSLog(@"data fetched");
            }];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else{
            if(error && response != nil){
                [self.view showIndicatorWithError:[[AppUtility sharedUtility] getErrorDescriptionFromDictionary:response]];
            }else{
                [self.view showIndicatorWithError:error.localizedDescription];
            }
        }
    }];
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(txtFieldInValid != nil){
        if(string.length >= 1){
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
        }
    }
    return YES;
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
