


@interface RegistrationVc : UIViewController{
    
    __weak IBOutlet UITextField *firstNameTextField;
    __weak IBOutlet UITextField *lastNameTextField;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *mobileNumberTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *confirmPasswordTextField;
    UITextField *txtFieldInValid;

}
- (IBAction)submitButtonTapped:(id)sender;
- (IBAction)backButtonTapped:(id)sender;
- (IBAction)termsAndConditionsTapped:(id)sender;

@end
