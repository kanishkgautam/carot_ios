

#import "RegistrationVc.h"
#import "SignUpOTPViewController.h"

@interface RegistrationVc ()
@property(nonatomic, strong) UIActivityIndicatorView* signUpActivity;
@end

@implementation RegistrationVc
@synthesize signUpActivity;

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [[AppUtility sharedUtility] trackScreenNameWith:@"Registration Screen"];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(txtFieldInValid != nil){
        if(string.length >= 1){
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kOTPLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kConfirmPasswordLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kFirstNameLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kLastNameLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kMobileNumberLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
        }
    }
   // if (textField == passwordTextField || textField == confirmPasswordTextField || textField == emailTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    //}
    
    return YES;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // self.title = @"";
    
}

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)termsAndConditionsTapped:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"https://www.carot.com/termsOfUse"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.carot.com/termsOfUse"]];
    }
}

- (IBAction)submitButtonTapped:(id)sender
{
    
    bool isEmailValid =false;
    bool isValidMobileNumber = false;
    
    txtFieldInValid = nil;
    if(firstNameTextField.text.length == 0){
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"First name cannot be left blank", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kFirstNameLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = firstNameTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidFirstName" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if (![[firstNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet alphanumericCharacterSet]] isEqualToString:@""]) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Only alphanumeric characters are allowed", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kFirstNameLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = firstNameTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidFirstName" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if(lastNameTextField.text.length == 0){
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Last name cannot be left blank", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kLastNameLabelTag].backgroundColor = [UIColor redColor];
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidLastName" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        txtFieldInValid = lastNameTextField;
        return;
    }
    
    if (![[lastNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet alphanumericCharacterSet]] isEqualToString:@""]) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Only alphanumeric characters are allowed", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kLastNameLabelTag].backgroundColor = [UIColor redColor];
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidLastName" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        txtFieldInValid = lastNameTextField;
        return;
    }
    
    if(emailTextField.text.length == 0){
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Email Id cannot be left blank", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = emailTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidEmail" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    
    isEmailValid = [[AppUtility sharedUtility] isValidEmailWithEmailToBeValidated:emailTextField.text];
    if(!isEmailValid){
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Invalid Email Id", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = emailTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidEmail" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:mobileNumberTextField.text].length != 10) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Please enter a valid 10 digit mobile number", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kMobileNumberLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = mobileNumberTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidMobileNumber" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    isValidMobileNumber = [[AppUtility sharedUtility] isValidPhoneNumberWithPhoneNumberToBeValidated:mobileNumberTextField.text];
    if(!isValidMobileNumber){
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Invalid Mobile No.", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kMobileNumberLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = mobileNumberTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidMobileNumber" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:passwordTextField.text].length < 6) {
        
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Password must contain min 6 characters , blank spaces are not allowed", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = passwordTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidPassword" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:confirmPasswordTextField.text].length < 1) {
        
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Confirm Password cannot be left blank", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kConfirmPasswordLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = confirmPasswordTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidConfirmPassword" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if(![passwordTextField.text isEqualToString:confirmPasswordTextField.text])
    {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Password and Confirm password must be same", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = [UIColor redColor];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kConfirmPasswordLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = confirmPasswordTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"PasswordMismatch" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
        
        return;
    }
    
    
    [[AppUtility sharedUtility] sendGAEventWithEventName:@"RegisterTapped" category:kEventCategoryRegister andScreenName:@"Registration Screen" eventLabel:NULL eventValue:NULL];
    [self.view showIndicatorWithProgressWithMessage:@"Creating account"];
    
    [SICUserRequest signupUserWithEmail:[emailTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""]
                              firstName:[[AppUtility sharedUtility] encodeStringWithStringToEncode:firstNameTextField.text]
                               lastName:[[AppUtility sharedUtility] encodeStringWithStringToEncode:lastNameTextField.text]
                            phoneNumber:mobileNumberTextField.text
                               password:passwordTextField.text
                               response:^(id responseObject, BOOL requestSuccess, NSError *error) {
                                   [self.view dismissProgress];
                                   if (requestSuccess) {
                                       
                                       [self.view showIndicatorWithInfo:@"Please check your email inbox! We’ve sent you an OTP Code for verifying your account"];
                                       [self performSegueWithIdentifier:kRegisterToOTPSegue sender:nil];
                                       
                                   }else{
                                       if (error && responseObject !=nil) {
                                           [self.view showIndicatorWithError:[[AppUtility sharedUtility] getErrorDescriptionFromDictionary:responseObject]];
                                       }else{
                                           [self.view showIndicatorWithError:error.localizedDescription];
                                       }
                                   }
                               }];
}

-(void )prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:kRegisterToOTPSegue]) {
        SignUpOTPViewController *signupOTPControllerObj = segue.destinationViewController;
        [signupOTPControllerObj setEmailConfirmString:emailTextField.text];
    }
}


@end
