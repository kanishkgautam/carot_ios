//
//  SignUpOTPViewController.h
//  iConnect
//
//  Created by Administrator on 1/23/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpOTPViewController : UIViewController{

    __weak IBOutlet UITextField *otpTextField;
    UITextField *txtFieldInValid;

}

@property(nonatomic,strong) NSString *emailConfirmString;

- (IBAction)submitButtonTapped:(id)sender;
- (IBAction)backButtonTapped:(id)sender;
- (IBAction)resendButtonTapped:(id)sender;

@end
