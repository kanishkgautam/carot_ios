//
//  ManagePreferencesViewController.swift
//  iConnect
//
//  Created by Vivek Joshi on 06/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class PreferenceCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var alertSwitch: UISwitch!
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    
    func setData(alertPreference: Preferences, pushPreference: Preferences) -> Void {
        
        if alertPreference.showDefaultValue! {
            self.titleLabel.text = alertPreference.preferenceName! + " (\(alertPreference.defaultValue!))"
        }
        else {
            self.titleLabel.text = alertPreference.preferenceName
            
        }
        
        self.alertSwitch.setOn(alertPreference.enabled!, animated: false)
        self.notificationSwitch.setOn(pushPreference.enabled!, animated: false)
        
    }
}


class ManagePreferencesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var alertsArray = NSMutableArray()
    var pushArray = NSMutableArray()
    
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Manage preferences screen")
        // Do any additional setup after loading the view.
        configureNavBar()
        fetchAlerts()
        self.tableView.rowHeight = 60
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "MANAGE PREFERENCES"
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
        
    }
    
    @objc func backButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func fetchAlerts() -> Void {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        self.view.showIndicatorWithProgress(message: "")
        
        CEventRequest.sharedInstance().getUserEventPreferenceResponse { (response, success, error) in
            
            if success {
                
                let prefArray = response as! NSMutableArray
                
                if prefArray.count == 0 {
                    return
                }
                
                for dict in prefArray {
                    
                    let preferenceInfo = Preferences()
                    preferenceInfo.setPropertiesFromDictionary(dict: (dict as? NSDictionary)!)
                    self.alertsArray.add(preferenceInfo)
                }
                
                DispatchQueue.main.async {
                    self.fetchNotificationPreference()
                }
            }
            else {
                self.view.dismissProgress()
            }
            
        }
        
    }
    
    func fetchNotificationPreference() -> Void {
        
        CEventRequest.sharedInstance().getUserPushPreferenceResponse { (response, success, error) in
            self.view.dismissProgress()
            
            if success {
                
                let prefArray = response as! NSArray
                
                if prefArray.count == 0 {
                    return
                }
                
                for dict in prefArray {
                    
                    let preferenceInfo = Preferences()
                    preferenceInfo.setPropertiesFromDictionary(dict: (dict as? NSDictionary)!)
                    self.pushArray.add(preferenceInfo)
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            else {
                
            }
        }
    }
    
    
    //MARK:- UITableVeiw Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return alertsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if alertsArray.count == 0 {
            return 0
        }
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if alertsArray.count == 0 {
            return nil
        }
        
        return self.headerView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let alertPreference = self.alertsArray[indexPath.row] as? Preferences
        let pushPreference = self.pushArray[indexPath.row] as? Preferences
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as? PreferenceCell
        cell!.selectionStyle = .none
        
        cell!.alertSwitch.addTarget(self, action: #selector(alertSwitchValueChanged(alertSwitch:)), for: .valueChanged)
        cell!.notificationSwitch.addTarget(self, action: #selector(notificationSwitchValueChanged(pushSwitch:)), for: .valueChanged)
        
        cell!.alertSwitch.tag = alertPreference?.preferenceId as! Int
        cell!.notificationSwitch.tag = pushPreference?.preferenceId as! Int
        
        cell!.setData(alertPreference: alertPreference!, pushPreference: pushPreference!)
        
        return cell!
    }
    
    
    //MARK:- Switch Value changed
    
    @objc func alertSwitchValueChanged(alertSwitch: UISwitch) -> Void {
        
        CEventRequest.sharedInstance().updateUserEventPreference(withId: Int32(alertSwitch.tag), userPreferenceIsOn: alertSwitch.isOn) { (response, success, error) in
            
            if success {
                
                for alert in self.alertsArray {
                    let pref = alert as? Preferences
                    if pref?.preferenceId?.intValue == alertSwitch.tag {
                        pref?.enabled = alertSwitch.isOn
                        break
                    }
                }
            }
            else {
                
                alertSwitch.isOn = !alertSwitch.isOn
            }
        }
        
    }
    
    @objc func notificationSwitchValueChanged(pushSwitch: UISwitch) -> Void {
        CEventRequest.sharedInstance().updateUserPushPreference(withId: Int32(pushSwitch.tag) , userPreferenceIsOn: pushSwitch.isOn) { (response, success, error) in
            
            if success {
                
                for alert in self.pushArray {
                    let pref = alert as? Preferences
                    if pref?.preferenceId?.intValue == pushSwitch.tag {
                        pref?.enabled = pushSwitch.isOn
                        break
                    }
                }
            }
            else {
                
                pushSwitch.isOn = !pushSwitch.isOn
            }
        }
    }
}
