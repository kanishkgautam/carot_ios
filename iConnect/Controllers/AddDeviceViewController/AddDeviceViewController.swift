//
//  AddDeviceViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 03/02/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import FTIndicator
@objcMembers class AddDeviceViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var labelVehicleMake: UILabel!
    @IBOutlet weak var labelVehicleModel: UILabel!
    @IBOutlet weak var labelVehicleYear: UILabel!
    @IBOutlet weak var labelfuelType: UILabel!
    @IBOutlet weak var labelGearType: UILabel!
    
    @IBOutlet weak var devicePreviewView: UIView!
    @IBOutlet weak var textFieldDeviceId: UITextField!
    @IBOutlet weak var textFieldDeviceName: UITextField!
    
    @IBOutlet weak var actionButton: UIButton!
    var isOpenedForEditing:Bool = false
    var prefilledDeviceObj:CDevice?
    var boolBackEnabled:ObjCBool = false
    lazy var presentationDelegate = PickerControllerPresentationManager()
    
    var arrayMake:Array = [String]()
    var arrayMakeModelsData : NSArray = [CVehicleMake]() as NSArray
    var arrayYears:Array = [String]()
    var selectedVehicleMake: Int = 0
    var selectedVehicleModelId:Int = 0
    var txtFieldInValid: UITextField?
    
    var allFuels = [CFuelType]()
    
    var isFirstTime = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppUtility.sharedUtility.trackScreenName(with: "Add Device Screen")
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        configureNavBar()
        
        
        //Get current Year
        let date = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: date)
        
        //add all years from current year till 1990
        
        let first = 1990
        let last = currentYear
        let interval = 1
        for year in stride(from: first, through: last, by: interval) {
            let stringValue = "\(year)"
            arrayYears.append(stringValue)
        }
        arrayYears.reverse()
        
        backButton.isHidden = true
        
        CVehicleRequest.sharedInstance().getVehicleReferenceDataResponse { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            if(error == nil && responseObject != nil)
            {
                if let vehicleObj:CVehicleReference = responseObject as? CVehicleReference{
                    
                    self.arrayMakeModelsData = vehicleObj.vehicleMakers as NSArray
                }
                
            }else{
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
        
        
        // checkNumberOfDevices()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        CommonData.sharedData.getAllFuelTypes { (fuels) in
            if fuels.count > 0 {
                self.allFuels = fuels
            }
        }
        
        if isOpenedForEditing {
            if (prefilledDeviceObj != nil) {
                if let vehicle:CVehicle = CVehicleRequest.sharedInstance().getVehicleDetails(for: prefilledDeviceObj) {
                    
                    textFieldDeviceId.text = prefilledDeviceObj?.serialNumber
                    textFieldDeviceId.isUserInteractionEnabled = false
                    textFieldDeviceName.text = AppUtility.sharedUtility.decodeString(stringToDecode: (prefilledDeviceObj?.name)!)
                    //                    CVehicleRequest.sharedInstance().vehicleMakeAndModel(forId: vehicle.vehicleModelId, response: { (vehicleModelMappingData, statusCode, errorObj) in
                    //                        print(vehicleModelMappingData?["make"] ?? "")
                    //                        self.labelVehicleModel.text = "\(vehicleModelMappingData?["model"])"
                    //
                    //                        self.labelVehicleMake.text = "\(vehicleModelMappingData?["make"])"
                    //
                    //                    })
                    //
                    CommonData.sharedData.getMakerAndModel(with: Int(vehicle.vehicleModelId), mappingCallback: { (makeObj, modelObj) in
                        self.labelVehicleModel.text = modelObj?.modelName! ?? ""
                        self.labelVehicleMake.text = makeObj?.makerName! ?? ""
                        self.labelVehicleModel.textColor = UIColor.black
                        self.labelVehicleMake.textColor = UIColor.black
                    })
                    
                    
                    //labelVehicleMake.text = vehicle.vehicleMake
                    //labelVehicleMake.textColor = UIColor.black
                    //labelVehicleModel.text = vehicle.vehicleModel
                    //labelVehicleModel.textColor = UIColor.black
                    labelVehicleYear.text = ((vehicle.makeYear < 1) ? "" : "\(vehicle.makeYear)")
                    labelVehicleYear.textColor = UIColor.black
                    labelfuelType.text = prefilledDeviceObj?.vehicleFuelType
                    labelfuelType.textColor = UIColor.black
                    labelGearType.text = vehicle.vehicleGearType
                    labelGearType.textColor = UIColor.black
                    labelfuelType.text = vehicle.vehicleFuelType
                    labelfuelType.textColor = UIColor.black
                    
                    self.selectedVehicleModelId = Int(vehicle.vehicleModelId)
                }
            }
        }
    }
    
    
    @IBAction func deviceIDHelpTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "DeviceHelpTapped", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
        devicePreviewView.isHidden = false
        
    }
    
    
    @IBAction func deviceIdPopupCrossTapped(_ sender: Any) {
        devicePreviewView.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        if isOpenedForEditing {
            headerTitle.text = "EDIT DEVICE"
            actionButton.setTitle("UPDATE", for: .normal)
            headerTitle.sizeToFit()
        }else{
            headerTitle.text = "ADD DEVICE"
            actionButton.setTitle("ADD", for: .normal)
            headerTitle.sizeToFit()
        }
        
        self.navigationItem.titleView = headerTitle
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
        
    }
    
    @objc func backButtonTapped() {
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func selectVehicleMake(_ sender: Any) {
        
        if(arrayMakeModelsData.count == 0){
            return
        }
        
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        
        self.arrayMake = CommonData.sharedData.getAllVehicleMakersName()!
        pickerView.arrayData = self.arrayMake as NSArray
        
        
        pickerView.doneButtonCallback { (row, component) in
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleMake))?.backgroundColor = UIColor.init(red: 155/255, green: 183/255, blue: 205/255, alpha: 1.0)
            self.selectedVehicleMake = row
            self.labelVehicleMake.text = self.arrayMake[row]
            self.labelVehicleMake.textColor = UIColor.black
            
            self.labelVehicleModel.text = "Select Vehicle Model*"
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    
    @IBAction func selectVehicleModel(_ sender: Any) {
        
        if(arrayMakeModelsData.count == 0){
            return
        }
        
        if !arrayMake.contains(self.labelVehicleMake.text!) {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Please Select a vehicle maker first")
            return
            
        }
        
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        //let values:CVehicleMake = ((arrayMakeModelsData as NSArray)[selectedVehicleMake] as? CVehicleMake)!
        
        let values:NSArray = (arrayMakeModelsData as NSArray).value(forKeyPath: "makerName") as! NSArray
        self.arrayMake = values as! [String]
        
        if let values = (arrayMakeModelsData as NSArray)[arrayMake.index(of: self.labelVehicleMake.text!)!] as? CVehicleMake {
            
            let sortedArray = values.models.sorted { (($0 as! CVehicleModel).modelName) < (($1 as! CVehicleModel).modelName) }
            let valuesModels:NSArray = (sortedArray as NSArray).value(forKeyPath: "modelName") as! NSArray
            pickerView.arrayData = valuesModels as NSArray
            
            pickerView.doneButtonCallback { (row, component) in
                self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleModel))?.backgroundColor =  UIColor.init(red: 155/255, green: 283/255, blue: 205/255, alpha: 1.0)

                self.labelVehicleModel.text = pickerView.arrayData[row] as? String
                self.labelVehicleModel.textColor = UIColor.black
               
                let valuesModelsId:NSArray = (sortedArray as NSArray).value(forKeyPath: "modelId") as! NSArray
                self.selectedVehicleModelId = valuesModelsId[row] as! Int
                
            }
            self.present(pickerView, animated: true, completion: nil)
            // }
        }
        else{
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Please Select a vehicle maker first")
        }
        
        
        
    }
    
    
    
    @IBAction func selectYear(_ sender: Any) {
        
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.arrayData = arrayYears as NSArray
        
        pickerView.doneButtonCallback { (row, component) in
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleYear))?.backgroundColor = UIColor.init(red: 155/255.0, green: 183/255.0, blue: 205/255.0, alpha: 1.0)

            self.labelVehicleYear.text = self.arrayYears[row]
            self.labelVehicleYear.textColor = UIColor.black
            
        }
        self.present(pickerView, animated: true, completion: nil)
        
        
        
    }
    @IBAction func selectGearType(_ sender: Any) {
        
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        let arrayGear : Array = ["MANUAL","AUTOMATIC"]
        pickerView.arrayData = arrayGear as NSArray
        
        pickerView.doneButtonCallback { (row, component) in
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleGearType))?.backgroundColor = UIColor.init(red: 155/255.0, green: 183/255.0, blue: 205/255.0, alpha: 1.0)

            self.labelGearType.text = ((row==0) ? "MANUAL": "AUTOMATIC")
            self.labelGearType.textColor = UIColor.black
            
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    @IBAction func selectFuelType(_ sender: Any) {
        
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        
        let fuelNameArray:NSMutableArray? = []
        
        for fuelObj in self.allFuels {
            fuelNameArray?.add(fuelObj.fuelType)
        }
        pickerView.arrayData = fuelNameArray!
        
        pickerView.doneButtonCallback { (row, component) in
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleFuelType))?.backgroundColor = UIColor.init(red: 155/255.0, green: 183/255.0, blue: 205/255.0, alpha: 1.0)
            self.labelfuelType.text = "\(fuelNameArray!.object(at:row))"
            self.labelfuelType.textColor = UIColor.black
        }
        self.present(pickerView, animated: true, completion: nil)
    }
    
    @IBAction func addDevice(_ sender: Any) {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        txtFieldInValid = nil
        
        let count = AppUtility.sharedUtility.trimWhiteSpaces(inString: textFieldDeviceId.text!).count
        
        if count == 0 || count > 20 {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Please enter a valid serial number.")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kdeviceIdTag))?.backgroundColor = UIColor.red
            txtFieldInValid = textFieldDeviceId
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidDeviceSerial", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
            
        }
        
        let validDeviceID:Bool = (AppUtility.sharedUtility.trimWhiteSpaces(inString: textFieldDeviceId.text!).trimmingCharacters(in: CharacterSet.alphanumerics) == "")
        
        if !validDeviceID {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Please enter a valid device ID")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kdeviceIdTag))?.backgroundColor = UIColor.red
            txtFieldInValid = textFieldDeviceId
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidDeviceID", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if((textFieldDeviceName.text?.count)!<3 || (textFieldDeviceName.text?.count)!>100){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Name length must lie between 3 to 100")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kdeviceNameTag))?.backgroundColor = UIColor.red
            txtFieldInValid = textFieldDeviceName
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidDeviceName", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        
        if((labelVehicleMake.text?.count)! == 0 || labelVehicleMake.text == "Select Vehicle Make*"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle Make cannot be empty")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleMake))?.backgroundColor = UIColor.red
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidVehicleMake", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if((labelVehicleModel.text?.count)! == 0 || labelVehicleModel.text == "Select Vehicle Model*"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle Model cannot be empty")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleModel))?.backgroundColor = UIColor.red
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidVehicleModel", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        
        if((labelVehicleYear.text?.count)! == 0 || labelVehicleYear.text == "Select year*"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle year cannot be empty")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleYear))?.backgroundColor = UIColor.red
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidVehicleYear", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if((labelGearType.text?.count)! == 0 || labelGearType.text == "Select gear type*"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle gear type cannot be empty")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleGearType))?.backgroundColor = UIColor.red
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidGearType", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        if((labelfuelType.text?.count)! == 0 || labelfuelType.text == "Select fuel type*"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle fuel type cannot be empty")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleFuelType))?.backgroundColor = UIColor.red
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidFuelType", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            return
        }
        
        
        if isOpenedForEditing {
            AppUtility.sharedUtility.sendGAEvent(withEventName: "UpdateDeviceTapped", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            if let vehicle:CVehicle = CVehicleRequest.sharedInstance().getVehicleDetails(for: prefilledDeviceObj) {
                self.view.showIndicatorWithProgress(message: "")
                
                let deviceName: NSString = NSString(string: (textFieldDeviceName.text?.replacingOccurrences(of: " ", with: ""))!)
                
                CVehicleRequest.sharedInstance().updateDevice(withSerial: textFieldDeviceId.text, deviceName: AppUtility.sharedUtility.encodeString(stringToEncode: deviceName) as String?, vehicleID:Int32(vehicle.vehicleId), modelId: UInt(self.selectedVehicleModelId), makeYear: UInt(labelVehicleYear.text!)!, fuelType: labelfuelType.text, gearType: labelGearType.text, response: { (responseObject, isSuccessful, error) in
                    
                    if(error == nil && responseObject != nil){
                        print(responseObject ?? NSDictionary())
                        self.view.showIndicatorWithInfo("Record updated successfully")
                        CommonData.sharedData.refreshAllData()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        if (responseObject != nil){
                            self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                            
                        }else{
                            self.view.showIndicatorWithError((error?.localizedDescription)!)
                        }
                        print(error?.localizedDescription ?? String())
                    }
                    FTIndicator.dismissProgress()
                })
            }
            
        }else{
            AppUtility.sharedUtility.sendGAEvent(withEventName: "AddDeviceTapped", category: kEventCategoryDeviceManagement, andScreenName: "Add Device Screen", eventLabel: nil, eventValue: nil)
            self.view.showIndicatorWithProgress(message: "Please wait")
            CVehicleRequest.sharedInstance().addNewDevice(withSerial: textFieldDeviceId.text, deviceName: AppUtility.sharedUtility.trimWhiteSpaces(inString: textFieldDeviceName.text!), modelId: UInt(self.selectedVehicleModelId), makeYear: UInt(labelVehicleYear.text!)!, fuelType: labelfuelType.text, gearType: labelGearType.text) { (responseObject, isSuccessful, error) in
                FTIndicator.dismissProgress()
                
                if(error == nil && responseObject != nil){
                    
                    self.showSuccessAlert()
                    
                }
                else
                {
                    if (responseObject != nil){
                        self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                        
                    }else{
                        self.view.showIndicatorWithError((error?.localizedDescription)!)
                    }
                    print(error?.localizedDescription ?? String())
                }
            }
        }
    }
    
    func showSuccessAlert() {
        let successAlert:UIAlertController = UIAlertController(title: "Great News!", message: "We have identified your device , it will take 48-72 for device to get activated", preferredStyle: .alert)
        let okAction:UIAlertAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.checkNumberOfDevices()
            
            if !self.isFirstTime {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        successAlert.addAction(okAction)
        self.present(successAlert, animated: true, completion: nil)
    }
    
    func checkNumberOfDevices()
    {
        CVehicleRequest.sharedInstance().getUserDevicesResponse { (responseObject, isSuccessful, error) in
            if(error == nil && responseObject != nil){
                let arrayDevices:NSArray = responseObject as! NSArray
                let values:NSArray = (arrayDevices as NSArray).value(forKeyPath: "status") as! NSArray
                var flag:Int = 0
                
                for deviceStatus in values {
                    let deviceObj:NSString = deviceStatus as! NSString
                    if((deviceObj as String == kDeviceStatusSubscriptionPending) || (deviceObj as String == kDeviceStatusActivationInProgress)){
                        flag += 1
                    }
                }
                
                if(arrayDevices.count == flag){
                    //manage device
                    print(self.navigationController?.viewControllers ?? Array())
                    
                    if(self.navigationController?.viewControllers == nil){
                        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeviceManagementViewController") as! DeviceManagementViewController
                        viewController.hideBackButton = true
                        
                        
                        
                        let navigationController = UINavigationController(rootViewController: viewController)
                        navigationController.navigationBar.isTranslucent=false
                        self.present(navigationController, animated: true, completion: nil)
                        
                    }
                    else{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else
                {
                    self.dismiss(animated: true, completion: {});
                }
                
            }
            
            
        }
    }
    
    
    @nonobjc public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(txtFieldInValid != nil){
            if(string.count >= 1){
                self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kdeviceIdTag))?.backgroundColor = UIColor.init(red: 155/255, green: 183/255, blue: 205/255, alpha: 1.0)
                self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kdeviceNameTag))?.backgroundColor = UIColor.init(red: 155/255, green: 183/255, blue: 205/255, alpha: 1.0)
            }
            
        }
        
        if textField == textFieldDeviceName && string == " " {
            return false
        }
        
        return true
    }
    
    
}
