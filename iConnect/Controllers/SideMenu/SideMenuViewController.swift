//
//  SideMenuViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 06/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import MFSideMenu

@objc class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var cellDataArray:Array = [String]()
    var tipCount = 0
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        cellDataArray = [NSLocalizedString("MY PROFILE", comment: ""), NSLocalizedString("MANAGE ACCOUNT", comment: ""), NSLocalizedString("MY PREFERENCES", comment: ""), NSLocalizedString("MY MAILBOX", comment: ""), NSLocalizedString("LOGOUT", comment: "")]
        
        NotificationCenter.default.addObserver(self, selector: #selector(getTipCount(notification:)), name: NSNotification.Name("toggleLeftMenu"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @objc func getTipCount(notification: Notification) -> Void {
        
        if notification.object == nil {
            
            CMailBoxRequest.sharedInstance().getCurrentActivePromotionsResponse { (response, isSuccessful, error) in
                if(isSuccessful){
                    self.tipCount = (response as! [CUserInboxItem]).count
                    
                    let indexPath = NSIndexPath(row: 3, section: 0)
                    self.tableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
                    
                }
            }
        }
    }
    
    @IBAction func sosbuttonTapped(_ sender: Any) {
        let notificationName = Notification.Name("toggleLeftMenu")
        NotificationCenter.default.post(name: notificationName, object: "isFromSideMenu")
        
        if let centreController = self.menuContainerViewController.centerViewController as? CentreViewController {
            centreController.performSegue(withIdentifier: kCentreToSOSSegue, sender: nil)
            
        }
        
    }
    
    // MARK: TableView
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return cellDataArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let identifier = "SideMenuCell"
        
        let cell: SideMenuCell! = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SideMenuCell
        
        if indexPath.row == 3 {
            cell.cellTitleLabel.text = self.cellDataArray[indexPath.row] + "  ( \(self.tipCount) )  "
            //            self.getTipCount(notification: nil)
            
        }
        else {
            cell.cellTitleLabel.text = cellDataArray[indexPath.row]
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let centreController = self.menuContainerViewController.centerViewController as? CentreViewController {
            switch indexPath.row {
            case 0:
                centreController.performSegue(withIdentifier: kCentreToMyProfileSegue, sender: nil)
                AppUtility.sharedUtility.sendGAEvent(withEventName: "MyProfileTapped", category: kEventCategoryNavigation, andScreenName: "Left Panel", eventLabel: nil, eventValue: nil)
            case 1:
                centreController.performSegue(withIdentifier: kCentreToDeviceManagementSegue, sender: nil)
                AppUtility.sharedUtility.sendGAEvent(withEventName: "DeviceManagementTapped", category: kEventCategoryNavigation, andScreenName: "Left Panel", eventLabel: nil, eventValue: nil)
            case 2:
                centreController.performSegue(withIdentifier: kCentreToManagePreferenceSegue, sender: nil)
                AppUtility.sharedUtility.sendGAEvent(withEventName: "ManagePreferencesTapped", category: kEventCategoryNavigation, andScreenName: "Left Panel", eventLabel: nil, eventValue: nil)
            case 3:
                centreController.performSegue(withIdentifier: kCentreToMailboxSegue, sender: nil)
                AppUtility.sharedUtility.sendGAEvent(withEventName: "MailboxTapped", category: kEventCategoryNavigation, andScreenName: "Left Panel", eventLabel: nil, eventValue: nil)
            case 4:
                AppUtility.sharedUtility.sendGAEvent(withEventName: "LogoutTapped", category: kEventCategoryNavigation, andScreenName: "Left Panel", eventLabel: nil, eventValue: nil)
                
                let logoutAlert:UIAlertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout of the app ?", preferredStyle: .alert)
                
                let okAction:UIAlertAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                    CUserRequest.sharedInstance().logoutUser()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "loginNavController")
                    self.present(controller, animated: true, completion: nil)
                    CommonData.sharedData.clearDataForLogout()
                    
                    //reset tabbar to dashboard screen
                    centreController.selectedIndex = 0;
                    
                    if let navController = centreController.viewControllers?[0] as? UINavigationController{
                        if let viewController = navController.viewControllers.first as? AppDashboardViewController {
                            if viewController.isViewLoaded {
                                viewController.vehicleCurrentState = nil
                                viewController.driveScore = nil
                                viewController.dashboardTableView.reloadData()
                                viewController.timerVehicleStateRefresh?.invalidate()
                            }
                        }
                    }
                    
                    //reset filter on logout
                    if let navController = centreController.viewControllers?[1] as? UINavigationController{
                        if let viewController = navController.viewControllers.first as? TripsViewController {
                            if viewController.isViewLoaded {
                                viewController.hideFilterView()
                                
                                viewController.filterButton.isSelected = false
                                viewController.filterTitleLabel.text = ""
                                
                                viewController.clearButton.isHidden = true
                                
                                viewController.arrayTrips.removeAll()
                                viewController.tableView.reloadData()
                                viewController.resetFilterDates()
                                //                                viewController.clearTapped(nil)
                            }
                        }
                    }

                    //reset EmergencyVC data
                    if let navController = centreController.viewControllers?[3] as? UINavigationController{
                        if let viewController = navController.viewControllers.first as? EmergencyViewController {
                            if viewController.isViewLoaded {
                                viewController.clearData()
                                viewController.contactsArray.removeAllObjects()
                            }
                        }
                    }
                })
                
                let cancelAction:UIAlertAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) in
                    // nothing to do here
                })
                
                logoutAlert.addAction(okAction)
                logoutAlert.addAction(cancelAction)
                
                self.present(logoutAlert, animated: true, completion: nil)
                
            default: break
                // nothing to do here
            }
            
            let notificationName = Notification.Name("toggleLeftMenu")
            NotificationCenter.default.post(name: notificationName, object: "isFromSideMenu")
            
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
