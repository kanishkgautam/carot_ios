//
//  ImageViewerView.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 27/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import FTIndicator




class ImageViewerView: UIView {
    
    @IBOutlet weak var topHeadingLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var documentImageView: UIImageView!
    
    @IBOutlet weak var indicatorView:UIView!
    
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var docDetails:CDocumentTypeDetails?
    var flagAddDoc:Bool = false
    var selectedDateString:String?
    
    var docName:String?
    var docData:NSData?
    var docExpiryDate:String?
    var docType:String?
    var deegateObjectRefrence:UIViewController?
    
    var callbackForAddedDocument:EventPerformedCallback?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //indicatorView
        // indicatorView.showProgressWithmessage("")
        //self.showIndicatorWithProgress(message: "")
        
    }
    
    func updateView() {
        self.deleteButton.setTitle("UPDATE", for: .normal)

        if flagAddDoc == true {
            addButton.isHidden = true
            getImage(docDetailObj: docDetails!) { (imageObj) in
                self.documentImageView.image = imageObj
            }
            //datePicker.date = Date(timeIntervalSince1970: TimeInterval((docDetails?.expiryDate)!)!)//AppUtility.sharedUtility.convertDateStringToDate(dateStr: (docDetails?.expiryDate)!)
            topHeadingLabel.text = docDetails?.docType
        }
        else {
            self.documentImageView.image = #imageLiteral(resourceName: "noImageBig")

            addButton.isHidden = false
            topHeadingLabel.text = docName
        }
        
        //datePicker.cur
        
        
//        let dateSelect:Date = Date()
//        let newDateSelect = UInt64(dateSelect.timeIntervalSince1970*1000)
//        selectedDateString = "\(newDateSelect)"
        

        let tomorrow = Date().addingTimeInterval(24 * 60 * 60)
        datePicker.minimumDate = tomorrow
        
        if docDetails?.expiryDate == nil {
            datePicker.date = tomorrow
        }
        else {
            if AppUtility.sharedUtility.isDocumentExpired(documentExpiry: docDetails?.expiryDate) {
                datePicker.date = tomorrow

            }
            else {
                datePicker.date = Date(timeIntervalSince1970: TimeInterval((docDetails?.expiryDate)!)!)

            }
        }
        selectedDateString = "\(datePicker.date.timeIntervalSince1970)"
    }
    
    
    @IBAction func crossButtonTapped(_ sender: Any?) {
        self.removeFromSuperview()
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        
        //if selectedDateString == nil {
        let validTill = "\(NSNumber(value:datePicker.date.timeIntervalSince1970 * 1000))"
        selectedDateString = validTill.components(separatedBy: ".").first
        
        let validFromDate = (TimeInterval((docDetails?.validFrom!)!)! * 1000) as NSNumber//AppUtility.sharedUtility.convertDateStringToDate(dateStr: (docDetails?.validFrom)!).timeIntervalSince1970 * 1000 as NSNumber
        
        //TODO: Update document here
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        if deleteButton.currentTitle == "UPDATE" {
            indicatorView.isHidden = false
            spinner.startAnimating()
            message.text = ""
            
            CDocumentWalletRequest.sharedInstance().updateDataforVehicle(withId: Int32(vehicle.vehicleId), docId: "\(docDetails?.docId ?? 0)", fileName: docDetails?.imageName ?? "", validFrom: "\(validFromDate.intValue)", validTill: selectedDateString, docType: docDetails?.docType, responseData: { (responseData, isSuccess, error) in
                
                if isSuccess {
                    self.spinner.stopAnimating()
                    self.message.text = "Record updated sucessfully"
                    let dispatchTime = DispatchTime.now() //+ .seconds(2)
                    self.clearData()
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                        self.dismissIndicatorView()
                        //self.addedDocument()
                        NotificationCenter.default.post(name: Notification.Name("DocumentUpdateNotification"), object: self.docDetails?.docType!)
                    }
                    
                    
                }
                else {
                    self.spinner.stopAnimating()
                    let dispatchTime = DispatchTime.now() + .seconds(2)
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                        self.dismissIndicatorView()
//                        self.addedDocument()
                        NotificationCenter.default.post(name: Notification.Name("DocumentUpdateNotification"), object: self.docDetails?.docType!)

                        
                    }
                    
                    if responseData != nil {
                        self.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseData as! NSDictionary))
                    }else{
                        self.showIndicatorWithError(error?.localizedDescription)
                    }
                }
                
            })
        }
        
    }
    
    func clearData() -> Void {
        self.documentImageView.image = #imageLiteral(resourceName: "noImageBig")
    }
    
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        let button = UIButton(type: .system)
        docType = docDetails?.docType
        addButtonTapped(button)
    }
    
    
    @IBAction func addButtonTapped(_ sender: Any) {
        
        indicatorView.isHidden = false
        spinner.startAnimating()
        message.text = ""
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        var data = documentImageView.image!.jpegData(compressionQuality: 0.5)
        
        if (data?.count)! > 300000 {
            
            let currentRatio: CGFloat = (documentImageView.image?.size.width)! / (documentImageView.image?.size.height)!
            let width: CGFloat = 700.0
            let rect = CGRect(x: 0, y: 0, width: width, height: width/currentRatio)
            UIGraphicsBeginImageContext(rect.size)
            
            documentImageView.image!.draw(in: rect)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            if (image!.jpegData(compressionQuality: 1)?.count)! > 102400 {
                data = image!.jpegData(compressionQuality: 0.7)
                
            } else {
                data = image!.jpegData(compressionQuality: 1)
            }
            
            UIGraphicsEndImageContext()
        }
        
        let validTill = "\(NSNumber(value:datePicker.date.timeIntervalSince1970 * 1000))"
        selectedDateString = validTill.components(separatedBy: ".").first

//        selectedDateString = "\(NSNumber(value:datePicker.date.timeIntervalSince1970 * 1000).intValue)"
        CDocumentWalletRequest.sharedInstance().uploadDataforVehicle(withId: Int32(vehicle.vehicleId), file: data as Data?, fileName: docName, validFrom: "", validTill: selectedDateString, docType: docType) { (response, isSuccessful, error) in
            
            if isSuccessful == true {
                print(response ?? CDocumentTypeDetails())
                
                self.spinner.stopAnimating()
                self.message.text = "Record added sucessfully"
                let dispatchTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                    self.dismissIndicatorView()
                    self.addedDocument()
                    
                }
            }else{
                self.spinner.stopAnimating()
                let dispatchTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                    self.dismissIndicatorView()
                    self.addedDocument()
                    
                }
                
                if response != nil {
                    self.message.text = AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary)

                    //self.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                }else{
                    //self.showIndicatorWithError(error?.localizedDescription)
                    self.message.text = error?.localizedDescription

                }
            }
//            if(isSuccessful && response != nil){
//                
//            }
//            else {
//                self.message.text = NSLocalizedString("operation.error", comment: "")
//            }
        }
    }
    
    func callbackForAddedDocTapped(with callback:@escaping EventPerformedCallback){
        self.callbackForAddedDocument = callback
    }
    
    func addedDocument() {
        if callbackForAddedDocument != nil {
            callbackForAddedDocument!()
        }
        
    }
    
    func dismissIndicatorView()
    {
        indicatorView.isHidden = true
        self.removeFromSuperview()
        
    }
    
    @IBAction func dateChange(_ sender: Any) {
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: (sender as AnyObject).date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            print("\(day) \(month) \(year)")
        }
        
        /* let formatter = DateFormatter()
         formatter.calendar = datePicker.calendar
         //formatter.dateStyle = .MediumStyle
         //formatter.timeStyle = .NoStyle
         let dateString = formatter.string(from: datePicker.date)
         print(dateString)*/
        
        
        let dateSelect:Date = Calendar.current.date(from: componenets)!
        
        let newDateSelect = UInt64(AppUtility.sharedUtility.getStartOfDay(startDateObj: dateSelect).timeIntervalSince1970*1000)
        
        if newDateSelect <=  UInt64(AppUtility.sharedUtility.getStartOfDay(startDateObj: Date()).timeIntervalSince1970*1000) {
            
            self.showIndicatorWithError("Expiry date should be greater than today")
            return
        }
        
        // let roundedDate = AppUtility.sharedUtility.roundTo(places: 0, value: newDateSelect)
        //let distanceFloat: Float = newDateSelect.floatValue
        
        selectedDateString = "\(newDateSelect)"
    }
    
    
    func deleteDocument() {
        // self.showIndicatorWithProgress(message: "")
        indicatorView.isHidden = false
        spinner.startAnimating()
        message.text = ""
        
        // let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().deleteDocument(forDocumentId: (docDetails?.docId)!, fileName: docDetails?.imageName) { (response, isSuccessful, error) in
            
            if(isSuccessful && response != nil){
                // self.dismissProgress()
                // self.showIndicatorWithInfo("Document successfully deleted.")
                self.spinner.stopAnimating()
                self.message.text = NSLocalizedString("Document successfully deleted.", comment: "")
                let dispatchTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                    self.dismissIndicatorView()
                }
            }
            else {
                //self.dismissProgress()
                //self.showIndicatorWithError(NSLocalizedString("operation.error", comment: ""))
                self.spinner.stopAnimating()
                self.message.text = NSLocalizedString("operation.error", comment: "")
                let dispatchTime = DispatchTime.now() + .seconds(2)
                DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                    self.dismissIndicatorView()
                }
            }
        }
    }
    
    //MARK: Get image
    func downloadImageFromServer(docDetailsObj:CDocumentTypeDetails, onCompletion: @escaping (_ imageName: NSString) -> Void){
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().downloadDocumentImage(forVehicleId: Int32(vehicle.vehicleId), docType: docDetailsObj.docType, fileName: docDetailsObj.imageName) { (response, isSuccessful, error) in
            if(isSuccessful && response != nil){
                let imgName:NSString = response as! NSString
                onCompletion(imgName)
            }
        }
    }
    
    
    func getImage(docDetailObj:CDocumentTypeDetails, completion: @escaping (_ image: UIImage) -> Void)
    {
        
        let cacheDirectoryPath = FileManager.SearchPathDirectory.cachesDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(cacheDirectoryPath, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(docDetailObj.imageName)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if (image != nil) {
                completion(image!)
            }
            else{
                //download from SDK
                downloadImageFromServer(docDetailsObj: docDetailObj, onCompletion: { (imageName) in
                    
                    let nsCacheDirectory = FileManager.SearchPathDirectory.cachesDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsCacheDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(docDetailObj.imageName)
                        let image    = UIImage(contentsOfFile: imageURL.path)
                        if (image != nil) {
                            completion(image!)
                        }
                    }
                    
                })
            }
            
        }
    }
    
}
