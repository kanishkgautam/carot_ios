//
//  DTCDetailCell.swift
//  iConnect
//
//  Created by Vivek Joshi on 26/04/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DTCDetailCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
