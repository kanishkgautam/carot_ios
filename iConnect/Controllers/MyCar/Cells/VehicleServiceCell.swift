//
//  VehicleServiceCell.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 03/04/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import FTIndicator

//typealias EventPerformedCallback = () -> Void

class ServiceHistoryCell: UITableViewCell {
    
    @IBOutlet weak var vehicleNameCell: UILabel!
    @IBOutlet weak var odometerCell: UILabel!
    @IBOutlet weak var serviceDueCell: UILabel!
    
    @IBOutlet weak var checkmarkButton: UIButton!
    
}

class VehicleServiceCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var vehicleServiceTableCell: UITableView!
    
    @IBOutlet weak var odometerReadingTextField: UITextField!
    @IBOutlet weak var dueDateButton:UIButton!
    @IBOutlet weak var createUpdateDeleteButton:UIButton!
    
    @IBOutlet weak var buttonService: UIButton!
    @IBOutlet weak var buttonDelete:UIButton!
    
    var arrayServiceHistory = [CServiceHistory]()
    var selectedCheckMarkIndex:Int = 0
    var selectedServiceObj:CServiceHistory = CServiceHistory()
    let cellIdentifier = "ServiceHistoryCell"
    var arrayIndexSelection:Array = [NSNumber]()
    var selectedDueServiceDate:Date?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var callbackForServiceUpdates:EventPerformedCallback?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let notificationIdentifier = Notification.Name("UpdateDueDateOnService")
        NotificationCenter.default.addObserver(self, selector: #selector(setDueDate), name: notificationIdentifier, object: nil)
        
        
        
    }
    @IBAction func deleteCellTapped(_ sender: Any) {
        
        if arrayIndexSelection.count == 0 {
            return
        }
        
        for serviceObj in arrayServiceHistory {
            if serviceObj.documentID == "\(arrayIndexSelection.first!)" {
                
                deleteService(serviceObj: serviceObj)
                break
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.arrayIndexSelection.removeAll()
        
        odometerReadingTextField.text = ""
        dueDateButton.setTitle("Select due date", for: .normal)
        createUpdateDeleteButton.setTitle("CREATE", for: .normal)
        
        if(dueDateButton.titleLabel?.text == "Service Due Date") {
            dueDateButton.setTitleColor(UIColor.darkGray, for: .normal)
        }
        //        for _ in arrayServiceHistory{
        //            arrayIndexSelection.append("0")
        //        }
        
        buttonDelete.isHidden = true
        
        vehicleServiceTableCell.reloadData()
        
        if !selected {
            self.selectedDueServiceDate = nil
        }
    }
    
    @IBAction func createServiceButtonTapped(_ sender: Any) {
        self.endEditing(true)
        
        let button:UIButton = sender as! UIButton
        if(button.titleLabel?.text == "CREATE"){
            addService()
        }
        else if(button.titleLabel?.text == "UPDATE"){
            editService(serviceObj: selectedServiceObj)
        }
    }
    
    @IBAction func serviceDueButtonTapped(_ sender: Any) {
        self.endEditing(true)
        
    }
    
    func validateOdometerReading(text:String) -> Bool
    {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = text.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return text == numberFiltered
    }
    
    
    @IBAction func checkMarkTappedToDeleteService(_ sender: UIButton) {
        
        //        for (index, _) in arrayIndexSelection.enumerated() {
        //            arrayIndexSelection.insert("0", at: index)
        //        }
        self.clearData()
        
        if(dueDateButton.titleLabel?.text == "Service Due Date") {
            dueDateButton.setTitleColor(UIColor.darkGray, for: .normal)
        }
        
        let tag = sender.tag as NSNumber
        
        if self.arrayIndexSelection.contains(tag) {
            self.arrayIndexSelection.removeAll()
            
            selectedCheckMarkIndex = -1
            
        }
        else {
            self.arrayIndexSelection.removeAll()
            self.arrayIndexSelection.append(tag)
            
            selectedCheckMarkIndex = Int(tag)
            
        }
        
        if(arrayIndexSelection.count > 0){
            buttonDelete.isHidden = false
        }
        else {
            buttonDelete.isHidden = true
            
        }
        //        if(arrayIndexSelection[button.tag] == "0"){
        //            button.setImage(#imageLiteral(resourceName: "checkbox_filled"), for: .normal)
        //            selectedCheckMarkIndex = button.tag
        //            arrayIndexSelection.insert("1", at: button.tag)
        //        }
        //        else {
        //            button.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
        //            selectedCheckMarkIndex = 0
        //            arrayIndexSelection.insert("0", at: button.tag)
        //        }
        //
        //        if(arrayIndexSelection.contains("1")){
        //            buttonDelete.isHidden = false
        //        }
        //        else {
        //            buttonDelete.isHidden = true
        //
        //        }
        vehicleServiceTableCell.reloadData()
        
    }
    
    //MARK:- Service ADD/UPDATE/DELETE
    func addService() {
        
        if selectedDueServiceDate == nil{
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Please select Due date.")
            return
        }
        
        if AppUtility.sharedUtility.isDocumentExpired(documentExpiry: "\(selectedDueServiceDate!.timeIntervalSince1970)")  {
            
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Due date must be greater than today")
            
            return
        }
        
        if self.odometerReadingTextField.text! == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Please enter Odometer reading")
            return
        }
        
        let flag = validateOdometerReading(text: self.odometerReadingTextField.text!)
        if(flag == false) {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Odometer reading not valid!")
            return
        }
        
        self.showIndicatorWithProgress(message: "")
        
        let dateDue = NSNumber(value: (selectedDueServiceDate?.timeIntervalSince1970)!)
        
        let str = "\(dateDue.intValue*1000)"
        
        let odometerReading = (self.odometerReadingTextField.text?.count)! > 0 ? self.odometerReadingTextField.text : "0"
        
        self.createUpdateDeleteButton.isUserInteractionEnabled = false
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        CommonData.sharedData.getMakerAndModel(with: Int(vehicle.vehicleModelId), mappingCallback: { (makeObj, modelObj) in
            
            CDocumentWalletRequest.sharedInstance().uploadServiceInfo(forVehicleId: Int32(vehicle.vehicleId),
                                                                      docType: kDocTypeService,
                                                                      model: modelObj?.modelName, odometerReading: odometerReading ,
                                                                      dueOn: str,
                                                                      edit: Int32(ServiceAdd.rawValue),
                                                                      documentID: "",
                                                                      carName: modelObj?.modelName) { (response, isSuccessful, error) in
                                                                        self.createUpdateDeleteButton.isUserInteractionEnabled = true
                                                                        if(isSuccessful && response != nil){
                                                                            self.selectedDueServiceDate = nil
                                                                            self.updateServiceHistory()
                                                                            //FTIndicator.dismissProgress()
                                                                            // FTIndicator.showInfo(withMessage: "Service successfully added.")
                                                                        }
                                                                        else {
                                                                            FTIndicator.dismissProgress()
                                                                            
                                                                            if response != nil {
                                                                                self.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                                                                            }
                                                                            else {
                                                                                self.showIndicatorWithError(error?.localizedDescription)
                                                                            }
                                                                            
                                                                        }
                                                                        
            }
            
        })
    }
    
    func editService(serviceObj:CServiceHistory) {
        
        let dueDateStr = "\(selectedDueServiceDate!.timeIntervalSince1970)"
        
        if (selectedDueServiceDate?.timeIntervalSince1970)! < 0 || AppUtility.sharedUtility.isDocumentExpired(documentExpiry: dueDateStr)  {
            
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Due date must be greater than today")
            
            return
        }
        
        let dateDue = NSNumber(value: (selectedDueServiceDate?.timeIntervalSince1970)! )
        
        let str = "\(dateDue.intValue*1000)"
        
        FTIndicator.setIndicatorStyle(.dark)
        FTIndicator.showProgress(withMessage: "")
        self.createUpdateDeleteButton.isUserInteractionEnabled = false
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        CommonData.sharedData.getMakerAndModel(with: Int(vehicle.vehicleModelId), mappingCallback: { (makeObj, modelObj) in
            
            CDocumentWalletRequest.sharedInstance().uploadServiceInfo(forVehicleId: Int32(vehicle.vehicleId), docType: kDocTypeService, model: modelObj?.modelName, odometerReading: self.odometerReadingTextField.text, dueOn: str, edit: Int32(ServiceEdit.rawValue), documentID: serviceObj.documentID, carName: modelObj?.modelName) { (response, isSuccessful, error) in
                self.createUpdateDeleteButton.isUserInteractionEnabled = true
                
                if(isSuccessful && response != nil){
                    self.arrayIndexSelection.removeAll()
                    
                    self.updateServiceHistory()
                    self.clearData()
                    
                    //                    FTIndicator.dismissProgress()
                    //                    FTIndicator.showInfo(withMessage: "Service successfully updated.")
                }
                else {
                    
                    if response != nil {
                        self.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                    }
                    else {
                        self.showIndicatorWithError(error?.localizedDescription)
                    }
                    
                    //                    FTIndicator.dismissProgress()
                    //                    FTIndicator.showError(withMessage: NSLocalizedString("operation.error", comment: ""))
                }
                
            }
            
        })
        
        
        
        
    }
    
    func deleteService(serviceObj:CServiceHistory){
        
        FTIndicator.setIndicatorStyle(.dark)
        FTIndicator.showProgress(withMessage: "")
        
        self.buttonDelete.isUserInteractionEnabled = false
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        CommonData.sharedData.getMakerAndModel(with: Int(vehicle.vehicleModelId), mappingCallback: { (makeObj, modelObj) in
            
            CDocumentWalletRequest.sharedInstance().uploadServiceInfo(forVehicleId: Int32(vehicle.vehicleId), docType: kDocTypeService, model: modelObj?.modelName, odometerReading: serviceObj.odometer, dueOn: serviceObj.dueDate, edit: Int32(ServiceDelete.rawValue), documentID: serviceObj.documentID, carName: modelObj?.modelName) { (response, isSuccessful, error) in
                if(isSuccessful && response != nil){
                    self.buttonDelete.isUserInteractionEnabled = true
                    
                    self.arrayIndexSelection.removeAll()
                    self.buttonDelete.isHidden = true
                    
                    self.updateServiceHistory()
                    self.clearData()
                    
                }
                else {
                    self.dismissProgress()
                    self.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                }
            }
        })
    }
    
    @objc func setDueDate(notification: Notification) {
        
        let date:Date = (notification.object as? Date)!
        selectedDueServiceDate = date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-YYYY"
        let dateString = formatter.string(from: date as Date)
        
        dueDateButton.setTitle(dateString, for: .normal)
        dueDateButton.setTitleColor(UIColor.black, for: .normal)
        
    }
    
    func updateServiceHistory() {
        //self.view.showIndicatorWithProgress(message: "")
        self.createUpdateDeleteButton.isUserInteractionEnabled = false
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().getPastServicesForVehicle(withId: Int32(vehicle.vehicleId)) { (response, isSuccessful, error) in
            self.createUpdateDeleteButton.isUserInteractionEnabled = true
            
            if(isSuccessful && response != nil){
                self.arrayServiceHistory = response as! [CServiceHistory]
                self.arrayIndexSelection.removeAll()
                
                self.vehicleServiceTableCell.reloadData()
                FTIndicator.dismissProgress()
                FTIndicator.showInfo(withMessage: "Success")
                self.clearData()
            }
            else
            {
                FTIndicator.dismissProgress()
                
                //self.view.showIndicatorWithError(error?.localizedDescription)
            }
            
        }
    }
    
    func callbackForServiceTapped(with callback:@escaping EventPerformedCallback){
        self.callbackForServiceUpdates = callback
    }
    
    //MARK: - Upload Document
    func updateServiceData() {
        if callbackForServiceUpdates != nil {
            callbackForServiceUpdates!()
        }
        
    }
    
    //MARK: - service cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayServiceHistory.count
    }
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier) as? ServiceHistoryCell
        if cell == nil {
            
            cell = ServiceHistoryCell()
        }
        let serviceDetails:CServiceHistory = self.arrayServiceHistory[indexPath.row]
        let dueDate = Date(timeIntervalSince1970: TimeInterval(serviceDetails.dueDate)!)
        
        cell?.serviceDueCell.text = "Service due date : " + AppUtility.sharedUtility.stringFromDate(date: dueDate)
        cell?.vehicleNameCell.text = serviceDetails.vehicle?.name ?? ""
        cell?.odometerCell.text = "Odometer : " + serviceDetails.odometer
        cell?.checkmarkButton.tag = Int(serviceDetails.documentID)!
        
        if(arrayIndexSelection.contains((cell?.checkmarkButton.tag)! as NSNumber)) {
            cell?.checkmarkButton.setImage(#imageLiteral(resourceName: "checkbox_filled"), for: .normal)
        }
        else {
            cell?.checkmarkButton.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let serviceDetails:CServiceHistory = self.arrayServiceHistory[indexPath.row]
        
        
        let dueDate = Date(timeIntervalSince1970: TimeInterval(serviceDetails.dueDate)!)
        selectedDueServiceDate = dueDate//AppUtility.sharedUtility.stringFromDate(date: dueDate)
        
        odometerReadingTextField.text = serviceDetails.odometer
        dueDateButton.setTitle(AppUtility.sharedUtility.stringFromDate(date:dueDate), for: .normal)
        dueDateButton.setTitleColor(UIColor.black, for: .normal)
        createUpdateDeleteButton.setTitle("UPDATE", for: .normal)
        selectedServiceObj = serviceDetails
    }
    
    func clearData() -> Void {
        odometerReadingTextField.text?.removeAll()
        dueDateButton.setTitle("Select due date", for: .normal)
        dueDateButton.setTitleColor(UIColor.darkGray, for: .normal)
        createUpdateDeleteButton.setTitle("CREATE", for: .normal)
        buttonDelete.isHidden = true
    }
}
