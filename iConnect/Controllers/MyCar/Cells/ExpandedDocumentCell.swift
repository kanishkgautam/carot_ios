//
//  ExpandedDocumentCell.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 27/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

import FTIndicator



class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionCellImageView: UIImageView!
    @IBOutlet weak var collectionCellLabel: UILabel!
    
    
    
    
    func setDataOnCellectionCell(){
        // self.collectionCellImageView.image = #imageLiteral(resourceName: "NoDocIcon")
        collectionCellImageView.layer.cornerRadius = 3.0
        collectionCellImageView.layer.borderWidth = 1.0
        collectionCellImageView.layer.borderColor = UIColor.lightGray.cgColor
    }
}


class ExpandedDocumentCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var documentsCollectionView: UICollectionView!
    @IBOutlet weak var buttonEditDocument:UIButton!
    
    var callbackForAddDocument:EventPerformedCallback?
    var callbackForUpdateDocument:EventPerformedCallback?
    
    @IBOutlet weak var documentTitle: UILabel!
    let imagesCellIdentifier:String = "ImagesCellIdentifier"
    var arrayDocDetails = [CDocumentTypeDetails]()
    var arrayServiceHistory = [CServiceHistory]()
    var currentSelectedItem:Int = 0
    var selectedDocument:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.documentsCollectionView.register(ImageCollectionCell.self, forCellWithReuseIdentifier: "ImageCollectionCell")
        self.buttonEditDocument.setImage(#imageLiteral(resourceName: "editOrangeIcon"), for: .normal)
        self.selectedImageView.layer.cornerRadius = 3.0
        
        self.buttonEditDocument.imageView?.tintColor = UIColor.white
        self.buttonEditDocument.imageView?.backgroundColor = UIColor.white
        
        self.selectedImageView.image = #imageLiteral(resourceName: "noImageBig")
        
        //        self.selectedImageView.contentMode = .scaleAspectFit
        //self.buttonEditDocument.layer.borderColor = UIColor.black.cgColor
        // self.buttonEditDocument.layer.borderWidth = 1.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if(selected == true){
            currentSelectedItem = 0
            return
        }
        documentsCollectionView.reloadData()
        self.selectedImageView.image = #imageLiteral(resourceName: "noImageBig")

        if(arrayDocDetails.count > 0){
            let docTypeDetail:CDocumentTypeDetails = arrayDocDetails[currentSelectedItem]
            
            if docTypeDetail.docType == "DL" {
                documentTitle.text = "DRIVING LICENSE"
                
            }
            else {
                documentTitle.text = docTypeDetail.docType
                
            }
            
            getImage(docDetailObj: docTypeDetail) { (imageObj) in
                self.selectedImageView.image = imageObj
            }
            buttonEditDocument.isHidden = false
            
        }
        else {
            if selectedDocument == "DL" {
                documentTitle.text = "DRIVING LICENSE"
                
            }
            else {
                documentTitle.text = selectedDocument
                
            }
            buttonEditDocument.isHidden = true
        }
        // Configure the view for the selected state
    }
    
    @IBAction func editDocument(_ sender: Any) {
        let userDoc:CDocumentTypeDetails = arrayDocDetails[currentSelectedItem]
        
        let alert: UIAlertController = UIAlertController(title: "", message: "Choose an option", preferredStyle: .actionSheet)
        let editAction:UIAlertAction = UIAlertAction(title: "Edit", style: .default) { (action) in
            
            self.showImageViewer()
        }
        
        let deleteAction:UIAlertAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive) { (action) in
            
            // self.showIndicatorWithProgress(message: "Deleting document")
            CDocumentWalletRequest.sharedInstance().deleteDocument(forDocumentId: (userDoc.docId), fileName: userDoc.imageName) { (response, isSuccessful, error) in
                
                self.dismissProgress()
                
                if(isSuccessful && response != nil){
                    
                    //                    if self.arrayDocDetails.count == 1 {
                    //                        self.currentSelectedItem = 0
                    //                    }
                    
                    if self.currentSelectedItem > 0 {
                        self.currentSelectedItem -= 1
                    }
                    self.selectedImageView.image = #imageLiteral(resourceName: "noImageBig")
                    //                    else if self.arrayDocDetails.count == 1 {
                    //                        self.currentSelectedItem -= 1
                    //                    }
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "DocumentDeletedNotification"), object: userDoc.docType!)
                }
                else {
                    self.showIndicatorWithError("Unable to delete document")
                    
                }
            }
            
        }
        
        let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(editAction)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        //        self.present(alert, animated: true, completion: nil)
        
        UIApplication.shared.windows.last?.rootViewController?.present(alert, animated: true, completion: {
            
        })
        
    }
    
    func showImageViewer() -> Void {
        let docTypeDetail:CDocumentTypeDetails = arrayDocDetails[currentSelectedItem]
        
        
        let imageViewer:ImageViewerView = Bundle.main.loadNibNamed("ImageViewerView", owner: nil, options: nil)?.first as! ImageViewerView
        imageViewer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        //        imageViewer.documentImageView.contentMode = .scaleAspectFit
        imageViewer.docDetails = docTypeDetail
        imageViewer.flagAddDoc = true
        imageViewer.callbackForAddedDocTapped(with: {
            
            self.updateDocument()
            self.addDocument()
        })
        
        imageViewer.updateView()
        // imageViewer.docName = imageUrl.lastPathComponent
        //  imageViewer.docType = docDetails.docType
        //imageViewer.docExpiryDate =
        UIApplication.shared.windows.last?.addSubview(imageViewer)
        
        //        if let centreController = .centerViewController as? CentreViewController {
        //            centreController.presen
        //        }
        
    }
    
    func downloadImageFromServer(docDetailsObj:CDocumentTypeDetails, onCompletion: @escaping (_ imageName: NSString) -> Void){
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().downloadDocumentImage(forVehicleId: Int32(vehicle.vehicleId), docType: docDetailsObj.docType, fileName: docDetailsObj.imageName) { (response, isSuccessful, error) in
            if(isSuccessful && response != nil){
                let imgName:NSString = response as! NSString
                onCompletion(imgName)
            }
        }
    }
    
    
    func getImage(docDetailObj:CDocumentTypeDetails, completion: @escaping (_ image: UIImage) -> Void)
    {
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.cachesDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(docDetailObj.imageName)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if (image != nil) {
                completion(image!)
            }
            else{
                //download from SDK
                downloadImageFromServer(docDetailsObj: docDetailObj, onCompletion: { (imageName) in
                    
                    let nsCacheDirectory = FileManager.SearchPathDirectory.cachesDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsCacheDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(docDetailObj.imageName)
                        let image    = UIImage(contentsOfFile: imageURL.path)
                        if (image != nil) {
                            DispatchQueue.main.async {
                                completion(image!)
                                
                                // cell.collectionCellImageView.image = imageObj
                                //self.documentsCollectionView.reloadData()
                            }
                            
                            //                            completion(image!)
                        }
                    }
                    
                })
            }
            
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(arrayDocDetails.count > 0){
            return arrayDocDetails.count+1 }
        else {
            return 1
            
        }
        
        /*if(arrayDocDetails.count > 0){
         return arrayDocDetails.count+1}
         else if(arrayServiceHistory.count > 0){
         return arrayServiceHistory.count
         }
         return 0*/
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imagesCellIdentifier, for: indexPath) as! ImageCollectionCell
        cell.setDataOnCellectionCell()
        
        if(arrayDocDetails.count > 0){
            if(indexPath.row == arrayDocDetails.count){
                cell.collectionCellImageView.image = #imageLiteral(resourceName: "addimage")
                cell.collectionCellLabel.text = "Add New"
                
            }
            else{
                let docTypeDetail:CDocumentTypeDetails = arrayDocDetails[indexPath.item]
                cell.collectionCellImageView.image = #imageLiteral(resourceName: "NoDocIcon")
                // if(cell.collectionCellImageView.image != #imageLiteral(resourceName: "NoDocIcon")){
                getImage(docDetailObj: docTypeDetail) { (imageObj) in
                    
                    DispatchQueue.main.async {
                        cell.collectionCellImageView.image = imageObj
                    }
                }
                
                let expDate = Date(timeIntervalSince1970: TimeInterval(docTypeDetail.expiryDate)!)
                cell.collectionCellLabel.text = "Expiring\n\(AppUtility.sharedUtility.stringFromDate(date: expDate))"
            }
        }
        else {
            cell.collectionCellImageView.image = #imageLiteral(resourceName: "addimage")
            cell.collectionCellLabel.text = "Add New"
        }
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        if(indexPath.row == arrayDocDetails.count){
            addDocument()
            
        } else {
            let docTypeDetail:CDocumentTypeDetails = arrayDocDetails[indexPath.item]
            currentSelectedItem = indexPath.row
            self.selectedImageView.image = #imageLiteral(resourceName: "noImageBig")

            getImage(docDetailObj: docTypeDetail) { (imageObj) in
                self.selectedImageView.image = imageObj
            }
        }
    }
    
    func callbackForAddButtonTapped(with callback:@escaping EventPerformedCallback){
        self.callbackForAddDocument = callback
    }
    
    func callbackForUpdateButtonTapped(with callback:@escaping EventPerformedCallback){
        self.callbackForUpdateDocument = callback
    }
    
    
    //MARK: - Upload Document
    func addDocument() {
//        if(arrayDocDetails.count > 0){
//            currentSelectedItem = arrayDocDetails.count
//        }
//        self.arrayDocDetails.removeAll()
//        self.documentsCollectionView.reloadData()
        
        //        else {
        //            currentSelectedItem = 0
        //        }
        
        if callbackForAddDocument != nil {
            callbackForAddDocument!()
        }
        
    }
    
    func updateDocument() -> Void {
        
        if callbackForUpdateDocument != nil {
            callbackForUpdateDocument!()
        }
    }
    
}
