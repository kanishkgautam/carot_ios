//
//  DTCEventDetailView.swift
//  iConnect
//
//  Created by Administrator on 3/8/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DTCEventDetailView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var buttonClose:UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var labelTitle:UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    var dtcDetailDictionary:NSDictionary!
    var titleString: String?
    
    let keysArray = ["description", "implications", "simpleChecks", "Electrical Checks", "possibleTroubleParts", "sensorCircuit", "problemSymptoms", "confirmationOfFault", "checkingAndFixingProcedure", "checksPostFixingProblems"]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.tableView.register(UINib(nibName: "DTCDetailCell", bundle: nil), forCellReuseIdentifier: "DTCDetailCell")
        
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "DTCEventDetailView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        baseView?.layer.cornerRadius = 9.0
        //        baseView?.layer.masksToBounds = true
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DTCEventDetailView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
        
        populateData()
        // let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HealthViewId")
        // buttonClose.addTarget(viewController, action: Selector(("closePopUp:")), for: UIControlEvents.touchUpInside)
        
        // self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        // self.getAllVehiclesMakeModelData()
        
    }
    
    func populateData()
    {
        
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        removeFromSuperview()
        
    }
    
    //MARK:- UITableView Delegate Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return keysArray.count
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return UITableViewAutomaticDimension
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "DTCDetailCell") as? DTCDetailCell
        
        if cell == nil {
            cell = DTCDetailCell()
        }
        
        let title = keysArray[indexPath.row]
        let description = dtcDetailDictionary.value(forKey: title) as? String
        
        cell!.titleLabel.text = NSLocalizedString(title, comment: "")
        
        if description == nil || description == "" {
            cell!.descriptionLabel.text = "N/A"
        }
        else {
            cell!.descriptionLabel.text = dtcDetailDictionary.value(forKey: title) as? String

        }
        
        return cell!
    }
}
