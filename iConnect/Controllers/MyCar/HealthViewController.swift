//
//  HealthViewController.swift
//  iConnect
//
//  Created by Administrator on 2/9/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

// TODO: Fix UI for all the screen types
class HealthTitleCell: UITableViewCell {
    
    @IBOutlet weak var labelHealthTitle: UILabel!
    //@IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    
    
}


class CollapsedDocumentCell: UITableViewCell {
    
    @IBOutlet weak var addImageIcon: UIImageView!
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var cellSubtitleLabel: UILabel!
    @IBOutlet weak var warningIcon: UILabel!
    
}


class HealthHistoryCell: UITableViewCell {
    
    @IBOutlet weak var labelLocation: UILabel!
    // @IBOutlet weak var labelHistoryTime: UILabel!
    @IBOutlet weak var labelHistoryDate: UILabel!
    //@IBOutlet weak var constraintlocationLabelHeight: NSLayoutConstraint!
    
    func setCellDataForEvent(event: CEvent) -> Void {
        
        self.labelHistoryDate.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: (event.timeStamp)!)
        if event.onLocation?.geoCode != nil {
            self.labelLocation.text = event.onLocation.geoCode
        }else{
            self.labelLocation.text = "Fetching location..."
            
            if let onLocation = event.onLocation {
                let location = CLLocationCoordinate2D(latitude: onLocation.latitude as! CLLocationDegrees, longitude: onLocation.longitude as! CLLocationDegrees)
                
                CVehicleRequest.sharedInstance().getAddressForLocation(location, response: { (responseObject, isSuccessful, error) in
                    if(error == nil && responseObject != nil)
                    {
                        event.onLocation.geoCode = responseObject as! String?
                        self.labelLocation.text = responseObject as! String?
                        print("ONLOCATION address is " + (responseObject as! String?)!)
                    }
                })
            }
            else {
                self.labelLocation.text = "Location not found"
            }
            
            self.labelLocation.lineBreakMode = NSLineBreakMode.byWordWrapping
            self.labelLocation.numberOfLines = 0
            self.labelLocation.sizeToFit()
        }
        
    }
}

enum CurrentVisbleMyCarScreen{
    case Health
    case Maintenance
    case CarProfile
}


@objcMembers class HealthViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var currentScreen:CurrentVisbleMyCarScreen = .Health
    
    @IBOutlet weak var towViewObj: UIView!
    var vehicleHeader:HeaderPickerArea?
    lazy var presentationDelegate = PickerControllerPresentationManager()
    var selectedVehicleMake: Int = 0
    
    var arrayMake:Array = [String]()
    var devicesArray:[CVehicle] = []
    
    var isImagePickerPresented = false
    
    @IBOutlet weak var buttonCarHealth: UIButton!
    @IBOutlet weak var buttonMaintenance: UIButton!
    @IBOutlet weak var buttonCarProfile: UIButton!
    
    @IBOutlet weak var imageSelectedHealthType: UIImageView!
    @IBOutlet weak var imageSelectedCarHealth: UIImageView!
    @IBOutlet weak var imageSelectedMaintenance: UIImageView!
    @IBOutlet weak var imageSelectedCarProfile: UIImageView!
    
    @IBOutlet weak var buttonBattery: UIButton!
    @IBOutlet weak var buttonTow: UIButton!
    @IBOutlet weak var buttonImpact: UIButton!
    
    @IBOutlet weak var labelHistoryTitle: UILabel!
    
    @IBOutlet weak var labelBattery: UILabel!
    @IBOutlet weak var labelTow: UILabel!
    @IBOutlet weak var labelImpact: UILabel!
    @IBOutlet weak var labelDTCCount: UILabel!
    
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var viewTableBase: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var historyTableView: UITableView!
    
    @IBOutlet weak var carHealthBackgroundView: UIView!
    @IBOutlet weak var maintenanceBackgroundView: UIView!
    @IBOutlet weak var carProfileBackgroundView: UIView!
    
    // my car variables and stuff
    
    var isEditingCarView:Bool = false
    @IBOutlet weak var myCarDeviceIDField: UITextField!
    @IBOutlet weak var myCarDeviceNameField: UITextField!
    @IBOutlet weak var myCarVehicleMakeLabel: UILabel!
    @IBOutlet weak var myCarVehicleModelLabel: UILabel!
    @IBOutlet weak var myCarYearLabel: UILabel!
    @IBOutlet weak var myCarGearLabel: UILabel!
    @IBOutlet weak var myCarFuelLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    
    @IBOutlet weak var carNameUnderlineView: UIView!
    @IBOutlet weak var carMakeUnderlineView: UIView!
    @IBOutlet weak var carModelUnderlineView: UIView!
    @IBOutlet weak var carYearUnderlineView: UIView!
    @IBOutlet weak var carGearUnderlineView: UIView!
    @IBOutlet weak var carFuelUnderlineView: UIView!
    
    @IBOutlet weak var eventsButton: UIButton!
    
    @IBOutlet weak var documentsTableView: UITableView!
    
    lazy var arrayOfYears:Array = [String]()
    lazy var arrayMakeModelsData : NSArray = [CVehicleMake]() as NSArray
    
    var flagHistoryPopupVisible:Bool = true
    
    var arrayDTCEvents = [CEvent]()
    var arrHistoryAlerts = [CEvent]()
    var headerButton:UIButton = UIButton(type: .custom)
    
    //pop up
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var viewDtcEventDetail: UIView!
    var detailView:DTCEventDetailView?
    
    //var cell : UITableViewCell = CollapsedDocumentCell()
    let collapsedCellIdentifier = "CollapsedDocumentCell"
    let expandedCellIdentifier = "ExpandedDocumentCell"
    
    //Maintenance
    var arrayUserDocuments = [CUserDocuments]()
    var dicServiceHistory:NSDictionary?
    lazy var currentSelectedIndex:Int = -1
    var arraydocDetails = [CDocumentTypeDetails]()
    let imagePicker = UIImagePickerController()
    var arrayServiceHistory = [CServiceHistory]()
    var arrayServiceDue = [CServiceHistory]()
    
    let expiryTime = 1296000 //15 days
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "My Car Screen")
        //  self.documentsTableView.register(ExpandedDocumentCell.self, forCellReuseIdentifier: "ExpandedDocumentCell")
        
        // self.documentsTableView.register(CollapsedDocumentCell.self, forCellReuseIdentifier: "CollapsedDocumentCell")
        //documentsTableView.register(ExpandedDocumentCell(), forCellReuseIdentifier: "ExpandedDocumentCell")
        // documentsTableView.register(CollapsedDocumentCell.self, forCellReuseIdentifier: "CollapsedDocumentCell")
        //documentsTableView.register(ExpandedDocumentCell.self, forCellReuseIdentifier: "ExpandedDocumentCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(documentDeletedNotification), name: Notification.Name(rawValue: "DocumentDeletedNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(documentDeletedNotification), name: Notification.Name(rawValue: "DocumentUpdateNotification"), object: nil)
        
        imagePicker.delegate = self
        
        labelBattery.layer.borderColor = AppUtility.sharedUtility.appColor(color: klightGrayColor).cgColor
        labelTow.layer.borderColor = AppUtility.sharedUtility.appColor(color: klightGrayColor).cgColor
        labelImpact.layer.borderColor = AppUtility.sharedUtility.appColor(color: klightGrayColor).cgColor
        
        viewStatus?.layer.cornerRadius = 3.0
        viewStatus?.layer.masksToBounds = true
        
        viewTableBase?.layer.cornerRadius = 3.0
        viewTableBase?.layer.masksToBounds = true
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        self.navigationItem.titleView = self.createHeaderView()
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            if vehicles.count < 1 {
                self.getVehicleHealth()
            }else{
                self.devicesArray = vehicles
                self.arrayMake = AppUtility.sharedUtility.arrayMake
                self.setSelectedVehicleDataOnView()
            }
            self.updateViewForEditingOption()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        if let deviceMode = UserDefaults.standard.string(forKey: "deviceMode") {
            if deviceMode == "TOYOTA" {
                self.labelDTCCount.isHidden = true
                self.imageSelectedHealthType.isHidden = true
                towViewObj.isHidden = true
            }
        }
        
        /*if let vehivleobj:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle() {
            print(vehivleobj)
            if let _ = self.navigationItem.titleView {
                self.getVehicleHealth()
                self.setHeaderTitle(title: vehivleobj.name!)
            }
            else {
                self.navigationItem.titleView = self.createHeaderView()
            }
            if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehivleobj){
                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
            }

        }*/
        self.setHeaderTitle(title: nil)
        if CommonData.sharedData.isDataPrefetched {

        CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (vehivleobj) in
            if vehivleobj != nil {
                //print(vehicleObj)
                if let _ = self.navigationItem.titleView {
                    self.getVehicleHealth()
                    self.setHeaderTitle(title: vehivleobj?.name!)
                }
                else {
                    self.navigationItem.titleView = self.createHeaderView()
                }
                if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehivleobj){
                    AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                }
            }
            else {
            CommonData.sharedData.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                
                for vehicle in vehicles{
                    
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle){
                        AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                    }
                }
            })
        }
            })
        }
        
        
        if let readStatus = AppUtility.sharedUtility.allEventsRead {
            if readStatus {
                self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
            }
            else {
                self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                
            }
        }
        self.getListOfUnreadEvents()
        
        self.setSelectedVehicleDataOnView()
        
       // if !self.imagePicker.isViewLoaded {
        //    self.carHealthTapped(nil)
        //}
        
         if !self.imagePicker.isViewLoaded {
            if currentSelectedIndex != -1 {
                toggleTableviewScroll()
                currentSelectedIndex = -1
                DispatchQueue.main.async {
                    self.documentsTableView.contentOffset = CGPoint.zero
                    self.documentsTableView.reloadData()
                }
            }
            self.carHealthTapped(nil)
            
        }
}
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

//        if currentSelectedIndex != -1 {
//            toggleTableviewScroll()
//            currentSelectedIndex = -1
//            DispatchQueue.main.async {
//                self.documentsTableView.contentOffset = CGPoint.zero
//                self.documentsTableView.reloadData()
//            }
//        }
//        self.carHealthTapped(nil)

        
    }
    func getListOfUnreadEvents() -> Void {
        
        let dateString = CommonData.sharedData.lastEventTimeStamp
        
        CEventRequest.sharedInstance().getNewEventsCount(afterTime: dateString) { (responseData, isSuccess, error) in
            
            if isSuccess {
                
                let eventCount = responseData as? NSNumber
                
                if eventCount == 0 {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = true
                }
                else {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = false
                }
            }
            else {
                
                
            }
        }
    }
    
    func createHeaderView() -> UIButton {
        
        headerButton.titleLabel?.font = UIFont(name: "Helvetica", size: 13)
        headerButton.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        
        self.setHeaderTitle(title: nil)
        
        return headerButton
    }
    
    func setHeaderTitle(title: String?) -> Void {
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            
            if vehicles.count <= 1 {
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!), for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name), for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first!.name), for: .normal)
                        }
                    })
                }
            }else{
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!) + " ▼" , for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name) + " ▼" , for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first!.name) + " ▼" , for: .normal)
                        }
                    })
                }
            }
        }
       // self.headerButton.titleLabel?.sizeToFit()
        self.headerButton.sizeToFit()
    }
    
    func setSelectedVehicleDataOnView() -> Void {
        
        CommonData.sharedData.getSelectedVehicle { (selectedVehicle) in
            if selectedVehicle != nil {
                print(selectedVehicle ?? CVehicle())
                let deviceObj:CDevice = CVehicleRequest.sharedInstance().getDeviceFor(selectedVehicle)
                self.myCarDeviceIDField.text = deviceObj.serialNumber//String(describing: selectedVehicle!.vehicleId)
                self.myCarDeviceNameField.text = AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name)
                //self.myCarVehicleMakeLabel.text = selectedVehicle!.vehicleMake
                self.myCarYearLabel.text =  ((selectedVehicle!.makeYear < 1) ? "" : String(describing: selectedVehicle!.makeYear))
                
                self.myCarGearLabel.text = selectedVehicle!.vehicleGearType
                self.myCarFuelLabel.text = selectedVehicle!.vehicleFuelType
                //  self.myCarVehicleModelLabel.text = selectedVehicle!.vehicleModel
                
                CommonData.sharedData.getMakerAndModel(with: Int(selectedVehicle!.vehicleModelId), mappingCallback: { (makeObj, modelObj) in
                    self.myCarVehicleModelLabel.text = modelObj?.modelName! ?? ""
                    self.myCarVehicleMakeLabel.text = makeObj?.makerName! ?? ""
                    
                })
                
            }
        }
        
        
    }
    
    
    @IBAction func headerTapped(_ sender:Any) {
        
        if devicesArray.count == 0 {
            self.view.showIndicatorWithError("No devices found")
            return
        }
        else if devicesArray.count == 1 {
            return
        }
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            
            
            let pickerView:CustomPickerView = CustomPickerView()
            pickerView.transitioningDelegate = self.presentationDelegate
            pickerView.modalPresentationStyle = .custom
            pickerView.isShowingHeader = true
            pickerView.pickerTitle = "Select a Vehicle"
            
            var vehicleNameArray:[String] = []
            for vehicle in vehicles {
                vehicleNameArray.append(vehicle.name)
            }
            
            pickerView.arrayData = vehicleNameArray as NSArray//.sorted() as NSArray
            
            pickerView.doneButtonCallback { (row, component) in
                self.arrayServiceDue.removeAll()
                self.arraydocDetails.removeAll()
                self.arrayUserDocuments.removeAll()
                self.arrayServiceHistory.removeAll()
    
                self.isEditingCarView = false
                self.updateViewForEditingOption()
                
                let vehicle = vehicles[row]
                
                if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle) {
                    if device.status == kDeviceStatusSubscriptionPending || device.status == kDeviceStatusExpired || device.status == kDeviceStatusTerminated || device.status == kDeviceStatusActivationInProgress {
                        
                        pickerView.dismiss(animated: true, completion: {
                            AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                        })
                        //return
                    }//else if device.status == kDeviceStatusActivationInProgress {
                        //                        pickerView.dismiss(animated: true, completion: {
                        //                            AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                        //
                        //                            pickerView.dismiss(animated: true, completion: nil)
                        //                            self.selectedVehicleMake = row
                        //
                        //                            var error:NSError? = nil
                        //                            CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError:&error)
                        //
                        //                            if(error != nil) {
                        //                                self.view.dismissProgress()
                        //                                print(error?.localizedDescription ?? String())
                        //                            }
                        //                            else{
                        //                                // self.view.dismissProgress()
                        //                                self.setHeaderTitle(title: vehicle.name)
                        //
                        //                                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: kVehicleChangedNotification)))
                        //
                        //                                self.setSelectedVehicleDataOnView()
                        //
                        //                                self.getVehicleHealth()
                        //
                        //                                if self.currentScreen == .Maintenance {
                        //                                    //                                self.arrayUserDocuments.removeAll()
                        //                                    //                                self.tableView.reloadData()
                        //
                        //                                    self.getAllUserDocuments()
                        //                                }
                        //                            }
                        //                        })
                        //    }
                    else {
                        pickerView.dismiss(animated: true, completion: nil)
                    }
                    self.selectedVehicleMake = row
                    
                    var error:NSError? = nil
                    CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError:&error)
                    
                    if(error != nil) {
                        self.view.dismissProgress()
                        print(error?.localizedDescription ?? String())
                    }
                    else{
                        // self.view.dismissProgress()
                        self.setHeaderTitle(title: vehicle.name)
                        
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: kVehicleChangedNotification)))
                        
                        self.setSelectedVehicleDataOnView()
                        
                        self.getVehicleHealth()
                        
                        if self.currentScreen == .Maintenance {
                            
                            //                                if self.currentSelectedIndex == 4 {
                            //                                    let indexpath = IndexPath(row: 4, section: 0)
                            //                                    self.tableView(self.tableView, didSelectRowAt: indexpath)
                            //                                }
                            
                            self.currentSelectedIndex = -1
                            self.getAllUserDocuments()
                        }
                    }
                    //  }
                }
            }
            self.present(pickerView, animated: true, completion: nil)
            print("header tapped")
            
        }
        
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        
        let notificationName = Notification.Name("toggleLeftMenu")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
    }
    
    @IBAction func eventsButtonTapped(_ sender: Any) {
        
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(withIdentifier: "AlertsNavController")
           vc.modalPresentationStyle = .fullScreen;
           self.navigationController?.present(vc, animated: true, completion: {
           })
    }
    
    func getVehicleHealth() {
        if !AppUtility.sharedUtility.isNetworkAvailable() || currentScreen != .Health {
            
            return
        }
        
        self.view.showIndicatorWithProgress(message: "Please wait")
        
        CVehicleRequest.sharedInstance().getHealthStatus { (response, isSuccessful, error) in
            self.view.dismissProgress()
            
            if (isSuccessful && response != nil) {
                let vehicleObj:CVehicleHealthStatus = response as! CVehicleHealthStatus
                print(vehicleObj)
                self.populateHealthData(healthdata:vehicleObj)
                
                if (vehicleObj.obdSupported.boolValue == true) {
                    if(vehicleObj.dtcEvents.count > 0){
                        self.arrayDTCEvents = vehicleObj.dtcEvents as! [CEvent]
                        self.labelDTCCount.text = "\(self.arrayDTCEvents.count) Problem(s) Detected"
                        
                        self.labelDTCCount.textColor = UIColor.red
                        /*let first = 0
                         let last = vehicleObj.dtcEvents.count-1
                         let interval = 1
                         for event in stride(from: first, through: last, by: interval) {
                         let eventObj:CEvent = dtcArray[event] as! CEvent
                         self.getDTCEvents(dtcCode: eventObj.dtcCode)
                         }*/
                    }else{
                        self.labelDTCCount.text = "Engine health looks fine"
                        self.arrayDTCEvents.removeAll()
                    }
                }else{
                    self.labelDTCCount.text = "Not supported in your car"
                }
                self.tableView.reloadData()
            }else{
                if (response != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
            
        }
        
    }
    
    func populateHealthData(healthdata:CVehicleHealthStatus)
    {
        
        if (healthdata.recentCrashEventFound) {
            buttonImpact.setImage(#imageLiteral(resourceName: "selectedImpact"), for: .normal)
            labelImpact.layer.borderColor = UIColor.red.cgColor
            labelImpact.textColor = UIColor.red
        }else{
            buttonImpact.setImage(#imageLiteral(resourceName: "DashboardImpact"), for: .normal)
            labelImpact.layer.borderColor = AppUtility.sharedUtility.appColor(color: klightGrayColor).cgColor
            labelImpact.textColor = UIColor.black
        }
        
        if (healthdata.recentTowEventFound) {
            buttonTow.setImage(#imageLiteral(resourceName: "selectedTow"), for: .normal)
            labelTow.layer.borderColor = UIColor.red.cgColor
            labelTow.textColor = UIColor.red
        }else{
            buttonTow.setImage(#imageLiteral(resourceName: "DashboardTow"), for: .normal)
            labelTow.layer.borderColor = AppUtility.sharedUtility.appColor(color: klightGrayColor).cgColor
            labelTow.textColor = UIColor.black
        }
        
        if (healthdata.batteryEvent as? [String:Any] != nil) {
            buttonBattery.setImage(#imageLiteral(resourceName: "DashboardBatteryRed"), for: .normal)
            labelBattery.layer.borderColor = UIColor.red.cgColor
            labelBattery.textColor = UIColor.red
        }else{
            buttonBattery.setImage(#imageLiteral(resourceName: "DashboardBattery"), for: .normal)
            labelBattery.layer.borderColor = AppUtility.sharedUtility.appColor(color: klightGrayColor).cgColor
            labelBattery.textColor = UIColor.black
        }
        
        (((healthdata.dtcEvents.count) > 0) ? (imageSelectedHealthType.image = #imageLiteral(resourceName: "DashboardEngineRed")) : (imageSelectedHealthType.image = #imageLiteral(resourceName: "DashboardEngine")))
        
        
    }
    
    
    func getDTCEventDetail(dtcEvent:CEvent)
    {
        self.view.showIndicatorWithInfo("Please wait...")
        CVehicleRequest.sharedInstance().getDtcDetail(forDtcCode: dtcEvent.dtcCode) { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            
            if(error == nil && responseObject != nil)
            {
                print(responseObject ?? NSDictionary())
                self.view.dismissProgress()
                //                let detailView:DTCEventDetailView = UIView (frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)) as! DTCEventDetailView
                //                detailView.dtcDetailDictionary = responseObject as! NSDictionary
                //                self.view.addSubview(detailView)
                
                let viewC:DTCEventDetailView = Bundle.main.loadNibNamed("DTCEventDetailView", owner: nil, options: nil)?.first as! DTCEventDetailView
                viewC.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                viewC.dtcDetailDictionary = responseObject as! NSDictionary
                viewC.labelTitle.text = dtcEvent.name
                
                UIApplication.shared.keyWindow?.addSubview(viewC)

                //UIApplication.shared.windows.last?.addSubview(viewC)
                
                
            }
            else
            {
                self.view.showIndicatorWithError((error?.localizedDescription)!)
            }
        }
    }
    
    
    //TODO: show information for different events
    func getVehicleHealthEvent(eventType: String, completion: @escaping (_ eventDetail: Array<CEvent>) -> Void)
    {
        self.view.showIndicatorWithProgress(message: "Please wait")
        
        if eventType == "TOWING" {
            
            CEventRequest.sharedInstance().getTowAlert(withPageNumber: 0, andResponse: { (responseObject, error) in
                
                if (error == nil && responseObject != nil) {
                    print(responseObject ?? NSArray())
                    self.arrHistoryAlerts = responseObject as! [CEvent]
                    completion(responseObject as! Array<CEvent>)
                    
                }
                else {
                    print(error?.localizedDescription ?? String())
                    // let arr:Array = [CEvent]()
                    //let strError:String = (error?.localizedDescription)!
                    // completion(arr)
                    
                    self.view.showIndicatorWithError(error?.localizedDescription)
                    
                }
                
            })
        }else{
            let currentDate = Int64(Date().timeIntervalSince1970*1000)
            CEventRequest.sharedInstance().getEventsForAlarmType(eventType, startDate:"\(currentDate)", endDate: "0") { (responseObject, error) in
                
                if (error == nil && responseObject != nil) {
                    print(responseObject ?? NSArray())
                    self.arrHistoryAlerts = responseObject as! [CEvent]
                    completion(responseObject as! Array<CEvent>)
                    
                }
                else {
                    print(error?.localizedDescription ?? String())
                    // let arr:Array = [CEvent]()
                    //let strError:String = (error?.localizedDescription)!
                    // completion(arr)
                    
                    self.view.showIndicatorWithError(error?.localizedDescription)
                    
                }
            }
        }
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func carHealthTapped(_ sender: Any?) {
        self.currentScreen = .Health
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "HealthTapped", category: kEventCategoryHealth, andScreenName: "My Car Screen", eventLabel: nil, eventValue: nil)
        buttonCarHealth.setTitleColor(UIColor.black, for: .normal)
        buttonMaintenance.setTitleColor(UIColor.white, for: .normal)
        buttonCarProfile.setTitleColor(UIColor.white, for: .normal)
        
        imageSelectedCarHealth.isHidden = false
        imageSelectedMaintenance.isHidden = true
        imageSelectedCarProfile.isHidden = true
        
        carHealthBackgroundView.isHidden = false
        maintenanceBackgroundView.isHidden = true
        carProfileBackgroundView.isHidden = true
    }
    
    @IBAction func carProfileTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "CarProfileTapped", category: kEventCategoryHealth, andScreenName: "My Car Screen", eventLabel: nil, eventValue: nil)
        self.currentScreen = .CarProfile
        
        buttonCarHealth.setTitleColor(UIColor.white, for: .normal)
        buttonMaintenance.setTitleColor(UIColor.white, for: .normal)
        buttonCarProfile.setTitleColor(UIColor.black, for: .normal)
        
        imageSelectedCarHealth.isHidden = true
        imageSelectedMaintenance.isHidden = true
        imageSelectedCarProfile.isHidden = false
        
        carHealthBackgroundView.isHidden = true
        maintenanceBackgroundView.isHidden = true
        carProfileBackgroundView.isHidden = false
        
        if arrayOfYears.count < 1 {
            self.updateMakeModelArray()
        }
        isEditingCarView = false
        
        
    }
    
    func updateViewForEditingOption(){
        
        myCarDeviceNameField.isUserInteractionEnabled = isEditingCarView
        carNameUnderlineView.isHidden = !isEditingCarView
        carMakeUnderlineView.isHidden = !isEditingCarView
        carModelUnderlineView.isHidden = !isEditingCarView
        carYearUnderlineView.isHidden = !isEditingCarView
        carGearUnderlineView.isHidden = !isEditingCarView
        carFuelUnderlineView.isHidden = !isEditingCarView
        
        if isEditingCarView {
            editButton.setBackgroundImage(#imageLiteral(resourceName: "saveOrangeIcon"), for: .normal)
        }else{
            editButton.setBackgroundImage(#imageLiteral(resourceName: "editOrangeIcon"), for: .normal)
        }
        
    }
    
    func updateMakeModelArray() {
        
        let date = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: date)
        
        //add all years from current year till 1990
        
        let first = 1990
        let last = currentYear
        let interval = 1
        for year in stride(from: first, through: last, by: interval) {
            let stringValue = "\(year)"
            arrayOfYears.append(stringValue)
        }
        arrayOfYears.reverse()
        
        
        self.view.showIndicatorWithProgress(message: "Fetching Details")
        
        CVehicleRequest.sharedInstance().getVehicleReferenceDataResponse { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            
            if isSuccessful {
                if let vehicleObj:CVehicleReference = responseObject as? CVehicleReference{
                    
                    self.arrayMakeModelsData = vehicleObj.vehicleMakers as NSArray
                }
            }else{
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
        
    }
    
    @IBAction func closePopup(_ sender: Any) {
        //flagHistoryPopupVisible = false
        viewHistory.isHidden = true
        detailView?.removeFromSuperview()
    }
    
    
    @IBAction func batteryTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "BatteryTapped", category: kEventCategoryHealth, andScreenName: "My Car Screen", eventLabel: nil, eventValue: nil)
        flagHistoryPopupVisible = true
        self.getVehicleHealthEvent(eventType: "LOW_VOLTAGE") { (eventDetail) in
            if((eventDetail as AnyObject).count > 0){
                self.viewHistory.isHidden = false
                self.labelHistoryTitle.text = "Battery - (\(self.arrHistoryAlerts.count) in last 12 months)"
                self.historyTableView.reloadData()
                self.view.dismissProgress()
            }
            else {
                self.view.showIndicatorWithError("No battery event")
            }
        }
        
    }
    @IBAction func towTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "TowTapped", category: kEventCategoryHealth, andScreenName: "My Car Screen", eventLabel: nil, eventValue: nil)
        flagHistoryPopupVisible = true
        self.getVehicleHealthEvent(eventType: "TOWING") { (eventDetail) in
            if((eventDetail as AnyObject).count > 0){
                //self.arrHistoryAlerts = [eventDetail]
                self.viewHistory.isHidden = false
                self.labelHistoryTitle.text = "Tow - (\(self.arrHistoryAlerts.count) in last 12 months)"
                self.historyTableView.reloadData()
                self.view.dismissProgress()
            }
            else {
                self.view.showIndicatorWithError("No tow event")
            }
        }
    }
    @IBAction func impactTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "ImpactTapped", category: kEventCategoryHealth, andScreenName: "My Car Screen", eventLabel: nil, eventValue: nil)
        flagHistoryPopupVisible = true
        self.getVehicleHealthEvent(eventType: "IMPACT") { (eventDetail) in
            if((eventDetail as AnyObject).count > 0){
                //self.arrHistoryAlerts = [eventDetail]
                self.viewHistory.isHidden = false
                self.labelHistoryTitle.text = "Impact - (\(self.arrHistoryAlerts.count) in last 12 months)"
                
                self.historyTableView.reloadData()
                self.view.dismissProgress()
            }
            else {
                self.view.showIndicatorWithError("No impact event")
            }
        }
    }
    //MARK: Table View Delegates and DataSource
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: 300))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == documentsTableView {
            if indexPath.row == currentSelectedIndex{
                return (UIScreen.main.bounds.height - 163)
            }
        }
        return 80
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        if tableView == documentsTableView {
            return (UIScreen.main.bounds.height - 153)
        }
        return 60
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentScreen == .Maintenance {
            return arrayUserDocuments.count+1
        }else{
            if(tableView == self.tableView){
                return arrayDTCEvents.count
            }
            else {
                return arrHistoryAlerts.count
            }
        }
        
    }
    
    
    
    
    @objc func serviceDueButtonTapped() -> Void {
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.pickerTitle = "Select due date"
        pickerView.minimumDate = Date()
        
        pickerView.doneButtonCallback { (date) in
            pickerView.dismiss(animated: true, completion: nil)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-YYYY"
            
            _ = formatter.string(from: date as Date)
            
            let notificationName = Notification.Name("UpdateDueDateOnService")
            
            NotificationCenter.default.post(name: notificationName, object: date)
            
        }
        
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    func toggleTableviewScroll() -> Void {
        self.documentsTableView.isScrollEnabled = !self.documentsTableView.isScrollEnabled
return
//        if currentSelectedIndex == -1 {
//            self.documentsTableView.isScrollEnabled = true
//
//        }
//        else {
//            self.documentsTableView.isScrollEnabled = false
//
//        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.currentScreen == .Maintenance {
            
            if self.currentSelectedIndex == indexPath.row {
                if(indexPath.row == 3){
                    let cell = tableView.dequeueReusableCell(withIdentifier:"VehicleServiceCell", for: indexPath) as! VehicleServiceCell
                    cell.selectionStyle = .none
                    cell.dueDateButton.addTarget(self, action: #selector(serviceDueButtonTapped), for: .touchUpInside)
                    cell.arrayServiceHistory = self.arrayServiceHistory
                    cell.callbackForServiceTapped(with: {
                        self.getServiceHistory()
                    })
                    
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                    return cell
                }
                
                var cell = tableView.dequeueReusableCell(withIdentifier: self.expandedCellIdentifier) as? ExpandedDocumentCell
                if cell == nil {
                    cell = ExpandedDocumentCell()
                }
                
                //vivek
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                
                cell!.selectionStyle = .none
                cell!.arrayServiceHistory.removeAll()
                cell!.arrayDocDetails = self.arraydocDetails
                let userDoc:CUserDocuments = arrayUserDocuments[indexPath.row]
                
                cell!.selectedDocument = userDoc.docType
                
                //                cell!.buttonEditDocument.addTarget(self, action: #selector(presentEditOrDeleteMenuForCell(cell:)), for: .touchUpInside)
                
                cell!.tag = indexPath.row
                cell!.callbackForAddButtonTapped(with: {
                    
                    let alert: UIAlertController = UIAlertController(title: "", message: "Choose Image", preferredStyle: .actionSheet)
                    let libraryAction:UIAlertAction = UIAlertAction(title: "Library", style: .default) { (action) in
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .photoLibrary
                        self.present(self.imagePicker, animated: true, completion: {
                            
                        })
                    }
                    
                    let cameraAction:UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (action) in
                        
                        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
                            self.view.showIndicatorWithError("Camera unavailable")
                            return
                        }
                        
                        self.imagePicker.allowsEditing = false
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.cameraCaptureMode = .photo

                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                    
                    let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    
                    alert.addAction(libraryAction)
                    alert.addAction(cameraAction)
                    alert.addAction(cancelAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                })
                
                cell?.callbackForUpdateButtonTapped {
                    self.getdocumentsForDocType(docType: userDoc.docType)
                    
                }
                
                return cell!
            }
            else{
                var cell = tableView.dequeueReusableCell(withIdentifier:self.collapsedCellIdentifier) as? CollapsedDocumentCell
                
                if cell == nil {
                    cell = CollapsedDocumentCell()
                }
                
                cell!.selectionStyle = .none
                
                if (indexPath.row == self.arrayUserDocuments.count) {
                    
                    cell!.cellTitleLabel.text = "SERVICE DUE"
                    if(self.arrayServiceDue.count > 0){
                        
                        if self.arrayServiceHistory.count == 1 {
                            let serviceObj:CServiceHistory = self.arrayServiceDue[0]
                            let date = Date(timeIntervalSince1970: TimeInterval(serviceObj.dueDate)!)
                            cell!.cellSubtitleLabel.text = "Due on " + AppUtility.sharedUtility.stringFromDate(date:  date)
                            
                        } else {
                            cell!.cellSubtitleLabel.text = "\(self.arrayServiceDue.count) services due"
                        }
                    }
                    else {
                        cell!.cellSubtitleLabel.text = ""//"No Service Due"
                        
                    }
                    cell!.addImageIcon.image = #imageLiteral(resourceName: "NoDocIcon")
                    
                }
                else {
                    var subtitleText = ""
                    var documentCountTitle = ""

                    let userDoc:CUserDocuments = arrayUserDocuments[indexPath.row]
                    if let expiringDetails:CDocumentTypeDetails = userDoc.expiringDocDetail{
                        cell!.addImageIcon.image = #imageLiteral(resourceName: "NoDocIcon")

                        cell!.cellSubtitleLabel.text = expiringDetails.expiryDate
                        getImage(docDetailObj: expiringDetails) { (imageObj) in
                            cell!.addImageIcon.image = imageObj
                        }
                        
                        let docCount = userDoc.numberOfDocs!
                        
                        
                        if docCount == "0" {
                            documentCountTitle =  "(No Document attached)\n"
                            
                        }
                        else if docCount == "1" {
                            documentCountTitle = "\(docCount) Document attached\n"
                        }
                        else {
                            documentCountTitle =  "\(docCount) Documents attached\n"
                            
                        }
                        
                        let date = Date(timeIntervalSince1970: TimeInterval(expiringDetails.expiryDate)!)
                        
                        if AppUtility.sharedUtility.isDocumentExpired(documentExpiry: expiringDetails.expiryDate) {
                            subtitleText = "Expired on " + AppUtility.sharedUtility.stringFromDate(date:  date)
                            
                        }
                        else {
                            subtitleText = "Expiring on " + AppUtility.sharedUtility.stringFromDate(date:  date)
                        }
                        
                    }else {
                        //subtitleText = "No Documents Attached"
                        subtitleText = "(No Document attached)\n"

                        cell!.addImageIcon.image = #imageLiteral(resourceName: "NoDocIcon")
                    }
                    
                    cell?.cellSubtitleLabel.text = documentCountTitle + subtitleText

                    cell?.addImageIcon.layer.borderWidth = 0.1
                    cell?.addImageIcon.layer.borderColor = UIColor.lightGray.cgColor
                    cell?.addImageIcon.layer.masksToBounds = true
                    
                    
                    if userDoc.docType == "DL" {
                        cell!.cellTitleLabel.text = "DRIVING LICENSE"
                        
                    }
                    else {
                        cell!.cellTitleLabel.text = userDoc.docType
                        
                    }
                    
                }
                return cell!
            }
            
        }else{
            if(tableView == historyTableView){
                let cell = tableView.dequeueReusableCell(withIdentifier:"CellHealthHistory", for: indexPath) as! HealthHistoryCell
                cell.selectionStyle = .none
                
                let eventObj:CEvent = (arrHistoryAlerts[indexPath.row] as AnyObject) as! CEvent
                //  let height = heightForLabel(text: cell.labelLocation.text!, font: UIFont(name:"Helvetica", size:15)!, width: 200)
                cell.setCellDataForEvent(event: eventObj)
                // cell.labelLocation.frame = CGRect(x: cell.labelLocation.frame.origin.x, y: cell.labelLocation.frame.origin.y, width: cell.labelLocation.frame.size.width, height: height)
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier:"CellHealth", for: indexPath) as! HealthTitleCell
                cell.selectionStyle = .none
                
                let event:CEvent = arrayDTCEvents[indexPath.row]
                
                cell.labelDate.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: event.timeStamp)
                cell.labelHealthTitle.text = event.name
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.currentScreen == .Maintenance {
            self.documentsTableView.isUserInteractionEnabled = false
            self.toggleTableviewScroll()

            if(currentSelectedIndex == indexPath.row){
                currentSelectedIndex = -1
                DispatchQueue.main.async {
                    self.documentsTableView.contentOffset = CGPoint.zero
                    self.documentsTableView.reloadData()

                }

                //self.documentsTableView.contentInset = UIEdgeInsets.zero
                getAllUserDocuments()
            }
            else {
                currentSelectedIndex = indexPath.row
                arraydocDetails.removeAll()
                if(indexPath.row == 3){
                    getServiceHistory()
                }
                else {
                    let userDoc:CUserDocuments = arrayUserDocuments[indexPath.row]
                    getdocumentsForDocType(docType: userDoc.docType)
                    
                }
                
            }

        }else{
            
            if(tableView != historyTableView){
                let event:CEvent = arrayDTCEvents[indexPath.row]
                self.getDTCEventDetail(dtcEvent: event)
            }
        }
        
    }
    
    //MARK: Maintenance methods
    
    @IBAction func maintenanceTapped(_ sender: Any) {
        self.currentScreen = .Maintenance
        currentSelectedIndex = -1
        buttonCarHealth.setTitleColor(UIColor.white, for: .normal)
        buttonMaintenance.setTitleColor(UIColor.black, for: .normal)
        buttonCarProfile.setTitleColor(UIColor.white, for: .normal)
        
        imageSelectedCarHealth.isHidden = true
        imageSelectedMaintenance.isHidden = false
        imageSelectedCarProfile.isHidden = true
        
        carHealthBackgroundView.isHidden = true
        maintenanceBackgroundView.isHidden = false
        carProfileBackgroundView.isHidden = true
        
        getAllUserDocuments()
    }
    
    func getAllUserDocuments()
    {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        self.view.showIndicatorWithProgress(message: "")
        CommonData.sharedData.getSelectedVehicle { (vehicle) in
            
            if vehicle != nil {
                CDocumentWalletRequest.sharedInstance().getUserDocumentsforVehicle(withId: Int32((vehicle?.vehicleId)!)) { (response, isSuccessful, error) in
                    if(isSuccessful && response != nil){
                        self.arrayUserDocuments = response as! [CUserDocuments]
                        
                        //self.arrayUserDocuments.sort { $0.docType < $1.docType }
                        
                        self.getDueService()
                        //self.documentsTableView.reloadData()
                        //self.view.dismissProgress()
                    }
                    else
                    {
                        self.documentsTableView.isUserInteractionEnabled = true

                        self.view.dismissProgress()
                        self.view.showIndicatorWithError(error?.localizedDescription)
                    }
                }
            }
            
        }
        //        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
    }
    
    /*  func getdocumentsForDocType(docType:String){
     self.view.showIndicatorWithProgress(message: "")
     
     let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
     
     CDocumentWalletRequest.sharedInstance().getDocumentDetail(forDocType: docType, forVehicleId: Int32(vehicle.vehicleId)) { (response, isSuccessful, error) in
     }*/
    func getdocumentsForDocType(docType:String){
        
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        self.view.showIndicatorWithProgress(message: "")
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().getDocumentDetail(forDocType: docType, forVehicleId: Int32(vehicle.vehicleId)) { (response, isSuccessful, error) in
            self.view.dismissProgress()
            self.documentsTableView.isUserInteractionEnabled = true

            if(isSuccessful && response != nil){
                self.view.dismissProgress()
                self.arraydocDetails = response as! [CDocumentTypeDetails]
                self.arraydocDetails.reverse()
                self.documentsTableView.reloadData()
                
            }
            else
            {
                self.view.dismissProgress()
                self.view.showIndicatorWithError(error?.localizedDescription)
            }
        }
    }
    func addButtonTapped() {
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        let data = #imageLiteral(resourceName: "DashboardViewMapSelected").pngData() as NSData?
        
        
        CDocumentWalletRequest.sharedInstance().uploadDataforVehicle(withId: Int32(vehicle.vehicleId), file: data as Data?, fileName: "DashboardViewMapSelected.png", validFrom: "1490790106000", validTill: "1493468505000", docType: "DL") { (response, isSuccessful, error) in
            if(isSuccessful && response != nil){
                print(response ?? NSString())
            }
        }
    }
    
    func downloadImageFromServer(docDetailsObj:CDocumentTypeDetails, onCompletion: @escaping (_ imageName: NSString) -> Void){
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().downloadDocumentImage(forVehicleId: Int32(vehicle.vehicleId), docType: docDetailsObj.docType, fileName: docDetailsObj.imageName) { (response, isSuccessful, error) in
            if(isSuccessful && response != nil){
                let imgName:NSString = response as! NSString
                onCompletion(imgName)
            }
        }
    }
    
    
    func getImage(docDetailObj:CDocumentTypeDetails, completion: @escaping (_ image: UIImage) -> Void)
    {
        
        let cacheDirectoryPath = FileManager.SearchPathDirectory.cachesDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(cacheDirectoryPath, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(docDetailObj.imageName)
            let image    = UIImage(contentsOfFile: imageURL.path)
            if (image != nil) {
                completion(image!)
            }
            else{
                //download from SDK
                downloadImageFromServer(docDetailsObj: docDetailObj, onCompletion: { (imageName) in
                    
                    let nsCacheDirectory = FileManager.SearchPathDirectory.cachesDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsCacheDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(docDetailObj.imageName)
                        let image    = UIImage(contentsOfFile: imageURL.path)
                        if (image != nil) {
                            completion(image!)
                        }
                    }
                    
                })
            }
            
        }
    }
    
    
    //MARK:- Service History
    
    func getDueService() {
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().getDueServicesForVehicle(withId: Int32(vehicle.vehicleId)) { (response, isSuccessful, error) in
            self.documentsTableView.isUserInteractionEnabled = true

            if(isSuccessful && response != nil){
                self.arrayServiceDue = response as! [CServiceHistory]
                
                DispatchQueue.main.async {
                    self.documentsTableView.reloadData()
                    self.view.dismissProgress()
                    
                }
                
            }
            else
            {
                print("due service error")
                self.view.dismissProgress()
                self.view.showIndicatorWithError(error?.localizedDescription)
            }
            
        }
        
        
    }
    
    func getServiceHistory() {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        self.view.showIndicatorWithProgress(message: "")
        
        let vehicle:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
        
        CDocumentWalletRequest.sharedInstance().getPastServicesForVehicle(withId: Int32(vehicle.vehicleId)) { (response, isSuccessful, error) in
            self.documentsTableView.isUserInteractionEnabled = true

            if(isSuccessful && response != nil){
                self.arrayServiceHistory = response as! [CServiceHistory]
                self.documentsTableView.reloadData()
                self.view.dismissProgress()
                
            }
            else
            {
                self.view.dismissProgress()
                self.view.showIndicatorWithError(error?.localizedDescription)
            }
            
        }
    }
    //MARK: - Vehicle details view methods
    
    func validateAllFields() -> Bool {
        
        if((myCarDeviceNameField.text?.count)!<3 || (myCarDeviceNameField.text?.count)!>100){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Name length must lie between 3 to 100")
            self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kdeviceNameTag))?.backgroundColor = UIColor.red
            // txtFieldInValid = textFieldDeviceName
            return false
        }
        
        
        if((myCarVehicleMakeLabel.text?.count)! == 0 || myCarVehicleMakeLabel.text == "Select Vehicle Make"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle Make cannot be empty")
            //self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleMake))?.backgroundColor = UIColor.red
            return false
        }
        
        if((myCarVehicleModelLabel.text?.count)! == 0 || myCarVehicleModelLabel.text == "Select Vehicle Model"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle Model cannot be empty")
            //self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleModel))?.backgroundColor = UIColor.red
            
            return false
        }
        
        
        if((myCarYearLabel.text?.count)! == 0 || myCarYearLabel.text == "Select year"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle year cannot be empty")
            
            return false
        }
        
        if((myCarGearLabel.text?.count)! == 0 || myCarGearLabel.text == "Select gear type"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle gear type cannot be empty")
            //  self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleGearType))?.backgroundColor = UIColor.red
            
            return false
        }
        
        if((myCarFuelLabel.text?.count)! == 0 || myCarFuelLabel.text == "Select fuel type"){
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed:"Vehicle fuel type cannot be empty")
            //self.view.viewWithTag(Int(kBaseViewofLabels))?.viewWithTag(Int(kvehicleFuelType))?.backgroundColor = UIColor.red
            
            return false
        }
        
        return true
        
    }
    
    
    @IBAction func vehicleMakeButtonTapped(_ sender: Any) {
        if !isEditingCarView {
            return
        }
        self.view.endEditing(true)
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        
        let sortedArray = arrayMakeModelsData.sorted { (($0 as! CVehicleMake).makerName) < (($1 as! CVehicleMake).makerName) }
        
        arrayMakeModelsData = sortedArray as NSArray
        let values:NSArray = (arrayMakeModelsData as NSArray).value(forKeyPath: "makerName") as! NSArray
        self.arrayMake = values as! [String]
        pickerView.arrayData = self.arrayMake as NSArray
        
        
        pickerView.doneButtonCallback { (row, component) in
            self.selectedVehicleMake = row
            self.myCarVehicleMakeLabel.text = self.arrayMake[row]
            
            self.myCarVehicleModelLabel.text = "Select Vehicle Model"
            self.selectedVehicleMake = 0
            
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    @IBAction func vehicleModelButtonTapped(_ sender: Any) {
        if !isEditingCarView {
            return
        }
        
        if self.myCarVehicleMakeLabel.text?.count == 0 || self.myCarVehicleMakeLabel.text == nil {
            self.view.showIndicatorWithError("Please select Vehicle Make first")
            return
        }
        
        self.view.endEditing(true)
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        
        let values:NSArray = (arrayMakeModelsData as NSArray).value(forKeyPath: "makerName") as! NSArray
        self.arrayMake = values as! [String]
        
        
        if let values = (arrayMakeModelsData as NSArray)[arrayMake.index(of: self.myCarVehicleMakeLabel.text!)!] as? CVehicleMake {
            
            let sortedArray = values.models.sorted { (($0 as! CVehicleModel).modelName) < (($1 as! CVehicleModel).modelName) }
            let valuesModels:NSArray = (sortedArray as NSArray).value(forKeyPath: "modelName") as! NSArray
            pickerView.arrayData = valuesModels as NSArray
            
            // self.arrayMake = valuesModels as! [String]
            // let sortedArray = self.arrayMake.sorted { $0 < $1 }
            //self.arrayMake = (sortedArray as NSArray) as! [String]
            
            pickerView.doneButtonCallback { (row, component) in
                self.myCarVehicleModelLabel.text = valuesModels[row] as? String
                
                let valuesModelsId:NSArray = (sortedArray as NSArray).value(forKeyPath: "modelId") as! NSArray
                self.selectedVehicleMake = valuesModelsId[row] as! Int
            }
            self.present(pickerView, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func vehicleYearButtonTapped(_ sender: Any) {
        if !isEditingCarView {
            return
        }
        
        self.view.endEditing(true)
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.arrayData = arrayOfYears as NSArray
        
        pickerView.doneButtonCallback { (row, component) in
            self.myCarYearLabel.text = self.arrayOfYears[row]
            //            self.labelVehicleYear.textColor = UIColor.black
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    @IBAction func vehicleGearButtonTapped(_ sender: Any) {
        if !isEditingCarView {
            return
        }
        self.view.endEditing(true)
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        
        let arrayGear : Array = ["MANUAL","AUTOMATIC"]
        pickerView.arrayData = arrayGear as NSArray
        
        pickerView.doneButtonCallback { (row, component) in
            self.myCarGearLabel.text = ((row==0) ? "MANUAL": "AUTOMATIC")
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    @IBAction func vehicleEditButtonTapped(_ sender: Any) {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "EditVehicleTapped", category: kEventCategoryHealth, andScreenName: "My Car Screen", eventLabel: nil, eventValue: nil)
        if isEditingCarView {
            if validateAllFields() {
                
                let vehicleModelId = String(self.selectedVehicleMake)
                
                CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                    
                    if selectedVehicle != nil {
                        
                        let deviceID = selectedVehicle!.deviceId
                        
                        let params = ["serialNumber": deviceID, //self.myCarDeviceIDField.text!,
                            "name":AppUtility.sharedUtility.encodeString(stringToEncode: self.myCarDeviceNameField.text! as NSString),
                            "makeYear":self.myCarYearLabel.text!,
                            "vehicleModelId":vehicleModelId,
                            "fuelType":self.myCarFuelLabel.text!,
                            "gearType":self.myCarGearLabel.text!,
                            "vehicleMake":self.myCarVehicleMakeLabel.text!,
                            "vehicleModel":self.myCarVehicleModelLabel.text!
                            ] as [String : Any]
                        
                        let vehicleId = Int32(selectedVehicle!.vehicleId)
                        
                        CVehicleRequest.sharedInstance().updateDeviceInformation(withInfo: params, forVehicleWithId: vehicleId, response: { (responseObject, isSuccessful, error) in
                            
                            self.view.dismissProgress()
                            
                            if(error == nil && responseObject != nil){
                                self.view.showIndicatorWithInfo("Record updated sucessfully")
                                
                                self.isEditingCarView = false
                                self.updateViewForEditingOption()
                                
                                
                                CommonData.sharedData.fetchAndUpdateVehiclesFromServer(with: { (arrayVehicles, isSuccess) in
                                    if arrayVehicles.count > 0 {
                                        self.devicesArray = arrayVehicles
                                        CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                                            if selectedVehicle != nil {
                                                self.arrayMake.removeAll()
                                                
                                                for vehicle in self.devicesArray {
                                                    
                                                    let cVehicle = vehicle
                                                    self.arrayMake.append(cVehicle.name)
                                                    
                                                    if cVehicle.vehicleId == selectedVehicle!.vehicleId {
                                                        CommonData.sharedData.setSelectedVehicle(selectedVehicle: cVehicle)
                                                    }
                                                }
                                                
                                                AppUtility.sharedUtility.arrayMake = self.arrayMake
                                                
                                                self.setHeaderTitle(title: nil)
                                            }
                                            
                                        })
                                        
                                    }
                                })
                            }
                            else {
                                self.view.showIndicatorWithError(error?.localizedDescription)
                            }
                            
                        })
                    }
                })
            }
        }else {
            isEditingCarView = !isEditingCarView
            updateViewForEditingOption()
        }
        
    }
    
    @IBAction func vehicleFuelButtonTapped(_ sender: Any) {
        
        if !isEditingCarView {
            return
        }
        self.view.endEditing(true)
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.arrayData = ["PETROL","DIESEL"]
        
        pickerView.doneButtonCallback { (row, component) in
            self.myCarFuelLabel.text = ((row==0) ? "PETROL": "DIESEL")
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    //MARK: - UIImagePickerController
    //MARK: - Delegates
    // private func imagePickerController(_ picker: UIImagePickerController,
    //        didFinishPickingMediaWithInfo info: [String : AnyObject])
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
        
    {
        if let _ = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            dismiss(animated: true, completion: nil)
            var choosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            
            if picker.sourceType == .camera {
                if picker.cameraDevice == .front {
                    choosenImage = UIImage(cgImage: (choosenImage?.cgImage)!, scale: (choosenImage?.scale)!, orientation: .leftMirrored)
                }
            }
            
            var docExtension = ""
            
            let userDoc:CUserDocuments = arrayUserDocuments[currentSelectedIndex]
            
            if info[UIImagePickerController.InfoKey.mediaMetadata] != nil {
                // image picked from camera. type = JPG
                docExtension = "JPG"
            }else if info[UIImagePickerController.InfoKey.referenceURL] != nil {
                // image picked from gallery. type = get it from URL
                
                if let imageLocalURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                    let queryArray = imageLocalURL.query?.components(separatedBy: "&")
                    for query in queryArray! {
                        var individualElement = query.components(separatedBy: "=")
                        if individualElement[0] == "ext" {
                            docExtension = individualElement[1]
                        }
                    }
                }
                
            }else{
                // show something unexepcted occoured
                self.view.showIndicatorWithError("Unable to parse selected data")
                return
            }
            
            
            
            let imageViewer:ImageViewerView = Bundle.main.loadNibNamed("ImageViewerView", owner: nil, options: nil)?.first as! ImageViewerView
            imageViewer.frame = self.view.frame//CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            imageViewer.documentImageView.contentMode = .scaleAspectFit
            imageViewer.docType = userDoc.docType
            imageViewer.flagAddDoc = false
            let date: NSNumber = NSNumber(value: Date().timeIntervalSince1970)

            imageViewer.datePicker.minimumDate = Date().addingTimeInterval(24 * 60 * 60)
            
            // let dateStr = AppUtility.sharedUtility.readableDateAndTimeFormat(date: date)
            let vehivleobj:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle()
            let strFileName = "\(Int(date))" + "-" + "\(vehivleobj.vehicleId).\(docExtension)"
            imageViewer.docName = strFileName


            imageViewer.callbackForAddedDocTapped(with: {
                self.getdocumentsForDocType(docType: userDoc.docType)
            })
    
            imageViewer.updateView()
            imageViewer.documentImageView.image = choosenImage

            UIApplication.shared.keyWindow?.addSubview(imageViewer)
//            UIApplication.shared.windows.last?.addSubview(imageViewer)
        }else{
            dismiss(animated: true, completion: nil)
            self.view.showIndicatorWithError("Unable to parse selected data")
            return
        }
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Handle notification
    
    @objc func documentDeletedNotification(notification: Notification) -> Void {
        let doctype = notification.object as! String
        
        self.getdocumentsForDocType(docType: doctype)
    }
    
    func handleUpdateDocumentNotification(notification: Notification) -> Void {
        let doctype = notification.object as! String
        self.getdocumentsForDocType(docType: doctype)
        
    }
    
    
}
