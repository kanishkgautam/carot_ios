//
//  MyProfileViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 06/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController, UITextFieldDelegate {
    
    private var isEditMode: Bool = false
    var userObj:CUser = CUser()
    
    @IBOutlet weak var phoneNumberUnderline: UIView!
    @IBOutlet weak var addressFirstUnderline: UIView!
    @IBOutlet weak var addressSecondUnderline: UIView!
    @IBOutlet weak var cityNameUnderline: UIView!
    @IBOutlet weak var zipCodeUnderline: UIView!
    @IBOutlet weak var selectStateUnderline: UIView!
    
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var addressFirstLineTextField: UITextField!
    @IBOutlet weak var addressSecondLineTextField: UITextField!
    @IBOutlet weak var cityNameTextField: UITextField!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var editSaveButton: UIBarButtonItem!
    
    
    @IBOutlet weak var changePasswordBGView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.switchScreenMode()
        configureNavBar()
        getProfileData()
        AppUtility.sharedUtility.trackScreenName(with: "My Profile Screen")
        self.zipCodeTextField.keyboardType = .numberPad
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changePasswordTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.isEditMode = false
        self.switchScreenMode()
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "ChangePasswordTapped", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
        //        let viewcontroller = UIViewController()
        //        viewcontroller.
        
        let viewC:ChangePasswordView = Bundle.main.loadNibNamed("ChangePasswordView", owner: nil, options: nil)?.first as! ChangePasswordView
        viewC.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        UIApplication.shared.windows.last?.addSubview(viewC)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func getProfileData(){
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        self.view.showIndicatorWithProgress(message: "Fetching profile data")
        CUserRequest.sharedInstance().getUserDetailResponse { (responseObj, isSuccess, error) in
            if (isSuccess == true){
                self.view.dismissProgress()
                
                if let user  = responseObj  as? CUser {
                    self.userObj = user
                    self.updateDataOnViewFromServer(userObj: self.userObj)
                }
                
            }else{
                if (responseObj != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObj as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
    }
    
    func updateDataOnViewFromServer(userObj:CUser) {
        firstNameLabel.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.firstName)
        lastNameLabel.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.lastName)
        emailAddressLabel.text = self.userObj.email
        mobileNumberTextField.text = self.userObj.phoneNumber
        addressFirstLineTextField.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.addressLine1)
        addressSecondLineTextField.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.addressLine2)
        cityNameTextField.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.city)
        stateLabel.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.state)
        zipCodeTextField.text = AppUtility.sharedUtility.decodeString(stringToDecode:self.userObj.zipPostalCode)
        
        
        
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func isFieldsValid() -> Bool {
        
        if mobileNumberTextField.text == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Mobile number cannot be empty")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidMobileNumber", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return false
        }
        else {
            if !AppUtility.sharedUtility.isValidPhoneNumber(phoneNumberToBeValidated: mobileNumberTextField.text!) {
                MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Invalid Mobile number")
                AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidMobileNumber", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
                return false
            }
        }
        
        
        if addressFirstLineTextField.text == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Address Line 1 cannot be empty ")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidAdd1", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return false
        }
        else if addressSecondLineTextField.text == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Address Line 2 cannot be empty ")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "IncalidAdd2", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return false
        }
        else if cityNameTextField.text == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "City cannot be empty ")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidCity", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return false
        }
        else if stateLabel.text == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "State cannot be empty.Please select city again")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidState", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return false
        }
        
        if zipCodeTextField.text == "" {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Zipcode cannot be empty ")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidZipcode", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            return false
        }
        else if (zipCodeTextField.text?.count)! < 6 {
            MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Zipcode cannot be less tha 6 characters")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "InvalidZipCode", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
        }
        
        return true
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        
        if isEditMode {
            AppUtility.sharedUtility.sendGAEvent(withEventName: "SaveProfileTapped", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            if !isFieldsValid() {
                return
            }
            
            
            userObj.phoneNumber = ((AppUtility.sharedUtility.trimWhiteSpaces(inString: mobileNumberTextField.text!).count > 0) ? AppUtility.sharedUtility.trimWhiteSpaces(inString: mobileNumberTextField.text!) : "")
            userObj.addressLine1 = ((AppUtility.sharedUtility.trimWhiteSpaces(inString: addressFirstLineTextField.text!).count > 0) ? AppUtility.sharedUtility.trimWhiteSpaces(inString: addressFirstLineTextField.text!) : "")
            userObj.addressLine2 = ((AppUtility.sharedUtility.trimWhiteSpaces(inString: addressSecondLineTextField.text!).count > 0) ? AppUtility.sharedUtility.trimWhiteSpaces(inString: addressSecondLineTextField.text!) : "")
            userObj.city = ((AppUtility.sharedUtility.trimWhiteSpaces(inString: cityNameTextField.text!).count > 0) ? AppUtility.sharedUtility.trimWhiteSpaces(inString: cityNameTextField.text!) : "")
            userObj.state = ((AppUtility.sharedUtility.trimWhiteSpaces(inString: stateLabel.text!).count > 0) ? AppUtility.sharedUtility.trimWhiteSpaces(inString: stateLabel.text!) : "")
            userObj.zipPostalCode = ((AppUtility.sharedUtility.trimWhiteSpaces(inString: zipCodeTextField.text!).count > 0) ? AppUtility.sharedUtility.trimWhiteSpaces(inString: zipCodeTextField.text!) : "")
            
            
            userObj.addressLine1 = AppUtility.sharedUtility.encodeString(stringToEncode: userObj.addressLine1 as NSString) as String?
            userObj.addressLine2 = AppUtility.sharedUtility.encodeString(stringToEncode: userObj.addressLine2 as NSString) as String?
            userObj.city = AppUtility.sharedUtility.encodeString(stringToEncode: userObj.city as NSString) as String?
            userObj.state = AppUtility.sharedUtility.encodeString(stringToEncode: userObj.state as NSString) as String?
            userObj.firstName = AppUtility.sharedUtility.encodeString(stringToEncode: userObj.firstName as NSString) as String?
            userObj.lastName = AppUtility.sharedUtility.encodeString(stringToEncode: userObj.lastName as NSString) as String?
            
            
            self.view.showIndicatorWithProgress(message: "Updating User Info")
            
            CUserRequest.sharedInstance().editUserProfile(withUserInfo: userObj, response: { (responseObj, isSuccess, error) in
                if (isSuccess == true){
                    self.view.showIndicatorWithSuccess("Record updated sucessfully")
                    self.isEditMode = false
                    self.switchScreenMode()
                }else{
                    if (responseObj != nil){
                        self.view.dismissProgress()
                        MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObj as! NSDictionary))
                    }else{
                        
                        if let errorField = (error! as NSError).userInfo["field"] as? String {
                            if errorField == "ZIP" {
                                self.view.showIndicatorWithError("Please enter a valid ZIP code.")
                                
                            }
                            else {
                                self.view.dismissProgress()
                            }
                        }
                        else {
                            self.view.showIndicatorWithError(error?.localizedDescription)
                            
                        }
                    }
                    
                }
            })
            
        }else{
            isEditMode = !isEditMode
            AppUtility.sharedUtility.sendGAEvent(withEventName: "EditProfileTapped", category: kEventCategoryMyProfile, andScreenName: "My Profile Screen", eventLabel: nil, eventValue: nil)
            self.switchScreenMode()
        }
        
    }
    
    func switchScreenMode() {
        
        if !isEditMode {
            phoneNumberUnderline.isHidden = true
            addressFirstUnderline.isHidden = true
            addressSecondUnderline.isHidden = true
            cityNameUnderline.isHidden = true
            zipCodeUnderline.isHidden = true
            selectStateUnderline.isHidden = true
            
            mobileNumberTextField.isUserInteractionEnabled = false
            addressFirstLineTextField.isUserInteractionEnabled = false
            addressSecondLineTextField.isUserInteractionEnabled = false
            cityNameTextField.isUserInteractionEnabled = false
            zipCodeTextField.isUserInteractionEnabled = false
            
            mobileNumberTextField.becomeFirstResponder()
            editSaveButton.image = #imageLiteral(resourceName: "myProfileEditIcon")
            
        }else{
            
            phoneNumberUnderline.isHidden = false
            addressFirstUnderline.isHidden = false
            addressSecondUnderline.isHidden = false
            cityNameUnderline.isHidden = false
            zipCodeUnderline.isHidden = false
            selectStateUnderline.isHidden = false
            
            mobileNumberTextField.isUserInteractionEnabled = true
            addressFirstLineTextField.isUserInteractionEnabled = true
            addressSecondLineTextField.isUserInteractionEnabled = true
            cityNameTextField.isUserInteractionEnabled = true
            zipCodeTextField.isUserInteractionEnabled = true
            editSaveButton.image = #imageLiteral(resourceName: "myProfileSaveIcon")
        }
    }
    
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "MY PROFILE"
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
    }
    
    
    //MARK: UITextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cityNameTextField {
            
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CitySelectionVC") as! CitySelectionVC
            
            viewController.citySelectionCallback(withCityCallback: { (city) in
                self.cityNameTextField.text = city
            })
            
            viewController.setStateSelctionCallback(withStateCallback: { (state) in
                self.stateLabel.text = state
                
            })
            
            self.navigationController?.pushViewController(viewController, animated: true)
            //            self.navigationController?.present(viewController, animated: true, completion: nil)
            
            self.view.endEditing(true)
        }
        
    }
}
