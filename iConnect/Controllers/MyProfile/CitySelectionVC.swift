//
//  CitySelectionVC.swift
//  iConnect
//
//  Created by Vivek Joshi on 13/04/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

typealias StateSelectedCallback = (_ selectedState: String) -> Void
typealias CitySelectedCallback = (_ selectedCity: String) -> Void

class CitySelectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var stateCityDict = NSMutableDictionary()
    
    lazy var resultsArray = Array<String>()
    
    lazy var allStates = Array<String>()
    
    var isStateSelected = false
    
    var getStateCallback: StateSelectedCallback?
    var getCityCallback: CitySelectedCallback?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "City Selection Screen")
        // Do any additional setup after loading the view.
        configureNavBar()
        getStateCity()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        headerTitle.text = isStateSelected ? "SELECT A STATE" : "SELECT A CITY"
        headerTitle.sizeToFit()
        
        self.navigationItem.titleView = headerTitle
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
    }
    
    @objc func backButtonTapped() {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func getStateCity() -> Void {
        self.view.showIndicatorWithProgress(message: "")
        CUserRequest.sharedInstance().getStateCityMapResponse { (responseDict, isSuccess, error) in
            self.view.dismissProgress()
            if isSuccess {
                self.stateCityDict = (responseDict as! NSDictionary).mutableCopy() as! NSMutableDictionary
            }
            else {
                
            }
        }
    }
    
    func getStateName(searchText: String) -> Void {
        let predicate = NSPredicate(format: "SELF contains %@", searchText)
        
        let states = self.stateCityDict.allKeys as NSArray
        
        let filteredArray: Array = states.filtered(using: predicate)
        
        if filteredArray.count > 0 {
            self.resultsArray.append(contentsOf:filteredArray as! [String])
        }
        
    }
    
    func getCityName(searchText: String) -> Void {
        
        self.resultsArray.removeAll()
        
        let predicate = NSPredicate(format: "SELF contains %@", searchText)
        
        
        for key in self.stateCityDict.allKeys {
            
            let cities = self.stateCityDict.object(forKey: key as! String) as! NSArray
            
            let filteredArray: Array = cities.filtered(using: predicate)
            
            if filteredArray.count > 0 {
                self.resultsArray.append(contentsOf:filteredArray as! [String])
            }
            
            
        }
        
        self.tableView.reloadData()
        
    }
    
    func getStateForCity(city: String) -> String {
        let predicate = NSPredicate(format: "SELF == %@", city)
        
        
        for key in self.stateCityDict.allKeys {
            
            let cities = self.stateCityDict.object(forKey: key as! String) as! NSArray
            
            let filteredArray: Array = cities.filtered(using: predicate)
            
            if filteredArray.count > 0 {
                
                if (filteredArray.first as! String) == city {
                    
                    return key as! String
                }
            }
        }
        
        return ""
    }
    
    
    //MARK: UITableview Datasource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier")
        
        if cell == nil {
            cell = UITableViewCell()
        }
        
        cell!.textLabel?.font = UIFont(name: "Helvetica", size: 11)
        cell!.textLabel?.text = self.resultsArray[indexPath.row]
        cell!.selectionStyle = .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)

        if self.isStateSelected {
            
            if getStateCallback != nil {
                self.getStateCallback!(self.resultsArray[indexPath.row])
            }
        }
        else {
            if getCityCallback != nil {
                self.getCityCallback!(self.resultsArray[indexPath.row])
            }
            if getStateCallback != nil {
                self.getStateCallback!(self.getStateForCity(city: self.resultsArray[indexPath.row]))
            }
        }
        
        self.backButtonTapped()
    }
    
    func setStateSelctionCallback(withStateCallback state: @escaping StateSelectedCallback) -> Void {
        self.getStateCallback = state
    }
    
    func citySelectionCallback(withCityCallback city: @escaping CitySelectedCallback) -> Void {
        self.getCityCallback = city
        
    }
    
    //MARK:- UITextField Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if isStateSelected {
            self.getStateName(searchText: searchText)
        }
        else {
            self.getCityName(searchText: searchText)
            
        }
    }
    
}
