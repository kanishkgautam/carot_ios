//
//  EmbededWebViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 27/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class EmbededWebViewController: UIViewController {
    
    var urlToOpen:String?
    
    var navigationTitle: String?
    
    var request: URLRequest?
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if navigationTitle != nil {
              //  configureNavBar()
        }
        
        if request != nil {
            webView.loadRequest(request!)
        }
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = self.navigationTitle!
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if urlToOpen != nil {
            webView.loadRequest(URLRequest(url: NSURL(string: urlToOpen!) as! URL))
        }
    }
    
    
}
