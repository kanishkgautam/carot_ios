//
//  EmergencyViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 16/03/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import AddressBookUI
import IQKeyboardManager

class EmergencyViewController: UIViewController, UITextFieldDelegate, ABPeoplePickerNavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var myMobileNumber: UITextField!
    
    @IBOutlet weak var pref1Name: UITextField!
    @IBOutlet weak var pref1Number: UITextField!
    
    @IBOutlet weak var pref2Name: UITextField!
    @IBOutlet weak var pref2Number: UITextField!
    
    @IBOutlet weak var pref3Name: UITextField!
    @IBOutlet weak var pref3Number: UITextField!
    
    @IBOutlet weak var pref4Name: UITextField!
    @IBOutlet weak var pref4Number: UITextField!
    
    @IBOutlet weak var eventsButton: UIButton!
    
    var isContactPermissionGranted = false
    
    var selectdTextField: UITextField?
    
    var contactsArray = NSMutableArray()
    
    var textFieldTapGesture : UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Emergency Contacts Screen")
        initialiseVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        self.getPermissionForContactAccess()
        IQKeyboardManager.shared().isEnableAutoToolbar = false

        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        
        if let readStatus = AppUtility.sharedUtility.allEventsRead {
            if readStatus {
                self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
            }
            else {
                self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                
            }
        }
        self.getListOfUnreadEvents()
        
        
        
        if let vehivleobj:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle() {
            print(vehivleobj)
            if let _ = self.navigationItem.titleView {
                self.getAllEmergencyContacts()
                self.getMyPhoneNumber()
            }
            else {
            }
            if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehivleobj){
                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
            }
            
        }
        else {
            CommonData.sharedData.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                for vehicle in vehicles{
                    
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle){
                        AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                    }
                }
            })
            
            //            CommonData.sharedData.getAllVehicles { (vehicles) in
            //                if vehicles.count > 0 {
            //                    self.arrayMake = AppUtility.sharedUtility.arrayMake
            //
            //                    self.navigationItem.titleView = self.createHeaderView()
            //                    self.getVehicleHealth()
            //                }
            //            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    func getListOfUnreadEvents() -> Void {
        
        let dateString = CommonData.sharedData.lastEventTimeStamp
        
        CEventRequest.sharedInstance().getNewEventsCount(afterTime: dateString) { (responseData, isSuccess, error) in
            
            if isSuccess {
                
                let eventCount = responseData as? NSNumber
                
                if eventCount == 0 {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = true
                }
                else {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = false
                }
            }
            else {
                
            }
        }
    }
    
    func initialiseVC() -> Void {
        
        self.myMobileNumber.isUserInteractionEnabled = false
        self.configureNavBar()
        
        //        textFieldTapGesture = UITapGestureRecognizer(target: self, action: #selector())
        //        textFieldTapGesture!.delegate = self
        //        pref1Name.addGestureRecognizer(textFieldTapGesture!)
        //        pref2Name.addGestureRecognizer(textFieldTapGesture!)
        //        pref3Name.addGestureRecognizer(textFieldTapGesture!)
        //        pref4Name.addGestureRecognizer(textFieldTapGesture!)
        //
        //        pref1Number.superview?.addGestureRecognizer(textFieldTapGesture!)
        //        pref2Number.addGestureRecognizer(textFieldTapGesture!)
        //        pref3Number.addGestureRecognizer(textFieldTapGesture!)
        //        pref4Number.addGestureRecognizer(textFieldTapGesture!)
        
        pref1Number.addTarget(self, action: #selector(textFieldHasBegunEditing(textField:)), for: .editingDidBegin)
        pref2Number.addTarget(self, action: #selector(textFieldHasBegunEditing(textField:)), for: .editingDidBegin)
        pref3Number.addTarget(self, action: #selector(textFieldHasBegunEditing(textField:)), for: .editingDidBegin)
        pref4Number.addTarget(self, action: #selector(textFieldHasBegunEditing(textField:)), for: .editingDidBegin)
        
        
        
    }
    
    func getMyPhoneNumber() -> Void {
        CUserRequest.sharedInstance().getUserDetailResponse { (responseObj, isSuccess, error) in
            print(responseObj ?? "")
            if let user:CUser = responseObj as? CUser {
                if user.phoneNumber != nil {
                    self.myMobileNumber.text = user.phoneNumber!
                }
            }
            //            self.myMobileNumber.text = ""
        }
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "EMERGENCY CONTACTS"
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
        
    }
    
    
    func getPermissionForContactAccess() -> Void {
        
        let permission = ABAddressBookGetAuthorizationStatus()
        
        if permission == .authorized {
            self.isContactPermissionGranted = true
        }
        else if permission == .notDetermined {
            let addressBookRef = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            
            ABAddressBookRequestAccessWithCompletion(addressBookRef) { (granted, error) in
                
                self.isContactPermissionGranted = granted
                
            }
        }
        else {
            self.view.showIndicatorWithError("Please goto Settings and give Carot permission to access contacts")
        }
    }
    
    
    func showContactsPickerForTextFields(textField: UITextField) -> Void {
        
        let peoplePicker = ABPeoplePickerNavigationController()
        peoplePicker.peoplePickerDelegate = self
        peoplePicker.displayedProperties = [NSNumber(value: kABPersonPhoneProperty),NSNumber(value: kABPersonFirstNameProperty),NSNumber(value: kABPersonLastNameProperty)]
        self.present(peoplePicker, animated: true) {
            
        }
    }
    
    
    func getAllEmergencyContacts() -> Void {
        
        CUserRequest.sharedInstance().getAllEmergencyContactsResponse { (responseData, isSuccess, error) in
            
            if isSuccess {
                self.view.dismissProgress()
                
                self.contactsArray = responseData as! NSMutableArray
                self.updateDataOnView(contactArray: responseData as! NSArray)
            }
            else {
                self.view.dismissProgress()
            }
            self.enableNextField()
        }
    }
    
    func updateDataOnView(contactArray: NSArray) -> Void {
        clearData()
        
        for contact in (contactArray as? [CUserContacts])!{
            
            var name = ""
            
            if let _ = contact.lastName {
                
                if contact.lastName == "" || contact.lastName == " " {
                    name = contact.firstName
                }
                else {
                    name = contact.firstName + " " + contact.lastName
                }
            }
            else {
                name = contact.firstName
            }
            
            switch contact.preference {
                
            case 1:
                self.pref1Name.text = name
                self.pref1Number.text = contact.contactNumber
            case 2:
                self.pref2Name.text = name
                self.pref2Number.text = contact.contactNumber
            case 3:
                self.pref3Name.text = name
                self.pref3Number.text = contact.contactNumber
            case 4:
                self.pref4Name.text = name
                self.pref4Number.text = contact.contactNumber
                
            default:
                break
            }
        }
    }
    
    func clearData() -> Void {
        self.pref1Name.text = ""
        self.pref1Number.text = ""
        self.pref2Name.text = ""
        self.pref2Number.text = ""
        self.pref3Name.text = ""
        self.pref3Number.text = ""
        self.pref4Name.text = ""
        self.pref4Number.text = ""
        
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        
        let notificationName = Notification.Name("toggleLeftMenu")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
    }
    
    @IBAction func eventsButtonTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertsNavController")
        vc.modalPresentationStyle = .fullScreen;
        self.navigationController?.present(vc, animated: true, completion: {
        })
    }
    
    
    //MARK:- ABPeoplePickerNavigationController Delegate
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier) {
        self.view.endEditing(true)
        
        var mobNumber = ""
        var firstName = ""
        var lastName = ""
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        let index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        //  let email = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as! String
        
        if let mobNo = ABMultiValueCopyValueAtIndex(multiValue, index) {
            mobNumber = mobNo.takeRetainedValue() as! String
            
        }
        
        if let fName = ABRecordCopyValue(person, kABPersonFirstNameProperty) {
            firstName = fName.takeRetainedValue() as! String
        }
        
        if let lName = ABRecordCopyValue(person, kABPersonLastNameProperty) {
            lastName = lName.takeRetainedValue() as! String
        }
        
        
        let contact = CUserContacts()
        contact.firstName = firstName.trimmingCharacters(in: .whitespacesAndNewlines)
        contact.lastName = lastName.trimmingCharacters(in: .whitespacesAndNewlines)
        
        // NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""]
        mobNumber = AppUtility.sharedUtility.trimWhiteSpaces(inString: mobNumber)
        
        let charSet = CharacterSet(charactersIn: "0123456789+").inverted
        let components = mobNumber.components(separatedBy: charSet)
        
        contact.contactNumber = components.joined(separator: "")
        contact.preference = self.selectdTextField?.tag as NSNumber?
        
        self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
        
    }
    
    func addContact(contact: CUserContacts) -> Void {
        self.view.showIndicatorWithProgress(message: "Adding contact")
        
        CUserRequest.sharedInstance().addNewEmergencyContact(contact) { (responseData, isSuccess, error) in
            self.view.dismissProgress()
            
            if isSuccess {
                self.addContactInArray(contact: responseData as! CUserContacts)
                self.setDataForSelectedField(contact: contact)
                
            }
            else {
                
            }
            self.enableNextField()
            
        }
    }
    
    func updateContact(contact: CUserContacts) -> Void {
        self.view.showIndicatorWithProgress(message: "Updating contact")
        if contact.lastName == nil {
            contact.lastName = ""
        }
        
        CUserRequest.sharedInstance().updateEmergencyContact(contact) { (responseData, isSuccess, error) in
            
            self.view.dismissProgress()
            
            if isSuccess {
                
                self.addContactInArray(contact: contact)
                self.setDataForSelectedField(contact: contact)
            }
            else {
                
            }
        }
    }
    
    func setDataForSelectedField(contact: CUserContacts) -> Void {
        
        var name = ""
        
        if let _ = contact.lastName {
            
            if contact.lastName == "" || contact.lastName == " " {
                name = contact.firstName
            }
            else {
                name = contact.firstName + " " + contact.lastName
            }
        }
        else {
            name = contact.firstName
        }
        
        if self.selectdTextField == self.pref1Name || self.selectdTextField == self.pref1Number {
            self.pref1Name.text = name
            self.pref1Number.text = contact.contactNumber
        }
        else if self.selectdTextField == self.pref2Name || self.selectdTextField == self.pref2Number {
            self.pref2Name.text = name
            self.pref2Number.text = contact.contactNumber
        }
        else if self.selectdTextField == self.pref3Name || self.selectdTextField == self.pref3Number {
            self.pref3Name.text = name
            self.pref3Number.text = contact.contactNumber
        }
        else if self.selectdTextField == self.pref4Name || self.selectdTextField == self.pref4Number {
            self.pref4Name.text = name
            self.pref4Number.text = contact.contactNumber
        }
        
    }
    
    func enableNextField() -> Void {
        
        self.pref1Name.isUserInteractionEnabled = true
        self.pref1Number.isUserInteractionEnabled = true
        
        switch self.contactsArray.count {
        case 0:
            
            self.pref2Name.isUserInteractionEnabled = false
            self.pref2Number.isUserInteractionEnabled = false
            
            self.pref3Name.isUserInteractionEnabled = false
            self.pref3Number.isUserInteractionEnabled = false
            
            self.pref4Name.isUserInteractionEnabled = false
            self.pref4Number.isUserInteractionEnabled = false
            
        case 1:
            
            self.pref2Name.isUserInteractionEnabled = true
            self.pref2Number.isUserInteractionEnabled = true
            
            self.pref3Name.isUserInteractionEnabled = false
            self.pref3Number.isUserInteractionEnabled = false
            
            self.pref4Name.isUserInteractionEnabled = false
            self.pref4Number.isUserInteractionEnabled = false
            
        case 2:
            
            self.pref2Name.isUserInteractionEnabled = true
            self.pref2Number.isUserInteractionEnabled = true
            
            self.pref3Name.isUserInteractionEnabled = true
            self.pref3Number.isUserInteractionEnabled = true
            
            self.pref4Name.isUserInteractionEnabled = false
            self.pref4Number.isUserInteractionEnabled = false
            
        case 3:
            
            self.pref2Name.isUserInteractionEnabled = true
            self.pref2Number.isUserInteractionEnabled = true
            
            self.pref3Name.isUserInteractionEnabled = true
            self.pref3Number.isUserInteractionEnabled = true
            
            self.pref4Name.isUserInteractionEnabled = true
            self.pref4Number.isUserInteractionEnabled = true
            
        default:
            break
        }
        
    }
    
    func addContactInArray(contact: CUserContacts) -> Void {
        
        if self.contactsArray.count == 0 {
            self.contactsArray.add(contact)
            return
        }
        
        var availableContact: CUserContacts?
        
        for emContact in self.contactsArray {
            if (emContact as! CUserContacts).contactId == contact.contactId {
                availableContact = contact
                self.contactsArray.remove(emContact)
                break
            }
        }
        
        self.contactsArray.add(availableContact ?? contact)
        
    }
    
    func doneButtonTapped() -> Void {
        
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
    }
    
    
    //MARK:- UITextField Delegate
    
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //
    //
    //        if textField == pref1Name || textField == pref2Name || textField == pref3Name || textField == pref4Name  {
    //
    //        }
    //        else {
    //            if isContactPermissionGranted {
    //                self.view.endEditing(true)
    //
    //                self.selectdTextField = textField
    //                self.showContactsPickerForTextFields(textField: textField)
    //
    //                return false
    //            }
    //            else {
    //
    //                return true
    //            }
    //        }
    //        return true
    //
    //    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true
        }
        
        switch textField {
        case pref1Name,pref2Name,pref3Name,pref4Name:
            
            return true
        case pref1Number,pref2Number,pref3Number,pref4Number:
            
            if !"0123456789".contains(string) {
                return false
            }
            
            break
        default:
            break
        }
        
        
        
        //            switch textField {
        //            case pref1Number,pref2Number,pref3Number,pref4Number :
        //
        //                if  { //AppUtility.sharedUtility.isValidPhoneNumber(phoneNumberToBeValidated: string) {
        //                    return true
        //                }
        //                else {
        //                    return false
        //                }
        //
        //            default:
        //                return true
        //            }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //self.view.endEditing(true)
        let preferenceId = NSNumber(integerLiteral: textField.tag)
        
        let contact: CUserContacts = self.getContactWithPrefId(preferenceId: preferenceId)
        
        if textField == self.pref1Name {
            self.pref1Number.becomeFirstResponder()
        }
        else if textField == self.pref1Number {
            self.view.endEditing(true)
            
            self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
        }
        else if textField == self.pref2Name {
            self.pref2Number.becomeFirstResponder()
        }
        else if textField == self.pref2Number {
            self.view.endEditing(true)
            
            self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
            
        }
        else if textField == self.pref3Name {
            self.pref3Number.becomeFirstResponder()
        }
        else if textField == self.pref3Number {
            self.view.endEditing(true)
            
            self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
            
        }
        else if textField == self.pref4Name {
            self.pref4Number.becomeFirstResponder()
        }
        else if textField == self.pref4Number {
            self.view.endEditing(true)
            
            self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
            
        }
        
        return true
    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //
    //        let preferenceId = NSNumber(integerLiteral: textField.tag)
    //
    //        let contact: CUserContacts = self.getContactWithPrefId(preferenceId: preferenceId)
    //
    ////        IQKeyboardManager.shared().goNext()
    ////
    ////        return
    //       // if !self.isContactPermissionGranted {
    //            if textField == self.pref1Name {
    //                self.pref1Number.becomeFirstResponder()
    //            }
    //            else if textField == self.pref1Number {
    //                self.view.endEditing(true)
    //
    //                self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
    //            }
    //            else if textField == self.pref2Name {
    //                self.pref2Number.becomeFirstResponder()
    //            }
    //            else if textField == self.pref2Number {
    //                self.view.endEditing(true)
    //
    //                self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
    //
    //            }
    //            else if textField == self.pref3Name {
    //                self.pref3Number.becomeFirstResponder()
    //            }
    //            else if textField == self.pref3Number {
    //                self.view.endEditing(true)
    //
    //                self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
    //
    //            }
    //            else if textField == self.pref4Name {
    //                self.pref4Number.becomeFirstResponder()
    //            }
    //            else if textField == self.pref4Number {
    //                self.view.endEditing(true)
    //
    //                self.addOrUpdateContact(contact: contact, preferenceId: contact.preference)
    //
    //            }
    //      //  }
    //    }
    
    func getContactWithPrefId(preferenceId: NSNumber) -> CUserContacts {
        
        let contact = CUserContacts()
        contact.preference = preferenceId
        
        switch preferenceId {
        case 1:
            contact.firstName = self.pref1Name.text
            contact.contactNumber = self.pref1Number.text
            
        case 2:
            contact.firstName = self.pref2Name.text
            contact.contactNumber = self.pref2Number.text
        case 3:
            contact.firstName = self.pref3Name.text
            contact.contactNumber = self.pref3Number.text
        case 4:
            contact.firstName  = self.pref4Name.text
            contact.contactNumber = self.pref4Number.text
        default:
            break
            
        }
        
        return contact
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func addOrUpdateContact(contact: CUserContacts, preferenceId: NSNumber) -> Void {
        
        if contact.firstName == nil || contact.firstName == "" {
            
            if contact.contactNumber == nil || contact.contactNumber == "" {
                return
                
            }
            else {
                self.view.showIndicatorWithError("First name cannot be empty")
                return
            }
        }
        else if contact.contactNumber == nil || contact.contactNumber == "" {
            self.view.showIndicatorWithError("Contact number cannot be empty")
            return
        }
        
//        if !AppUtility.sharedUtility.isValidPhoneNumber(phoneNumberToBeValidated: contact.contactNumber) {
//            self.view.showIndicatorWithError("Please enter a valid phone number")
//            return
//        }
        
        self.view.endEditing(true)
        
        for prevContact in self.contactsArray {
            
            if (prevContact as! CUserContacts).preference == preferenceId {
                contact.contactId = (prevContact as! CUserContacts).contactId
                self.updateContact(contact: contact)
                return
            }
        }
        
        self.addContact(contact: contact)
    }
    
    //MARK:- UIGestureRecognizer Delegate
    
    //    func textFieldTapGestureAction(gesture: UITapGestureRecognizer) -> Void {
    //
    //        if let tappedView = gesture.view as? UITextField {
    //
    //            switch tappedView {
    //            case pref1Name,pref2Name,pref3Name,pref4Name:
    //                tappedView.becomeFirstResponder()
    //                break
    //            case pref1Number,pref2Number,pref3Number,pref4Number:
    //                if isContactPermissionGranted {
    //                    self.view.endEditing(true)
    //
    //                    self.selectdTextField = tappedView
    //                    self.showContactsPickerForTextFields(textField: tappedView)
    //
    //                }
    //                break
    //            default:
    //                break
    //            }
    //
    //        }
    
    //     }
    
    //    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //        if gestureRecognizer == textFieldTapGesture {
    //
    //            return true
    //        }
    //        else {
    //            return false
    //        }
    //    }
    
    
    @objc private func textFieldHasBegunEditing(textField: UITextField) -> Void {
        // self.view.endEditing(true)
        
        if isContactPermissionGranted {
            
            self.selectdTextField = textField
            self.showContactsPickerForTextFields(textField: textField)
            textField.endEditing(true)
            //return false
        }
        else {
            // return true
        }
        
    }
}
