
//
//  TripsViewController.swift
//  iConnect
//
//  Created by Administrator on 2/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {
    
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelAverageSpeed: UILabel!
    @IBOutlet weak var labelIdleTime: UILabel!
    
    @IBOutlet weak var labelStartTime: UILabel!
    @IBOutlet weak var labelStartDate: UILabel!
    
    @IBOutlet weak var idleTimeViewObj: UIView!
    
    @IBOutlet weak var labelEndTime: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    
    @IBOutlet weak var labelStartAddress: UILabel!
    @IBOutlet weak var labelEndAddress: UILabel!
    
    @IBOutlet weak var progressBackgroundView: LoaderView!
    @IBOutlet weak var progressView: LoaderView!
    
    @IBOutlet weak var labelTripScore: UILabel!
    
    @IBOutlet weak var buttonViewMap: UIButton!
    @IBOutlet weak var buttonNavigate: UIButton!
    @IBOutlet weak var buttonShare: UIButton!
    
    @IBOutlet weak var tripScoreButton: UIButton!
    
    func beautifyView() {
        
        bgView.layer.cornerRadius = 5.0
        bgView.layer.masksToBounds = true
        
        self.progressBackgroundView.loaderColor = UIColor(red:0.89, green:0.91, blue:0.92, alpha:1.0).cgColor
        self.progressBackgroundView.progress = 1.0
        self.progressBackgroundView.circlePathLayer.lineWidth = 1
        self.progressView.circlePathLayer.lineWidth = 1
        
        
        self.progressView.loaderColor = UIColor(red:0.97, green:0.58, blue:0.11, alpha:1.0).cgColor
        self.progressView.progress = 0.0
        
        //        roundedBackgroundView.layer.cornerRadius = 3.0
        //        // drop shadow
        //        roundedBackgroundView.layer.shadowColor = UIColor.black.cgColor
        //        roundedBackgroundView.layer.shadowOpacity = 0.2
        //        roundedBackgroundView.layer.shadowRadius = 1.0
        //        roundedBackgroundView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        //
        
        self.labelTripScore.text = ""
        
    }
    
    
    //    override func prepareForReuse() {
    //        self.labelTripScore.text = ""
    //        // DispatchQueue.main.async {
    //        self.progressView.progress = 0.0
    //
    //
    //    }
    
    func setDriverScoreForTrip(tripObj: CTrip) {
        bgView.layer.cornerRadius = 5.0
        bgView.layer.masksToBounds = true
        
        self.progressBackgroundView.loaderColor = UIColor(red:0.89, green:0.91, blue:0.92, alpha:1.0).cgColor
        self.progressBackgroundView.progress = 1.0
        self.progressBackgroundView.circlePathLayer.lineWidth = 1
        self.progressView.circlePathLayer.lineWidth = 1
        
        self.progressView.loaderColor = UIColor(red:0.97, green:0.58, blue:0.11, alpha:1.0).cgColor
        
        if tripObj.driveScore == nil {
            self.labelTripScore.text = "..."
            self.progressView.progress = 0.0
            
            
            CTripRequest.sharedInstance().getTripScore(for: tripObj, withCallback: { (updatedTrip) in
                if let score = updatedTrip?.driveScore {
                    if score.isScorable {
                        
                        self.labelTripScore.text = "\(Int(score.pScore))%"
                        self.progressView.progress = CGFloat(score.pScore)/100
                        
                    }
                    else {
                        
                        self.labelTripScore.text = "N/A"
                        self.progressView.progress = 0.0
                    }
                } else {
                    self.labelTripScore.text = "..."
                    self.progressView.progress = 0.0
                    
                }
            })
            
        }else{
            let score = tripObj.driveScore!
            if tripObj.driveScore.isScorable {
                
                self.labelTripScore.text = "\(Int(score.pScore))%"
                self.progressView.progress = CGFloat(score.pScore)/100
                
            }
            else {
                
                self.labelTripScore.text = "N/A"
                self.progressView.progress = 0.0
            }
        }
    }
}

class TripsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate {
    
    var vehicleHeader:HeaderPickerArea?
    var presentationDelegate = PickerControllerPresentationManager()
    
    var selectedVehicleMake: Int = 0
    
    var arrayMake:Array = [String]()
    
    var arrayTrips = [Any]()
    
    var startDate : Date?
    var endDate : Date?
    
    let cellIdentifier = "TripCellIdentifier"
    
    @IBOutlet weak var tableView: UITableView!
    
    var isMovedToTripDetail = false

    //Filter UI Elements
    
    @IBOutlet var datePickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var filterTitleLabel: UILabel!
    
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    
    @IBOutlet weak var dateViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var NoTripsLabel: UILabel!
    
    @IBOutlet weak var eventsButton: UIButton!
    var isDatePickerVisible = false
    
    var headerButton:UIButton = UIButton(type: .custom)
    
    var lastLocation:Location?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(refreshControl:)), for: UIControl.Event.valueChanged)
        
        return refreshControl
    }()
    
    @IBOutlet weak var datePickerTitleLabel: UILabel!
    
    @IBOutlet weak var dateView: UIView!
    
    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var filterBgView: UIView!
    
    @IBOutlet weak var filterViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var applyButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Trips Screen")
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.filterViewHeightConstraint.constant = 40
        self.dateView.isHidden = true
        
        self.filterBgView.layer.cornerRadius = 5.0
        self.filterBgView.layer.masksToBounds = true
        
        self.clearButton.isHidden = true
        
        NoTripsLabel.isHidden = true
        
//        CommonData.sharedData.getSelectedVehicle { (selectedVehicle) in
//            if selectedVehicle != nil {
                self.navigationItem.titleView = self.createHeaderView()
//            }
//        }
        
        self.applyButton.layer.cornerRadius = 2.0
        self.applyButton.layer.borderColor = UIColor.black.cgColor
        self.applyButton.layer.borderWidth = 1.0
        self.applyButton.layer.masksToBounds = true
        
        //Show trips from last year and to today
        self.resetFilterDates()
        
        self.addPullToRefresh()
        
    }
    
    func resetFilterDates() -> Void {
        
        
        //let lastYear = Calendar.current.date(byAdding: .day, value: -365, to: Date())
        self.startDate = AppUtility.sharedUtility.getStartOfDay(startDateObj: Date()) //lastYear!
        self.endDate = AppUtility.sharedUtility.getEndOfDay(endDateObj: Date())
        self.fromLabel.text = self.stringFromDate(date: self.startDate!)
        self.toLabel.text = self.stringFromDate(date: self.endDate!)
        
    }
    
    func checkIfNoTripsLAbelIsToBeDisplayed() {
        if arrayTrips.count < 1 {
            NoTripsLabel.isHidden = false
        }else{
            NoTripsLabel.isHidden = true
        }
    }
    
    func addPullToRefresh(){
        self.tableView.addSubview(self.refreshControl)
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        
        if self.filterButton.isSelected {
            self.fetchTrips(from: self.startDate!, end: self.endDate!)
        }
        else {
            self.getTripList()
            
        }
    }
    
    func createHeaderView() -> UIButton {
        
        headerButton.titleLabel?.font = UIFont(name: "Helvetica", size: 13)
        headerButton.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        self.setHeaderTitle(title: nil)
        
        return headerButton
    }
    
    func setHeaderTitle(title: String?) -> Void {
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            
            if vehicles.count <= 1 {
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!), for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name), for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first!.name), for: .normal)
                        }
                    })
                }
            }else{
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!) + " ▼" , for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name) + " ▼" , for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first!.name) + " ▼" , for: .normal)
                        }
                    })
                }
            }
        }
        //self.headerButton.titleLabel?.sizeToFit()
        self.headerButton.sizeToFit()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        if let readStatus = AppUtility.sharedUtility.allEventsRead {
            
            if readStatus {
                self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
            }
            else {
                self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)

            }
        }
        self.getListOfUnreadEvents()
        
        
        CommonData.sharedData.getSelectedVehicle { (selectedVehicle) in
            self.setHeaderTitle(title: nil)
            
            if selectedVehicle != nil {
                if let _ = self.navigationItem.titleView {
                    
                    if self.isMovedToTripDetail == false {
                        self.getTripList()
                    }
                    self.setHeaderTitle(title: selectedVehicle?.name!)
                }else{
                    self.navigationItem.titleView = self.createHeaderView()
                }
                if let device = CVehicleRequest.sharedInstance().getDeviceFor(selectedVehicle){
                    AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                }

            }else{
                    CommonData.sharedData.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                        
                        for vehicle in vehicles{
                            
                            if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle){
                                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                            }
                        }
                    })
                
            }
        }
        
        if isMovedToTripDetail == true {
            isMovedToTripDetail = false
        }
    }
    
    func getListOfUnreadEvents() -> Void {
        
        let dateString = CommonData.sharedData.lastEventTimeStamp
        
        CEventRequest.sharedInstance().getNewEventsCount(afterTime: dateString) { (responseData, isSuccess, error) in
            
            if isSuccess {
                
                let eventCount = responseData as? NSNumber
                
                if eventCount == 0 {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = true
                }
                else {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = false
                }
            }
            else {
                
                
            }
        }
    }
    
    func getTripList() -> Void {
        
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            self.refreshControl.endRefreshing()

            return
        }
        
        self.arrayTrips.removeAll()
        self.tableView.reloadData()
        
        if filterButton.isSelected {
            self.fetchTrips(from: self.startDate!, end: self.endDate!)
            
            return
        }
        
        CTripRequest.sharedInstance().getTrips(20, response: { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            
            self.refreshControl.endRefreshing()
            
            if(error == nil && responseObject != nil){
                
                print(responseObject ?? NSArray())
                self.arrayTrips = responseObject as! [CTrip]
                DispatchQueue.main.async {
                    self.checkIfNoTripsLAbelIsToBeDisplayed()
                    self.tableView.reloadData()
                }
            } else {
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }) { (responseRefresh, errorAny) in
            
            self.view.dismissProgress()
            if(errorAny == nil && responseRefresh != nil){
                
                let newTrips = responseRefresh as! [CTrip]
                
                
                for trip in newTrips {
                    if !self.isTripArrayContainsTrip(newTrip: trip) {
                        self.arrayTrips.append(trip)
                    }
                }
                
                DispatchQueue.main.async {
                    self.checkIfNoTripsLAbelIsToBeDisplayed()
                    self.tableView.reloadData()
                    
                }
            } else {
                self.view.showIndicatorWithError(errorAny?.localizedDescription)
            }
        }
    }
    
    func isTripArrayContainsTrip(newTrip: CTrip) -> Bool {
        if arrayTrips.count == 0 {
            return false
        }
        
        for i in 0...self.arrayTrips.count-1 {
            
            let cTrip = self.arrayTrips[i] as! CTrip
            if cTrip.tripId == newTrip.tripId {
                self.arrayTrips[i] = newTrip
                return true
            }
        }
        return false
        
    }

    
    
    func showFilterView() -> Void {
        
        UIView.animate(withDuration: 0.5) {
            
            self.filterViewHeightConstraint.constant = 125
            self.view.layoutIfNeeded()
            
        }
        self.dateView.isHidden = false
        
    }
    
    func hideFilterView() -> Void {
        UIView.animate(withDuration: 0.25) {
            
            self.filterViewHeightConstraint.constant = 40
            self.view.layoutIfNeeded()
            self.dateView.isHidden = true
            
        }
    }
    
    @IBAction func headerTapped(_ sender:Any) {
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            if vehicles.count <= 1 {
                return
            }else{
                let pickerView:CustomPickerView = CustomPickerView()
                pickerView.transitioningDelegate = self.presentationDelegate
                self.presentationDelegate.shoulAlignViewInCenter = false
                
                pickerView.modalPresentationStyle = .custom
                pickerView.isShowingHeader = true
                pickerView.pickerTitle = "Select a Vehicle"
                
                var vehicleNameArray:[String] = []
                for vehicle in vehicles {
                    vehicleNameArray.append(vehicle.name)
                }
                
                pickerView.arrayData = vehicleNameArray as NSArray//.sorted() as NSArray
                
                pickerView.doneButtonCallback { (row, component) in
                    
                    let vehicle = vehicles[row]
                    var error:NSError? = nil
                    
                    CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError:&error)

                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle) {
                        if device.status == kDeviceStatusSubscriptionPending || device.status == kDeviceStatusActivationInProgress || device.status == kDeviceStatusTerminated || device.status == kDeviceStatusExpired {
                            
                            pickerView.dismiss(animated: true, completion: {
                                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                            })
                            //return
                        }else {
                            pickerView.dismiss(animated: true, completion: nil)
                        }
//                            self.arrayTrips.removeAll()
//                            self.tableView.reloadData()
                            
                            
                            self.selectedVehicleMake = row
                        
                            if(error != nil) {
                                self.view.dismissProgress()
                                print(error?.localizedDescription ?? String())
                            }
                            else{
                                self.clearTapped(nil)
                                // self.view.dismissProgress()
                                self.setHeaderTitle(title: vehicle.name)
                                
                                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: kVehicleChangedNotification)))
                                
                                self.view.showIndicatorWithProgress(message: "Getting vehicle details..")
                                
                                //self.getTripList()
                            }
                     //   }
                    }
                    
                    //                    self.setHeaderTitle(title: vehicles[row].name)
                    //
                    //                    if CommonData.sharedData.setSelectedVehicle(selectedVehicle: vehicles[row]) {
                    //                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: kVehicleChangedNotification)))
                    //                        self.getTripList()
                    //
                    //                    }else{
                    //                        print("error in selecting vehicle")
                    //                    }
                }
                self.present(pickerView, animated: true, completion: nil)
            }
        }
        
    }
    
    func getAddressofLocation(location:Location, completion: @escaping (_ address: String) -> Void)
    {
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: CLLocationDegrees((location.latitude)!), longitude: CLLocationDegrees((location.longitude)!))
        
        CVehicleRequest.sharedInstance().getAddressForLocation(location) { (responseObject, isSucessful, error) in
            if(error == nil && responseObject != nil)
            {
                DispatchQueue.main.async  {
                    
                    let addressStr:String = responseObject as! String
                    
                    //save  vehicle last location
                    //UserDefaults.standard.set(addressStr, forKey: "lastSavedLocation")
                    
                    completion(addressStr)
                }
                
            }
            else{
                DispatchQueue.main.async  {
                    completion("Unable to get location")
                }
            }
            
            
        }
    }
    
    
    //MARK: IBACTIONS
    
    @IBAction func mapButtonTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "MapTapped", category: kEventCategoryTrips, andScreenName: "Trips Screen", eventLabel: nil, eventValue: nil)
        let btn:UIButton = sender as! UIButton
        let tripObj:CTrip = self.arrayTrips[btn.tag] as! CTrip
        //if(tripObj != nil){
        isMovedToTripDetail = true
        let controller = storyboard?.instantiateViewController(withIdentifier: "tripDetailID") as! TripDetailViewController
        controller.tripInfo = tripObj
        
        self.navigationController?.pushViewController(controller, animated: true)
        // }
    }
    
    @IBAction func navigateButtonTapped(_ sender: UIButton) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "NavigateTapped", category: kEventCategoryTrips, andScreenName: "Trips Screen", eventLabel: nil, eventValue: nil)

        let tripObj:CTrip = self.arrayTrips[sender.tag] as! CTrip
        self.lastLocation = tripObj.endLocation
        // getTripLocationsForTrip(trip: tripObj)
        //getCurrentLocation()
        self.showNavigationOnMaps()
    }
    
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "ShareTripTapped", category: kEventCategoryTrips, andScreenName: "Trips Screen", eventLabel: nil, eventValue: nil)

        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        let btn:UIButton = sender as! UIButton
        let tripObj:CTrip = self.arrayTrips[btn.tag] as! CTrip
        
        self.view.showIndicatorWithProgress(message: "Please wait")
        
        
        CTripRequest.sharedInstance().getTripDetailsForTrip(withId: tripObj.tripId) { (responseObject, isSuccessful, error) in
             if(error == nil && responseObject != nil){
                 let allDictionaries:NSDictionary = responseObject as! NSDictionary
  
                 if(allDictionaries.object(forKey: "locations") != nil){
                     var arrayLocations:NSArray = allDictionaries.object(forKey: "locations") as! NSArray
                     let sortedArray = arrayLocations.sorted(by:
                               {
                                  Double(truncating: ($0 as! Location).timeStamp!) > Double(truncating: ($1 as! Location).timeStamp!)
                               })
                               
                     arrayLocations = sortedArray as NSArray
                    
                    var aPath = "path=color:0x0000ff"
                    
                    for anObj in arrayLocations {
                        
                        let locationObj = anObj as! Location
                        
                        var subString = ""
                        
                        subString.append("|")
                        subString.append(String.init(format: "%f", Double(truncating: locationObj.latitude!)))
                        subString.append(",")
                        subString.append(String.init(format: "%f", Double(truncating: locationObj.longitude!)))
                        
                        if aPath.contains(subString) == false{
                            aPath.append(subString)
                        }
                    }
                    
                    let finalImageUrl = String.init(format: "https://maps.googleapis.com/maps/api/staticmap?style=visibility:on&size=400x400&key=AIzaSyDeDarI_BW90aUeFulaGmU05IAV1Lkui5M&%@", aPath)
                    
                    
                    self.shareTripWithAvailableInfov2(destinationAddress: tripObj.endLocationStr, imageUrl: finalImageUrl)
                 }
                
                
             }
            
            self.view.dismissProgress()
         }
        
        
        
        
        
        
//        //get destination address for sharing
//        getAddressofLocation(location: tripObj.endLocation!) {
//            (address: String) in
//            print("got back: \(address)")
//            // self.destinationAddress = address
//
//            //Get image of trip
//            self.getTripImage(forTrip:tripObj, completion: { (imageUrl) in
//
//                self.view.dismissProgress()
//
//                print("got back: \(imageUrl)")
//                self.shareTripWithAvailableInfo(forTrip:tripObj, destinationAddress: address, imagePath: imageUrl)
//            })
//        }
     
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        
        let notificationName = Notification.Name("toggleLeftMenu")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
    }
    
    @IBAction func eventsButtonTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertsNavController")
        vc.modalPresentationStyle = .fullScreen;
        self.navigationController?.present(vc, animated: true, completion: {
        })
    }
    
    
    
    func getTripImage(forTrip:CTrip, completion: @escaping (_ imageUrl: String) -> Void)
    {
        CTripRequest.sharedInstance().getTripStaticImageForTrip(withId: forTrip.tripId, size: CGSize(width: 400, height: 400)) { (responseObject, isSuccesful, error) in
            
            if(error == nil && responseObject != nil){
                
                let urlStr = responseObject
                completion(urlStr as! String)
            }
            else{
                completion("")
            }
        }
    }
    func shareTripWithAvailableInfo(forTrip:CTrip, destinationAddress:String,imagePath:String)
    {
        
        self.view.dismissProgress()
        let stringToShare: String = "I have checkedIn to \"\(destinationAddress)\".\nStay connected via Carot - www.carot.com"
        
        var shareItems:Array = [Any]()
        var img: UIImage
        
        if(imagePath.count > 1){
            img =  UIImage(contentsOfFile: imagePath)!
            shareItems = [img,stringToShare] as [Any]
        }
        else{
            shareItems = [stringToShare] as [Any]
        }
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        if #available(iOS 9.0, *) {
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                            UIActivity.ActivityType.postToWeibo,
                                                            UIActivity.ActivityType.copyToPasteboard,
                                                            UIActivity.ActivityType.addToReadingList,
                                                            UIActivity.ActivityType.postToVimeo,
                                                            UIActivity.ActivityType.postToFacebook,
                                                            UIActivity.ActivityType.assignToContact,
                                                            UIActivity.ActivityType.saveToCameraRoll,
                                                            UIActivity.ActivityType.airDrop,
                                                            UIActivity.ActivityType.openInIBooks,
                                                            UIActivity.ActivityType.assignToContact,
                                                            UIActivity.ActivityType.postToFlickr,
                                                            UIActivity.ActivityType.postToTencentWeibo]
        } else {
            // F
        }
        
        self.view.dismissProgress()
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareTripWithAvailableInfov2(destinationAddress:String,imageUrl:String)
    {

        self.view.showIndicatorWithProgress(message: "Please wait")
               
        self.view.dismissProgress()
        let stringToShare: String = "I have checkedIn to \"\(destinationAddress)\".\nStay connected via Carot - www.carot.com"
        
        if let encodedString  = imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: encodedString) {

            
            let session = URLSession(configuration: .default)

            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: url) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading a picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded a picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            

                           DispatchQueue.main.async {
                            self.loadShareScreen(image: image!, stringToShare: stringToShare)
                           }
                            // Do something with your image.
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
            
        }
       
        
       
    }
    

    func loadShareScreen(image:UIImage, stringToShare : String){
        let shareItems:Array  = [image,stringToShare] as [Any]
        //        }
                
                let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                if #available(iOS 9.0, *) {
                    activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                                    UIActivity.ActivityType.postToWeibo,
                                                                    UIActivity.ActivityType.copyToPasteboard,
                                                                    UIActivity.ActivityType.addToReadingList,
                                                                    UIActivity.ActivityType.postToVimeo,
                                                                    UIActivity.ActivityType.postToFacebook,
                                                                    UIActivity.ActivityType.assignToContact,
                                                                    UIActivity.ActivityType.saveToCameraRoll,
                                                                    UIActivity.ActivityType.airDrop,
                                                                    UIActivity.ActivityType.openInIBooks,
                                                                    UIActivity.ActivityType.assignToContact,
                                                                    UIActivity.ActivityType.postToFlickr,
                                                                    UIActivity.ActivityType.postToTencentWeibo]
                } else {
                    // F
                }
                
                self.view.dismissProgress()
                self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func getTripLocationsForTrip(trip:CTrip)
    {
        self.view.showIndicatorWithProgress(message: "-get-locations")
        CTripRequest.sharedInstance().getTripLocationForTrip(withId: trip.tripId) { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            if(error == nil && responseObject != nil)
            {
                print(responseObject ?? NSArray())
                let arr:NSArray = responseObject as! NSArray
                //self.arrayTripLocations = arr
                let sortedArray = arr.sorted(by:
                {
                    Double(($0 as! Location).timeStamp!) > Double(($1 as! Location).timeStamp!)
                })
                
                //let lastLoc:Location = (sortedArray[sortedArray.count-1] as? Location)!
                self.lastLocation = (sortedArray[sortedArray.count-1] as? Location)!
                //self.arrayTripLocations = sortedArray as NSArray
                //                if(self.arrayTripLocations.count  > 0){
                //                    self.showTripPath()
                //                }
            }
        }
    }
    
    
    func showNavigationOnMaps() {
        
        //        self.view.dismissProgress()
        
        let endLocationStr:String!
        let l1:Float = lastLocation?.latitude as? Float ?? 0
        let l2:Float = lastLocation?.longitude as? Float ?? 0
        
        endLocationStr = "\(l1),\(l2)"
        let routeStr:String = "http://maps.google.com/?daddr=\(endLocationStr!)"
        UIApplication.shared.openURL(URL(string: routeStr)!)
        
    }
    
    //MARK: UITableView datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.checkIfNoTripsLAbelIsToBeDisplayed()
        return arrayTrips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        if self.arrayTrips.count == 0 {
        //            return nil
        //        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! TripCell
        cell.beautifyView()
        
        
        var tripObj:CTrip = self.arrayTrips[indexPath.row] as! CTrip
        
        let averageSpeed:Double = tripObj.averageSpeed as! Double
        let averageSpeedTrimmed = AppUtility.sharedUtility.roundTo(places: 2, value: averageSpeed)
        let strSpeed:String = String(format: floor(averageSpeedTrimmed*3.6) == floor(averageSpeedTrimmed*3.6) ? "%.0f" : "%.0f", (averageSpeedTrimmed*3.6))
        cell.labelAverageSpeed.text = "\(strSpeed)Km/h"
        
        let idleTimeSecs = Double(tripObj.totalIdleTime)
        let idleTimeMin = floor(idleTimeSecs/60)
        let strIdleTime:String = String(format:"%.0f", idleTimeMin)
        cell.labelIdleTime.text = "\(strIdleTime)MIN"
                    
//        if let deviceMode = UserDefaults.standard.string(forKey: "deviceMode") {
//            if deviceMode == "TOYOTA" {
//                cell.idleTimeViewObj.isHidden = true
//            }
//        }else{
            cell.idleTimeViewObj.isHidden = false
//        }
        
        
        let mileage = Double(tripObj.totalMileage)
        let mileageKm = (mileage/1000)
        let strDistance:String = String(format: "%.1f", mileageKm)
        
        cell.labelDistance.text = "\(strDistance)KMS"
        
        
        cell.labelStartTime.text = AppUtility.sharedUtility.TimeInIndianTimezone(date: tripObj.startedOn)
        cell.labelStartDate.text = AppUtility.sharedUtility.DateInIndianTimezone(date: tripObj.startedOn)
        cell.labelEndTime.text = AppUtility.sharedUtility.TimeInIndianTimezone(date: tripObj.endedOn)
        cell.labelEndDate.text = AppUtility.sharedUtility.DateInIndianTimezone(date: tripObj.endedOn)
        
        cell.buttonShare.tag = indexPath.row
        cell.buttonViewMap.tag = indexPath.row
        cell.buttonNavigate.tag = indexPath.row
      
        if let tripStartLocation = tripObj.startLocationStr, let tripEndLocation = tripObj.endLocationStr {
            cell.labelStartAddress.text = tripStartLocation
            cell.labelEndAddress.text = tripEndLocation
        }else {
            cell.labelStartAddress.text = "Location not found"
            cell.labelEndAddress.text = "Location not found"
                        
        }
        
        
        //        cell.setDriverScore(score: tripObj.driveScore)
        
        if  tripObj.startLocation.geoCode == nil ||  tripObj.startLocation.geoCode == ""{
    

                if tripObj.tripScore != 0 {
                    cell.labelTripScore.text = "\(Int(truncating: tripObj.tripScore))%"
                    cell.progressView.progress = CGFloat(truncating: tripObj.tripScore)/100

                }else{

//                    if tripObj.driveScore.isScorable {
//
//                        cell.labelTripScore.text = "\(Int(truncating: tripObj.tripScore))%"
//                        cell.progressView.progress = CGFloat(truncating: tripObj.tripScore)/100
//
//                    }
//                    else {
                        
                        cell.labelTripScore.text = "N/A"
                        cell.progressView.progress = 0.0
//                    }
                    // self.tableView.reloadData()
                    
                }
//            }
        }else{
            // cell.setDriverScoreForTrip(tripObj: tripObj)

            if tripObj.driveScore == nil {
                cell.labelTripScore.text = "..."
                cell.progressView.progress = 0.0
                
                
                  
                cell.labelTripScore.text = "\(Int(truncating: tripObj.tripScore))%"
                cell.progressView.progress = CGFloat(truncating: tripObj.tripScore)/100
                                      
                
            }else{
                if tripObj.tripScore != 0 {
                                         
                      cell.labelTripScore.text = "\(Int(truncating: tripObj.tripScore))%"
                      cell.progressView.progress = CGFloat(truncating: tripObj.tripScore)/100
                                          
                }
                else {
                    
                    cell.labelTripScore.text = "N/A"
                    cell.progressView.progress = 0.0
                }
                // self.tableView.reloadData()
            }
            
        }
        //cell.setDriverScoreForTrip(tripObj: tripObj)
        cell.tripScoreButton.addTarget(self, action: #selector(tripScoreButtonTapped(sender:)), for: .touchUpInside)
        cell.tripScoreButton.tag = indexPath.row
        
        return cell
    }
    
    
    func getCellData(for trip:CTrip) {
        if trip.startLocation.geoCode == nil || trip.startLocation.geoCode == "" || trip.endLocation.geoCode == nil || trip.endLocation.geoCode == "" {
            
            let startLoc = CLLocationCoordinate2D(latitude: trip.startLocation.latitude as! CLLocationDegrees, longitude: trip.startLocation.longitude as! CLLocationDegrees)
            CVehicleRequest.sharedInstance().getAddressForLocation(startLoc, response: { (responseObject, isSuccessful, error) in
                if(error == nil && responseObject != nil)
                {
                    trip.startLocation.geoCode = responseObject as! String?
                }
                
                let endLoc = CLLocationCoordinate2D(latitude: trip.endLocation.latitude as! CLLocationDegrees, longitude: trip.endLocation.longitude as! CLLocationDegrees)
                CVehicleRequest.sharedInstance().getAddressForLocation(endLoc, response: { (responseObject, isSuccessful, error) in
                    
                    if(error == nil && responseObject != nil)
                    {
                        trip.endLocation.geoCode = responseObject as! String?
                    }
                    
                    //                    if trip.driveScore == nil {
                    //                        CTripRequest.sharedInstance().getTripScore(for: trip) { (responseData, isSuccess, error) in
                    //
                    //                            if isSuccess {
                    //                                if let score = responseData as? CDriveScore {
                    //                                    trip.driveScore = score
                    //                                }
                    //                                else {
                    //                                    trip.driveScore = nil
                    //                                }
                    //
                    //                            }
                    //                            DispatchQueue.main.async {
                    //                                self.tableView.reloadData()
                    //                            }
                    //                        }
                    //                    }
                    
                })
            })
        }
        
    }
    
    //    func getStartAddressForTrip(trip: CTrip) -> Void {
    //        let startLoc = CLLocationCoordinate2D(latitude: trip.startLocation.latitude as! CLLocationDegrees, longitude: trip.startLocation.longitude as! CLLocationDegrees)
    //
    //
    //        CVehicleRequest.sharedInstance().getAddressForLocation(startLoc, response: { (responseObject, isSuccessful, error) in
    //            if(error == nil && responseObject != nil)
    //            {
    //
    //                trip.startLocation.geoCode = responseObject as! String?
    //
    //
    //                //                    let visibleIndices = tableView.indexPathsForVisibleRows
    //                //
    //                //                    for  index in visibleIndices! {
    //                //                        if (index.row == indexPath.row) {
    //                //                            cell.labelStartAddress.text = responseObject as! String?
    //                //                           // tableView.reloadData()
    //                //
    //                //                            break
    //                //                        }
    //                //                    }
    //
    //            }
    ////            self.getEndAddressForTrip(trip: trip)
    //        })
    //
    //    }
    
    //    func getEndAddressForTrip(trip: CTrip) -> Void {
    //
    //        if  trip.endLocation.geoCode == nil ||  trip.endLocation.geoCode == "" {
    //
    //            let endLoc = CLLocationCoordinate2D(latitude: trip.endLocation.latitude as! CLLocationDegrees, longitude: trip.endLocation.longitude as! CLLocationDegrees)
    //
    //            CVehicleRequest.sharedInstance().getAddressForLocation(endLoc, response: { (responseObject, isSuccessful, error) in
    //
    //                if(error == nil && responseObject != nil)
    //                {
    //
    //                    trip.endLocation.geoCode = responseObject as! String?
    //
    //                    //                let visibleIndices = tableView.indexPathsForVisibleRows
    //                    //
    //                    //                for  index in visibleIndices! {
    //                    //                    if (index.row == indexPath.row) {
    //                    //                        cell.labelEndAddress.text = responseObject as! String?
    //                    //
    //                    //                        DispatchQueue.main.async {
    //                    //                            tableView.reloadData()
    //                    //                        }
    //                    //                        break
    //                    //                    }
    //                    //                }
    //
    //                }
    //                self.getScoreForTrip(trip: trip)
    //
    //            })
    //        }
    //        else {
    //            self.getScoreForTrip(trip: trip)
    //
    //        }
    //    }
    
    //    func getScoreForTrip(trip: CTrip) -> Void {
    //
    //        if trip.driveScore == nil {
    //            CTripRequest.sharedInstance().getTripScore(for: trip) { (responseData, isSuccess, error) in
    //
    //                if isSuccess {
    //                    if let score = responseData as? CDriveScore {
    //                        trip.driveScore = score
    //
    //                        //                        let visibleIndices = tableView.indexPathsForVisibleRows
    //
    //                        //                        for  index in visibleIndices! {
    //                        //                            if (index.row == indexPath.row) {
    //                        //
    //                        //  cell.setDriverScore(score: score)
    //
    //
    //                        //                                break
    //                        //                            }
    //                        //                        }
    //
    //                    }
    //                    else {
    //                        trip.driveScore = nil
    //
    //                    }
    //
    //                    DispatchQueue.main.async {
    //                        self.tableView.reloadData()
    //                    }
    //
    //                }
    //            }
    //        }
    //        else {
    //            DispatchQueue.main.async {
    //                self.tableView.reloadData()
    //            }
    //        }
    //    }
    
    @objc func tripScoreButtonTapped(sender:UIButton) -> Void {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "TripScoreTapped", category: kEventCategoryTrips, andScreenName: "Trips Screen", eventLabel: nil, eventValue: nil)
        let tripObj = self.arrayTrips[sender.tag] as! CTrip
        
        if tripObj.tripScore == 0 {
            let alert = UIAlertController(title: "New ", message: "Keep Calm! Your trip score will be reflected next day", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            let viewController:DriverScoreView = DriverScoreView()
            viewController.transitioningDelegate = presentationDelegate
            presentationDelegate.shoulAlignViewInCenter = true
            
            viewController.modalPresentationStyle = .custom
            
            viewController.tripScore = tripObj.tripScore.intValue
            viewController.driveScore = tripObj.driveScore
            self.present(viewController, animated: true) {
                self.presentationDelegate.shoulAlignViewInCenter = false
            }
        }
//        else {
//            let alert = UIAlertController(title: "Hey", message: "Your trip is too short", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tripObj = self.arrayTrips[indexPath.row]
        isMovedToTripDetail = true
        let controller = storyboard?.instantiateViewController(withIdentifier: "tripDetailID") as! TripDetailViewController
        controller.tripInfo = tripObj as! CTrip
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let nextpage = self.arrayTrips.count - 2
        if indexPath.row == nextpage {
            
            let lastTrip = self.arrayTrips.last as! CTrip?
            if  filterButton.isSelected {
                self.fetchTrips(from: self.startDate!, end:(lastTrip!.startedOn)! )
            }else{
                self.loadMoreTripsWithLastId(tripId: (lastTrip!.tripId)!)
            }
        }
    }
    
    func loadMoreTripsWithLastId(tripId: String) -> Void {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            self.refreshControl.endRefreshing()
            
            return
        }

        CTripRequest.sharedInstance().loadMoreAfterTrip(withId: tripId, response: { (responseFromDB, success, error) in
            
            if( responseFromDB != nil && ((responseFromDB as! NSArray).count) > 0){
                
                if self.arrayTrips.count == 0 {
                    self.arrayTrips = (responseFromDB as! NSArray) as! [CTrip]
                    self.tableView.reloadData()
                }else {
                    if (responseFromDB as! NSArray).count > 0{
                        
                        for newTrip in (responseFromDB as! NSArray) {
                            if !self.isTripsArrayContainsTrip(selectedTrip:newTrip as! CTrip) {
                                self.arrayTrips.append(newTrip as! CTrip)
                            }
                        }
                        
                        //self.arrayTrips.append(contentsOf: (responseFromDB as! NSArray))
                        self.tableView.reloadData()
                    }else{
                        self.view.showIndicatorWithInfo("No more data to load")
                    }
                    
                }
                
            }
            
        }) { (tripList, error) in
            
            if error == nil {
                if let _ = tripList {
                    if self.arrayTrips.count == 0 {
                        self.arrayTrips = tripList as! [CTrip]
                        self.tableView.reloadData()
                    }
                    else {
                        if tripList?.count != 0 {
                            for newTrip in tripList! {
                                if !self.isTripsArrayContainsTrip(selectedTrip:newTrip as! CTrip) {
                                    self.arrayTrips.append(newTrip as! CTrip)
                                }
                            }
                            self.tableView.reloadData()
                        }
                    }
                }
            }else{
                self.view.showIndicatorWithError((error?.localizedDescription)!)
                print(error?.localizedDescription ?? String())            }
        }
    }
    
    func isTripsArrayContainsTrip(selectedTrip: CTrip) -> Bool {
        
        for i in 0...self.arrayTrips.count-1 {
            let cTrip = self.arrayTrips[i] as! CTrip
            
            if cTrip.tripId == selectedTrip.tripId {
                self.arrayTrips[i] = selectedTrip
                return true
            }
        }
        return false
        
    }
    
    //MARK:- Date Picker Methods
    
    @IBAction func doneTapped(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5) {
            
            self.datePickerBottomConstraint.constant = -(self.tabBarController?.tabBar.frame.height)! - self.datePickerView.frame.height
            
            self.tabBarController?.tabBar.isHidden = false
            self.view.layoutIfNeeded()
            
        }
        
        if (self.datePickerTitleLabel.text?.contains("from"))! {
            self.startDate = self.datePicker.date
        }
        else {
            self.endDate = self.datePicker.date
        }
        
        //self.updateDateView()
        
    }
    
    func stringFromDate(date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY"
        
        return formatter.string(from: date as Date)
        
    }
    
    //    func dateWithShortFormat(date: Date) -> Date {
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "YYYY-dd-mm"
    //        formatter.dateStyle = .short
    //
    //        let dateStr = formatter.string(from: date)
    //        return formatter.date(from: dateStr)!
    //
    //    }
    
    
    
    func updateDateView() -> Void {
        
        self.filterTitleLabel.text = self.stringFromDate(date: self.startDate!)  + "-" + self.stringFromDate(date: self.endDate!)
        
        self.fromLabel.text = self.stringFromDate(date: self.startDate!)
        self.toLabel.text = self.stringFromDate(date: self.endDate!)
        
    }
    
    
    @IBAction func cancelTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            
            self.datePickerBottomConstraint.constant = -(self.tabBarController?.tabBar.frame.height)! - self.datePickerView.frame.height
            
            self.tabBarController?.tabBar.isHidden = true
            self.view.layoutIfNeeded()
            
        }
        
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    
    //MARK:- Filter Action Methods
    
    @IBAction func clearTapped(_ sender: Any?) {
        self.hideFilterView()
        
        self.filterButton.isSelected = false
        self.filterTitleLabel.text = ""
        
        self.clearButton.isHidden = true
        
        self.arrayTrips.removeAll()
        self.tableView.reloadData()
        self.resetFilterDates()
        
        self.getTripList()
        AppUtility.sharedUtility.sendGAEvent(withEventName: "ResetDatesTapped", category: kEventCategoryTrips, andScreenName: "Trips Screen", eventLabel: nil, eventValue: nil)
    }
    
    
    @IBAction func filterTapped(_ sender: Any) {
        
        if self.dateView.isHidden {
            self.showFilterView()
        }
        else {
            
            self.hideFilterView()
        }
    }
    
    
    @IBAction func fromTapped(_ sender: Any) {
        
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.pickerTitle = "Select from date"
        pickerView.maximumDate = Date()
        
        pickerView.doneButtonCallback { (date) in
            pickerView.dismiss(animated: true, completion: nil)
            
            self.startDate = AppUtility.sharedUtility.getStartOfDay(startDateObj: date)
            self.endDate! = self.startDate!.addingTimeInterval(24 * 60 * 60)
            
            //            if self.compareDates(date1: self.startDate!, date2: Date()) == .orderedSame { //Dates are same
            //                self.endDate! = self.startDate!
            //
            //            } else {
            //                self.endDate! = self.startDate!.addingTimeInterval(24 * 60 * 60)
            //
            //            }
            
            self.updateDateView()
            self.toTapped(nil)
        }
        
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    @IBAction func toTapped(_ sender: Any?) {
        self.datePickerTitleLabel.text = "Select to date"
        
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.pickerTitle = "Select To date"
        pickerView.minimumDate = self.startDate!
        pickerView.maximumDate = Date()
        
        pickerView.doneButtonCallback { (date) in
            
            let order = self.compareDates(date1: self.startDate!, date2: date)
            
            switch order {
                
            case .orderedDescending :
                self.view.showIndicatorWithError("To date cannot be less than From date")
                
                //            case .orderedSame :
                //
                //                if self.compareDates(date1: self.startDate!, date2: Date()) != .orderedSame {
                //                    self.view.showIndicatorWithError("To date should be greater than From date")
                //
                //                }
                //                else {
                //                    pickerView.dismiss(animated: true, completion: nil)
                //
                //                    self.endDate = date
                //                    self.updateDateView()
                //                }
                
            default:
                pickerView.dismiss(animated: true, completion: nil)
                
                self.endDate = AppUtility.sharedUtility.getEndOfDay(endDateObj: date)
                self.updateDateView()
                
            }
        }
        
        self.present(pickerView, animated: true, completion: nil)
        
        
    }
    
    @IBAction func applyTapped(_ sender: Any) {
        //TODO:- Call Trips API with selected Filters
        AppUtility.sharedUtility.sendGAEvent(withEventName: "TripsFiltered", category: kEventCategoryTrips, andScreenName: "Trips Screen", eventLabel: nil, eventValue: nil)
        self.filterButton.isSelected = true
        
        self.clearButton.isHidden = false
        
        self.arrayTrips.removeAll()
        self.tableView.reloadData()
        
        self.fetchTrips(from: self.startDate!, end: self.endDate!)
        
        updateDateView()
    }
    
    func fetchTrips(from fromDate:Date, end toDate:Date) {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            self.refreshControl.endRefreshing()
            
            return
        }

        CTripRequest.sharedInstance().filterTrips(from: fromDate.timeIntervalSince1970, toInterval: toDate.timeIntervalSince1970, response: { (responseData, isSuccess, error) in
            
            self.view.dismissProgress()
            self.refreshControl.endRefreshing()
            
            DispatchQueue.main.async {
                self.hideFilterView()
            }
            
            if isSuccess {
                
            }
            else {
                
            }
            
        }) { (responseFromServer, error) in
            self.view.dismissProgress()
            self.refreshControl.endRefreshing()
            
            DispatchQueue.main.async {
                self.hideFilterView()
            }
            
            if error == nil {
                if self.arrayTrips.count == 0 {
                    self.arrayTrips = responseFromServer as! [CTrip]
                    self.tableView.reloadData()
                }
                else {
                    if let trips = responseFromServer as? [CTrip] {
                        let currentCount = self.arrayTrips.count
                        
                        if trips.count > 0 {
                            
                            for trip in trips {
                                if !self.isTripsArrayContainsTrip(selectedTrip: trip) {
                                    self.arrayTrips.append(trip)
                                }
                            }
                            
                            if currentCount != self.arrayTrips.count {
                                self.tableView.reloadData()
                            }
                        }else{
                            self.view.showIndicatorWithInfo("No more data to load")
                        }
                    }
                }
            }
            else {
                self.view.showIndicatorWithError("Unable to fetch trips right now")
            }
        }
    }
    
    //    func fetchScoreForTrip(trip: CTrip) -> Void {
    //
    //        CTripRequest.sharedInstance().getTripScore(for: trip) { (responseData, isSuccess, error) in
    //
    //            if isSuccess {
    //                let score = responseData as? CDriveScore
    //
    //                for i in 0...self.arrayTrips.count {
    //
    //                    let oldTrip = self.arrayTrips[i] as! CTrip
    //
    //                    if oldTrip.tripId == trip.tripId {
    //
    //                        oldTrip.driveScore = score
    //
    //                        self.arrayTrips[i] = oldTrip
    //                        self.tableView.reloadData()
    //                        break
    //                    }
    //                }
    //
    //            }
    //            else {
    //
    //            }
    //
    //        }
    //
    //
    //    }
    
    func compareDates(date1: Date, date2: Date) -> ComparisonResult {
        return Calendar.current.compare(date1, to: date2, toGranularity: .day)
        
    }
    
    
    
}
