//
//  TripDetailViewController.swift
//  iConnect
//
//  Created by Administrator on 2/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import GoogleMaps


extension Float {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}


class TripDetailViewController: UIViewController, GMSMapViewDelegate {
    
    var tripInfo:CTrip = CTrip()
    var arrayTripLocations:NSArray = [Location]() as NSArray
    var dictTripBreaksEvents:NSDictionary?
    var arrayLocationsBreaksEvents:NSArray?
    var tripLocationsIndex:Int = 0
    var flagStartVideo:Bool = false
    var arrayEventsBreaksMarker:Array = [GMSMarker]()
    var mCurrentZoomLevel = 12.0
    var isMoved = false
    @IBOutlet weak var topIdleTimeViewObj: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var driverScoreBackgroundView: LoaderView!
    @IBOutlet weak var driverScoreView: LoaderView!
    //Trip replay
    @IBOutlet weak var labelEndTime:UILabel!
    @IBOutlet weak var sliderTripReplay:UISlider!
    @IBOutlet weak var btnPlayPause:UIButton!
    @IBOutlet weak var btnStartTripReplay:UIButton!
    var tripMutablePath = GMSMutablePath()
    @IBOutlet weak var labelTripScore: UILabel!
    var timerTripReplay : Timer?
    var timeIntervalToReplay: Float = 0.0
    var totalTripReplayDuration:Float = 0.0
    
    //////////
    var tripPath:GMSPolyline?
    var mapEndMarker:GMSMarker?
    var mapStartMarker:GMSMarker?
    
    var destinationAddress:String = ""
    var locationEnd:Location?
    
    
    var gestureRecognizer:UIPanGestureRecognizer?
    
    lazy var presentationDelegate = PickerControllerPresentationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Trip Details Screen")

        mapView.setMinZoom(Float(8), maxZoom: Float(kMapZoomLevelTripDetails))
        
        mapView.delegate = self
//        mapView.isRenderAllowed = true
//        mapView.set(tilt: 180, animation: .bow)
        
//        if let deviceMode = UserDefaults.standard.string(forKey: "deviceMode") {
//             if deviceMode == "TOYOTA" {
//                 topIdleTimeViewObj.isHidden = true
//             }
//         }
        sliderTripReplay.setThumbImage(#imageLiteral(resourceName: "ShareSliderIcon"), for: .normal)
        
        configureNavBar()
        
        
        driverScoreBackgroundView.loaderColor = UIColor(red:0.89, green:0.91, blue:0.92, alpha:1.0).cgColor
        driverScoreBackgroundView.progress = 1.0
        driverScoreBackgroundView.circlePathLayer.lineWidth = 1
        driverScoreView.circlePathLayer.lineWidth = 1
        driverScoreView.loaderColor = UIColor(red:0.97, green:0.58, blue:0.11, alpha:1.0).cgColor
        driverScoreView.progress = 0.0
        
        setDriverScore()
        
        // getTripLocationsForPath()
        getTripDetails()
        
     
        let startCoordinates = CLLocationCoordinate2DMake(Double(truncating: (tripInfo.startLocation?.latitude)!), Double(truncating: (tripInfo.startLocation?.longitude)!))
        let endCoordinates = CLLocationCoordinate2DMake(Double(truncating: (tripInfo.endLocation?.latitude)!), Double(truncating: (tripInfo.endLocation?.longitude)!))
                
        let boundingBox:GMSCoordinateBounds = GMSCoordinateBounds(coordinate: startCoordinates, coordinate: endCoordinates)
        let camera: GMSCameraUpdate = GMSCameraUpdate.fit(boundingBox)

        self.mapView.animate(with: camera)

        //        mapView.set(boundingBox: boundingBox, inside: CGRect(x: 25, y: 50, width: self.view.frame.width-50, height: self.view.frame.height-230), animation: .none)

        self.addStartMarker()
        self.addEndMarker()
        
        self.gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        self.view.addGestureRecognizer(gestureRecognizer!)
        
    }
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) -> Void {
        
        // Disable Pan gesture of MFSideMenucontoller to get Pan Gesture of Map
        
        if (recognizer.location(in: self.view).y > 66) { //
            recognizer.isEnabled = false
            recognizer.isEnabled = true
        }
    }
    
    @objc func backButtonTapped() {
        _ = navigationController?.popViewController(animated: true)
        tripLocationsIndex = 0
    }
    
    func setDriverScore() {
        
        self.labelTripScore.text = "\(Int(truncating: tripInfo.tripScore))%"
        self.driverScoreView.progress = CGFloat(truncating: tripInfo.tripScore)/100
    }
    
    /*func getTripLocationsForPath()
     {
     CTripRequest.sharedInstance().getTripLocationForTrip(withId: tripInfo.tripId) { (responseObject, isSuccessful, error) in
     if(error == nil && responseObject != nil)
     {
     print(responseObject ?? NSArray())
     let arr:NSArray = responseObject as! NSArray
     self.arrayTripLocations = arr
     let sortedArray = self.arrayTripLocations.sorted(by:
     {
     Double(($0 as! CLocation).timeStamp) > Double(($1 as! CLocation).timeStamp)
     })
     
     self.arrayTripLocations = sortedArray as NSArray
     if(self.arrayTripLocations.count  > 0){
     self.showTripPath()
     }
     }
     }
     }*/
    
    func getTripDetails()
    {
        CTripRequest.sharedInstance().getTripDetailsForTrip(withId: tripInfo.tripId) { (responseObject, isSuccessful, error) in
            if(error == nil && responseObject != nil){
                let eventsDict:NSDictionary = responseObject as! NSDictionary
                self.dictTripBreaksEvents = eventsDict
                self.showTripPath(withbreaksAndEvents: eventsDict)
               // self.btnPlayPause.isHidden = false
                
                //                self.showTripEvents(withbreaksAndEvents: eventsDict)
            }
        }
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "TRIP DETAIL"
        headerTitle.sizeToFit()
        self.navigationItem.titleView = headerTitle
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
        
    }
    
    func addStartMarker() {
        
        let locationObj:Location = tripInfo.startLocation
        var coordinates: CLLocationCoordinate2D
        coordinates = CLLocationCoordinate2DMake(Double(truncating: (locationObj.latitude)!), Double(truncating: (locationObj.longitude)!))
        mapStartMarker = GMSMarker.init(position: coordinates)
        mapStartMarker?.map = mapView

        
    }
    func addEndMarker() {
        
        let locationObj:Location = tripInfo.endLocation
        var coordinates: CLLocationCoordinate2D
        coordinates = CLLocationCoordinate2DMake(Double(truncating: (locationObj.latitude)!), Double(truncating: (locationObj.longitude)!))
        mapEndMarker = GMSMarker.init(position: coordinates)
        mapEndMarker?.map = mapView
    }
    //MARK: Trip Replay
    
    @IBAction func playPauseTapped(_ sender: Any) {
        
        let btn:UIButton = sender as! UIButton
        if(btn.isSelected){
            AppUtility.sharedUtility.sendGAEvent(withEventName: "TripResumeTapped", category: kEventCategoryTrips, andScreenName: "Trip Details Screen", eventLabel: nil, eventValue: nil)
            if(tripLocationsIndex == 0){
                      
                flagStartVideo = false
                btnStartTripReplay.isHidden = true
                sliderTripReplay.isHidden = false
                tripLocationsIndex = 0
                tripMutablePath = GMSMutablePath()

                arrayEventsBreaksMarker.removeAll()
                playTrip()
                btnPlayPause.setImage(#imageLiteral(resourceName: "pause"), for:.normal)
                btnPlayPause.isHidden = false
                
            }else {
                btnPlayPause.setImage(#imageLiteral(resourceName: "pause"), for:.normal)
                flagStartVideo = false
                startTimer()
            }
            btn.isSelected = false
            
            
        }
        else
        {
            AppUtility.sharedUtility.sendGAEvent(withEventName: "TripPauseTapped", category: kEventCategoryTrips, andScreenName: "Trip Details Screen", eventLabel: nil, eventValue: nil)
            btn.isSelected = true
            btnPlayPause.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            stopTimer()
        }
        
    }
    @IBAction func sliderValueUpdated(_ sender: Any) {
        let slider:UISlider = sender as! UISlider
        
        tripLocationsIndex = 0
        tripPath?.map = nil
        tripMutablePath = GMSMutablePath()
        self.mapView.clear()
        if mapEndMarker != nil {
            mapEndMarker?.map = nil
        }
        
        let first = 0
        let last = Int(slider.value)
        let interval = 1
        for _ in stride(from: first, through: last, by: interval) {
            flagStartVideo = true
            startTripVideo()
        }
        
        
        if(((arrayLocationsBreaksEvents?.count)!-2) == Int(slider.value)){
            tripLocationsIndex = 0
        }
        else {
            flagStartVideo = false
            tripLocationsIndex = Int(slider.value)
        }
    }
    
    @IBAction func tripReplayTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "TripReplayTapped", category: kEventCategoryTrips, andScreenName: "Trip Details Screen", eventLabel: nil, eventValue: nil)
        
        if dictTripBreaksEvents == nil {
            self.view.showIndicatorWithInfo("Please wait while your trip details are being loaded")
            return
        }
        
        flagStartVideo = false
        btnStartTripReplay.isHidden = true
        sliderTripReplay.isHidden = false
        tripPath?.map = nil
        tripLocationsIndex = 0
        tripMutablePath = GMSMutablePath()

        arrayEventsBreaksMarker.removeAll()
        playTrip()
        btnPlayPause.setImage(#imageLiteral(resourceName: "pause"), for:.normal)
        btnPlayPause.isHidden = false
        
    }
    @IBAction func tripScoreButtonTapped(_ sender: Any) {
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "TripScoreTapped", category: kEventCategoryTrips, andScreenName: "Trip Details Screen", eventLabel: nil, eventValue: nil)
        if tripInfo.tripScore == 0 {
            let alert = UIAlertController(title: "Hey", message: "Keep Calm! Your trip score will be reflected next day", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        else {
            
            let viewController:DriverScoreView = DriverScoreView()
            viewController.transitioningDelegate = presentationDelegate
            presentationDelegate.shoulAlignViewInCenter = true
            viewController.modalPresentationStyle = .custom
            viewController.driveScore = tripInfo.driveScore
            viewController.tripScore = tripInfo.tripScore.intValue
            
            self.present(viewController, animated: true) {
                self.presentationDelegate.shoulAlignViewInCenter = false
            }
        }
//        else {
//            let alert = UIAlertController(title: "Hey", message: "Your trip is too short", preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//
//        }
    }
    
    func startTimer () {
        
        
        //if timerTripReplay == nil {
        let interval:Float = timeIntervalToReplayTrip()
        timerTripReplay =  Timer.scheduledTimer(
            timeInterval: TimeInterval(interval),
            target      : self,
            selector    : #selector(startTripVideo),
            userInfo    : nil,
            repeats     : true)
        // }
    }
    
    func stopTimer() {
        if timerTripReplay != nil {
            timerTripReplay?.invalidate()
            timerTripReplay = nil
        }
    }
    
    func timeIntervalToReplayTrip() -> Float{
        var timeInterval:Float = 0.0
        let tripTotalMileage:Int = tripInfo.totalMileage as! Int
        
        let totalLocations:Float = Float(arrayTripLocations.count)
        //print(totalLocations)
        if (tripTotalMileage < 5000) {
            //end the whole trip in 10sec
            timeInterval =  Float((10)/totalLocations)
            totalTripReplayDuration = 10
            //labelEndTime.text = "10 sec"
        }
        else if (tripTotalMileage < 20000)
        {        //end the whole trip in 15sec
            timeInterval =  Float((15)/totalLocations)
            totalTripReplayDuration = 15
            // labelEndTime.text = "15 Sec"
        }
        else if (tripTotalMileage >= 20000){
            //end the whole trip in 30sec
            
            timeInterval =  Float((30)/totalLocations)
            totalTripReplayDuration = 30
            //labelEndTime.text = "30 Sec"
        }
        timeIntervalToReplay = timeInterval
        return timeInterval
    }
    
    func playTrip() {
        if dictTripBreaksEvents == nil {
            self.view.showIndicatorWithInfo("Please wait while your trip details are being loaded")
            return
        }

        if(tripPath == nil){
            let polyline:GMSPolyline = GMSPolyline()
               polyline.strokeColor = UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
               polyline.strokeWidth = 4.0
               tripPath = polyline
            tripPath?.map = self.mapView
            
        }
        
        tripMutablePath = GMSMutablePath()
        
        startTimer()
        
        let arrayLocations:NSArray = (dictTripBreaksEvents?.object(forKey: "locations") as! NSArray?)!
        let arrayEvents:NSArray = (dictTripBreaksEvents?.object(forKey: "events") as! NSArray?)!
        let arrayTripBreaks:NSArray = (dictTripBreaksEvents?.object(forKey: "breaks") as! NSArray?)!
   
        let arrayResult = CTripRequest.sharedInstance().mergeEventsBreaksAndLocationsforTripData(arrayLocations.copy() as? [Any], eventsData: (arrayEvents.copy() as! [Any]), breaksData: arrayTripBreaks.copy() as? [Any]) as NSArray
        
        arrayLocationsBreaksEvents = arrayResult
        sliderTripReplay.maximumValue = Float((arrayLocationsBreaksEvents?.count)!-2)
        
        if mapStartMarker == nil {
            addStartMarker()

        }
        
    }
    
    @objc func startTripVideo() {
        if let eventObj = arrayLocationsBreaksEvents?.object(at: tripLocationsIndex) as? CEvent{
            let eventType:NSArray = ["OVER_SPEEDING","HARD_ACCELERATION","HARD_DECELERATION","IDLE_ENGINE"]
            let eventTypeIcon:NSArray = ["orange_top","yellow_top","blue_top","idle_top"]
            
            let str:String = eventObj.vehicleAlarmType as String
            let indx: Int = eventType.index(of: str)
            
            if indx <= 3 {
                let imageToShowString : String = eventTypeIcon[indx] as! String
                
                let locationObj:Location = eventObj.onLocation as Location
                
                let coordinates = CLLocationCoordinate2DMake(locationObj.latitude as! Double, locationObj.longitude as! Double)
                
                let mapEventMarker:GMSMarker = GMSMarker.init(position: coordinates)
                mapEventMarker.icon = UIImage(named:imageToShowString)!
//                mapEventMarker.groundAnchor = CGPoint(x: 0, y: 2)
                mapEventMarker.map = self.mapView
                arrayEventsBreaksMarker.append(mapEventMarker)
            }
            
        }
        else if let locationObj = arrayLocationsBreaksEvents?.object(at: tripLocationsIndex) as? Location {
            if locationObj.latitude != nil && locationObj.longitude != nil {
                let coordinates = CLLocationCoordinate2DMake(Double(truncating: locationObj.latitude!), Double(truncating: locationObj.longitude!))
                tripMutablePath.add(coordinates)
                tripPath?.path = tripMutablePath
                tripPath?.map = self.mapView
                if(flagStartVideo == false)
                {
//                    mapView.animate(toLocation: coodinates)
                    let aBoundBox = GMSCoordinateBounds(path: tripMutablePath)
                            
                    let camera: GMSCameraUpdate = GMSCameraUpdate.fit(aBoundBox, withPadding: CGFloat(mCurrentZoomLevel))
                    
                    self.mapView.animate(with: camera)
                }
            }
            else {
                return
            }
        }
        else if let tripBreakObj = arrayLocationsBreaksEvents?.object(at: tripLocationsIndex) as? CTripBreak
        {
            //let obj = arrayLocationsBreaksEvents?.object(at: tripLocationsIndex)
            // print(obj)

            let locationObj:Location = tripBreakObj.location
            
            let coordinates = CLLocationCoordinate2DMake(locationObj.latitude as! Double, locationObj.longitude as! Double)
            
            let mapEventMarker:GMSMarker = GMSMarker.init(position: coordinates)
            mapEventMarker.icon = #imageLiteral(resourceName: "Map_break_top")
//            mapEventMarker.groundAnchor = CGPoint(x: 0, y: 2)
            mapEventMarker.map = self.mapView

            arrayEventsBreaksMarker.append(mapEventMarker)
            
        }
        
        
        
        tripLocationsIndex += 1
        let remaingTime:Float = totalTripReplayDuration - (timeIntervalToReplay*Float(tripLocationsIndex))
        labelEndTime.text = "\(remaingTime.roundTo(places: 2))"
        // incrementTimerLbl.text =[NSString stringWithFormat:@"%d Sec",(int)(timerIntervalForTrip*arrayIndex)];
        
        if(flagStartVideo == false){
            sliderTripReplay.setValue(Float(tripLocationsIndex), animated: true)
        }
        
        if(arrayLocationsBreaksEvents?.count == tripLocationsIndex+1){
            timerTripReplay?.invalidate()
            tripLocationsIndex = 0
            
            if mapEndMarker == nil {
                addEndMarker()
            }
            
            labelEndTime.text = "0:00"
            btnPlayPause.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            btnPlayPause.isSelected = true
        }
        
    }
    
    //MARK: IBACTIONS
    @IBAction func shareTripTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "ShareTripTapped", category: kEventCategoryTrips, andScreenName: "Trip Details Screen", eventLabel: nil, eventValue: nil)

        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        //        FTIndicator.setIndicatorStyle(.dark)
        //        FTIndicator.showProgressWithmessage(NSLocalizedString("Please wait", comment: ""))
        
        self.view.showIndicatorWithProgress(message: "Please wait")
        

        CTripRequest.sharedInstance().getTripDetailsForTrip(withId: tripInfo.tripId) { (responseObject, isSuccessful, error) in
                   if(error == nil && responseObject != nil){
                       let allDictionaries:NSDictionary = responseObject as! NSDictionary
        
                       if(allDictionaries.object(forKey: "locations") != nil){
                           var arrayLocations:NSArray = allDictionaries.object(forKey: "locations") as! NSArray
                           let sortedArray = arrayLocations.sorted(by:
                                     {
                                        Double(truncating: ($0 as! Location).timeStamp!) > Double(truncating: ($1 as! Location).timeStamp!)
                                     })
                                     
                           arrayLocations = sortedArray as NSArray
                          
                          var aPath = "path=color:0x0000ff"
                          
                          for anObj in arrayLocations {
                              
                              let locationObj = anObj as! Location
                              
                              var subString = ""
                              
                              subString.append("|")
                              subString.append(String.init(format: "%f", Double(truncating: locationObj.latitude!)))
                              subString.append(",")
                              subString.append(String.init(format: "%f", Double(truncating: locationObj.longitude!)))
                              
                              if aPath.contains(subString) == false{
                                  aPath.append(subString)
                              }
                          }
                          
                          let finalImageUrl = String.init(format: "https://maps.googleapis.com/maps/api/staticmap?style=visibility:on&size=400x400&key=AIzaSyDeDarI_BW90aUeFulaGmU05IAV1Lkui5M&%@", aPath)
                          
                          
                        self.shareTripWithAvailableInfov2(destinationAddress: self.tripInfo.endLocationStr, imageUrl: finalImageUrl)
                       }
                      
                      
                   }
                  
                  self.view.dismissProgress()
               }
              
        
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if gesture == true {
            isMoved = true
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom
        

        if isMoved == true {
            isMoved = false
//            print("mCurrentZoomLevel")
//            print(mCurrentZoomLevel)
            mCurrentZoomLevel = Double(zoom)
//            print(mCurrentZoomLevel)
        }
        
    }
    
    func shareTripWithAvailableInfov2(destinationAddress:String,imageUrl:String)
    {

        self.view.showIndicatorWithProgress(message: "Please wait")
               
        self.view.dismissProgress()
        let stringToShare: String = "I have checkedIn to \"\(destinationAddress)\".\nStay connected via Carot - www.carot.com"
        
        if let encodedString  = imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: encodedString) {

            
            let session = URLSession(configuration: .default)

            // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
            let downloadPicTask = session.dataTask(with: url) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading a picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded a picture with response code \(res.statusCode)")
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            

                           DispatchQueue.main.async {
                            self.loadShareScreen(image: image!, stringToShare: stringToShare)
                           }
                            // Do something with your image.
                        } else {
                            print("Couldn't get image: Image is nil")
                        }
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            
            downloadPicTask.resume()
            
        }
       
        
       
    }
    

    func loadShareScreen(image:UIImage, stringToShare : String){
        let shareItems:Array  = [image,stringToShare] as [Any]
        //        }
                
                let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                if #available(iOS 9.0, *) {
                    activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                                    UIActivity.ActivityType.postToWeibo,
                                                                    UIActivity.ActivityType.copyToPasteboard,
                                                                    UIActivity.ActivityType.addToReadingList,
                                                                    UIActivity.ActivityType.postToVimeo,
                                                                    UIActivity.ActivityType.postToFacebook,
                                                                    UIActivity.ActivityType.assignToContact,
                                                                    UIActivity.ActivityType.saveToCameraRoll,
                                                                    UIActivity.ActivityType.airDrop,
                                                                    UIActivity.ActivityType.openInIBooks,
                                                                    UIActivity.ActivityType.assignToContact,
                                                                    UIActivity.ActivityType.postToFlickr,
                                                                    UIActivity.ActivityType.postToTencentWeibo]
                } else {
                    // F
                }
                
                self.view.dismissProgress()
                self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func navigateTripTapped(_ sender: Any) {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "NavigateTapped", category: kEventCategoryTrips, andScreenName: "Trip Details Screen", eventLabel: nil, eventValue: nil)
        //        FTIndicator.setIndicatorStyle(.dark)
        //        FTIndicator.showProgressWithmessage(NSLocalizedString("Please wait", comment: ""))
        
        //self.view.showIndicatorWithProgress(message: "Please wait")
        
       // getCurrentLocation()
        self.showNavigationOnMaps()
    }
    
//    func getCurrentLocation() {
//        
//        
//        
//        
//        MICLocationManager.sharedManager.getCurrentLocationWithDelegate(delegateObj: self) { (locationObj:CLLocation?, success:Bool) in
//            if (success == true) {
//                print("success")
//                self.showNavigationOnMaps(locationObj: locationObj!)
//            }else{
//                print("fails")
//            }
//        }
//    }
    
    func showNavigationOnMaps() {
        
        //        FTIndicator.dismissProgress()
        self.view.dismissProgress()
        
        let endLocationStr:String!
        let l1:Double = locationEnd?.latitude?.doubleValue ?? 0//Double((locationEnd?.latitude ?? 0)!)
        let l2:Double = locationEnd?.longitude?.doubleValue ?? 0//Double((locationEnd?.longitude ?? 0)!)
        
        endLocationStr = "\(l1),\(l2)"
        // endLocationStr = "\(locationEnd?.latitude!),\(locationEnd?.longitude!)"
        let routeStr:String = "http://maps.google.com/?daddr=\(endLocationStr!)"
        UIApplication.shared.openURL(URL(string: routeStr)!)
        
        
        
    }
    
    func getTripImage(completion: @escaping (_ imageUrl: String) -> Void)
    {
        CTripRequest.sharedInstance().getTripStaticImageForTrip(withId: tripInfo.tripId, size: CGSize(width: 400, height: 400)) { (responseObject, isSuccesful, error) in
            
            if(error == nil && responseObject != nil){
                print(responseObject ?? String())
                let urlStr = responseObject
                completion(urlStr as! String)
            }
            else{
                completion("")
            }
        }
    }
    
    func shareTripWithAvailableInfo(destinationAddress:String,imagePath:String)
    {
        
        let stringToShare: String = "I have checkedIn to \"\(destinationAddress)\".\nStay connected via Carot - www.carot.com"
        
        var shareItems:Array = [Any]()
        var img: UIImage
        
        if(imagePath.count > 1){
            img =  UIImage(contentsOfFile: imagePath)!
            shareItems = [img,stringToShare] as [Any]
        }
        else{
            shareItems = [stringToShare] as [Any]
        }
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        if #available(iOS 9.0, *) {
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                            UIActivity.ActivityType.postToWeibo,
                                                            UIActivity.ActivityType.copyToPasteboard,
                                                            UIActivity.ActivityType.addToReadingList,
                                                            UIActivity.ActivityType.postToVimeo,
                                                            UIActivity.ActivityType.postToFacebook,
                                                            UIActivity.ActivityType.assignToContact,
                                                            UIActivity.ActivityType.saveToCameraRoll,
                                                            UIActivity.ActivityType.airDrop,
                                                            UIActivity.ActivityType.openInIBooks,
                                                            UIActivity.ActivityType.assignToContact,
                                                            UIActivity.ActivityType.postToFlickr,
                                                            UIActivity.ActivityType.postToTencentWeibo]
        } else {
            // F
        }
        //        FTIndicator.dismissProgress()
        
        self.view.dismissProgress()
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: Draw path and events
    
    func showTripPath(withbreaksAndEvents:NSDictionary)
    {
        
        if(withbreaksAndEvents.object(forKey: "locations") != nil){
            let arrayLocations:NSArray = withbreaksAndEvents.object(forKey: "locations") as! NSArray
            self.arrayTripLocations = arrayLocations

            let sortedArray = self.arrayTripLocations.sorted(by:
            {
               Double(truncating: ($0 as! Location).timeStamp!) > Double(truncating: ($1 as! Location).timeStamp!)
            })
            
            self.arrayTripLocations = sortedArray as NSArray

            if(self.arrayTripLocations.count > 0){
                
                if(tripPath == nil){
                    
                    
                    let polyline:GMSPolyline = GMSPolyline()
                    polyline.strokeColor = UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
                      polyline.strokeWidth = 4.0
                      tripPath = polyline
                      tripPath?.map = self.mapView
                    
                }
                
                //Start Icon
                let locationStart:Location = (arrayTripLocations[arrayTripLocations.count-1] as? Location)!
                let startCoordinates = CLLocationCoordinate2DMake(Double(truncating: locationStart.latitude!), Double(truncating: locationStart.longitude!))

                mapStartMarker?.position = startCoordinates
                mapStartMarker?.icon = #imageLiteral(resourceName: "starticon")
                mapStartMarker?.map = self.mapView
                
                //End Icon
                locationEnd = arrayTripLocations[0] as? Location
                let endCoordinates = CLLocationCoordinate2DMake(Double(truncating: locationEnd!.latitude!), Double(truncating: locationEnd!.longitude!))
                mapEndMarker?.position = endCoordinates
                mapEndMarker?.icon = #imageLiteral(resourceName: "endicon")
                mapEndMarker?.map = self.mapView
                
                // Zooming the map to the desired area
                var bounds = GMSCoordinateBounds()
                bounds = bounds.includingCoordinate(startCoordinates)
                bounds = bounds.includingCoordinate(endCoordinates)
               self.mapView.moveCamera(GMSCameraUpdate.fit(bounds))

                
                //Set map zoom level
               
                    let mutablePath = GMSMutablePath()
                   for wayPoint in self.arrayTripLocations {
                    
                            let location:Location = (wayPoint as? Location)!
                            print(location)
                            let coordinate = CLLocationCoordinate2DMake(Double(truncating: location.latitude!), Double(truncating: location.longitude!))
                            mutablePath.add(coordinate)
                    
                   }
                
                tripPath?.path = mutablePath
//
                let aBoundBox = GMSCoordinateBounds(path: mutablePath)
                
                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(aBoundBox, withPadding: 50.0)
                
                self.mapView.animate(with: camera)
                
////
////                   if path.count() > 0 {
////                       tripPathLocateCar?.path = path
////                   }
//////
//
//                for indx in stride(from: first, through: last, by: interval) {
//
//                    // for location in arrayTripLocations {
//
//                    let locationObj:Location = arrayTripLocations[indx] as! Location//location as! Location
//                    let coordinates = CLLocationCoordinate2DMake(locationObj.latitude as! Double, locationObj.longitude as! Double)
//
//                    path.add(coordinates)
//
////                    arrayForBoundingBox.insert(coordinates, at: indx)
//
//                }
//
                
            }
        }
        self.showTripEvents(withbreaksAndEvents: withbreaksAndEvents)
        
    }
    
    func showTripEvents(withbreaksAndEvents:NSDictionary)
    {
       
        let eventType:NSArray = ["OVER_SPEEDING","HARD_ACCELERATION","HARD_DECELERATION","IDLE_ENGINE"]
        let eventTypeIcon:NSArray = ["orange_top","yellow_top","blue_top","idle_top"]
        
        if(withbreaksAndEvents.object(forKey: "events") != nil){
            let arrayEvents:NSArray = withbreaksAndEvents.object(forKey: "events") as! NSArray
            // let sortedEventsArray = arrayEvents.sorted(by: { ($0 as AnyObject).timeStamp > ($1 as AnyObject).timeStamp })
            let sortedEventsArray = arrayEvents.sorted { (($0 as! CEvent).timeStamp) < (($1 as! CEvent).timeStamp) }
                        
            for event in sortedEventsArray {
                let eventObj:CEvent = event as! CEvent
                let str:String = eventObj.vehicleAlarmType as String
                let indx: Int = eventType.index(of: str)
                
                if indx <= eventType.count {

                       let imageToShowString : String = eventTypeIcon[indx] as! String
                       
                       let locationObj:Location = eventObj.onLocation as Location
                       let coordinates = CLLocationCoordinate2DMake(locationObj.latitude as! Double,  locationObj.longitude as! Double)
                    
                       let mapEventMarker:GMSMarker = GMSMarker.init(position: coordinates)
                       mapEventMarker.icon = UIImage(named:imageToShowString)
                       mapEventMarker.map = self.mapView
                       
                       arrayEventsBreaksMarker.append(mapEventMarker)
                       
                }
                
            }
            
        }
        
        if(withbreaksAndEvents.object(forKey: "breaks") != nil){
            if let arrayBreaks:NSArray = withbreaksAndEvents.object(forKey: "breaks") as? NSArray {
                
                
                let sortedBreaksArray = arrayBreaks.sorted { (($0 as! CTripBreak).startedAt) < (($1 as! CTripBreak).startedAt) }
                
                print(sortedBreaksArray)
                
                for tripBreak in sortedBreaksArray {
                    let tripBreakObj:CTripBreak = tripBreak as! CTripBreak
                    
                    let locationObj:Location = tripBreakObj.location as Location
                    
                    let coordinates = CLLocationCoordinate2DMake(locationObj.latitude as! Double,  locationObj.longitude as! Double)
                    let mapEventMarker:GMSMarker = GMSMarker.init(position: coordinates)
                    mapEventMarker.icon =  #imageLiteral(resourceName: "Map_break_top")
//                    mapEventMarker.groundAnchor = CGPoint(x: 0, y: 2)
                    
                    mapEventMarker.map = self.mapView
                    arrayEventsBreaksMarker.append(mapEventMarker)
                    
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
}
