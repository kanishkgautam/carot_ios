//
//  DriverScoreController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 10/04/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DriverScoreController: UIViewController {
    
    var driverScoreOverlay:DriverScoreOverlay?
    
    lazy var presentationDelegate = PickerControllerPresentationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Driver Score screen")
        // Do any additional setup after loading the view.
        
        if self.driverScoreOverlay == nil {
            self.driverScoreOverlay = Bundle.main.loadNibNamed("DriverScoreOverlay", owner: nil, options: nil)?.first as? DriverScoreOverlay
            driverScoreOverlay!.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            
            driverScoreOverlay?.layer.cornerRadius = 0
            
            driverScoreOverlay?.toButton.addTarget(self, action: #selector(toTapped), for: .touchUpInside)
            driverScoreOverlay?.fromButton.addTarget(self, action: #selector(fromTapped), for: .touchUpInside)
            driverScoreOverlay?.filterButton.addTarget(self, action: #selector(filterTapped), for: .touchUpInside)
            driverScoreOverlay?.toCalenderButton.addTarget(self, action: #selector(toTapped), for: .touchUpInside)
            driverScoreOverlay?.fromCalenderButton.addTarget(self, action: #selector(fromTapped), for: .touchUpInside)
            
        }
        
        self.view.addSubview(driverScoreOverlay!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureNavBar()
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        headerTitle.text = "DRIVER SCORE"
        headerTitle.sizeToFit()
        
        self.navigationItem.titleView = headerTitle
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
        
    }

    @objc func backButtonTapped() {
        //self.dismiss(animated: true, completion: nil)
         self.navigationController?.popViewController(animated: true)
    }
    
    func compareDates(date1: Date, date2: Date) -> ComparisonResult {
        return Calendar.current.compare(date1, to: date2, toGranularity: .day)
        
    }
    
    @objc func fromTapped() -> Void {
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.pickerTitle = "Select From date"
        pickerView.maximumDate = Date()
        
        pickerView.minimumDate = nil
        
        pickerView.doneButtonCallback { (date) in
            pickerView.dismiss(animated: true, completion: nil)
            
            self.driverScoreOverlay?.fromDate = AppUtility.sharedUtility.getStartOfDay(startDateObj: date)
            
            self.updateViews()
            self.toTapped()
        }
        
        self.present(pickerView, animated: true, completion: {
            
            pickerView.pickerView.date = (self.driverScoreOverlay?.fromDate)!
        })
        
    }
    
    @objc func toTapped() -> Void {
        
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.pickerTitle = "Select To date"
        pickerView.minimumDate = self.driverScoreOverlay?.fromDate
        pickerView.maximumDate = Date()
        
        pickerView.doneButtonCallback { (date) in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-YYYY"
            
            let order = self.compareDates(date1: self.driverScoreOverlay!.fromDate, date2: date)
            
            switch order {
            case .orderedDescending :
                pickerView.view.showIndicatorWithError("To date cannot be less than From date")
                
            default:
                self.driverScoreOverlay!.toDate = date
                pickerView.dismiss(animated: true, completion: nil)
                
                let title = self.driverScoreOverlay?.filterButton.titleLabel!.text!.replacingOccurrences(of: " ▼", with: "")
                
                if title == self.driverScoreOverlay?.filterTypeArray![0] {
                    if !(self.driverScoreOverlay?.isLastSevenDaysSelected())! {
                        self.driverScoreOverlay!.filterButton.setTitle(self.driverScoreOverlay!.filterTypeArray![1] + " ▼", for: .normal)
                    }
                    
                }
                
            }
            
            self.updateViews()
        }
        
        self.present(pickerView, animated: true, completion: {
            
            pickerView.pickerView.date = (self.driverScoreOverlay?.toDate)!
            
        })
    }
    
    @objc func filterTapped() -> Void {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "FilterDriveScoreTapped", category: kEventCategoryDriveScore, andScreenName: "Driver Score screen", eventLabel: nil, eventValue: nil)
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = self.presentationDelegate
        self.presentationDelegate.shoulAlignViewInCenter = false
        
        pickerView.modalPresentationStyle = .custom
        pickerView.isShowingHeader = false
        pickerView.pickerTitle = "Select filter type"
        
        pickerView.arrayData = self.driverScoreOverlay!.filterTypeArray! as NSArray
        pickerView.doneButtonCallback { (selectedIndex, selectedComponent) in
            if selectedIndex == 0 {
                self.driverScoreOverlay?.setDataforLastSevenDays()
                self.driverScoreOverlay?.updateViews()
            }
            
            self.driverScoreOverlay!.filterButton.setTitle(self.driverScoreOverlay!.filterTypeArray![selectedIndex] + " ▼", for: .normal)
            
            
           // if self.driverScoreOverlay?.barGraphView != nil {
                self.driverScoreOverlay?.barGraphView?.setBottomTextForbar(selectedType: self.driverScoreOverlay!.filterTypeArray![selectedIndex])
            //}

        }
        
        self.present(pickerView, animated: true, completion:nil)
        
    }
    
    func updateViews() -> Void {
        
        self.driverScoreOverlay?.toButton.setTitle(self.stringFromDate(date: (self.driverScoreOverlay?.toDate)!), for: .normal)
        self.driverScoreOverlay?.fromButton.setTitle(self.stringFromDate(date: (self.driverScoreOverlay?.fromDate)!), for: .normal)
        
    }
    
    func stringFromDate(date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY"
        
        return formatter.string(from: date as Date)
    }
    
    
}
