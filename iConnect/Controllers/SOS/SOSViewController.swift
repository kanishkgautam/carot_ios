//
//  SOSViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 16/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import MessageUI
import ReachabilitySwift

class SOSViewController: UIViewController, UIGestureRecognizerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var sosView: UIView!
    
    @IBOutlet weak var sosImageView: UIImageView!
    @IBOutlet weak var noEmergencyContactsLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var contactsArray = NSMutableArray()
    
    var longPressGesture: UILongPressGestureRecognizer?
    
    var processInitiated: UILongPressGestureRecognizer?
    
    var timer: Timer?
    
    var longPressInitiatedTime: Date?
    
    var isSOSTriggered = false
    
    var currentLocation: CLLocationCoordinate2D?
    
    let kNoEmergencyContacts = "You don't have any Emergency Contacts."
    
    @IBOutlet weak var progressBar: LoaderView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        AppUtility.sharedUtility.trackScreenName(with: "SOS Screen")
        //let _ = AppUtility.sharedUtility.isNetworkAvailable()
        
        configureNavBar()
        configureProgressBar()
        
        initialiseGesture()
        
        self.noEmergencyContactsLabel.isHidden  = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getAllEmergencyContacts()
        
    }
    
    func initialiseGesture() -> Void {
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture(gesture:)))
        longPressGesture!.minimumPressDuration = 3.0
        longPressGesture!.numberOfTapsRequired = 0
        longPressGesture!.delegate = self
        
        processInitiated = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture(gesture:)))
        processInitiated!.minimumPressDuration = 0.1
        processInitiated!.numberOfTapsRequired = 0
        processInitiated!.delegate = self
        
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "SOS"
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
        
    }
    
    @objc func backButtonTapped() {
        self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
    }
    
    func configureProgressBar() -> Void {
        let radians = CGFloat(-90 * Double.pi / 180)
        
        let rotation = CATransform3DMakeRotation(radians, 0, 0, 1.0)
        progressBar.circlePathLayer.transform = CATransform3DTranslate(rotation, 0, 0, 0)
        
        progressBar.loaderColor = UIColor.black.cgColor//UIColor(red:0.89, green:0.91, blue:0.92, alpha:1.0).cgColor
        progressBar.progress = 0
        progressBar.circlePathLayer.lineWidth = 2
        progressBar.circlePathLayer.strokeStart = 0
        
    }
    
    func addGestures() -> Void {
        self.sosView.addGestureRecognizer(self.longPressGesture!)
        self.sosView.addGestureRecognizer(self.processInitiated!)
        
    }
    
    func removeGestures() -> Void {
        
        self.sosView.removeGestureRecognizer(self.longPressGesture!)
        self.sosView.removeGestureRecognizer(self.processInitiated!)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    func shakeWarningLabel() {
        let animation:CABasicAnimation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 8
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: descriptionLabel.center.x-20, y: descriptionLabel.center.y)
        animation.toValue = CGPoint(x: descriptionLabel.center.x+20, y: descriptionLabel.center.y)
        descriptionLabel.layer.add(animation, forKey: "position")

//        [[lockView layer] addAnimation:animation forKey:@"position"];
    }
    
    @objc func handleGesture(gesture: UILongPressGestureRecognizer) -> Void {
        
        if self.contactsArray.count == 0 {
            return
        }
        
        if !(Reachability()?.isReachable)! {
            if !MICSnackbar.sharedSnackBar.isVisible {
                MICSnackbar.sharedSnackBar.showWithText(stringToBeDisplayed: "Network not available")
                
            }
            return
        }
        
        if gesture.state == .began {
            
            if gesture == self.longPressGesture {
                
                if !MFMessageComposeViewController.canSendText() {
                    self.cancelSOSProcess()
                    self.view.showIndicatorWithError("This device cannot send text messages.")
                    
                }
                else {
                    
                    self.triggerSOS()
                }
            }
            else if gesture == self.processInitiated {
                self.longPressInitiatedTime = Date()
                self.progressBar.setProgressWithDuration(seconds: 3)
            }
            
        }
        else if gesture.state == .ended {
            
            if gesture == self.processInitiated {
                self.cancelSOSProcess()
            }
        }
    }
    
    func triggerSOS() -> Void {
        self.sosImageView.isHighlighted = true
        self.isSOSTriggered = true
        
        self.sendSMSToEmergencyContacts()
        
    }
    
    func cancelSOSProcess() -> Void {
        
        self.sosImageView.isHighlighted = false
        
        if Date().timeIntervalSince(self.longPressInitiatedTime!) < 3{
            self.progressBar.resetProgressWithAnimation(interval: 1)
            
        }
    }
    
    func getCurrentLocation() -> Void {
        
        MICLocationManager.sharedManager.getCurrentLocationWithDelegate(delegateObj: self) { (location, isSuccess) in
            self.view.dismissProgress()

            if isSuccess {
                if !self.isSOSTriggered && self.contactsArray.count>0 {
                    self.addGestures()
                }
                else {
                    self.removeGestures()
                    self.shakeWarningLabel()
                }

                self.currentLocation = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.latitude)!)
                
            } else {
                self.removeGestures()
                self.shakeWarningLabel()
//                self.isSOSTriggered = false
//                self.cancelSOSProcess()
                self.view.showIndicatorWithError("Unable to get your current location.")
                AppUtility.sharedUtility.sendGAEvent(withEventName: "LocationFailed", category: kEventCategorySOS, andScreenName: "SOS Screen", eventLabel: nil, eventValue: nil)
                
            }
        }
    }
    
    func getAllEmergencyContacts() -> Void {
        
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        self.view.showIndicatorWithProgress(message: "Fetching contacts")
        CUserRequest.sharedInstance().getAllEmergencyContactsResponse { (responseData, isSuccess, error) in
            
            self.view.dismissProgress()

            if isSuccess {
                
                self.contactsArray = responseData as! NSMutableArray
                
                if self.contactsArray.count == 0 {
                    self.removeGestures()
                    self.shakeWarningLabel()
                    self.noEmergencyContactsLabel.text = self.kNoEmergencyContacts
                    self.noEmergencyContactsLabel.isHidden  = false
                    
                }else {
                    self.getCurrentLocation()

//                    if !self.isSOSTriggered {
                        self.removeGestures()
                    self.shakeWarningLabel()
//                    }
                    self.noEmergencyContactsLabel.text = self.kNoEmergencyContacts

                    self.noEmergencyContactsLabel.isHidden  = true
                    
                }
                
            }
            else {
                self.view.dismissProgress()
                self.removeGestures()
                self.shakeWarningLabel()
                self.noEmergencyContactsLabel.text = self.kNoEmergencyContacts

                self.noEmergencyContactsLabel.isHidden  = true
            }
        }
    }
    
    func sendSMSToEmergencyContacts() -> Void {
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "SOSMessageInitiated", category: kEventCategorySOS, andScreenName: "SOS Screen", eventLabel: nil, eventValue: nil)
        let numberArray = NSMutableArray()
        
        for contact in self.contactsArray {
            
            let number = (contact as! CUserContacts).contactNumber
            numberArray.add(number!)
        }
        
        let messageBody = "I am not safe, need HELP right now.\nMy location is " + "https://www.here.com/?map=\(currentLocation!.latitude),\(currentLocation!.longitude),16,normal"
        
        let messageComposer = MFMessageComposeViewController()
        messageComposer.body = messageBody
        messageComposer.messageComposeDelegate = self
        messageComposer.recipients = numberArray.mutableCopy() as? [String]
        self.progressBar.progress = 1
        self.removeGestures()
        self.isSOSTriggered = true

        DispatchQueue.main.async {
            self.navigationController?.present(messageComposer, animated: true, completion: {
                
            })
            
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        controller.dismiss(animated: true, completion: {
            
            self.progressBar.resetProgressWithAnimation(interval: 60)
            self.perform(#selector(self.setProgressBarToZero), with: nil, afterDelay: 60)
            
            switch result {
                
            case .sent:

                self.perform(#selector(self.showSmsSuccessMessage(isSuccess:)), with: NSNumber(integerLiteral: 1), afterDelay: 1.0)
            case .failed:

                self.perform(#selector(self.showSmsSuccessMessage(isSuccess:)), with: NSNumber(integerLiteral: 0), afterDelay: 1.0)
            case .cancelled:
//                self.setProgressBarToZero()
                    break
            }
        })
    }
    
    @objc func setProgressBarToZero() -> Void {
        
        self.progressBar.progress = 0
        self.cancelSOSProcess()
        self.addGestures()
        self.isSOSTriggered = false
        self.noEmergencyContactsLabel.text = self.kNoEmergencyContacts
        self.noEmergencyContactsLabel.isHidden = self.contactsArray.count > 0 ? true : false
        self.descriptionLabel.isHidden = false

    }
    
    @objc func showSmsSuccessMessage(isSuccess: Bool) -> Void {
        
        if isSuccess {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy hh:mma "
            let dateStr = formatter.string(from: Date())
            
            //self.view.showIndicatorWithSuccess("SMS with your mobile location @ " + dateStr + " has been sent to your registered contacts ")
            self.noEmergencyContactsLabel.isHidden  = false
            self.noEmergencyContactsLabel.text = "SMS with your mobile location @ " + dateStr + " has been sent to your registered contacts "
            self.descriptionLabel.isHidden = true
        }
        else {
            
            self.view.showIndicatorWithError("Not able to trigger SMS , Check your network connection")
        }
        
        
    }
}
