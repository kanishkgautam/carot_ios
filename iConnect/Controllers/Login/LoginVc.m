//
//  LoginVc.m
//  iConnect
//
//  Created by Amit Priyadarshi on 28/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "LoginVc.h"
#import "CAROT-Swift.h"
#import "SignUpOTPViewController.h"

//#import "AddDeviceView.swift"

@interface LoginVc ()
@property(nonatomic, strong) UIActivityIndicatorView* signInActivity;
@end

@implementation LoginVc
@synthesize signInActivity;
- (void)viewDidLoad {
    [super viewDidLoad];
//    userNameTextField.text = @"data@yopmail.com";//@"shrutitest@yopmail.com";//@"78095-iconnect-carotdemo@yopmail.com";//@"75241-iconnect-travel1@yopmail.com"; // @"59-iconnect-marutidemo01@gmail.com";
//    passwordTextField.text = @"demo123";
    [[AppUtility sharedUtility] trackScreenNameWith:@"Login Screen"];
    /* BOOL checkUser = [SICUserRequest isUserLoggedIn];
     if(checkUser)
     {
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     AddDeviceViewController *myVC = (AddDeviceViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AddDeviceControllerID"];
     // myVC.boolBackEnabled = false;
     [self.navigationController pushViewController:myVC animated:NO];
     
     
     }*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    
}

- (IBAction)loginButtonTapped:(id)sender {
    txtFieldInValid = nil;
    bool isEmailValid =false;
    // bool isValidPassword = false;
    //    NSLog(@"%@",self.navigationController.viewControllers);
    
    isEmailValid = [[AppUtility sharedUtility] isValidEmailWithEmailToBeValidated:userNameTextField.text];
    
    if(userNameTextField.text.length == 0){
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"User Id is Required", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = userNameTextField;
        return;
    }
    isEmailValid = [[AppUtility sharedUtility] isValidEmailWithEmailToBeValidated:userNameTextField.text];
    if (!isEmailValid) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Invalid Email Id", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = userNameTextField;
        return;
    }
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:passwordTextField.text].length < 6) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Password must contain min 6 characters , blank spaces are not allowed", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = passwordTextField;
        return;
    }
    
    [self.view showIndicatorWithProgressWithMessage:@""];
    self.view.userInteractionEnabled = NO;
    //NSString *deviceToken = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:(@"deviceToken")];
    
    if (deviceToken == nil || deviceToken.length < 1) {
        deviceToken = @"";
    }
    

    [[AppUtility sharedUtility] sendGAEventWithEventName:@"LoginTapped" category:kEventCategoryLogin andScreenName:@"Login Screen" eventLabel:NULL eventValue:NULL];
    
    [SICUserRequest loginUserWithId:[[AppUtility sharedUtility] trimWhiteSpacesInString:userNameTextField.text] password:[[AppUtility sharedUtility] trimWhiteSpacesInString:passwordTextField.text] deviceToken:deviceToken response:^(id responseObject, BOOL requestSuccess, NSError *error) {
        
        self.view.userInteractionEnabled = YES;
        
        if (!error && requestSuccess) {
            [self.view dismissProgress];
            
            [SICVehicleRequest getUserDevicesResponse:^(id response, BOOL isSuccessful, NSError *error) {
                if(isSuccessful){
                    NSArray *arrayResponse = response;
                    //[[CommonData sharedData] clearDataForLogout];
                    
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastSavedLocation"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastSavedLocationDate"];
                    
                    if (arrayResponse.count == 0) {
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        AddDeviceViewController *addDeviceViewController = (AddDeviceViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AddDeviceControllerID"];
                        addDeviceViewController.isFirstTime = YES;
                        [self.navigationController presentViewController:addDeviceViewController animated:YES completion:^{
                            // nothing to do here for now
                        }];
                        
                    }
                    else {
                        
                        __block int flag = 0;
                        [arrayResponse enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            CDevice *deviceObj = (CDevice*)obj;
                            if([deviceObj.status isEqualToString:kDeviceStatusSubscriptionPending] || [deviceObj.status isEqualToString:kDeviceStatusActivationInProgress]){
                                flag++;
                            }
                        }];
                        
                        //CDevice *device = arrayResponse[0];
                        if(arrayResponse.count == flag){
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            DeviceManagementViewController *deviceManagementViewController = (DeviceManagementViewController *)[storyboard instantiateViewControllerWithIdentifier:@"DeviceManagementViewController"];
                            deviceManagementViewController.hideBackButton = true;
                            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:deviceManagementViewController];
                            [navController.navigationBar setTranslucent:false];
                            [self presentViewController:navController animated:YES completion:^{
                                // nothing to do here for now
                            }];
                        }
                        else
                        {
                            //                            [self getVehiclesAndSet];
                            AppDelegate *delegateObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            [[delegateObj getCentreController] showAnimationViewAndUpdateDataInBackground];
                            
                            [self dismissViewControllerAnimated:YES completion:^{
                                //  nothing to do here
                                
                            }];
                        }
                    }
                }
            }];
        }else{
            if (error && error.code == 400) {
                [self.view showIndicatorWithError:@"Invalid username or password."];
            }else if (error && error.code == 401){
                [self.view dismissProgress];
                [self performSegueWithIdentifier:kLoginToOTPSegue sender:nil];
            }else{
                [self.view showIndicatorWithError:error.localizedDescription];
            }
        }
    }];
    
}

-(void )prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:kLoginToOTPSegue]) {
        SignUpOTPViewController *signupOTPControllerObj = segue.destinationViewController;
        [signupOTPControllerObj setEmailConfirmString:userNameTextField.text];
        
    }
}


-(void)getVehiclesAndSet {
    [SICVehicleRequest getUserVehiclesResponse:^(id response, BOOL isSuccessful, NSError *error) {
        NSArray *arrayVehicles = response;
        
        if (arrayVehicles != nil && arrayVehicles.count > 0) {
            CVehicle *vehicleObj = (CVehicle*) [arrayVehicles objectAtIndex:0];
            
            //            [AppUtility sharedUtility].vehiclesArray = arrayVehicles;
            
            NSMutableArray *vehiclenameArray = [[NSMutableArray alloc] init];
            
            for (int i = 0; i<arrayVehicles.count; i++) {
                CVehicle *vehicleObj = (CVehicle*) [arrayVehicles objectAtIndex:i];
                
                [vehiclenameArray addObject:vehicleObj.name];
            }
            
            //            [AppUtility sharedUtility].vehiclesArray  = arrayVehicles.copy;
            [AppUtility sharedUtility].arrayMake = vehiclenameArray;
            
            [[CommonData sharedData] setSelectedVehicleWithSelectedVehicle:vehicleObj];
            
            [SICVehicleRequest setSelectedForVehicleWithId:vehicleObj.vehicleId withError:&error];//22976 alto    21657 innova    21656 liva //vehicleObj.vehicleId //90 qa
            if(error)
                NSLog(@"%@",error.description);
        }
    }];
}


//MARK: UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(txtFieldInValid != nil){
        if(string.length >= 1){
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:true];
    if (textField == userNameTextField) {
        [passwordTextField becomeFirstResponder];
    } else {
        [self.view endEditing:true];
        //[self loginButtonTapped:nil];
    }
    return true;
}


- (IBAction)createAccountTapped:(id)sender {
    [self performSegueWithIdentifier:kLoginToCreateAcountSegue sender:nil];
}

- (IBAction)forgotPasswordTapped:(id)sender {
    
    [self performSegueWithIdentifier:kLoginToForgotPasswordSegue sender:nil];
}
@end
