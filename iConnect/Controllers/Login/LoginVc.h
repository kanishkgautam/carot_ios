//
//  ViewController.h
//  iConnect
//
//  Created by Administrator on 11/17/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVc : UIViewController{
    
    __weak IBOutlet UITextField *userNameTextField;
    __weak IBOutlet UITextField *passwordTextField;
    UITextField *txtFieldInValid;
}
- (IBAction)loginButtonTapped:(id)sender;
- (IBAction)createAccountTapped:(id)sender;
- (IBAction)forgotPasswordTapped:(id)sender;


@end

