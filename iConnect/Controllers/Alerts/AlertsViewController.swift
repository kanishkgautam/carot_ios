//
//  AlertsViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 10/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class AlertTableViewCell: UITableViewCell {
    
    @IBOutlet weak var alertIamgeIcon: UIImageView!
    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var limitLabel: UILabel!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    func setDataOnCellFrom(eventObj event:CEvent, index: Int) {
        alertTitle.text = event.name
        
        if let onLocation = event.onLocation {
            
            if let geoCode = onLocation.geoCode {
                
                if geoCode.count > 0 {
                    locationLabel.text = ("Location :" + geoCode)
                }else{
                    locationLabel.text = ("Location: Updating location")
                    CEventRequest.sharedInstance().resolveAddress(for: event, atIndex: 0, onComplete: { (resolvedEvent, index) in
                        if (resolvedEvent!.onLocation.geoCode!.count > 0){
                            self.locationLabel.text = ("Location: " + resolvedEvent!.onLocation.geoCode!)
                        }else{
                            self.locationLabel.text = ("Location: Address unavailable")
                        }
                    })
                }
                
            } else {
                locationLabel.text = ("Location: Updating location")
                CEventRequest.sharedInstance().resolveAddress(for: event, atIndex: 0, onComplete: { (resolvedEvent, index) in
                    if let geoCode = (resolvedEvent?.onLocation?.geoCode){
                        
                        if geoCode.count > 0 {
                            self.locationLabel.text = ("Location: " + resolvedEvent!.onLocation.geoCode!)
                            
                        }
                    }else{
                        self.locationLabel.text = ("Location: Address unavailable")
                    }
                })
            }
        } else {
            self.locationLabel.text = ("Location: Address unavailable")
            
        }
        
        if event.showCurrentAndThresholdValue.boolValue == true {
            
            limitLabel.text =   ("Limit :" + event.thresholdValue.stringValue)
            currentLabel.text =  ("Current :" + event.currentValue.stringValue)
            
        }else{
            currentLabel.text = ""
            limitLabel.text = ""
        }
        
        dateTimeLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: (event.timeStamp)!)
        
        if event.read {
            alertTitle.font = UIFont(name: "Helvetica", size: 11)
            alertTitle.textColor = UIColor(red:0.25, green:0.30, blue:0.33, alpha:1.0)
        }else{
            alertTitle.font = UIFont(name: "Helvetica-Bold", size: 11)
            alertTitle.textColor = UIColor(red:0.97, green:0.58, blue:0.11, alpha:1.0)
        }
        
        var eventType:String = ""
        eventType = event.vehicleAlarmType
        if event.eventUnit.count > 0 {
            
            if event.thresholdValue == 0 {
                currentLabel.isHidden = true
                limitLabel.isHidden = true
                
            }
            else {
                currentLabel.isHidden = false
                limitLabel.isHidden = false
                currentLabel.text = (currentLabel.text! + " " + event.eventUnit)
                limitLabel.text = (limitLabel.text! + " " + event.eventUnit)
                
            }
            
            
        }else{
            currentLabel.isHidden = true
            limitLabel.isHidden = true
        }
        
        
        switch eventType {
        case "HARD_DECELERATION", "HARD_ACCELERATION":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertScoreRead") : #imageLiteral(resourceName: "AlertScoreUnread"))
            
        case "HIGH_RPM", "IDLE_ENGINE":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertScoreRead") : #imageLiteral(resourceName: "AlertScoreUnread"))
            
        case  "OVER_SPEEDING":
            
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertScoreRead") : #imageLiteral(resourceName: "AlertScoreUnread"))
            
        case "LOW_VOLTAGE":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertBatteryRead") : #imageLiteral(resourceName: "AlertBatteryUnread"))
            
        case "TOWING":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertTowRead") : #imageLiteral(resourceName: "AlertTowUnread"))
        case "CRASH":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertImpactRead") : #imageLiteral(resourceName: "AlertImpactUnread"))
        case "POWER_ON", "POWER_OFF":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertDeviceRead") : #imageLiteral(resourceName: "AlertDeviceUnread"))
        case "IGNITION_ON", "IGNITION_OFF":
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertEngineEventRead") : #imageLiteral(resourceName: "AlertEngineEventUnread"))
        default:
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertDeviceRead") : #imageLiteral(resourceName: "AlertDeviceUnread"))
        }
        
        if event.vehicleEventType == "SYSTEM" {
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "DocumentRead") : #imageLiteral(resourceName: "DocumentUnread"))
            
            self.locationLabel.isHidden = true
        }
        
        if event.dtcCode != nil && event.dtcCode != "" {
            alertIamgeIcon.image = (event.read ? #imageLiteral(resourceName: "AlertDTCRead") : #imageLiteral(resourceName: "AlertDTCUnread"))
        }
    }
}

class TipCell: UITableViewCell {

    @IBOutlet weak var tipImageView: UIImageView!
    
    @IBOutlet weak var tipTitleLable: UILabel!
}


class AlertsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var arrPromotions = [CUserInboxItem]()
    var arrAlerts = [Any]()
    //    var refreshControl:UIRefreshControl!
    
    var lastRefreshTime = ""
    
    @IBOutlet weak var viewAlertDescription: UIView!
    
    @IBOutlet weak var labelAlertLocation: UILabel!
    @IBOutlet weak var labelAlertTitle: UILabel!
    @IBOutlet weak var labelAlertTime: UILabel!
    
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBOutlet weak var lastRefrestTitle: UILabel!
    
    @IBOutlet weak var noRecordsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Alerts Screen")
        self.addPullToRefresh()
        self.navigationController?.navigationBar.isHidden = false
        self.tableView.rowHeight = UITableView.automaticDimension
        
        configureNavBar()
        self.noRecordsLabel.isHidden = true
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "ALERTS"
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
    }
    
    
    func addPullToRefresh(){
        self.tableView.addSubview(self.refreshControl)
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        self.getPromotions()
//        self.getEvents()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        AppUtility.sharedUtility.allEventsRead = true
        
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
        
        self.refreshControl.beginRefreshing()
        
        DispatchQueue.main.async {
            self.arrAlerts.removeAll()
            self.tableView.reloadData()
        }
        
        self.getPromotions()
        
        self.lastRefreshTime = self.getCurrentDate()
        self.lastRefrestTitle.text = self.lastRefreshTime
    }
    
    func getCurrentDate() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy hh:mma "
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        return formatter.string(from: NSDate() as Date)
        
    }
    
    @IBAction func closeAlertDescriptionPopUp(_ sender: Any) {
        viewAlertDescription.isHidden = true
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AlertsViewController.handleRefresh(refreshControl:)), for: UIControl.Event.valueChanged)
        
        return refreshControl
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        self.getPromotions()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func getPromotions()
    {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
        return
        }
        
        CMailBoxRequest.sharedInstance().getCurrentActivePromotionsResponse { (response, isSuccessful, error) in
            if(isSuccessful){
                self.arrPromotions = response as! [CUserInboxItem]
            }
            self.getEvents()
        }
    }
    
    func checkIfNoAlertsLabelNeedsToBeShown(){
        if (self.arrAlerts.count + self.arrPromotions.count) == 0 {
            self.noRecordsLabel.isHidden = false
        } else {
            if arrAlerts.count == 0 {
                AppUtility.sharedUtility.allEventsRead = true

                return
            }

            let alert = self.arrAlerts.first as? CEvent
            CommonData.sharedData.lastEventTimeStamp = String(describing: alert!.timeStamp.timeIntervalSince1970 * 1000)

            UserDefaults.standard.set(CommonData.sharedData.lastEventTimeStamp, forKey: "lastEventTimeStamp")
            AppUtility.sharedUtility.allEventsRead = true
            self.noRecordsLabel.isHidden = true
        }
    }
    
    func getEvents(){
        self.lastRefreshTime = self.getCurrentDate()
        self.lastRefrestTitle.text = self.lastRefreshTime
        
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
        
        CEventRequest.sharedInstance().getTopEvents(20, response: { (responseFromDB, isSuccessful, error) in
            
            self.refreshControl.endRefreshing()
            if(isSuccessful) {
                
                if( responseFromDB != nil && ((responseFromDB as! NSArray).count) > 0){
                    self.arrAlerts = (responseFromDB as! NSArray) as! [Any]
                    self.refreshControl.endRefreshing()
                    
                    self.checkIfNoAlertsLabelNeedsToBeShown()
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        })
        { (eventsList, error) in
            self.refreshControl.endRefreshing()
            
            if(eventsList != nil) {
                
                if let _ = eventsList {
                    if self.arrAlerts.count == 0 {
                        self.arrAlerts = eventsList!
                    }
                    else {
                        for newEvent in eventsList! {
                            let newCEvent = newEvent as! CEvent
                            
                            if !self.isAlertsArrayContainsAlert(newEvent: newCEvent) {
                                self.arrAlerts.append(newCEvent)
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            self.checkIfNoAlertsLabelNeedsToBeShown()
            
        }
    }
    
    func loadMoreEventsWithLastAlert(count: Int) -> Void {
        if !AppUtility.sharedUtility.isNetworkAvailable() {
            return
        }
                
        CEventRequest.sharedInstance().loadMoreEvents(withOffsetData: 20, withOffset: Int32(count), response: { (responseFromDB, isSuccessful, error) in
            
            if( responseFromDB != nil && ((responseFromDB as! NSArray).count) > 0){
                
                if self.arrAlerts.count == 0 {
                    self.arrAlerts = (responseFromDB as! NSArray) as! [CTrip]
                    self.tableView.reloadData()
                }else {
                    if (responseFromDB as! NSArray).count > 0{
                        self.arrAlerts.append(contentsOf: (responseFromDB as! NSArray))
                        self.tableView.reloadData()
                    }else{
                        self.view.showIndicatorWithInfo("No more data to load")
                    }
                }
                CommonData.sharedData.markAlertsAsRead(alertsArray: self.arrAlerts as! [CEvent])

            }
            
        }) { (eventsList, error) in
            
            if error == nil {
                let prevCount = self.arrAlerts.count
                
                if let _ = eventsList {
                    if self.arrAlerts.count == 0 {
                        self.arrAlerts = eventsList as! [CEvent]
                        self.tableView.reloadData()
                    }
                    else {
                        if eventsList?.count != 0 {
                            for newEvent in eventsList! {
                                if !self.isAlertsArrayContainsAlert(newEvent: newEvent as! CEvent) {
                                    self.arrAlerts.append(newEvent as! CEvent)
                                }
                            }
                            if prevCount != self.arrAlerts.count {
                                self.tableView.reloadData()

                            }
                        }

//                        if (eventsList?.count)! > 0{
//                            self.arrAlerts.append(contentsOf: eventsList as! NSArray)
//                            self.tableView.reloadData()
//                        }else{
//                            self.view.showIndicatorWithInfo("No more data to load")
//                        }
//
                    
                    }
                }
                CommonData.sharedData.markAlertsAsRead(alertsArray: self.arrAlerts as! [CEvent])

            }else{
                self.view.showIndicatorWithError(error?.localizedDescription)
            }
        }
    }
    
    func isAlertsArrayContainsAlert(newEvent: CEvent) -> Bool {
        
        for i in 0...self.arrAlerts.count-1 {
            
            let cEvent = self.arrAlerts[i] as! CEvent
            if cEvent.eventId == newEvent.eventId {
                self.arrAlerts[i] = newEvent
                return true
            }
        }
        return false
        
    }
    

    //MARK:- UITableView delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAlerts.count + self.arrPromotions.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.arrPromotions.count > 0 && self.arrPromotions.count > indexPath.row {
            return 50
        }
        
        return UITableView.automaticDimension
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //create cell for Promotion/Tip
        if arrPromotions.count > 0 && arrPromotions.count > indexPath.row {
            let promotion = arrPromotions[indexPath.row]
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"TipCell", for: indexPath) as! TipCell
            
            cell.tipTitleLable.text = promotion.title
            
            return cell
            
        }
        else {

            //create cell for alerts
            let cell = tableView.dequeueReusableCell(withIdentifier:"CellAlerts", for: indexPath) as! AlertTableViewCell

            let index = (indexPath.row - arrPromotions.count) < 0 ? (arrPromotions.count - indexPath.row) : (indexPath.row - arrPromotions.count)
            
            var event = self.arrAlerts[index] as! CEvent
            
            //cell.backgroundColor = UIColor.white
            cell.dateTimeLabel.isHidden = false
            cell.locationLabel.isHidden = false

            cell.setDataOnCellFrom(eventObj: event, index:indexPath.row)
            
            if (event.onLocation != nil) && (event.onLocation.geoCode != nil) {
                cell.locationLabel.text = ("Location : " + event.onLocation.geoCode!)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.arrPromotions.count > 0 && indexPath.row < self.arrPromotions.count {
            let promotion = self.arrPromotions[indexPath.row]
            let alert = UIAlertController(title: promotion.title, message: promotion.message, preferredStyle: .alert)
            self.markPromotionRead(promotion: promotion)

            let okAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: { (action) in
                self.getPromotions()

            })
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
            let event = self.arrAlerts[indexPath.row - self.arrPromotions.count] as! CEvent
            
            if event.dtcCode != nil && event.dtcCode != "" {
                self.getDTCEventDetail(dtcEvent: event)
            }

        }
    }
    
    func getDTCEventDetail(dtcEvent:CEvent)
    {
        self.view.showIndicatorWithProgress(message: "Please wait...")
        CVehicleRequest.sharedInstance().getDtcDetail(forDtcCode: dtcEvent.dtcCode) { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            
            if(error == nil && responseObject != nil)
            {
                print(responseObject ?? NSDictionary())
                self.view.dismissProgress()
                //                let detailView:DTCEventDetailView = UIView (frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)) as! DTCEventDetailView
                //                detailView.dtcDetailDictionary = responseObject as! NSDictionary
                //                self.view.addSubview(detailView)
                
                let viewC:DTCEventDetailView = Bundle.main.loadNibNamed("DTCEventDetailView", owner: nil, options: nil)?.first as! DTCEventDetailView
                viewC.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                viewC.dtcDetailDictionary = responseObject as! NSDictionary
                viewC.labelTitle.text = dtcEvent.name
                
                UIApplication.shared.keyWindow?.addSubview(viewC)
            }
            else
            {
                if (responseObject != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseObject as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
    }
    
    func markPromotionRead(promotion: CUserInboxItem) -> Void {
        
        CMailBoxRequest.sharedInstance().markReadForPromotion(withId: promotion.messageId as! UInt) { (responseDict, isSuccess, error) in
            
            if isSuccess {
//                for i in 0...self.arrPromotions.count-1 {
//                    let promo = self.arrPromotions[i]
//                    if promotion.messageId == promo.messageId {
//                        self.arrPromotions.remove(at: i)
//                        self.tableView.reloadData()
//                        break
//                    }
//                }
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrAlerts.count == 0 {
            return
        }
        
        let  nextpage = self.arrAlerts.count + self.arrPromotions.count - 2
        if indexPath.row == nextpage {
            
            let count = self.arrAlerts.count
            self.loadMoreEventsWithLastAlert(count: count)
        }
    }
}
