//
//  GeoFenceTableViewCell.swift
//  iConnect
//
//  Created by Lokesh on 22/12/20.
//  Copyright © 2020 Administrator. All rights reserved.
//

import Foundation

import UIKit


class GeoFenceTableViewCell: UITableViewCell {
  
    @IBOutlet weak var dotsButtonObj: UIButton!
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var fenceNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))

    }
  
    func updateCellLayout(_ obj: CGeoFence) {
        fenceNameLabel.text = obj.name
        radiusLabel.text = String.init(format: "%d KM", obj.radius/1000)
        
    }

}
