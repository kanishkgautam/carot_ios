//
//  GeoFenceViewController.swift
//  iConnect
//
//  Created by Lokesh on 22/12/20.
//  Copyright © 2020 Administrator. All rights reserved.
//

import Foundation
import UIKit
import AAFloatingButton

class GeoFenceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    lazy var presentationDelegate = PickerControllerPresentationManager()
    @IBOutlet weak var fenceTableViewObj: UITableView!
    internal var isMapVisible:Bool = false
    var vehicleHeader:HeaderPickerArea?

    var arrayOfFences:Array = [CGeoFence]()
    
    var uin:String?
    
    var timerVehicleStateRefresh : Timer?
    var vehicleCurrentState:CVehicleCurrentState?
    var currentTag:NSNumber?
    var locationsDelta = [NSDictionary]()
    var oldTripId: String?
    
    let headerButton = UIButton(type: .custom)
    var arrayCurrentTripLocations = [CLocation]()
    
    var gestureRecognizer:UIPanGestureRecognizer?
    
    var driveScore: CDriveScore?
    
    var driverScoreOverlay:DriverScoreOverlay?
    
    var updateInterval = kVehicleLocationUpdateIntervalTenSec
        
    var healthStatus:CVehicleHealthStatus?
    //MARK:- Life Cycle Methods
    @IBOutlet weak var eventsButton: UIButton!
    
    @IBAction func alertsButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertsNavController")
        vc.modalPresentationStyle = .fullScreen;
        self.navigationController?.present(vc, animated: true, completion: {
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // oldTripId = "None"
        AppUtility.sharedUtility.trackScreenName(with: "Dashboard Screen")
        let currentState:CVehicleCurrentState = CVehicleCurrentState()
        let locationObj:CLocation = CLocation()
        
        currentState.location = locationObj
        vehicleCurrentState = currentState

        self.gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        let notificationIdentifier = Notification.Name("UpdateDataObDashboard")
        NotificationCenter.default.addObserver(self, selector: #selector(fetchDataForDashboardView), name: notificationIdentifier, object: nil)
        self.view.aa_addFloatingButton("+", .orange, size: 50, bottomMargin: 40) {
                      
            let storyBoard : UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
            let VC = storyBoard.instantiateViewController(withIdentifier: "AddEditGeoFenceVC") as! AddEditGeoFenceVC
            self.navigationController?.pushViewController(VC, animated: true)
                        
        }
    }

    func getAllFences(){
        CVehicleGeofence.sharedInstance()?.getAllFence({ (arrayOfGeoFences, isCompleted, error) in
                     
            if self.arrayOfFences.count > 0 {
                self.arrayOfFences.removeAll()
            }
            
            if ((arrayOfGeoFences) != nil){
                let allFences = arrayOfGeoFences as! NSArray//.sorted() as NSArray
                 for fence in allFences{
                    let fenceObj = fence as! CGeoFence
                    self.arrayOfFences.append(fenceObj)
                }
            }
            
            self.fenceTableViewObj.reloadData()
         })
    }
    
    @objc func handlePan(recognizer: UIPanGestureRecognizer) -> Void {
        
        // Disable Pan gesture of MFSideMenucontoller to get Pan Gesture of Map
        
        if (recognizer.location(in: self.view).y > 66) {
            recognizer.isEnabled = false
            recognizer.isEnabled = true
        }
    }
    
    func getDataForDashboardHeader(){
        if CommonData.sharedData.isDataPrefetched {
            CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (vehicleObj) in
                if vehicleObj != nil {
                    //print(vehicleObj)
                    self.getVehicleCurrentLocation()
                    
                    if let _ = self.navigationItem.titleView {
                        self.setHeaderTitle(title: vehicleObj?.name!)
                    }
                    else {
                        self.navigationItem.titleView = self.createHeaderView()
                    }
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicleObj){
                        AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                    }
                    
                }else{
                    
                    CommonData.sharedData.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                        
                        for vehicle in vehicles{
                            
                            if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle){
                                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                            }
                        }
                    })
                    
                }
            })
        }
        
        getAllFences()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        
        self.navigationItem.titleView = self.createHeaderView()
      
        
          if CUserRequest.sharedInstance().isUserLoggedIn() && CommonData.sharedData.isDataPrefetched {
              
              if let readStatus = AppUtility.sharedUtility.allEventsRead {
                  if readStatus {
                      self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                  }
                  else {
                      self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                      
                  }
                  
              }
              
              self.getListOfUnreadEvents()
              
              self.fetchDataForDashboardView()
              
          }
          
    }
    
    @objc func fetchDataForDashboardView() {
        self.getDataForDashboardHeader()
        self.getVehicleHealth()
        self.getListOfUnreadEvents()
        self.navigationItem.titleView = self.createHeaderView()
        
    }
    
    func getListOfUnreadEvents() -> Void {
           
           let dateString = CommonData.sharedData.lastEventTimeStamp
           
           CEventRequest.sharedInstance().getNewEventsCount(afterTime: dateString) { (responseData, isSuccess, error) in
               
               if isSuccess {
                   
                   let eventCount = responseData as? NSNumber
                   
                   if eventCount == 0 {
                       self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                       AppUtility.sharedUtility.allEventsRead = true
                   }
                   else {
                       self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                       AppUtility.sharedUtility.allEventsRead = false
                   }
               }
               else {
                   
                   
               }
           }
       }
    
    
    //MARK:- Vehicles Fetch Method
    
    func getVehicleList() -> Void {
        
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            if vehicles.count == 0 {
                return
            }
            
            self.navigationItem.titleView = self.createHeaderView()
            let vehicle:CVehicle = vehicles.first!
            var errorObj:NSError?
            CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError: &errorObj)
         
            self.setHeaderTitle(title: vehicle.name)
            self.getVehicleCurrentLocation()
        }
        
    }
    
    
    //MARK:- Header Tap Method
    
    @IBAction func headerTapped(_ sender:Any) {
        
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            if vehicles.count > 1{
                
                
                let pickerView:CustomPickerView = CustomPickerView()
                pickerView.transitioningDelegate = self.presentationDelegate
                pickerView.modalPresentationStyle = .custom
                pickerView.isShowingHeader = true
                pickerView.pickerTitle = "Select a Vehicle"
                
                self.arrayCurrentTripLocations.removeAll()
                
                
                var vehicleNameArray:[String] = []
                for vehicle in vehicles {
                    vehicleNameArray.append(vehicle.name)
                }
                
                pickerView.arrayData = vehicleNameArray as NSArray//.sorted() as NSArray
                
                pickerView.doneButtonCallback { (row, component) in
                    self.vehicleCurrentState = nil
                   
                    let vehicle = vehicles[row]
                    
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle) {
                        pickerView.dismiss(animated: true, completion: nil)
                        
                        
                        var error:NSError? = nil
                        CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError:&error)
                        
                        if device.status == kDeviceStatusSubscriptionPending || device.status == kDeviceStatusActivationInProgress || device.status == kDeviceStatusTerminated || device.status == kDeviceStatusExpired {
                            pickerView.dismiss(animated: true, completion: {
                                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                            })
                            
                            
                        }//else{
                        
                        self.vehicleCurrentState = nil
                        UserDefaults.standard.removeObject(forKey: "lastSavedLocation")
                        UserDefaults.standard.removeObject(forKey: "lastSavedLocationDate")
                        
                        self.setHeaderTitle(title: vehicle.name)
                        
                        if let _ = self.timerVehicleStateRefresh {
                            self.timerVehicleStateRefresh?.invalidate()
                            self.timerVehicleStateRefresh = nil
                        }
                        
                        if(error != nil) {
                            self.view.dismissProgress()
                            print(error?.localizedDescription ?? String())
                        }
                        else{
                            // self.view.dismissProgress()
                            self.view.showIndicatorWithProgress(message: "Getting vehicle details..")
                            
                            self.getVehicleCurrentLocation()
                            self.getOverallScoreForSelectedVehicle()
                            //                        print("selected vehicle is"+vehicle.name)
                        }
                        //                        }
                    }
                }
                
                self.present(pickerView, animated: true, completion: nil)
                print("header tapped")
                
            }
        }
    }
    
    func setHeaderTitle(title: String?) -> Void {
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            
            if vehicles.count <= 1 {
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!), for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name), for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first?.name ?? ""), for: .normal)
                        }
                    })
                }
            }else{
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!) + " ▼" , for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name) + " ▼" , for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first!.name) + " ▼" , for: .normal)
                        }
                        
                    })
                }
            }
        }
        self.headerButton.titleLabel?.sizeToFit()
        //self.headerButton.sizeToFit()
    }
    
    func startTimer () {
        
        if timerVehicleStateRefresh == nil {
            timerVehicleStateRefresh =  Timer.scheduledTimer(
                timeInterval: TimeInterval(self.updateInterval),
                target      : self,
                selector    : #selector(refreshVehicleCurrentState),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopTimer() {
        if timerVehicleStateRefresh != nil {
            timerVehicleStateRefresh?.invalidate()
            timerVehicleStateRefresh = nil
        }
    }

    
    func getAddressofLocation(completion: @escaping (_ address: String) -> Void)
    {
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: (vehicleCurrentState?.location.latitude)!, longitude: (vehicleCurrentState?.location.longitude)!)
        
        CVehicleRequest.sharedInstance().getAddressForLocation(location) { (responseObject, isSucessful, error) in
            if(error == nil && responseObject != nil)
            {
                DispatchQueue.main.async  {
                    
                    let addressStr:String = responseObject as! String
                    if(addressStr == "Unable to find location" || addressStr == kUnableToGetLocation){
                        
                        if (UserDefaults.standard.value(forKey: "lastSavedLocation") != nil) {
                            let locationStr = UserDefaults.standard.value(forKey: "lastSavedLocation") as! String
                            completion(locationStr)
                        }
                        else {
                            completion("Location not found")
                        }
                        
                        
                    }else {
                        //save  vehicle last location
                        UserDefaults.standard.set(addressStr, forKey: "lastSavedLocation")
                        UserDefaults.standard.set(self.vehicleCurrentState?.updateTime, forKey: "lastSavedLocationDate")
                        
                        completion(addressStr)
                    }
                }
                
            }
            else{
                DispatchQueue.main.async {
                    let locationStr = UserDefaults.standard.value(forKey: "lastSavedLocation") as! String?
                    completion(locationStr!)
                }
            }
            
            
        }
    }
    
    func getVehicleCurrentLocation()
    {
        
        self.startTimer()
        
        CVehicleRequest.sharedInstance().getCurrentState { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            
            if(error == nil  && responseObject != nil){
                self.vehicleCurrentState = responseObject as! CVehicleCurrentState?
                
                if let deviceMode = self.vehicleCurrentState?.deviceMode
                {
                    UserDefaults.standard.set(deviceMode, forKey:"deviceMode")
                    UserDefaults.standard.synchronize()
                }
                
                self.uin = self.vehicleCurrentState?.uin
                             
                self.getOverallScoreForSelectedVehicle()
                          
                self.getAllFences()
                
                if let _ = self.vehicleCurrentState?.location {
                    
                    if self.updateInterval == kVehicleLocationUpdateIntervalThirtyMin {
                        self.stopTimer()
                        self.updateInterval = kVehicleLocationUpdateIntervalTenSec
                        self.startTimer()
                    }
                    
                }
                else {
                    if self.updateInterval == kVehicleLocationUpdateIntervalThirtyMin {
                        self.stopTimer()
                        self.updateInterval = kVehicleLocationUpdateIntervalTenSec
                        self.startTimer()
                    }
                    
                }
                
                //get all location if vehicle is running/trip exists
                if(self.vehicleCurrentState?.trip != nil){
                    if((self.vehicleCurrentState?.trip.tripId) != ""){
                        
                        self.oldTripId = self.vehicleCurrentState?.trip.tripId
                    }
                    
                }
                else{
                    self.oldTripId = "None"
                }
                /* self.getAddressofLocation() {
                 (address: String) in
                 print("got back: \(address)")
                 dashBoardLocationCell.addressLabel.text = address
                 if let updateTime = self.vehicleCurrentState?.updateTime {
                 dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
                 
                 }
                 }*/
                
            }
            else {
            
                
            }
            
        }
    }
    
    @objc func refreshVehicleCurrentState()
    {
                
//        CVehicleRequest.sharedInstance().getCurrentStateOfVehicle(withTag: currentTag, response: { (responseObject, isSuccessful, error) in
//            if(error == nil  && responseObject != nil){
//
//                let indexPath = IndexPath(item: 0, section: 0)
//                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
//                dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
//
//
//                let vehicleLocation:CVehicleCurrentState = (responseObject as! CVehicleCurrentState?)!
//
//                if let selectedVehivleobj:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle() {
//
//                    let selectedVehicleId:NSNumber = NSNumber(integerLiteral: Int(selectedVehivleobj.vehicleId))
//                    if(vehicleLocation.vehicleId == selectedVehicleId)
//                    {
//                        // self.vehicleCurrentState = vehicleLocation
//                        self.checkCurrentTripExists(currentLocation: vehicleLocation)
//                        dashBoardLocationCell.setCellData(vehicleState: vehicleLocation)
//                        //                        self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
//                    }
//                }
//
//            }
//            else {
//
//                if responseObject != nil && error == nil {
//
//                    if !(self.timerVehicleStateRefresh?.isValid)! && self.updateInterval != kVehicleLocationUpdateIntervalThirtyMin {
//                        self.stopTimer()
//                        self.updateInterval = kVehicleLocationUpdateIntervalThirtyMin
//                        self.startTimer()
//
//                    }
//
//                }
//
//                let indexPath = IndexPath(item: 0, section: 0)
//                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
//
//                dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusInactive") //now
//            }
//        }) { (tag, delta) in
//            if((tag != nil) && (self.vehicleCurrentState?.trip != nil) && (delta != nil))
//            {
//                let indexPath = IndexPath(item: 0, section: 0)
//                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
//                if(dashBoardLocationCell.tripPath?.vertices != nil){
//                    self.currentTag = tag
//                    if let deltaArray = delta as? [NSDictionary] {
//                        self.locationsDelta = deltaArray
//                    }
//
//
//                    if(dashBoardLocationCell.tripPath?.vertices.count == self.arrayCurrentTripLocations.count){
//
//                        for (index, element) in (delta?.enumerated())! {
//                            print("Item \(index): \(element)")
//
//                            if let locationObj = delta?[index] as? CLocation {
//                                self.arrayCurrentTripLocations.append(locationObj)
//                            }
//
//                            //                            let clocationObj = delta as AnyObject
//                            //                            self.arrayCurrentTripLocations.append(clocationObj as! CLocation)
//
//                        }
//                        //sort on timestamp
//                        let sortedArray = self.arrayCurrentTripLocations.sorted(by:
//                        {
//                            Double(($0 ).timeStamp) > Double(($1 ).timeStamp)
//                        })
//                        //print(sortedArray)
//
//                        //update cell data
//                        dashBoardLocationCell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: false,tripExists: true )
//                    }
//                }
//
//
//
//
//            }
//        }

        
    }
    
//    -(CLocation*)mappingToUserModel
//    {
//        CLocation* newObj = [CLocation new];
//        newObj.direction = [self.direction floatValue];
//        newObj.latitude = [self.latitude floatValue];
//        newObj.longitude = [self.longitude floatValue];
//        newObj.speed = [self.speed floatValue];
//        newObj.rpm = [self.rpm floatValue];
//        newObj.geoCode = self.geoCode;
//        newObj.timeStamp = [self.timeStamp doubleValue];
//        newObj.forTripLocation = [self.forTripLocation userModel];
//        newObj.forTripStaticData = [self.forTripStaticData mappingUserModel];
//        newObj.forTripEndLocation = [self.forTripEndLocation userModel];
//        newObj.fotTripStartLocation = [self.fotTripStartLocation userModel];
//        newObj.forTripBreak = [self.forTripBreak userModel];
//
//
//        return newObj;
//
//    }
    
    func checkCurrentTripExists(currentLocation:CVehicleCurrentState)
    {
        
        if((currentLocation.location.latitude == self.vehicleCurrentState?.location.latitude ?? Double()) && (currentLocation.location.longitude == self.vehicleCurrentState?.location.longitude ?? Double())){
            print("same")
            if(currentLocation.trip == nil || currentLocation.trip.tripId == ""){
                self.oldTripId = "None"
            }
            
        }
        else{
            print("diff")
            
            self.vehicleCurrentState = currentLocation
            
            if (currentLocation.trip != nil && currentLocation.trip.tripId == self.oldTripId)
            {
            }
            else if((self.vehicleCurrentState?.trip) != nil){
                self.currentTag = nil
                self.locationsDelta.removeAll()
                self.oldTripId = self.vehicleCurrentState?.trip.tripId
            }
            
            if(currentLocation.trip == nil || currentLocation.trip.tripId == "")
            {
                self.oldTripId = "None"
            }
            
            
        }
    }
 
    
    func getOverallScoreForSelectedVehicle() -> Void {
        
        CommonData.sharedData.getSelectedVehicle { (selectedVehicleobj) in
            if selectedVehicleobj != nil {
                
                CTripRequest.sharedInstance().getOverAllTripScore(forVehiclev2: self.uin, response: { (responseData, isSuccess, error) in
                    
                    
                    if isSuccess {
                        
                        let score = responseData as? CDriveScore
                        self.driveScore = score
                        
                        //                        if let dashboardDriverScoreCell = self.dashboardTableView.cellForRow(at: indexPath) as? DashboardDriverScoreCell {
                        //                            dashboardDriverScoreCell.setDriverScore(score: score!)
                        //                        self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
                        //  }
                        
                    }else {
                        
                        if (responseData != nil) && (error != nil) {
                            self.driveScore = nil
                            
                            //                            self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseData as! NSDictionary))
                        }
                        
                    }
                })
            }
        }
        
    }
    
    
    @IBAction func navigateToHealthScene(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 2
        
    }
    
    func createHeaderView() -> UIButton {
        headerButton.titleLabel?.font = UIFont(name: "Helvetica", size: 13)
        headerButton.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        //headerButton.frame = CGRect(x: 46, y: 0, width: self.view.frame.width - 92, height: (self.navigationController?.navigationBar.frame.height)!)
        
        self.setHeaderTitle(title: nil)
        
        return headerButton
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        //self.isMapVisible = false
        // toggleMapOnScreen()
        let notificationName = Notification.Name("toggleLeftMenu")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.stopTimer()
    }
    
    func openNearbyScreenForFuelStations(){
        if let centreController = self.menuContainerViewController.centerViewController as? CentreViewController {
            centreController.performSegue(withIdentifier: kCentreToNearbySegue, sender: "FUEL")
        }
    }
    
    func openNearbyScreenForServiceStations(){
        if let centreController = self.menuContainerViewController.centerViewController as? CentreViewController {
            centreController.performSegue(withIdentifier: kCentreToNearbySegue, sender: "SERVICE")
        }
    }

  
    func showDriverScoreView() {
        if self.driverScoreOverlay == nil {
            self.driverScoreOverlay = Bundle.main.loadNibNamed("DriverScoreOverlay", owner: nil, options: nil)?.first as? DriverScoreOverlay
        }
        
        //        driverScoreOverlay!.fromButton.addTarget(self, action: #selector(fromTapped(_:)), for: .touchUpInside)
        //        driverScoreOverlay!.toButton.addTarget(self, action: #selector(toTapped(_:)), for: .touchUpInside)
        
        driverScoreOverlay!.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        UIApplication.shared.windows.last?.addSubview(driverScoreOverlay!)
        
    }
  
    //MARK: - Vehicle Health
    func getVehicleHealth(){
        
        // self.view.showIndicatorWithProgress(message: "Please wait")
        
        CVehicleRequest.sharedInstance().getHealthStatus { (response, isSuccessful, error) in
            self.view.dismissProgress()
            
            if (isSuccessful && response != nil) {
                let vehicleHealthObj:CVehicleHealthStatus = response as! CVehicleHealthStatus
                self.healthStatus = vehicleHealthObj
              
            }else{
                if (response != nil) {
                    print(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                }else{
                    print(error!.localizedDescription)
                }
            }
            
        }
        
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeoFenceTableViewCell", for: indexPath) as! GeoFenceTableViewCell
        cell.updateCellLayout(arrayOfFences[indexPath.row])
        cell.dotsButtonObj.tag = indexPath.row
        cell.dotsButtonObj.addTarget(self, action: #selector(dotsTapped), for: .touchUpInside)
        
        return cell
    }
    
    
    @objc func dotsTapped(_ sender: Any?) -> Void {
        let tappedIndex = (sender as! UIButton).tag
        
        let alert: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
         let editAction:UIAlertAction = UIAlertAction(title: "Edit", style: .default) { (action) in
          let toBeUpdated = self.arrayOfFences[tappedIndex]
                 
                 self.openEditScreen(geoFenceObj: toBeUpdated)
         }
         
         let deleteAction:UIAlertAction = UIAlertAction(title: "Delete", style: .default) { (action) in
            
            let toBeDeletedFence = self.arrayOfFences[tappedIndex]
            
            self.callForDeletionAPI(geoFenceObj: toBeDeletedFence)
            
         }
         
         let cancelAction:UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
         
         alert.addAction(editAction)
         alert.addAction(deleteAction)
         alert.addAction(cancelAction)
         
         self.present(alert, animated: true, completion: nil)
    }
    
    func openEditScreen(geoFenceObj:CGeoFence){
        
        let storyBoard : UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        let VC = storyBoard.instantiateViewController(withIdentifier: "AddEditGeoFenceVC") as! AddEditGeoFenceVC
        VC.isEditMode = true
        VC.geoFenceObj = geoFenceObj
        self.navigationController?.pushViewController(VC, animated: true)
            
    }
    
    func callForDeletionAPI(geoFenceObj:CGeoFence){
        
        CVehicleGeofence.sharedInstance()?.deleteFence(withID: geoFenceObj.geoFenceId, response: { (arrayOfGeoFences, isCompleted, error) in
            
            if(isCompleted){
                self.getAllFences()
            }
        })
     
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
        return arrayOfFences.count
         
     }
     
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
         return 80

     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
          
        print(indexPath.row)
      }
      
}
