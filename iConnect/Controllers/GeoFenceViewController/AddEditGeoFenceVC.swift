//
//  AddEditGeoFenceVC.swift
//  iConnect
//
//  Created by Lokesh on 26/12/20.
//  Copyright © 2020 Administrator. All rights reserved.
//

import Foundation

import UIKit

import GoogleMaps
import GooglePlaces

class AddEditGeoFenceVC : UIViewController{
    
    //location manager for accessing current location of the device
    @IBOutlet weak var addGeoFenceButton: UIButton!
    private let locationManager = CLLocationManager()
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    var cirlce: GMSCircle!
    var sliderObj: MICSlider!
    var geoFenceObj:CGeoFence?
    var zoomLevel = 14
    var zoomLevelArray:  [Int] = [14,13,12,12,12,11,10,10,10,9]
    var isEditMode:Bool = false

    @IBOutlet weak var enterFenceDisplayName: UITextField!
    
    @IBAction func addFenceTapped(_ sender: Any) {
    
        let aString = enterFenceDisplayName.text
        let trimmedString = aString?.trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedString == ""{
            
            let alert = UIAlertController(title: "Geofence name needed", message: "Geofence name is mangatory to create a new geofence.", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
             self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        if isEditMode == false {

            let position = CLLocationCoordinate2D(latitude: self.mLat , longitude: self.mLong )
//            let newLoc = self.locationWithBearing(bearingRadians: 3.14159, distanceMeters: Double(mCurrentRadius*1000),origin: position)
            createAFence(newLocation: position, fenceName: trimmedString!)
                
        }else{
            let position = CLLocationCoordinate2D(latitude: self.mLat , longitude: self.mLong )
//            let newLoc = self.locationWithBearing(bearingRadians: 3.14159, distanceMeters: Double(mCurrentRadius*1000),origin: position)
            updateAFence(newLocation: position, fenceName: trimmedString!)
        }
        
    }
    
    func updateAFence(newLocation:CLLocationCoordinate2D, fenceName:String){

        self.geoFenceObj!.active = true
        self.geoFenceObj!.destLat = newLocation.latitude
        self.geoFenceObj!.destLon = newLocation.longitude
        self.geoFenceObj!.longitude = newLocation.longitude
        self.geoFenceObj!.latitude = newLocation.latitude
        self.geoFenceObj!.name = fenceName
 
        self.geoFenceObj!.radius = Int((Double(mCurrentRadius*1000)))
        
        CVehicleGeofence.sharedInstance()?.update(geoFenceObj, response: { (arrayOfGeoFences, isCompleted, error) in
            
            if(isCompleted){
                let alert = UIAlertController(title: "Goefence Created", message: "Geofence has been created successfully.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                }))

                self.present(alert, animated: true, completion: nil)
                        
            }
            
        })
        
        
    }
    
    func createAFence(newLocation:CLLocationCoordinate2D, fenceName:String){

        let geoFenceObj: CGeoFence = CGeoFence()
        geoFenceObj.active = true
        geoFenceObj.destLat = newLocation.latitude
        geoFenceObj.destLon = newLocation.longitude
        geoFenceObj.longitude = newLocation.longitude
        geoFenceObj.latitude = newLocation.latitude
        geoFenceObj.name = fenceName
        geoFenceObj.radius = Int((Double(mCurrentRadius*1000)))
        CVehicleGeofence.sharedInstance()?.save(geoFenceObj, response: { (arrayOfGeoFences, isCompleted, error) in
            
            if(isCompleted){
                let alert = UIAlertController(title: "Goefence Created", message: "Geofence has been created successfully.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                }))

                self.present(alert, animated: true, completion: nil)
                        
            }
            
        })
        
        
    }
    
    var mLat = 0.0
    var mLong = 0.0
    var address = ""
    
    var mCurrentRadius = 1
    
    var marker:GMSMarker?
    
    @IBOutlet weak var sliderPlaceholderView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = isEditMode ? "Edit Geo Fence" : "Add Geo Fence"
        self.mapView.setMinZoom(Float(8), maxZoom: Float(kMapZoomLevelTripDetails))
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes

            // Do any additional setup after loading the view.
            self.mapView.delegate = self
            //initializing CLLocationManager
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            
            
            //searchButton ui implementations
            self.searchButton.layer.cornerRadius = 15.0

            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "NewSearchLocationSelected"),
                     object: nil,
                     queue: nil,
                     using:newSearchLocationSelected)
        
        fetchCurrentAddress()
        
        searchButton.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        searchButton.titleLabel?.numberOfLines = 2
        
        marker = GMSMarker.init()
//        marker!.icon = UIImage(named:"CarStatusInactive")!
        
        self.searchButton.layer.cornerRadius = 6.0
        self.searchButton.layer.borderColor = UIColor.lightGray.cgColor
        self.searchButton.layer.borderWidth = 0.5
        self.searchButton.layer.masksToBounds = true
        
        if isEditMode == true && geoFenceObj?.name != nil {
            enterFenceDisplayName.text = geoFenceObj?.name
            
            mCurrentRadius = geoFenceObj!.radius

            self.mLong = geoFenceObj!.longitude

            self.mLat = geoFenceObj!.latitude

            locatePlaceWithLatLong(lon: String.init(format: "%f", geoFenceObj!.longitude), andLatitude: String.init(format: "%f", geoFenceObj!.latitude), andAddress: "")
            
            fetchAddressAndItsDetails(lon: String.init(format: "%f", geoFenceObj!.longitude), andLatitude: String.init(format: "%f", geoFenceObj!.latitude))
            
            addGeoFenceButton.setTitle("Edit Geo Fence", for: .normal)
        }
        
        createSliderView()
    }
    
    func fetchAddressAndItsDetails(lon: String, andLatitude lat: String){
         
         let latDouble = Double(lat)
         let lonDouble = Double(lon)
                           
             let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: latDouble!, longitude: lonDouble!)
             
             let coder = GMSGeocoder()
                 coder.reverseGeocodeCoordinate(coordinate, completionHandler: { response, err in
                     DispatchQueue.main.async {
                         if err == nil {
                             let address = response?.firstResult()?.lines?.joined(separator: ",")
                             self.address = address!
                            self.searchButton.setTitle(self.address, for: .normal)
                         } else {
                             print("EMPTY")
                         }
                     }
                     
                 })

         
     }
    
    func fetchCurrentAddress(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    
    func createSliderView() {
        self.view.layoutSubviews()
        if sliderObj == nil {
            sliderObj = MICSlider(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width-30, height: 80))
                let arrayDuration:  [String] = ["1km", "2km", "3km","4km","5km","10km","15km","20km","25km","30km"]
                
                let radius:  [Int] = [1,2,3,4,5,10,15,20,25,30]
                sliderObj?.setDataArray(titleArray: arrayDuration)
                sliderPlaceholderView.addSubview(sliderObj!)
                sliderObj.resetBubbleText()
            
            
                let index:Int = mCurrentRadius == 1 ? 0 :radius.firstIndex(of: mCurrentRadius/1000)!
                 
                let totalBars = arrayDuration.count
            
                zoomLevel = zoomLevelArray[index]
            
                sliderObj?.scrollToIndex(index: index, length: totalBars)
                sliderObj?.setCallbackForSelectedDuration { (selectedIndex) in

                    self.zoomLevel = self.zoomLevelArray[selectedIndex]
                    
                    self.createRadius(radiusBy: radius[selectedIndex]*1000)
                }
        }
    }
    
    func createRadius(radiusBy:Int){
        mCurrentRadius = radiusBy / 1000
        
        locatePlaceWithLatLong(lon: String.init(format: "%f", self.mLong), andLatitude: String.init(format: "%f", self.mLat), andAddress: address)
    }
    
    
//    if slider == nil {
//           slider = MICSlider(frame: CGRect(x: 10, y: 60, width: UIScreen.main.bounds.width-30, height: 80))
//           let arrayDuration:  [String] = ["15m", "30m", "1hr","2hr","3hr","4hr","5hr","6hr","7hr","8hr","9hr","10hr", "11hr", "12hr"]
//
//           let arrayDurationInSeconds:  [Int] = [15,30,60,120,180,240,300,360,420,480,540,600,660,720]
//
//           slider?.setDataArray(titleArray: arrayDuration)
//           sliderPlaceholderView.addSubview(slider!)
    
    
    @objc func newSearchLocationSelected(notification:Notification) -> Void  {
          
          if let dict = notification.userInfo as NSDictionary? {

              guard let lat = dict["lat"] as? String else { return }
            
              guard let long = dict["long"] as? String else { return }
            
              guard let address = dict["address"] as? String else { return }
            
              self.mLat = Double(lat) ?? 0.0
            
              self.mLong = Double(long) ?? 0.0
                       
              locatePlaceWithLatLong(lon: long, andLatitude: lat, andAddress: address)
            }
      }
      
      func locatePlaceWithLatLong(lon: String, andLatitude lat: String, andAddress address: String) {
          DispatchQueue.main.async {
              
              let latDouble = Double(lat)
              let lonDouble = Double(lon)
            
               if (latDouble != 0.0 ) {
                
                self.mLat = latDouble!
                
                self.mLong = lonDouble!
               
              }
            
            
              if address != "" {
                  self.address = address
              }else{
                  self.address = "Search Location"
              }
                 
            
            self.mapView.clear()
        
            let position = CLLocationCoordinate2D(latitude: self.mLat , longitude: self.mLong )
            
            let camera = GMSCameraPosition.camera(withLatitude: self.mLat , longitude: self.mLong , zoom: Float(self.zoomLevel))
            
            self.mapView.camera = camera
            
            self.searchButton.setTitle(self.address, for: .normal)
            
//            marker.map = self.mapView
            
            self.cirlce = GMSCircle(position: position, radius: CLLocationDistance(self.mCurrentRadius*1000)) // In Meters
            
            let color = UIColor(red:130.0/255.0, green:177.0/255.0, blue:210.0/255.0, alpha:1.00)
            
            self.cirlce.fillColor = color

            
            self.cirlce.map = self.mapView
            
            self.marker?.position = position
            
            self.marker?.map = self.mapView
            self.marker?.icon = UIImage(named:"CarStatusInactive")!
          }
      }
    
    
    override func viewWillAppear(_ animated: Bool) {
    
        
    }
    
    
}

//implementing extension from CLLocationManagerDelegate
extension AddEditGeoFenceVC: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
            print("\(position.target.latitude) \(position.target.longitude)")
            if cirlce != nil {
               cirlce.position = position.target
            }
        
        mLat = position.target.latitude
        mLong = position.target.longitude
        
        self.marker?.position = position.target
        
        fetchAddressAndItsDetails(lon: String.init(format: "%f", position.target.longitude), andLatitude: String.init(format: "%f", position.target.latitude))
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        
        mLat  = location.coordinate.latitude

        mLong  = location.coordinate.longitude
        
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: Float(zoomLevel), bearing: 0, viewingAngle: 0)
        
        locationManager.stopUpdatingLocation()
        
    }
    
}

