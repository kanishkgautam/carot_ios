//
//  AutocompleteSearchViewController.swift
//  iConnect
//
//  Created by Lokesh on 26/12/20.
//  Copyright © 2020 Administrator. All rights reserved.
//
import UIKit
import GooglePlaces
import Alamofire
import GoogleMaps
import SwiftyJSON

var g_lat: String!
var g_long: String!
var g_address: String!

class AutocompleteSearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var placeIDArray = [String]()
    var resultsArray = [String]()
    var primaryAddressArray = [String]()
    var searchResults = [String]()
    var searhPlacesName = [String]()
    let googleApiKey = "AIzaSyDeDarI_BW90aUeFulaGmU05IAV1Lkui5M"
    var fetcher: GMSAutocompleteFetcher?
    var placesClient = GMSPlacesClient()
    var token: GMSAutocompleteSessionToken? = nil
    //search Controller implementations
    let searchController = UISearchController(searchResultsController: nil)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GMSPlacesClient.provideAPIKey(googleApiKey)
        token = GMSAutocompleteSessionToken.init()
        // Do any additional setup after loading the view.
        tableViewSetup()
        setSearchController()
    }
    
    func tableViewSetup(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableHeaderView = searchController.searchBar
        self.tableView.backgroundColor = UIColor.white
    }
    func setSearchController() {
        searchController.searchResultsUpdater = self
        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = false
        } else {
            // Fallback on earlier versions
        }
        searchController.searchBar.placeholder = "Search Location"
        searchController.searchBar.searchBarStyle = .minimal
        navigationItem.title = "Search Location"
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            // Fallback on earlier versions
        }
        definesPresentationContext = true
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForTextSearch(searchText: String){
        placeAutocomplete(text_input: searchText)
        self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchController.searchBar.showsCancelButton = true
        self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
    }
    func isFiltering() -> Bool{
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchBarIsEmpty()){
            searchBar.text = ""
        }else{
            placeAutocomplete(text_input: searchText)
            
        }
    }
    
    
    //Table view methods to be implemented
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = searchResults[indexPath.row]
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let correctedAddress = self.resultsArray[indexPath.row].addingPercentEncoding(withAllowedCharacters: .symbols) else {
            print("Error. cannot cast name into String")
            return
        }
        
        //print(correctedAddress)
        let urlString =  "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress)&sensor=false&key=\(self.googleApiKey)"
        
        let url = URL(string: urlString)

        Alamofire.request(url!, method: .get, headers: nil)
        .validate()
            .responseJSON { (response) in
                switch response.result {
                case.success(let value):
                    let json = JSON(value)
                    
                    let lat = json["results"][0]["geometry"]["location"]["lat"].rawString()
                    let lng = json["results"][0]["geometry"]["location"]["lng"].rawString()
                    let formattedAddress = json["results"][0]["formatted_address"].rawString()
                    
                    g_lat = lat
                    g_long = lng
                    g_address = formattedAddress
                    
                    DispatchQueue.main.async {
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NewSearchLocationSelected"), object: nil, userInfo: ["lat":g_lat!,"long":g_long!,"address":g_address!])
                        

                        self.presentingViewController?.dismiss(animated: true, completion: {
                            
                        })
                    }
                    
                   
                case.failure(let error):
                    print("\(error.localizedDescription)")
                }
            
                
        }
        
    }
    
    
    
    //function for autocomplete
    func placeAutocomplete(text_input: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
    
        placesClient.findAutocompletePredictions(fromQuery: text_input, filter: filter, sessionToken: token) { (results, error) in
               self.placeIDArray.removeAll()
                               self.resultsArray.removeAll()
                               self.primaryAddressArray.removeAll()
                               if let error = error {
                                   print("Autocomplete error \(error)")
                                   return
                               }
                               if let results = results {
                                   for result in results {
                                       self.primaryAddressArray.append(result.attributedPrimaryText.string)
                                       //print("primary text: \(result.attributedPrimaryText.string)")
                                       //print("Result \(result.attributedFullText) with placeID \(String(describing: result.placeID!))")
                                       self.resultsArray.append(result.attributedFullText.string)
                                       self.primaryAddressArray.append(result.attributedPrimaryText.string)
                                    self.placeIDArray.append(result.placeID)
                                   }
                               }
                               self.searchResults = self.resultsArray
                               self.searhPlacesName = self.primaryAddressArray
                               self.tableView.reloadData()
        }
        
    }
    
   
}

extension AutocompleteSearchViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForTextSearch(searchText: searchController.searchBar.text!)
    }
    
}
