//
//  LoaderView.swift
//  ImageLoaderIndicator
//
//  Created by Vaibhav Gautam on 06/02/17.
//  Copyright © 2017 Rounak Jain. All rights reserved.
//

import UIKit

class LoaderView: UIView {

    let circlePathLayer = CAShapeLayer()
//    let circleRadius: CGFloat = 40.0
//    var loaderColor: CGColor = UIColor.red.cgColor
//    
    var loaderColor:CGColor{
        get{
            if (circlePathLayer.strokeColor != nil) {
                return circlePathLayer.strokeColor!
            }
            return UIColor.clear.cgColor
            
        }
        
        set{
            circlePathLayer.strokeColor = newValue
        }
    }
    
    var progress:CGFloat{
        get{
            return circlePathLayer.strokeEnd
        }
        
        set{
            if (newValue > 1) {
                circlePathLayer.strokeEnd = 1
            }else if (newValue < 0){
                circlePathLayer.strokeEnd = 0
            }else{
                
            circlePathLayer.strokeEnd = newValue
            }
        }
    }
    
    func setProgressWithDuration(seconds: NSNumber) -> Void {
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the starting value
        animation.fromValue = 0
        
        // Set the completion value
        animation.toValue = 1
        //animation.isRemovedOnCompletion = false

        // How may times should the animation repeat?
        animation.repeatCount = 0
        
        animation.duration = CFTimeInterval(seconds)
        // Finally, add the animation to the layer
        circlePathLayer.add(animation, forKey: "strokeEnd")
        
    }
    
    func resetProgressWithAnimation(interval: TimeInterval) -> Void {
        
        self.circlePathLayer.removeAllAnimations()
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the starting value
        animation.fromValue = self.progress
        
        // Set the completion value
        animation.toValue = 0
        
//        animation.isRemovedOnCompletion = false
        // How may times should the animation repeat?
        animation.repeatCount = 0
        
        animation.duration = CFTimeInterval(interval)
        // Finally, add the animation to the layer
        circlePathLayer.add(animation, forKey: "strokeEnd")
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    

    func configure() {
        circlePathLayer.frame = bounds
        circlePathLayer.lineWidth = 5.0
        circlePathLayer.fillColor = UIColor.clear.cgColor
        circlePathLayer.strokeColor = loaderColor
        circlePathLayer.strokeStart = 0.25
        circlePathLayer.lineCap = CAShapeLayerLineCap.round
        layer.addSublayer(circlePathLayer)
        backgroundColor = UIColor.clear
        progress = 0
        
        let radians = CGFloat(45 * Double.pi / 180)
        let rotation = CATransform3DMakeRotation(radians, 0, 0, 1.0)
        circlePathLayer.transform = CATransform3DTranslate(rotation, 20, 30, 0)
    }
    
    func circleFrame() -> CGRect {
        var circleFrame = CGRect(x: 0, y: 0, width: frame.width, height: frame.width) /*used frame.width because we want circle not oval.*/
        circleFrame.origin.x = circlePathLayer.bounds.midX - circleFrame.midX
        circleFrame.origin.y = circlePathLayer.bounds.midY - circleFrame.midY
        return circleFrame
    }
    
    func circlePath() -> UIBezierPath {
        return UIBezierPath(ovalIn: circleFrame())
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        circlePathLayer.frame = bounds
        circlePathLayer.path = circlePath().cgPath
    }
    
    
}
