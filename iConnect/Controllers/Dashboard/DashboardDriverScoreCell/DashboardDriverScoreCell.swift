//
//  DashboardDriverScoreCell.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 25/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DashboardDriverScoreCell: UITableViewCell {

    @IBOutlet weak var safetyBar: UIView!
    @IBOutlet weak var efficeincyBar: UIView!
    @IBOutlet weak var fuelEconomyBar: UIView!
    
    @IBOutlet weak var driverScoreLeftView: UIView!
    @IBOutlet weak var roundedBackgroundView: UIView!
    @IBOutlet weak var driverScore: UILabel!
    
    let driverScoreLoader = LoaderView(frame: CGRect.zero)

    @IBOutlet weak var progressBackgroundView: LoaderView!
    @IBOutlet weak var progressView: LoaderView!
    
    
    @IBOutlet weak var safetyBarView: UIView!
    
    @IBOutlet weak var safetyLabel: UILabel!
    @IBOutlet weak var efficiencyLabel: UILabel!
    @IBOutlet weak var economyLabel: UILabel!
    
    
    @IBOutlet weak var driverScoreWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var driverScoreHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var safetyBarWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var efficiencyBarWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var economyBarWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.beautifyView()
    }
    
    func beautifyView() {
        
        if driverScoreLeftView.frame.size.width > driverScoreLeftView.frame.size.height {
            driverScoreWidthConstraint.priority = UILayoutPriority(rawValue: 900)
            driverScoreHeightConstraint.priority = UILayoutPriority(rawValue: 999)
        }else{
            driverScoreWidthConstraint.priority = UILayoutPriority(rawValue: 999)
            driverScoreHeightConstraint.priority = UILayoutPriority(rawValue: 900)
        }
        
        progressBackgroundView.loaderColor = UIColor(red:0.89, green:0.91, blue:0.92, alpha:1.0).cgColor
        progressBackgroundView.progress = 1.0
        
        
        progressView.loaderColor = UIColor(red:0.97, green:0.58, blue:0.11, alpha:1.0).cgColor
        progressView.progress = 0.0
        
        
        roundedBackgroundView.layer.cornerRadius = 3.0
       // setDriverScore(score: 0.5)
        // drop shadow
        roundedBackgroundView.layer.shadowColor = UIColor.black.cgColor
        roundedBackgroundView.layer.shadowOpacity = 0.2
        roundedBackgroundView.layer.shadowRadius = 1.0
        roundedBackgroundView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)

        
    }
    

    func setDriverScore(score:CDriveScore?) {
        
        if score == nil {
            self.driverScore.text = "0"
            
            self.economyLabel.text = "0"
            self.safetyLabel.text = "0"
            self.efficiencyLabel.text = "0"
            
            
            self.progressView.progress = 0
            
            UIView .animate(withDuration: 1.5) {
                
                self.efficiencyBarWidthConstraint.constant = self.getBarWidthFromScore(value: 0.0)
                self.safetyBarWidthConstraint.constant = self.getBarWidthFromScore(value: 0.0)
                self.economyBarWidthConstraint.constant = self.getBarWidthFromScore(value: 0.0)
                
                self.layoutIfNeeded()
            }
        }
        else {
            self.driverScore.text = "\(score!.pScore.intValue)%"
            
            self.economyLabel.text = "\(score!.pFuelScore.intValue)%"
            self.safetyLabel.text =  "\(score!.pSafetyScore.intValue)%"
            self.efficiencyLabel.text = "\(score!.pTripScore.intValue)%"
            
            self.progressView.progress = CGFloat((score?.pScore.floatValue)!/100)
            
            UIView .animate(withDuration: 1.5) {
                
                self.efficiencyBarWidthConstraint.constant = self.getBarWidthFromScore(value: score!.pTripScore as! CGFloat)
                self.safetyBarWidthConstraint.constant = self.getBarWidthFromScore(value: score!.pSafetyScore as! CGFloat)
                self.economyBarWidthConstraint.constant = self.getBarWidthFromScore(value: score!.pFuelScore as! CGFloat)
                
                self.layoutIfNeeded()
            }
        }
        
        
        
        print("PROGRESS BAR___ \(self.progressView.progress)")
        
       // UIView .animate(withDuration: 2) {
//            self.layoutIfNeeded()
//        }
    }
    
    func getBarWidthFromScore(value: CGFloat) -> CGFloat {
        
        let totalWidth = self.safetyBarView.frame.width
        
        let trailingValue = totalWidth - (totalWidth*value/100)
        
        return trailingValue
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
