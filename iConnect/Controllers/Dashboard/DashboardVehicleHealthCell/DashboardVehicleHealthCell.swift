//
//  DashboardVehicleHealthCell.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 25/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class DashboardVehicleHealthCell: UITableViewCell {
    
    @IBOutlet weak var roundedBackgroundView: UIView!
    // @IBOutlet weak var imageEngine:UIImageView!
    
    @IBOutlet weak var engineView: UIView!
    @IBOutlet weak var batteryView: UIView!
    @IBOutlet weak var towView: UIView!
    @IBOutlet weak var ImpactView: UIView!
    
    
    @IBOutlet weak var imageDashboardEngine: UIImageView!
    @IBOutlet weak var imageBattery: UIImageView!
    @IBOutlet weak var imageTow: UIImageView!
    @IBOutlet weak var imageImpact: UIImageView!
    
    @IBOutlet weak var impactToggleIcon: UIImageView!
    @IBOutlet weak var towToggleIcon: UIImageView!
    @IBOutlet weak var batteryToggleIcon: UIImageView!
    @IBOutlet weak var engineToggleIcon: UIImageView!
    
    @IBOutlet weak var engineLabel: UILabel!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var towLabel: UILabel!
    @IBOutlet weak var impactLabel: UILabel!
    
    var vehicleHealthStatus:CVehicleHealthStatus?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        beautifyView()
    }
    
    func beautifyView(){
        roundedBackgroundView.layer.shadowColor = UIColor.black.cgColor
        roundedBackgroundView.layer.shadowOpacity = 0.2
        roundedBackgroundView.layer.shadowRadius = 1.0
        roundedBackgroundView.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if(vehicleHealthStatus != nil){
            populateHealthData()
        }
    }
    func populateHealthData()
    {
        
        if (vehicleHealthStatus?.recentCrashEventFound)! {
            imageImpact.image = #imageLiteral(resourceName: "selectedImpact")
            impactToggleIcon.image = #imageLiteral(resourceName: "healthIssueReported")
            impactLabel.textColor = UIColor.red
        }else{
            imageImpact.image = #imageLiteral(resourceName: "DashboardImpact")
            impactToggleIcon.image = #imageLiteral(resourceName: "healthIssueOK")
            impactLabel.textColor = UIColor.black
        }
        
        
        if (vehicleHealthStatus?.recentTowEventFound)! {
            imageTow.image = #imageLiteral(resourceName: "selectedTow")
            towToggleIcon.image = #imageLiteral(resourceName: "healthIssueReported")
            towLabel.textColor = UIColor.red
        }else{
            imageTow.image = #imageLiteral(resourceName: "DashboardTow")
            towToggleIcon.image = #imageLiteral(resourceName: "healthIssueOK")
            towLabel.textColor = UIColor.black
        }
        
        
        if (vehicleHealthStatus?.batteryEvent as? [String:Any] != nil) {
            imageBattery.image = #imageLiteral(resourceName: "DashboardBatteryRed")
            batteryToggleIcon.image = #imageLiteral(resourceName: "healthIssueReported")
            batteryLabel.textColor = UIColor.red
        }else{
            imageBattery.image = #imageLiteral(resourceName: "DashboardBattery")
            batteryToggleIcon.image = #imageLiteral(resourceName: "healthIssueOK")
            batteryLabel.textColor = UIColor.black
        }
        
        
        if ((vehicleHealthStatus?.dtcEvents.count)! > 0) {
            imageDashboardEngine.image = #imageLiteral(resourceName: "DashboardEngineRed")
            engineToggleIcon.image = #imageLiteral(resourceName: "healthIssueReported")
            engineLabel.textColor = UIColor.red
        }else{
            imageDashboardEngine.image = #imageLiteral(resourceName: "DashboardEngine")
            engineToggleIcon.image = #imageLiteral(resourceName: "healthIssueOK")
            engineLabel.textColor = UIColor.black
        }
        
    }
}

