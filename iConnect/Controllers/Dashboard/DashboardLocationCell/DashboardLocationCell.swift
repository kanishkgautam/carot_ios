//
//  DashboardLocationCell.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 25/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit
import SnapKit
import FTIndicator
import GoogleMaps
typealias EventPerformedCallback = () -> Void




class DashboardLocationCell: UITableViewCell {
    
    @IBOutlet weak var vehicleStatusArea: UIView!
    @IBOutlet weak var backgroundMapView: GMSMapView!
    
    
    @IBOutlet weak var mapLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var carStatusImage: UIImageView!
    
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var locateCar: UIButton!
    
    @IBOutlet weak var nearbyOverlayArea: UIView!
    
    @IBOutlet weak var nearbyView: UIView!
    @IBOutlet weak var locateMyCarDistanceTime: UILabel!
    
    var isPreviousTrip:Bool = false
    var isTripExists:Bool = false
    
    var parentViewController : AppDashboardViewController!
    
    var shareCallback:EventPerformedCallback?
    var mapCallback:EventPerformedCallback?
    var fuelCallback:EventPerformedCallback?
    var serviceStationCallback:EventPerformedCallback?
    
    var arrayLiveTrackingLocations = [CLocation]()
    var vehicleCoordinates:CLLocationCoordinate2D?
    
    var tripPath:GMSPolyline?
    var tripPathLocateCar:GMSPolyline?
    var mapEndMarker:GMSMarker?
    var mapStartMarker:GMSMarker?
    var timerlocateMycarRefresh : Timer?
    
    var vehicleStateObj:CVehicleCurrentState?
    
    var flagLocateCarZZom:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
                    
        if(tripPath == nil){

            tripPath = GMSPolyline()
            tripPath?.strokeColor =  UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
            tripPath?.strokeWidth = 4.0
        }
        beautifyView()
        let coordinates = CLLocationCoordinate2DMake(Double(truncating: (28.6538100)), Double(truncating: (77.2289700)))
        backgroundMapView.animate(toLocation: coordinates)
    }
    
    
    func beautifyView(){
        vehicleStatusArea.layer.shadowColor = UIColor.black.cgColor
        vehicleStatusArea.layer.shadowOpacity = 0.2
        vehicleStatusArea.layer.shadowRadius = 1.0
        vehicleStatusArea.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        nearbyOverlayArea.layer.borderWidth = 1.0
        nearbyOverlayArea.layer.borderColor = UIColor(red:0.90, green:0.91, blue:0.91, alpha:1.00).cgColor
        
        nearbyView.layer.borderWidth = 1.0
        nearbyView.layer.borderColor = UIColor(red:0.90, green:0.91, blue:0.91, alpha:1.00).cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func updateCellDataWith(isMapVisible:Bool, isOldTrip:Bool, tripExists:Bool) {
        isPreviousTrip = isOldTrip
        isTripExists = tripExists
        self.updateVehicleStatusFrame(withIsMapVisible: isMapVisible,isOldTrip: isOldTrip,tripExists: tripExists)
        
    }
    
    internal func updateVehicleStatusFrame(withIsMapVisible isMapVisible:Bool,isOldTrip:Bool,tripExists:Bool){
        let paddingValue:CGFloat = (isMapVisible ? 0.0 : 20.0 )
        
        mapButton.setImage((isMapVisible ? #imageLiteral(resourceName: "DashboardViewMapSelected") : #imageLiteral(resourceName: "DashboardViewMap") ), for: .normal)
        mapLeftConstraint.constant = paddingValue
        mapTopConstraint.constant = paddingValue
        mapTrailingConstraint.constant = paddingValue
        mapBottomConstraint.constant = paddingValue
        
        if(locateCar.isSelected == true) { return}
        print(isMapVisible)
        if(isMapVisible){
            
            if(tripExists == false){
                if(tripPath != nil){

                    tripPath?.map = nil
                    
//                    backgroundMapView.remove(mapObject: tripPath!)
                }
                if(mapStartMarker != nil){
                    
                    mapStartMarker?.map = nil
                }
                self.showEndMarker(forLocation: vehicleCoordinates!)
                
            }
            else {
                if(arrayLiveTrackingLocations.count > 0)
                {
                    //draw map polyline
                    self.showRouteForRunningVehicle(isOldTrip: isOldTrip)
                }
                else
                {
                    if(tripPath != nil){
                        tripPath?.map = nil
                    }
                    if(mapStartMarker != nil){
                        mapStartMarker?.map = nil
                    }
                    self.showEndMarker(forLocation: vehicleCoordinates!)
                    
                }
            }
        }
        
    }
    
    
    func setCellData(vehicleState: CVehicleCurrentState?) -> Void {
        
        //        if let updateTime = vehicleState?.updateTime {
        //            self.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
        //        }
        //        else {
        //            self.dateLabel.text = "--"
        //
        //        }
        
        vehicleStateObj = vehicleState
        
        if let address = vehicleState?.address {
            
                 print("got back: \(address)")
                 self.addressLabel.text = address
                 
                 if address != "Location not found" {
                     if let updateTime = vehicleState?.updateTime {
                         self.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
                         self.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
                         
                     }
                 }
                 else {
                     self.dateLabel.text = "--"
                     self.carStatusImage.image = #imageLiteral(resourceName: "CarStatusInactive") //now
                     
                 }
            
        }else {
        
                if let _ = vehicleState?.location {
                    
                    self.getAddressofLocation(vehicleLocation: vehicleState!) {
                        (address: String) in
                        print("got back: \(address)")
                        self.addressLabel.text = address
                        
                        if address != "Location not found" {
                            if let updateTime = vehicleState?.updateTime {
                                self.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
                                self.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
                                
                            }
                        }
                        else {
                            self.dateLabel.text = "--"
                            self.carStatusImage.image = #imageLiteral(resourceName: "CarStatusInactive") //now
                            
                        }
                        
                        //                if let updateTime = vehicleState?.updateTime {
                        //                    self.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
                        //                }
                        
                    }
                }
                else {
                    self.addressLabel.text = "Location not found"
                    self.dateLabel.text = "--"
                    self.carStatusImage.image = #imageLiteral(resourceName: "CarStatusInactive") //now
                    
                }
        }
    }
    
    func getAddressofLocation(vehicleLocation:CVehicleCurrentState ,completion: @escaping (_ address: String) -> Void)
    {
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: (vehicleLocation.location.latitude), longitude: (vehicleLocation.location.longitude))
        
        CVehicleRequest.sharedInstance().getAddressForLocation(location) { (responseObject, isSucessful, error) in
            if(error == nil && responseObject != nil)
            {
                DispatchQueue.main.async  {
                    
                    let addressStr:String = responseObject as! String
                    if(addressStr == "Unable to find location" || addressStr == kUnableToGetLocation){
                        
                        if (UserDefaults.standard.value(forKey: "lastSavedLocation") != nil) {
                            let locationStr = UserDefaults.standard.value(forKey: "lastSavedLocation") as! String
                            completion(locationStr)
                        }
                        else {
                            completion("Location not found")
                        }
                        
                        
                    }else {
                        //save  vehicle last location
                        UserDefaults.standard.set(addressStr, forKey: "lastSavedLocation")
                        UserDefaults.standard.set(vehicleLocation.updateTime, forKey: "lastSavedLocationDate")
                        
                        completion(addressStr)
                    }
                }
                
            }
            else{
                DispatchQueue.main.async {
                    if let locationStr = UserDefaults.standard.value(forKey: "lastSavedLocation") as? String {
                        completion(locationStr)
                        
                    }
                    else {
                        completion("Location not found")
                    }
                }
            }
        }
    }
    
    func showEndMarker(forLocation:CLLocationCoordinate2D)
    {
        if forLocation.latitude == 0.000000 || forLocation.longitude == 0.000000 {
            if mapEndMarker != nil {
                self.mapEndMarker = nil
            }
            
            return
        }
        
        //end marker on map view
        if(mapEndMarker == nil){
            mapEndMarker = GMSMarker.init(position: forLocation)
            mapEndMarker?.icon = #imageLiteral(resourceName: "mycar")
            mapEndMarker?.map = backgroundMapView
            backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
        }
        
        mapEndMarker?.position = forLocation
        
        backgroundMapView.animate(toLocation: forLocation)
        
    }
    
    func showRouteForRunningVehicle(isOldTrip:Bool)
    {
 
        if(tripPath == nil){
               
               let polyline:GMSPolyline = GMSPolyline()
                 polyline.strokeColor =  UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
                 polyline.strokeWidth = 4.0
                 tripPath = polyline
        }
        
        tripPath?.map = backgroundMapView

        let mutablePath = GMSMutablePath()
                
        if(isOldTrip == true){
            
            let paths = tripPath?.path
            
//            backgroundMapView.animate(toLocation: vehicleCoordinates!)
    
            let aBoundBox = GMSCoordinateBounds(path: paths!)
                  
            let camera: GMSCameraUpdate = GMSCameraUpdate.fit(aBoundBox)
                  
            self.backgroundMapView.animate(with: camera)
            
            if(mapStartMarker == nil){
                let locationObj:CLocation = arrayLiveTrackingLocations[0]
                let coordinates = CLLocationCoordinate2DMake(locationObj.latitude, locationObj.longitude)
                
                mapStartMarker = GMSMarker.init(position: coordinates)
                mapStartMarker?.icon =  #imageLiteral(resourceName: "starticon")
                mapStartMarker?.map = backgroundMapView
                
            }
            
            
        }
        else //new trip
        {
//            tripPath?.map = nil
            
            
            let sortedArray = self.arrayLiveTrackingLocations.sorted(by:
               {
                Double(truncating: NSNumber(value: ($0 ).timeStamp)) > Double(truncating: NSNumber(value: ($1 ).timeStamp))
               })
                        
            
            let locationObj:CLocation = sortedArray[0]
            let startCoordinates = CLLocationCoordinate2DMake(Double(truncating: NSNumber(value: locationObj.latitude)), Double(truncating: NSNumber(value: locationObj.longitude)))
            //  if(mapStartMarker == nil){
            if (mapStartMarker != nil) {
                mapStartMarker?.map = nil
 
            }
 
            mapStartMarker = GMSMarker.init(position: startCoordinates)
            mapStartMarker?.icon =  #imageLiteral(resourceName: "starticon")
            mapStartMarker?.map = backgroundMapView
       
//            mapStartMarker?.position = startCoordinates
            
            for location in sortedArray {
                let locationObj:CLocation = location
                let coordinates = CLLocationCoordinate2DMake(Double(truncating: NSNumber(value: locationObj.latitude)), Double(truncating: NSNumber(value: locationObj.longitude)))

                mutablePath.add(coordinates)
                print(coordinates)
                vehicleCoordinates = coordinates
            }
            
        }
                
//        mapStartMarker?.isVisible = true;
//        tripPath?.isVisible = true;
        
        //show End marker
        if(mapEndMarker == nil){
             mapEndMarker = GMSMarker.init(position: vehicleCoordinates!)
             mapEndMarker?.icon =  #imageLiteral(resourceName: "mycar")
             mapEndMarker?.map = backgroundMapView
             backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
        }
        
        tripPath?.path = mutablePath
        
        mapEndMarker?.position = vehicleCoordinates!
        
        let aBoundBox = GMSCoordinateBounds(path: mutablePath)
                
        let camera: GMSCameraUpdate = GMSCameraUpdate.fit(aBoundBox)
                
        self.backgroundMapView.animate(with: camera)
                
        
    }
    @IBAction func resumeButtonTapped(_ sender: Any) {
        backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
        AppUtility.sharedUtility.sendGAEvent(withEventName: "ReCenterTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard Screen", eventLabel: nil, eventValue: nil)
        backgroundMapView.animate(toLocation: vehicleCoordinates!)
    }
    
    @IBAction func nearbyButtonTapped(_ sender: Any) {
        layoutIfNeeded()
        var newAlpha = 0.0
        
        if nearbyView.alpha == 0.0 {
            newAlpha = 1.0
        }
        
        UIView.animate(withDuration: 0.5) {
            self.layoutIfNeeded()
            self.nearbyView.alpha = CGFloat(newAlpha)
        }
    }
    
    
    func shareCallback(shareLocationCallback:@escaping EventPerformedCallback){
        self.shareCallback = shareLocationCallback
    }
    
    @IBAction func shareLocationButtonTapped(_ sender: Any) {
        self.shareCallback!()
    }
    
    
    func mapButtonCallback(mapCallback:@escaping EventPerformedCallback){
        self.mapCallback = mapCallback
    }
    
    @IBAction func mapButtonTapped(_ sender: Any) {
        self.mapCallback!()
    }
    
    func servicesStationCallback(serviceCallback:@escaping EventPerformedCallback){
        self.serviceStationCallback = serviceCallback
    }
    
    @IBAction func serviceCentreButtonTapped(_ sender: Any) {
        self.serviceStationCallback!()
    }
    
    func fuelPumpsCallback(fuelStationCallback:@escaping EventPerformedCallback){
        self.fuelCallback = fuelStationCallback
    }
    
    @IBAction func pumpsButtonTapped(_ sender: Any) {
        self.fuelCallback!()
    }
    
    //MARK: Locate my car
    
    
    @IBAction func locateCarTapped(_ sender: Any) {
        
        
        //let button:UIButton = sender as! UIButton
        // button.isSelected = !button.isSelected
        if(locateCar.isSelected == false){
            print("true")
            AppUtility.sharedUtility.sendGAEvent(withEventName: "LocateCarTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard Screen", eventLabel: nil, eventValue: nil)
            getCurrentLocation()
            
        }
        else {
            print("false")
            locateCar.isSelected = false
            flagLocateCarZZom = false
            stopTimer()
            tripPathLocateCar?.map = nil
            
            FTIndicator.dismissProgress()
            
            if(arrayLiveTrackingLocations.count > 0){
                let locationObj:CLocation = arrayLiveTrackingLocations[0]
                let coordinates = CLLocationCoordinate2DMake(locationObj.latitude, locationObj.longitude)
                mapStartMarker?.position = coordinates
            }
            if mapStartMarker != nil {
                mapStartMarker?.map = nil
            }
            mapStartMarker = nil
            
            mapStartMarker?.icon = #imageLiteral(resourceName: "starticon")
            
            updateCellDataWith(isMapVisible: true, isOldTrip: isPreviousTrip,tripExists: isTripExists)
        }
        
        
    }
    
    func startTimer () {
        
        if timerlocateMycarRefresh == nil {
            timerlocateMycarRefresh =  Timer.scheduledTimer(
                timeInterval: TimeInterval(kLocateMyCarUpdateInterval),
                target      : self,
                selector    : #selector(refreshLocateMyCar),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopTimer() {
        if timerlocateMycarRefresh != nil {
            timerlocateMycarRefresh?.invalidate()
            timerlocateMycarRefresh = nil
        }
        locateMyCarDistanceTime.isHidden = true
        
    }
    
    @objc func refreshLocateMyCar(){
        flagLocateCarZZom = true
        getCurrentLocation()
    }
    
    func getCurrentLocation() {
        
        MICLocationManager.sharedManager.getCurrentLocationWithDelegate(delegateObj: parentViewController) { (locationObj:CLLocation?, success:Bool) in
            if (success == true) {
                print("success")
                if self.locateCar.isSelected == false {
                    self.locateCar.isSelected = true
                    self.startTimer()
                }
                self.trackVehicleLocation(userLocation: locationObj!)
            }else{
                print("location Manager fails")
                self.stopTimer()
                if(self.flagLocateCarZZom == false){
                    FTIndicator.dismissProgress()
                    //                FTIndicator.showError(withMessage: NSLocalizedString("Unable ", comment: ""))
                }
            }
        }
    }
    
    
    func drawRouteToVehicleNew(arrayLocations:NSArray, userLocation:CLLocationCoordinate2D, routeSummary:NSDictionary)
    {
        if(timerlocateMycarRefresh == nil) {
            return
        }
        var arrayForBoundingBox:Array = [CLLocationCoordinate2D]()
        if (mapStartMarker != nil) {
            mapStartMarker?.map = nil
            mapStartMarker = nil
        }
        if tripPath != nil {
            tripPath?.map = nil
        }
        
        if(tripPathLocateCar == nil){
            
            let polyline:GMSPolyline = GMSPolyline()
            polyline.strokeColor =  UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
            polyline.strokeWidth = 4.0
            tripPathLocateCar = polyline
            tripPathLocateCar?.map = backgroundMapView
        }
                
        let path = GMSMutablePath()
        
        for wayPoint in arrayLocations{
            let locationString:String = wayPoint as! String
            let latLong:Array = locationString.components(separatedBy: ",")
            let coordinates = CLLocationCoordinate2DMake(Double(latLong[0])!, Double(latLong[1])!)
            path.add(coordinates)
            arrayForBoundingBox.append(coordinates)
            
        }

        if path.count() > 0 {
            tripPathLocateCar?.path = path
        }
        
        
        //start marker on map view added once
        let startLocationString:String = arrayLocations[0] as! String
        let latLongStart:Array = startLocationString.components(separatedBy: ",")
        let startCoordinates = CLLocationCoordinate2DMake(Double(latLongStart[0])!, Double(latLongStart[1])!)
        
        if(mapStartMarker == nil ){
            
            mapStartMarker = GMSMarker.init(position: startCoordinates)
            mapStartMarker?.icon = UIImage(named:"tracke-car-man")!
            mapStartMarker?.map = backgroundMapView
            backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
            
            
        }
        //        else if(mapStartMarker?.icon.uiImageRepresentation() == nil){
        //            backgroundMapView.add(mapObject: mapStartMarker)
        //        }
        //
        //
        mapStartMarker?.position = startCoordinates
        //        let nmaImageObj:NMAImage = NMAImage.image(with: #imageLiteral(resourceName: "tracke-car-man")) as! NMAImage
        //        mapStartMarker?.icon = nmaImageObj
        
        //show End marker
        let lastLocationString:String = arrayLocations[arrayLocations.count-1] as! String
        let latLong:Array = lastLocationString.components(separatedBy: ",")

        let lastCoordinates = CLLocationCoordinate2DMake(Double(latLong[0])!, Double(latLong[1])!)
        if(mapEndMarker == nil){
            
             mapEndMarker = GMSMarker.init(position: vehicleCoordinates!)
             mapEndMarker?.icon =  #imageLiteral(resourceName: "mycar")
             mapEndMarker?.map = backgroundMapView
                 
        }
        
        mapEndMarker?.position = lastCoordinates
        
        if(flagLocateCarZZom == false){
            //set bounding box

                let mutablePath = GMSMutablePath()
               for wayPoint in arrayForBoundingBox {
                
//                        let location:Location = (wayPoint as? Location)!
//                        print(location)
//                        let coordinate = CLLocationCoordinate2DMake(Double(truncating: location.latitude!), Double(truncating: location.longitude!))
                        mutablePath.add(wayPoint)
                
               }
            
            tripPathLocateCar?.path = mutablePath
            
            tripPathLocateCar?.map = self.backgroundMapView
//
            let aBoundBox = GMSCoordinateBounds(path: mutablePath)
            
            let camera: GMSCameraUpdate = GMSCameraUpdate.fit(aBoundBox, withPadding: 50.0)
            
            self.backgroundMapView.animate(with: camera)
            
            
            
            //Set geo center
//            let coordinates = CLLocationCoordinate2DMake(userLocation.latitude, userLocation.longitude)
//            backgroundMapView.animate(toLocation: coordinates)
//            backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
            
        }
        
        
        //calculate Distance and time
        let distance:UInt64 = routeSummary.object(forKey: "distance") as! UInt64
        var distanceString:String!
        if((distance/1000) > 0){
            distanceString = "Distance: " + "\(distance/1000)" + "km"
        }
        else {
            distanceString = "Distance: " + "\(distance)" + "m"
            
        }
        let time:TimeInterval = routeSummary.object(forKey: "travelTime") as! TimeInterval
        let timeStr:String = AppUtility.sharedUtility.convertSecondsInHoursMinSec(timeInSeconds: time)
        //print(timeStr)
        locateMyCarDistanceTime.text = distanceString + "\nETA:" + timeStr
        locateMyCarDistanceTime.isHidden = false
        FTIndicator.dismissProgress()
        
    }
    
    
    func drawRouteToVehicle(arrayLocations:NSArray, userLocation:CLLocation, routeSummary:NSDictionary)
    {
        if(timerlocateMycarRefresh == nil) {
            return
        }
        var arrayForBoundingBox:Array = [CLLocationCoordinate2D]()
        if (mapStartMarker != nil) {
            mapStartMarker?.map = nil
            mapStartMarker = nil
        }
        if tripPath != nil {
            tripPath?.map = nil
        }
        
        if(tripPathLocateCar == nil){
            
            let polyline:GMSPolyline = GMSPolyline()
            polyline.strokeColor =  UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
            polyline.strokeWidth = 4.0
            tripPathLocateCar = polyline
            tripPathLocateCar?.map = backgroundMapView
        }
        
        tripPathLocateCar?.map = nil
        
        let path = GMSMutablePath()
        
        for wayPoint in arrayLocations{
            let locationString:String = wayPoint as! String
            let latLong:Array = locationString.components(separatedBy: ",")
            let coordinates = CLLocationCoordinate2DMake(Double(latLong[0])!, Double(latLong[1])!)
            path.add(coordinates)
            arrayForBoundingBox.append(coordinates)
            
        }

        if path.count() > 0 {
            tripPathLocateCar?.path = path
        }
        
        
        //start marker on map view added once
        let startLocationString:String = arrayLocations[0] as! String
        let latLongStart:Array = startLocationString.components(separatedBy: ",")
        let startCoordinates = CLLocationCoordinate2DMake(Double(latLongStart[0])!, Double(latLongStart[1])!)
        
        if(mapStartMarker == nil ){
            
            mapStartMarker = GMSMarker.init(position: startCoordinates)
            mapStartMarker?.icon = UIImage(named:"tracke-car-man")!
            mapStartMarker?.map = backgroundMapView
            backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
            
            
        }
        //        else if(mapStartMarker?.icon.uiImageRepresentation() == nil){
        //            backgroundMapView.add(mapObject: mapStartMarker)
        //        }
        //
        //
        mapStartMarker?.position = startCoordinates
        //        let nmaImageObj:NMAImage = NMAImage.image(with: #imageLiteral(resourceName: "tracke-car-man")) as! NMAImage
        //        mapStartMarker?.icon = nmaImageObj
        
        //show End marker
        let lastLocationString:String = arrayLocations[arrayLocations.count-1] as! String
        let latLong:Array = lastLocationString.components(separatedBy: ",")

        let lastCoordinates = CLLocationCoordinate2DMake(Double(latLong[0])!, Double(latLong[1])!)
        if(mapEndMarker == nil){
            
             mapEndMarker = GMSMarker.init(position: vehicleCoordinates!)
             mapEndMarker?.icon =  #imageLiteral(resourceName: "mycar")
             mapEndMarker?.map = backgroundMapView
                 
        }
        
        mapEndMarker?.position = lastCoordinates
        
        if(flagLocateCarZZom == false){
            //set bounding box
            
            
            let path = GMSMutablePath()

            for wayPoint in arrayForBoundingBox{
              let locationString:String = wayPoint as! String
              let latLong:Array = locationString.components(separatedBy: ",")
              let coordinates = CLLocationCoordinate2DMake(Double(latLong[0])!, Double(latLong[1])!)
              path.add(coordinates)
            }

            let boundingBox:GMSCoordinateBounds = GMSCoordinateBounds(path: path)
                
            let camera: GMSCameraUpdate = GMSCameraUpdate.fit(boundingBox)

            self.backgroundMapView.animate(with: camera)
            
            //Set geo center
            let coordinates = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
            backgroundMapView.animate(toLocation: coordinates)
            backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
            
        }
        
        
        //calculate Distance and time
        let distance:UInt64 = routeSummary.object(forKey: "distance") as! UInt64
        var distanceString:String!
        if((distance/1000) > 0){
            distanceString = "Distance: " + "\(distance/1000)" + "km"
        }
        else {
            distanceString = "Distance: " + "\(distance)" + "m"
            
        }
        let time:TimeInterval = routeSummary.object(forKey: "travelTime") as! TimeInterval
        let timeStr:String = AppUtility.sharedUtility.convertSecondsInHoursMinSec(timeInSeconds: time)
        //print(timeStr)
        locateMyCarDistanceTime.text = distanceString + "\nETA:" + timeStr
        locateMyCarDistanceTime.isHidden = false
        FTIndicator.dismissProgress()
        
    }
    
    func fetchRoute(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        
        let session = URLSession.shared
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=walking&key=AIzaSyDeDarI_BW90aUeFulaGmU05IAV1Lkui5M")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            guard let jsonResult = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                print("error in JSONSerialization")
                return
            }
            
            guard let routes = jsonResult["routes"] as? [Any] else {
                return
            }
            
            
            guard let route = routes[0] as? [String: Any] else {
                return
            }

            self.showAllRoutes(route: route,destLocation: destination, sourceLocation: source)


        })
        task.resume()
    }
    
  
    func showAllRoutes(route:[String: Any],destLocation:CLLocationCoordinate2D,sourceLocation:CLLocationCoordinate2D){
        
        
        
        guard let legs = route["legs"] as? [Any] else {
            return
        }

        guard let leg = legs[0] as? [String: Any] else {
            return
        }

        guard let steps = leg["steps"] as? [Any] else {
            return
        }
        

        
//          for item in steps {

//            guard let step = item as? [String: Any] else {
//                return
//            }
//
//            guard let polyline = step["polyline"] as? [String: Any] else {
//                return
//            }
//
//            guard let polyLineString = polyline["points"] as? String else {
//                return
//            }

            DispatchQueue.main.async  {
                
                
                            
                if steps.count > 0 {
                    
                    let arrayOfWayPoints:NSMutableArray = []
                    
                    
                        var summaryDistance = 0
                    
                        var summaryTime = 0
                    
                        for item in steps {
                            
                            var aLocationCoordinates = ""
                            
                            guard let step = item as? [String: Any] else {
                                continue
                            }
                            
                            guard let distance = step["distance"] as? [String: Any] else {
                                continue
                            }

                            
                            guard let duration = step["duration"] as? [String: Any] else {
                                continue
                            }
                            
                            guard let start_location = step["start_location"] as? [String: Any] else {
                                continue
                            }
                            
                            guard let distanceValue = distance["value"] as? Int else {
                                continue
                            }
                                                        
                            guard let durationValue = duration["value"] as? Int else {
                                continue
                            }

                            guard let start_location_lat = start_location["lat"] as? Double else {
                                continue
                            }

                            guard let start_location_long = start_location["lng"] as? Double else {
                                continue
                            }

                            summaryDistance += distanceValue
                            
                            summaryTime += durationValue
                            
                            aLocationCoordinates = String.init(format: "%f,%f", start_location_lat,start_location_long)
                            
                            arrayOfWayPoints.add(aLocationCoordinates)
                        }
                    
                    let summaryDict: NSMutableDictionary? = ["travelTime" : summaryTime,
                        "distance" : summaryDistance
                    ]

//                        let arrayResponse:NSArray = (dictResponseRoute.object(forKey: "route") as? NSArray)!
//                        let arrayWayPoints:NSArray = (arrayResponse[0] as! NSDictionary).object(forKey: "shape") as! NSArray
//                        let summaryDict:NSDictionary = (arrayResponse[0] as! NSDictionary).object(forKey: "summary") as! NSDictionary
//                        //print(arrayWayPoints)
                    
                    self.drawRouteToVehicleNew(arrayLocations: arrayOfWayPoints, userLocation: sourceLocation, routeSummary:summaryDict!)
                    }
                    else {
                        if(self.flagLocateCarZZom == false){
                            FTIndicator.dismissProgress()
                            FTIndicator.setIndicatorStyle(.dark)
                            FTIndicator.showError(withMessage: NSLocalizedString("Unable to locate car right now", comment: ""))
                        }

                    }
                
                
                
                
                
                
//
//                  let path = GMSPath(fromEncodedPath: polyLineString)
//                  let polylineObj = GMSPolyline(path: path)
//                  polylineObj.strokeWidth = 3.0
//                  polylineObj.map = self.backgroundMapView // Google MapView
//
//
//                  let cameraUpdate = GMSCameraUpdate.fit(GMSCoordinateBounds(coordinate: sourceLocation, coordinate: destLocation))
//                  self.backgroundMapView.moveCamera(cameraUpdate)
//                  let currentZoom = self.backgroundMapView.camera.zoom
//                  self.backgroundMapView.animate(toZoom: currentZoom - 1.4)
            }
//        }
            
//                if routes.count > 0 {
//
//                        let arrayResponse:NSArray = (dictResponseRoute.object(forKey: "route") as? NSArray)!
//                        let arrayWayPoints:NSArray = (arrayResponse[0] as! NSDictionary).object(forKey: "shape") as! NSArray
//                        let summaryDict:NSDictionary = (arrayResponse[0] as! NSDictionary).object(forKey: "summary") as! NSDictionary
//                        //print(arrayWayPoints)
//                        self.drawRouteToVehicle(arrayLocations: arrayWayPoints, userLocation: userLocation, routeSummary:summaryDict)
//                    }
//                    else {
//                        if(self.flagLocateCarZZom == false){
//                            FTIndicator.dismissProgress()
//                            FTIndicator.setIndicatorStyle(.dark)
//                            FTIndicator.showError(withMessage: NSLocalizedString("Unable to locate car right now", comment: ""))
//                        }
//
//                    }
    
    }
    
    func drawPath(from polyStr: String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor =  UIColor(red:56.0/255.0, green:120.0/255.0, blue:210.0/255.0, alpha:1.00)
        polyline.map = backgroundMapView
    }
    
    func trackVehicleLocation(userLocation:CLLocation) {
        
        
                  if(vehicleStateObj != nil){
                        
                      let destLocation:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: (vehicleStateObj!.location.latitude), longitude: (vehicleStateObj!.location.longitude))
                      
                      let sourceLocation:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude))
        
                        self.fetchRoute(source: sourceLocation, destination: destLocation)
                  }
                  else {
                    
                  }
                  
    }
}
