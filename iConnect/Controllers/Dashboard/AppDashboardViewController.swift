//
//  AppDashboardViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 04/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class AppDashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    lazy var presentationDelegate = PickerControllerPresentationManager()
    
    @IBOutlet weak var dashboardTableView: UITableView!
    
    internal var isMapVisible:Bool = false
    var picker = GMPicker()
    
    var vehicleHeader:HeaderPickerArea?
    
    var arrayMake:Array = [String]()
    
    var timerVehicleStateRefresh : Timer?
    var vehicleCurrentState:CVehicleCurrentState?
    var currentTag:NSNumber?
    var locationsDelta = [NSDictionary]()
    var oldTripId: String?
    
    let headerButton = UIButton(type: .custom)
    var arrayCurrentTripLocations = [CLocation]()
    
    var gestureRecognizer:UIPanGestureRecognizer?
    
    var driveScore: CDriveScore?
    
    var driverScoreOverlay:DriverScoreOverlay?
    
    var updateInterval = kVehicleLocationUpdateIntervalTenSec
    
    @IBOutlet weak var eventsButton: UIButton!
    
    var healthStatus:CVehicleHealthStatus?
    
    var uin:String?
    
    var thirdTabVC : UIViewController?
    //MARK:- Life Cycle Methods
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // oldTripId = "None"
        AppUtility.sharedUtility.trackScreenName(with: "Dashboard Screen")
        let currentState:CVehicleCurrentState = CVehicleCurrentState()
        let locationObj:CLocation = CLocation()
        
        currentState.location = locationObj
        vehicleCurrentState = currentState
        
        
        self.gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        
        let notificationIdentifier = Notification.Name("UpdateDataObDashboard")
        NotificationCenter.default.addObserver(self, selector: #selector(fetchDataForDashboardView), name: notificationIdentifier, object: nil)
        
    }
    @IBAction func alertsButtonTapped(_ sender: Any) {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AlertsNavController")
            vc.modalPresentationStyle = .fullScreen;
            self.navigationController?.present(vc, animated: true, completion: {
            })
    }
    @objc func handlePan(recognizer: UIPanGestureRecognizer) -> Void {
        
        // Disable Pan gesture of MFSideMenucontoller to get Pan Gesture of Map
        
        if (recognizer.location(in: self.view).y > 66) {
            recognizer.isEnabled = false
            recognizer.isEnabled = true
        }
    }
    
    func getDataForDashboardHeader(){
        if CommonData.sharedData.isDataPrefetched {
            CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (vehicleObj) in
                if vehicleObj != nil {
                    //print(vehicleObj)
                    self.getVehicleCurrentLocation()
                    
                    if let _ = self.navigationItem.titleView {
                        self.setHeaderTitle(title: vehicleObj?.name!)
                    }
                    else {
                        self.navigationItem.titleView = self.createHeaderView()
                    }
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicleObj){
                        AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                    }
                    
                }else{
                    
                    CommonData.sharedData.getAllVehicles(vehicles: { (vehicles, isSuccess) in
                        
                        for vehicle in vehicles{
                            
                            if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle){
                                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                            }
                        }
                    })
                    
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        
        self.navigationItem.titleView = self.createHeaderView()
        
        if CUserRequest.sharedInstance().isUserLoggedIn() && CommonData.sharedData.isDataPrefetched {
            
            if let readStatus = AppUtility.sharedUtility.allEventsRead {
                if readStatus {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                }
                else {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                    
                }
                
            }
            
            self.getListOfUnreadEvents()
            
            self.fetchDataForDashboardView()
            
        }
        
        if isMapVisible {
            self.toggleMapOnScreen()
        }
        
        
        self.hideOrunhideTab()
    }
    
    func hideOrunhideTab(){

        if let deviceMode = UserDefaults.standard.string(forKey: "deviceMode") {
            if deviceMode != "TOYOTA" {
                if let tabBarController = self.tabBarController {
                    let indexToRemove = 2
                     if tabBarController.viewControllers!.count == 5 {
                        if indexToRemove < tabBarController.viewControllers!.count {
                            var viewControllers = tabBarController.viewControllers
                            thirdTabVC = viewControllers![indexToRemove]
                            viewControllers?.remove(at: indexToRemove)
                            tabBarController.viewControllers = viewControllers
                        }
                    }
                }
            }else{
               if let tabBarController = self.tabBarController {
                   let indexToInsert = 2
                   if tabBarController.viewControllers!.count != 5 {
                       if indexToInsert < tabBarController.viewControllers!.count {
                           tabBarController.viewControllers?.insert(thirdTabVC!, at: indexToInsert)
                       }
                   }
               }
            }
        }
    }
    
    @objc func fetchDataForDashboardView() {
        self.getDataForDashboardHeader()
        self.getVehicleHealth()
        self.getListOfUnreadEvents()
        self.navigationItem.titleView = self.createHeaderView()
        
    }
    
    func getListOfUnreadEvents() -> Void {
        
        let dateString = CommonData.sharedData.lastEventTimeStamp
        
        CEventRequest.sharedInstance().getNewEventsCount(afterTime: dateString) { (responseData, isSuccess, error) in
            
            if isSuccess {
                
                let eventCount = responseData as? NSNumber
                
                if eventCount == 0 {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = true
                }
                else {
                    self.eventsButton.setImage(#imageLiteral(resourceName: "notification_selected"), for: .normal)
                    AppUtility.sharedUtility.allEventsRead = false
                }
            }
            else {
                
                
            }
        }
    }
    
    
    //MARK:- Vehicles Fetch Method
    
    func getVehicleList() -> Void {
        
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            if vehicles.count == 0 {
                return
            }
            
            self.navigationItem.titleView = self.createHeaderView()
            let vehicle:CVehicle = vehicles.first!
            var errorObj:NSError?
            CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError: &errorObj)
            
            self.setHeaderTitle(title: vehicle.name)
            self.getVehicleCurrentLocation()
        }
        
    }
    
    
    //MARK:- Header Tap Method
    
    @IBAction func headerTapped(_ sender:Any) {
        
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            if vehicles.count > 1{
                
                
                let pickerView:CustomPickerView = CustomPickerView()
                pickerView.transitioningDelegate = self.presentationDelegate
                pickerView.modalPresentationStyle = .custom
                pickerView.isShowingHeader = true
                pickerView.pickerTitle = "Select a Vehicle"
                
                self.arrayCurrentTripLocations.removeAll()
                
                let indexPath = IndexPath(item: 0, section: 0)
                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
                dashBoardLocationCell.arrayLiveTrackingLocations.removeAll()
                // dashBoardLocationCell.addressLabel.text = ""
                
                var vehicleNameArray:[String] = []
                for vehicle in vehicles {
                    vehicleNameArray.append(vehicle.name)
                }
                
                pickerView.arrayData = vehicleNameArray as NSArray//.sorted() as NSArray
                
                pickerView.doneButtonCallback { (row, component) in
                    self.vehicleCurrentState = nil
                    if self.isMapVisible {
                        self.toggleMapOnScreen()
                    }
                    
                    let vehicle = vehicles[row]
                    
                    if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle) {
                        pickerView.dismiss(animated: true, completion: nil)
                        
                        
                        var error:NSError? = nil
                        CVehicleRequest.sharedInstance().setSelectedForVehicleWithId(vehicle.vehicleId, withError:&error)
                        
                        if device.status == kDeviceStatusSubscriptionPending || device.status == kDeviceStatusActivationInProgress || device.status == kDeviceStatusTerminated || device.status == kDeviceStatusExpired {
                            pickerView.dismiss(animated: true, completion: { 
                                AppUtility.sharedUtility.showSubscriptionSpecificPopup(forController: self, withSubscriptionError: device.status)
                            })
                            
                            
                        }//else{
                        
                        self.vehicleCurrentState = nil
                        UserDefaults.standard.removeObject(forKey: "lastSavedLocation")
                        UserDefaults.standard.removeObject(forKey: "lastSavedLocationDate")
                        
                        self.setHeaderTitle(title: vehicle.name)
                        
                        if let _ = self.timerVehicleStateRefresh {
                            self.timerVehicleStateRefresh?.invalidate()
                            self.timerVehicleStateRefresh = nil
                        }
                        
                        if(error != nil) {
                            self.view.dismissProgress()
                            print(error?.localizedDescription ?? String())
                        }
                        else{
                            // self.view.dismissProgress()
                            self.view.showIndicatorWithProgress(message: "Getting vehicle details..")
                            
                            self.getVehicleCurrentLocation()
                            self.getOverallScoreForSelectedVehicle()
                            //                        print("selected vehicle is"+vehicle.name)
                        }
                        //                        }
                    }
                }
                
                self.present(pickerView, animated: true, completion: nil)
                print("header tapped")
                
            }
        }
    }
    
    func setHeaderTitle(title: String?) -> Void {
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            
            if vehicles.count <= 1 {
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!), for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name), for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first?.name ?? ""), for: .normal)
                        }
                    })
                }
            }else{
                if title != nil {
                    self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: title!) + " ▼" , for: .normal)
                }else{
                    CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicle) in
                        if selectedVehicle != nil {
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: selectedVehicle!.name) + " ▼" , for: .normal)
                        }else{
                            self.headerButton.setTitle(AppUtility.sharedUtility.decodeString(stringToDecode: vehicles.first!.name) + " ▼" , for: .normal)
                        }
                        
                    })
                }
            }
        }
        self.headerButton.titleLabel?.sizeToFit()
        //self.headerButton.sizeToFit()
    }
    
    func startTimer () {
        
        if timerVehicleStateRefresh == nil {
            timerVehicleStateRefresh =  Timer.scheduledTimer(
                timeInterval: TimeInterval(self.updateInterval),
                target      : self,
                selector    : #selector(refreshVehicleCurrentState),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopTimer() {
        if timerVehicleStateRefresh != nil {
            timerVehicleStateRefresh?.invalidate()
            timerVehicleStateRefresh = nil
        }
    }
    
    
    //MARK: Vehicle tracking
    
    func showLastSavedLocation()
    {
        let indexPath = IndexPath(item: 0, section: 0)
        
        if  let dashBoardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as? DashboardLocationCell {
            
            dashBoardLocationCell.setCellData(vehicleState: self.vehicleCurrentState)
        }
        
        //        let result = UserDefaults.standard.value(forKey: "lastSavedLocation")
        //        // print(result!)
        //        if(result != nil){
        //            if  let dashBoardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as? DashboardLocationCell {
        //                dashBoardLocationCell.addressLabel.text = result as! String?
        //            }
        //        }
        //
        //        if let updateTime = UserDefaults.standard.value(forKey: "lastSavedLocationDate") {
        //
        //            if  let dashBoardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as? DashboardLocationCell {
        //                dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime as! Date)
        //            }
        //
        //        }
        //        // print(result!)
        //        if(result != nil){
        //                    }
        
        
        
    }
    
    func getAddressofLocation(completion: @escaping (_ address: String) -> Void)
    {
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: (vehicleCurrentState?.location.latitude)!, longitude: (vehicleCurrentState?.location.longitude)!)
        
        CVehicleRequest.sharedInstance().getAddressForLocation(location) { (responseObject, isSucessful, error) in
            if(error == nil && responseObject != nil)
            {
                DispatchQueue.main.async  {
                    
                    let addressStr:String = responseObject as! String
                    if(addressStr == "Unable to find location" || addressStr == kUnableToGetLocation){
                        
                        if (UserDefaults.standard.value(forKey: "lastSavedLocation") != nil) {
                            let locationStr = UserDefaults.standard.value(forKey: "lastSavedLocation") as! String
                            completion(locationStr)
                        }
                        else {
                            completion("Location not found")
                        }
                        
                        
                    }else {
                        //save  vehicle last location
                        UserDefaults.standard.set(addressStr, forKey: "lastSavedLocation")
                        UserDefaults.standard.set(self.vehicleCurrentState?.updateTime, forKey: "lastSavedLocationDate")
                        
                        completion(addressStr)
                    }
                }
                
            }
            else{
                DispatchQueue.main.async {
                    let locationStr = UserDefaults.standard.value(forKey: "lastSavedLocation") as! String?
                    completion(locationStr!)
                }
            }
            
            
        }
    }
    
    func getVehicleCurrentLocation()
    {
        self.showLastSavedLocation()
        
        self.startTimer()
        
        CVehicleRequest.sharedInstance().getCurrentState { (responseObject, isSuccessful, error) in
            self.view.dismissProgress()
            
            if(error == nil  && responseObject != nil){
                self.vehicleCurrentState = responseObject as! CVehicleCurrentState?
                // print(self.vehicleCurrentState?.location ?? CLLocation())
                
                
                let indexPath = IndexPath(item: 0, section: 0)
                // self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
                dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
                dashBoardLocationCell.setCellData(vehicleState: self.vehicleCurrentState!)
                
                if let deviceMode = self.vehicleCurrentState?.deviceMode
                {
                    UserDefaults.standard.set(deviceMode, forKey:"deviceMode")
                    UserDefaults.standard.synchronize()
                    
                    self.hideOrunhideTab()
                }
                
                
                self.uin = self.vehicleCurrentState?.uin
                
                self.getOverallScoreForSelectedVehicle()
             
                if let _ = self.vehicleCurrentState?.location {
                    
                    if self.updateInterval == kVehicleLocationUpdateIntervalThirtyMin {
                        self.stopTimer()
                        self.updateInterval = kVehicleLocationUpdateIntervalTenSec
                        self.startTimer()
                    }
                    
                }
                else {
                    if self.updateInterval == kVehicleLocationUpdateIntervalThirtyMin {
                        self.stopTimer()
                        self.updateInterval = kVehicleLocationUpdateIntervalTenSec
                        self.startTimer()
                    }
                    
                }
                
                //get all location if vehicle is running/trip exists
                if(self.vehicleCurrentState?.trip != nil){
                    if((self.vehicleCurrentState?.trip.tripId) != ""){
                        
                        self.oldTripId = self.vehicleCurrentState?.trip.tripId
                        self.getDynamicLocations(forTrip: self.vehicleCurrentState?.trip)
                    }
                    
                }
                else{
                    self.oldTripId = "None"
                    self.updateLocationCellData(ifTripExists: false)
                }
                /* self.getAddressofLocation() {
                 (address: String) in
                 print("got back: \(address)")
                 dashBoardLocationCell.addressLabel.text = address
                 if let updateTime = self.vehicleCurrentState?.updateTime {
                 dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
                 
                 }
                 }*/
                
            }
            else {
                let indexPath = IndexPath(item: 0, section: 0)
                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
                dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusInactive") //now
                dashBoardLocationCell.setCellData(vehicleState: nil)
                
                //                let indexPath = IndexPath(item: 0, section: 0)
                //               // self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
                //
                
            }
            
        }
    }
    
    @objc func refreshVehicleCurrentState()
    {
                
//        CVehicleRequest.sharedInstance().getCurrentStateOfVehicle(withTag: currentTag, response: { (responseObject, isSuccessful, error) in
//            if(error == nil  && responseObject != nil){
//
//                let indexPath = IndexPath(item: 0, section: 0)
//                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
//                dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
//
//
//                let vehicleLocation:CVehicleCurrentState = (responseObject as! CVehicleCurrentState?)!
//
//                if let selectedVehivleobj:CVehicle = CVehicleRequest.sharedInstance().getSelectedVehicle() {
//
//                    let selectedVehicleId:NSNumber = NSNumber(integerLiteral: Int(selectedVehivleobj.vehicleId))
//                    if(vehicleLocation.vehicleId == selectedVehicleId)
//                    {
//                        // self.vehicleCurrentState = vehicleLocation
//                        self.checkCurrentTripExists(currentLocation: vehicleLocation)
//                        dashBoardLocationCell.setCellData(vehicleState: vehicleLocation)
//                        //                        self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
//                    }
//                }
//
//            }
//            else {
//
//                if responseObject != nil && error == nil {
//
//                    if !(self.timerVehicleStateRefresh?.isValid)! && self.updateInterval != kVehicleLocationUpdateIntervalThirtyMin {
//                        self.stopTimer()
//                        self.updateInterval = kVehicleLocationUpdateIntervalThirtyMin
//                        self.startTimer()
//
//                    }
//
//                }
//
//                let indexPath = IndexPath(item: 0, section: 0)
//                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
//
//                dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusInactive") //now
//            }
//        }) { (tag, delta) in
//            if((tag != nil) && (self.vehicleCurrentState?.trip != nil) && (delta != nil))
//            {
//                let indexPath = IndexPath(item: 0, section: 0)
//                let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
//                if(dashBoardLocationCell.tripPath?.vertices != nil){
//                    self.currentTag = tag
//                    if let deltaArray = delta as? [NSDictionary] {
//                        self.locationsDelta = deltaArray
//                    }
//
//
//                    if(dashBoardLocationCell.tripPath?.vertices.count == self.arrayCurrentTripLocations.count){
//
//                        for (index, element) in (delta?.enumerated())! {
//                            print("Item \(index): \(element)")
//
//                            if let locationObj = delta?[index] as? CLocation {
//                                self.arrayCurrentTripLocations.append(locationObj)
//                            }
//
//                            //                            let clocationObj = delta as AnyObject
//                            //                            self.arrayCurrentTripLocations.append(clocationObj as! CLocation)
//
//                        }
//                        //sort on timestamp
//                        let sortedArray = self.arrayCurrentTripLocations.sorted(by:
//                        {
//                            Double(($0 ).timeStamp) > Double(($1 ).timeStamp)
//                        })
//                        //print(sortedArray)
//
//                        //update cell data
//                        dashBoardLocationCell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: false,tripExists: true )
//                    }
//                }
//
//
//
//
//            }
//        }

        
    }
    
//    -(CLocation*)mappingToUserModel
//    {
//        CLocation* newObj = [CLocation new];
//        newObj.direction = [self.direction floatValue];
//        newObj.latitude = [self.latitude floatValue];
//        newObj.longitude = [self.longitude floatValue];
//        newObj.speed = [self.speed floatValue];
//        newObj.rpm = [self.rpm floatValue];
//        newObj.geoCode = self.geoCode;
//        newObj.timeStamp = [self.timeStamp doubleValue];
//        newObj.forTripLocation = [self.forTripLocation userModel];
//        newObj.forTripStaticData = [self.forTripStaticData mappingUserModel];
//        newObj.forTripEndLocation = [self.forTripEndLocation userModel];
//        newObj.fotTripStartLocation = [self.fotTripStartLocation userModel];
//        newObj.forTripBreak = [self.forTripBreak userModel];
//
//
//        return newObj;
//
//    }
    
    func getDynamicLocations(forTrip:CTrip!)
    {
        
//        let eventsDict:NSDictionary = responseObject as! NSDictionary
//                       self.dictTripBreaksEvents = eventsDict
//                       self.showTripPath(withbreaksAndEvents: eventsDict)
        
        CTripRequest.sharedInstance().getTripDetailsForTrip(withId: forTrip.tripId) { (responseObject, isSuccessful, error) in
             if(error == nil && responseObject != nil){
                 let indexPath = IndexPath(item: 0, section: 0)
                let eventsDict:NSDictionary = responseObject as! NSDictionary
                 let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
                 let arrayLocations:NSArray = eventsDict.object(forKey: "locations") as! NSArray
                
                var objectsofLocations = [CLocation]()
                
                for obj in arrayLocations {
                    
                    let locationObj = CLocation()
                    
                    locationObj.latitude  = ( obj as! Location ).latitude as! Double
                    locationObj.longitude  = ( obj as! Location ).longitude as! Double
                    locationObj.timeStamp  = ( obj as! Location ).timeStamp as! Double
                    
                    objectsofLocations.append(locationObj)
                }
                
                 dashBoardLocationCell.arrayLiveTrackingLocations = objectsofLocations
                 self.arrayCurrentTripLocations = objectsofLocations
                 dashBoardLocationCell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: false,tripExists: true )
                            
             }
         }
   
    }
    
    func checkCurrentTripExists(currentLocation:CVehicleCurrentState)
    {
        
        if((currentLocation.location.latitude == self.vehicleCurrentState?.location.latitude ?? Double()) && (currentLocation.location.longitude == self.vehicleCurrentState?.location.longitude ?? Double())){
            print("same")
            if(currentLocation.trip == nil || currentLocation.trip.tripId == ""){
                self.oldTripId = "None"
                self.updateLocationCellData(ifTripExists: false)
            }
            
        }
        else{
            print("diff")
            
            self.vehicleCurrentState = currentLocation
            
            if (currentLocation.trip != nil && currentLocation.trip.tripId == self.oldTripId)
            {
                self.updateTripData(vehicleCurrentLocation: currentLocation)
            }
            else if((self.vehicleCurrentState?.trip) != nil){
                self.currentTag = nil
                self.locationsDelta.removeAll()
                self.oldTripId = self.vehicleCurrentState?.trip.tripId
                self.getDynamicLocations(forTrip: self.vehicleCurrentState?.trip)
            }
            
            if(currentLocation.trip == nil || currentLocation.trip.tripId == "")
            {
                self.oldTripId = "None"
                self.updateLocationCellData(ifTripExists: false)
            }
            
            
        }
    }
    func updateTripData(vehicleCurrentLocation:CVehicleCurrentState)
    {
        let indexPath = IndexPath(item: 0, section: 0)
        let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
        
        let coordinates = CLLocationCoordinate2DMake((self.vehicleCurrentState?.location.latitude)!, (self.vehicleCurrentState?.location.longitude)!)
        dashBoardLocationCell.vehicleCoordinates = coordinates
        self.arrayCurrentTripLocations.append(vehicleCurrentLocation.location)
        dashBoardLocationCell.arrayLiveTrackingLocations = self.arrayCurrentTripLocations
        dashBoardLocationCell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: true,tripExists: true )
        dashBoardLocationCell.setCellData(vehicleState: self.vehicleCurrentState)
        
        
        
        //update Address and Time
        self.getAddressofLocation() {
            (address: String) in
            // print("got back: \(address)")
            dashBoardLocationCell.addressLabel.text = address
            
            if address != "Location not found" {
                if let updateTime = vehicleCurrentLocation.updateTime {
                    dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
                }
            }
            else {
                dashBoardLocationCell.dateLabel.text = "--"
                
            }
        }
        
    }
    
    func getOverallScoreForSelectedVehicle() -> Void {
        
        CommonData.sharedData.getSelectedVehicle { (selectedVehicleobj) in
            if selectedVehicleobj != nil {
                
                CTripRequest.sharedInstance().getOverAllTripScore(forVehiclev2: self.uin, response: { (responseData, isSuccess, error) in
                    
                    
                    if isSuccess {
                        
                        let score = responseData as? CDriveScore
                        self.driveScore = score
                        
                        //                        if let dashboardDriverScoreCell = self.dashboardTableView.cellForRow(at: indexPath) as? DashboardDriverScoreCell {
                        //                            dashboardDriverScoreCell.setDriverScore(score: score!)
                        //                        self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
                        //  }
                        
                    }else {
                        
                        if (responseData != nil) && (error != nil) {
                            self.driveScore = nil
                            
                            //                            self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseData as! NSDictionary))
                        }
                        
                    }
                    let indexPath = IndexPath(item: 1, section: 0)
                    self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
                    
                })
            }
        }
        
    }
    
    
    @IBAction func navigateToHealthScene(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 2
        
    }
    
    func createHeaderView() -> UIButton {
        headerButton.titleLabel?.font = UIFont(name: "Helvetica", size: 13)
        headerButton.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        //headerButton.frame = CGRect(x: 46, y: 0, width: self.view.frame.width - 92, height: (self.navigationController?.navigationBar.frame.height)!)
        
        self.setHeaderTitle(title: nil)
        
        return headerButton
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        //self.isMapVisible = false
        // toggleMapOnScreen()
        let notificationName = Notification.Name("toggleLeftMenu")
        NotificationCenter.default.post(name: notificationName, object: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.stopTimer()
    }
    
    func openNearbyScreenForFuelStations(){
        if let centreController = self.menuContainerViewController.centerViewController as? CentreViewController {
            centreController.performSegue(withIdentifier: kCentreToNearbySegue, sender: "FUEL")
        }
    }
    
    func openNearbyScreenForServiceStations(){
        if let centreController = self.menuContainerViewController.centerViewController as? CentreViewController {
            centreController.performSegue(withIdentifier: kCentreToNearbySegue, sender: "SERVICE")
        }
    }
    
    
    //MARK: Location cell UI updation
    
    func updateLocationCellData(ifTripExists:Bool)
    {
        let indexPath = IndexPath(item: 0, section: 0)
        let dashBoardLocationCell:DashboardLocationCell = self.dashboardTableView.cellForRow(at: indexPath) as! DashboardLocationCell
        
        dashBoardLocationCell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
        dashBoardLocationCell.setCellData(vehicleState: self.vehicleCurrentState)
        //        self.getAddressofLocation() {
        //            (address: String) in
        //            print("got back: \(address)")
        //            if let updateTime = self.vehicleCurrentState?.updateTime {
        //                dashBoardLocationCell.addressLabel.text = address
        //                dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
        //            }
        //            else {
        //                if let updateTime = UserDefaults.standard.value(forKey: "lastSavedLocationDate") as? Date {
        //                    dashBoardLocationCell.addressLabel.text = address
        //                    dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
        //                }
        //
        //            }
        //        }
        
        
        //        if(self.vehicleCurrentState?.updateTime != nil){
        //            let dt:NSDate = self.vehicleCurrentState!.updateTime as NSDate
        //            print(dt)
        //
        //
        //            //            CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicleObj) in
        //            //
        //            //                if selectedVehicleObj != nil {
        //            //
        //            //                    if let selectedDevice:CDevice = CVehicleRequest.sharedInstance().getDeviceFor(selectedVehicleObj){
        //            //
        //            //                        if (selectedDevice.status == kDeviceStatusSubscriptionPending || selectedDevice.status == kDeviceStatusActivationInProgress) {
        //            //
        //            //                            dashBoardLocationCell.dateLabel.text = "--"
        //            //
        //            //                        }else{
        //            //                            dashBoardLocationCell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: (self.vehicleCurrentState?.updateTime)!)
        //            //                        }
        //            //                    }
        //            //                }
        //            //            })
        //
        //        }else{
        //            dashBoardLocationCell.dateLabel.text = "--"
        //        }
        
        dashBoardLocationCell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: false,tripExists: ifTripExists )
    }
    
    // MARK: TableView methods
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if indexPath.row == 0 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardLocationCell", for: indexPath) as! DashboardLocationCell
            cell.setCellData(vehicleState: self.vehicleCurrentState)

            //            if let vehicleState = self.vehicleCurrentState {
            //
            //                //get all location if vehicle is running/trip exists
            ////                if(vehicleState.trip != nil){
            ////                    self.getAddressofLocation() {
            ////                        (address: String) in
            ////                        //print("got back: \(address)")
            ////                        cell.addressLabel.text = address
            ////                        if let updateTime = vehicleState.updateTime {
            ////                            cell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
            ////                        }
            ////
            ////                        cell.carStatusImage.image = #imageLiteral(resourceName: "CarStatusActive") //now
            ////                    }
            ////
            //////                    if(vehicleState.trip.tripId != "") {
            //////
            //////                        self.oldTripId = self.vehicleCurrentState?.trip.tripId
            //////                        self.getDynamicLocations(forTrip: self.vehicleCurrentState?.trip)
            //////                    }
            ////                    //else
            ////                    //{
            ////                    //   self.updateLocationCellData(ifTripExists: false)
            ////                    // }
            ////                }
            ////                else{
            ////                    self.oldTripId = "None"
            ////                    //self.updateLocationCellData(ifTripExists: false)
            ////                }
            //
            //            }
            //            else {
            //                if let lastLocation = UserDefaults.standard.value(forKey: "lastSavedLocation") as! String? {
            //
            //                    if let updateTime = UserDefaults.standard.value(forKey: "lastSavedLocationDate") as? Date {
            //                        cell.addressLabel.text = lastLocation
            //                        cell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: updateTime)
            //                    }
            //
            //                }
            //                else {
            //                    cell.addressLabel.text = "Getting current location..."
            //                }
            //                self.getVehicleCurrentLocation()
            //            }

            //            getAddressofLocation() {
            //                (address: String) in
            //                print("got back: \(address)")
            //                cell.addressLabel.text = address
            //            }
            cell.parentViewController = self


            //            if(vehicleCurrentState?.updateTime != nil) {
            //                let vehicleLastDate:NSDate = vehicleCurrentState!.updateTime as NSDate
            //                print(vehicleLastDate)
            //                cell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: (self.vehicleCurrentState?.updateTime)!)
            //
            ////                                CommonData.sharedData.getSelectedVehicle(selectedVehicleCallback: { (selectedVehicleobj) in
            ////
            ////                                    if selectedVehicleobj != nil {
            ////                                        if let selectedDevice:CDevice = CVehicleRequest.sharedInstance().getDeviceFor(selectedVehicleobj){
            ////
            ////                                            if (selectedDevice.status == kDeviceStatusSubscriptionPending || selectedDevice.status == kDeviceStatusActivationInProgress) {
            ////
            ////                                                cell.dateLabel.text = "--"
            ////
            ////                                            }else{
            ////                                                cell.dateLabel.text = AppUtility.sharedUtility.readableDateAndTimeFormat(date: (self.vehicleCurrentState?.updateTime)!)
            ////                                            }
            ////                                        }
            ////                                    }
            ////                                })
            //            }else{
            //                cell.dateLabel.text = "--"
            //            }

            if self.vehicleCurrentState != nil {
                let coordinates = CLLocationCoordinate2DMake( (vehicleCurrentState?.location.latitude)!,  (vehicleCurrentState?.location.longitude)!)
                if(self.isMapVisible == false) {
                    cell.backgroundMapView.animate(toLocation: coordinates)
                    cell.backgroundMapView.animate(toZoom: Float(kMapZoomLevel))
                }
                cell.vehicleCoordinates = coordinates
                cell.arrayLiveTrackingLocations = self.arrayCurrentTripLocations

                if(self.vehicleCurrentState?.trip == nil || self.vehicleCurrentState?.trip.tripId == ""){
                    cell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: false,tripExists: false )
                }else {
                    cell.updateCellDataWith(isMapVisible: self.isMapVisible, isOldTrip: false,tripExists: true )
                }
            }

            cell.shareCallback {
                AppUtility.sharedUtility.sendGAEvent(withEventName: "ShareMyLocationTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard screen", eventLabel: nil, eventValue: nil)
                self.performSegue(withIdentifier: kDashboardToShareSegue, sender: nil)
            }

            cell.mapButtonCallback {
                // this will be executed when user tap map button
                print("map tapped")
                self.toggleMapOnScreen()
            }

            cell.servicesStationCallback {
                // this will be executed when user will tap on service station callback
                print("service tapped")
                self.openNearbyScreenForServiceStations()
            }

            cell.fuelPumpsCallback {
                // this will be executed when user will tap on fuel pumps
                self.openNearbyScreenForFuelStations()
            }

            return cell

        }else  if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardDriverScoreCell", for: indexPath) as! DashboardDriverScoreCell
            cell.setDriverScore(score: self.driveScore)
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardVehicleHealthCell", for: indexPath) as! DashboardVehicleHealthCell
            cell.vehicleHealthStatus = self.healthStatus
            
            if let deviceMode = UserDefaults.standard.string(forKey: "deviceMode") {
                if deviceMode == "TOYOTA" {
                    cell.towView.isHidden = true
                    cell.engineView.isHidden = true
                }else{
                    cell.towView.isHidden = false
                    cell.engineView.isHidden = false
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if isMapVisible && indexPath.row == 0 {
            return self.dashboardTableView.frame.size.height
        }else if (indexPath.row == 0){
            return 160
        }
        
        let heightForCells = (self.dashboardTableView.frame.size.height - 160) / 2
        
        return (((self.dashboardTableView.frame.size.height/3) < 140) ? 140 : heightForCells)
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        switch indexPath.row {
        case 0:
            self.toggleMapOnScreen()
        case 1:
//            self.performSegue(withIdentifier: kDashboardToDriverScoreSegue, sender: nil)
            AppUtility.sharedUtility.sendGAEvent(withEventName: "DriveScoreTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard screen", eventLabel: nil, eventValue: nil)
        default:
            AppUtility.sharedUtility.sendGAEvent(withEventName: "HealthTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard screen", eventLabel: nil, eventValue: nil)
            break
        }
    }
    
    func showDriverScoreView() {
        if self.driverScoreOverlay == nil {
            self.driverScoreOverlay = Bundle.main.loadNibNamed("DriverScoreOverlay", owner: nil, options: nil)?.first as? DriverScoreOverlay
        }
        
        //        driverScoreOverlay!.fromButton.addTarget(self, action: #selector(fromTapped(_:)), for: .touchUpInside)
        //        driverScoreOverlay!.toButton.addTarget(self, action: #selector(toTapped(_:)), for: .touchUpInside)
        
        driverScoreOverlay!.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        UIApplication.shared.windows.last?.addSubview(driverScoreOverlay!)
        
    }
    
    @IBAction func fromTapped(_ sender: Any) {
        
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        pickerView.pickerTitle = "Select from date"
        pickerView.maximumDate = Date()
        
        pickerView.doneButtonCallback { (date) in
            pickerView.dismiss(animated: true, completion: nil)
            
            //            self.startDate = AppUtility.sharedUtility.getStartOfDay(startDateObj: date)
            //            self.endDate! = self.startDate!.addingTimeInterval(24 * 60 * 60)
            
            //            if self.compareDates(date1: self.startDate!, date2: Date()) == .orderedSame { //Dates are same
            //                self.endDate! = self.startDate!
            //
            //            } else {
            //                self.endDate! = self.startDate!.addingTimeInterval(24 * 60 * 60)
            //
            //            }
            
            //self.updateDateView()
            self.toTapped(nil)
        }
        
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    @IBAction func toTapped(_ sender: Any?) {
        //  self.datePickerTitleLabel.text = "Select to date"
        
        let pickerView:CustomDatePicker = CustomDatePicker()
        pickerView.transitioningDelegate = presentationDelegate
        pickerView.modalPresentationStyle = .custom
        //        pickerView.pickerTitle = "Select To date"
        //        pickerView.minimumDate = self.startDate!
        pickerView.maximumDate = Date()
        
        pickerView.doneButtonCallback { (date) in
            
            // let order = self.compareDates(date1: self.startDate!, date2: date)
            
            //            switch order {
            //
            //            case .orderedDescending :
            //                self.view.showIndicatorWithError("To date cannot be less than From date")
            //
            //                //            case .orderedSame :
            //                //
            //                //                if self.compareDates(date1: self.startDate!, date2: Date()) != .orderedSame {
            //                //                    self.view.showIndicatorWithError("To date should be greater than From date")
            //                //
            //                //                }
            //                //                else {
            //                //                    pickerView.dismiss(animated: true, completion: nil)
            //                //
            //                //                    self.endDate = date
            //                //                    self.updateDateView()
            //                //                }
            //
            //            default:
            //                pickerView.dismiss(animated: true, completion: nil)
            //
            //                self.endDate = AppUtility.sharedUtility.getEndOfDay(endDateObj: date)
            //                //self.updateDateView()
            //
            //            }
        }
        
        self.present(pickerView, animated: true, completion: nil)
    }
    
    func toggleMapOnScreen() {
        self.isMapVisible = !self.isMapVisible
        dashboardTableView.setContentOffset(CGPoint.zero, animated: true)
        dashboardTableView.isScrollEnabled = !dashboardTableView.isScrollEnabled
        self.dashboardTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        
        if self.isMapVisible {
            self.view.addGestureRecognizer(gestureRecognizer!)
            AppUtility.sharedUtility.sendGAEvent(withEventName: "ShowMapTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard screen", eventLabel: nil, eventValue: nil)
        }
        else {
            AppUtility.sharedUtility.sendGAEvent(withEventName: "HideMapTapped", category: kEventCategoryDashboard, andScreenName: "Dashboard screen", eventLabel: nil, eventValue: nil)
            self.view.removeGestureRecognizer(gestureRecognizer!)
        }
    }
    
    //MARK: - Vehicle Health
    func getVehicleHealth(){
        
        // self.view.showIndicatorWithProgress(message: "Please wait")
        
        CVehicleRequest.sharedInstance().getHealthStatus { (response, isSuccessful, error) in
            self.view.dismissProgress()
            
            if (isSuccessful && response != nil) {
                let vehicleHealthObj:CVehicleHealthStatus = response as! CVehicleHealthStatus
                self.healthStatus = vehicleHealthObj
                let indexPath = IndexPath(item: 2, section: 0)
                self.dashboardTableView.reloadRows(at: [indexPath], with: .automatic)
                
            }else{
                if (response != nil) {
                    print(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                }else{
                    print(error!.localizedDescription)
                }
            }
            
        }
        
    }
}
