//
//  DeviceManagementViewController.swift
//  iConnect
//
//  Created by Vaibhav Gautam on 06/01/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit


@objcMembers class DeviceListCell : UITableViewCell {
    
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceIdLabel: UILabel!
    @IBOutlet weak var deviceValidityLabel: UILabel!
    
    var editCallback:EventPerformedCallback?
    
    var isValidityExpired = false
    
    func setDataOnCell(withObject deviceObj:CDevice) {
        deviceNameLabel.text = AppUtility.sharedUtility.decodeString(stringToDecode: deviceObj.name)
        deviceIdLabel.text = deviceObj.serialNumber
        deviceValidityLabel.text = deviceObj.status
    }
    
    @IBAction func editDeviceTapped(_ sender: Any) {
        self.editCallback!()
    }
    
    func editTapped(editDeviceCallback:@escaping EventPerformedCallback){
        self.editCallback = editDeviceCallback
    }
}

class SubscriptionListCell : UITableViewCell {
    
    @IBOutlet weak var validityLabel: UILabel!
    
    @IBOutlet weak var vehicleNameButton: UIButton!
    
    @IBOutlet weak var allocateButton: UIButton!
    
    func setData(subscription: CUnusedSubscription) -> Void {
        
        self.validityLabel.text = "\(subscription.validityInDays!) Days "
        
        if subscription.selectedDevice != nil {
            
            self.vehicleNameButton.setTitle(subscription.selectedDevice.name + "  ▼", for: .normal)
        }
        else {
            self.vehicleNameButton.setTitle("Select a device  ▼", for: .normal)
            
        }
        
        self.vehicleNameButton.sizeToFit()
        self.layoutIfNeeded()
        
    }
}

class BuySubscriptionCell: UITableViewCell {
    
    @IBOutlet weak var buySubscriptionButton: UIButton!
    
    
}

@objcMembers class DeviceManagementViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var subscriptionTableView: UITableView!
    
    @IBOutlet var subscriptionTableViewHeader: UIView!
    
    
    @IBOutlet weak var noSubscriptionLabel: UILabel!
    
    var devicesArray:Array<CDevice>?
    
    var subscriptionArray = Array<CUnusedSubscription>()
    
    var hideBackButton:Bool = false
    var presentationDelegate = PickerControllerPresentationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.sharedUtility.trackScreenName(with: "Device Management Screen")
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        configureNavBar()
        
        self.initializeVC()

        self.view.showIndicatorWithProgress(message: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {

        refreshData()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateManageAlertUI"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name(rawValue: "updateManageAlertUI"), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "updateManageAlertUI"), object: nil)
    }
    
    func initializeVC() -> Void {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name(rawValue: "refreshSubscription"), object: nil)
        
        self.bgView.layer.cornerRadius = 5
        self.bgView.layer.masksToBounds = true
        
        self.tableView.estimatedRowHeight = 88
        self.tableView.rowHeight = UITableView.automaticDimension
        
        
        self.subscriptionTableView.layer.cornerRadius = 5
        self.subscriptionTableView.layer.masksToBounds = true
        
        self.subscriptionTableView.estimatedRowHeight = 100
        self.subscriptionTableView.rowHeight = UITableView.automaticDimension
        
        
        self.setNoUnusedSubscriptionView(isHidden: true)
        
       // self.subscriptionTableView.tableFooterView = self.buySubscriptionButton
        
    }
    
    @objc func refreshData() -> Void {
        self.subscriptionArray.removeAll()
        self.devicesArray?.removeAll()
        
        self.tableView.reloadData()
        self.getAllSubscriptions()
        self.getAllDevicesAndUpdateUI()
        
    }
    
    func configureNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.14, green: 0.16, blue: 0.20, alpha: 1.0)
        
        let headerTitle:UILabel = UILabel()
        headerTitle.font = UIFont(name: "Helvetica", size: 13)
        headerTitle.textColor = UIColor.white
        
        headerTitle.text = "MANAGE ACCOUNT"
        headerTitle.sizeToFit()
        self.navigationController?.navigationBar.topItem?.titleView = headerTitle
        if(hideBackButton == false){
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"backbtn"), style: .plain, target: self, action: #selector(backButtonTapped))
        }
    }
    
    func setNoUnusedSubscriptionView(isHidden: Bool) -> Void {
                                                                     //self.subscriptionTableView.isUserInteractionEnabled = isHidden
       // self.buySubscriptionButton.isHidden = isHidden
        self.noSubscriptionLabel.isHidden = isHidden
    
        self.subscriptionTableView.bounces = isHidden
    }
    
    func getAllDevicesAndUpdateUI() {
        
        CVehicleRequest.sharedInstance().getUserDevicesResponse { (response, isSuccess, error) in
            if(isSuccess){
                if let devices = response as? Array<CDevice>{
                    self.devicesArray = devices.sorted(by: { (obj1, obj2) -> Bool in
                        let date1 = Date.init(timeIntervalSince1970: obj1.registrationDate as! TimeInterval)
                        let date2 = Date.init(timeIntervalSince1970: obj2.registrationDate as! TimeInterval)
//                        print("Registeration date of device 1 = \(obj1.registrationDate)")
//                        print("Registeration date of device 2 = \(obj2.registrationDate)")

                        return date1 < date2
                        
                    })
                    
                    self.tableView.reloadData()
                }
            }else{
                if(response != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: response as! NSDictionary))
                }else{
                    self.view.showIndicatorWithError(error?.localizedDescription)
                }
            }
        }
        
        //Update Vehicles as well at abckened to reflec update device changes immediately
        CommonData.sharedData.fetchAndUpdateVehiclesFromServer(with: { (arrayVehicles, isSuccess) in
            if arrayVehicles.count > 0 {
                print("vehicles updated")
                
            }
        })
    }
    
    func getAllSubscriptions() -> Void {
        
        CVehicleRequest.sharedInstance().getUserSubscriptionsResponse { (responseData, isSuccess, error) in
            
            if isSuccess {
                self.perform(#selector(self.hideIndicator), with: self, afterDelay: 1)

                self.subscriptionArray = responseData as! [CUnusedSubscription]
                
                if self.subscriptionArray.count == 0 {
                    self.setNoUnusedSubscriptionView(isHidden: false)
                }
                else {
                    self.setNoUnusedSubscriptionView(isHidden: true)
                    
                }
                self.subscriptionTableView.reloadData()
                
            }
            else {
                self.perform(#selector(self.hideIndicator), with: self, afterDelay: 1)

            }
        }
    }
    
    @objc func backButtonTapped() {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func addDeviceTapped(_ sender: Any) {
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "AddDeviceTapped", category: kEventCategoryDeviceManagement, andScreenName: "Device Management Screen", eventLabel: nil, eventValue: nil)
        
        let controller = storyboard?.instantiateViewController(withIdentifier: "AddDeviceControllerID") as! AddDeviceViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func howToPlugDeviceTapped(_ sender: Any) {
        
        AppUtility.sharedUtility.sendGAEvent(withEventName: "HowToTapped", category: kEventCategoryDeviceManagement, andScreenName: "Device Management Screen", eventLabel: nil, eventValue: nil)
        self.performSegue(withIdentifier: kManageDeviceToSebViewSegue, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == kManageDeviceToSebViewSegue && sender == nil {
            if let webController = segue.destination as? EmbededWebViewController {
                webController.urlToOpen = "https://www.carot.com/howItWorks"
            }
        }
        else {
            if let webController = segue.destination as? EmbededWebViewController {
                webController.navigationTitle = "Buy Subscription"
                webController.request = sender as! URLRequest?
            }
        }
    }
    
    //MARK:- UITableView DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.subscriptionTableView {
            return self.subscriptionArray.count + 1
            
        }
        else {
            if self.devicesArray != nil {
                return self.devicesArray!.count
            }
            
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tableView {
            return self.headerView
            
        }
        else {
            return self.subscriptionTableViewHeader
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.subscriptionTableView {
            return subscriptionCellFor(cellForRowAt: indexPath)
        }
        
        
        let cell:DeviceListCell = tableView.dequeueReusableCell(withIdentifier: "DeviceListCell") as! DeviceListCell
        cell.selectionStyle = .none
        cell.setDataOnCell(withObject: (self.devicesArray?[indexPath.row])!)

        cell.editTapped {
            let storyboardObj = UIStoryboard(name: "Main", bundle: nil)
            let editController = storyboardObj.instantiateViewController(withIdentifier: "AddDeviceControllerID") as! AddDeviceViewController
            editController.isOpenedForEditing = true
            print((self.devicesArray?[indexPath.row])!)
            editController.prefilledDeviceObj = (self.devicesArray?[indexPath.row])!
            self.navigationController?.show(editController, sender: nil)
            AppUtility.sharedUtility.sendGAEvent(withEventName: "EditDeviceTapped", category: kEventCategoryDeviceManagement, andScreenName: "Device Management Screen", eventLabel: nil, eventValue: nil)
        }
        return cell
        
    }
    
    func subscriptionCellFor(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.subscriptionArray.count {
            
          // let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 20, height: 300))
            
            let cell = subscriptionTableView.dequeueReusableCell(withIdentifier: "BuySubscriptionCell") as! BuySubscriptionCell
            
            cell.selectionStyle = .none
            cell.buySubscriptionButton.layer.cornerRadius = 5
            cell.buySubscriptionButton.layer.masksToBounds = true

            cell.buySubscriptionButton.addTarget(self, action: #selector(buySubscriptionTapped(_:)), for: .touchUpInside)
            
            return cell
        }
        
        let subscription = self.subscriptionArray[indexPath.row]
        
        let cell = subscriptionTableView.dequeueReusableCell(withIdentifier: "SubscriptionListCell") as! SubscriptionListCell
        cell.selectionStyle = .none
        
        cell.vehicleNameButton.tag = indexPath.row
        cell.vehicleNameButton.addTarget(self, action:#selector(vehicleNameTapped(sender:)) , for: .touchUpInside)
        
        cell.allocateButton.tag = indexPath.row
        cell.allocateButton.addTarget(self, action:#selector(allocateTapped(sender:)) , for: .touchUpInside)
        
        cell.setData(subscription: subscription)
        
        return cell
    }
    
    @IBAction func vehicleNameTapped(sender: UIButton) -> Void {
        let subscription = self.subscriptionArray[sender.tag]
        
        
        let pickerView:CustomPickerView = CustomPickerView()
        pickerView.transitioningDelegate = presentationDelegate
        presentationDelegate.shoulAlignViewInCenter = false
        pickerView.modalPresentationStyle = .custom
        pickerView.isShowingHeader = true
        pickerView.pickerTitle = "Select a Vehicle"
        
        
        CommonData.sharedData.getAllVehicles { (vehicles, isSuccess) in
            
            var vehicleNameArray:[String] = []
            for vehicle in vehicles {
                vehicleNameArray.append(vehicle.name)
            }
            
            pickerView.arrayData = vehicleNameArray.sorted() as NSArray
            
            pickerView.doneButtonCallback { (row, component) in
                
                let vehicle = vehicles[row]
                
                if let device = CVehicleRequest.sharedInstance().getDeviceFor(vehicle) {
                    if device.status == kDeviceStatusSubscriptionPending || device.status == kDeviceStatusActivationInProgress {
                        
                        pickerView.view.showIndicatorWithError("Device not activated yet.")
                        return
                    }
                    else {
                        pickerView.dismiss(animated: true, completion: nil)
                        subscription.selectedDevice = device
                        
                        DispatchQueue.main.async {
                            self.subscriptionTableView.reloadData()
                        }
                    }
                }
               
            }
        }
        self.present(pickerView, animated: true, completion: nil)
        
    }
    
    @IBAction func allocateTapped(sender: UIButton) -> Void {
        AppUtility.sharedUtility.sendGAEvent(withEventName: "AllocateDeviceTapped", category: kEventCategoryDeviceManagement, andScreenName: "Device Management Screen", eventLabel: nil, eventValue: nil)
        let subscription = self.subscriptionArray[sender.tag]
        
        if subscription.selectedDevice == nil {
            self.view.showIndicatorWithError("Please select the vehicle for allocation")
        }
        else {
            
            self.view.showIndicatorWithProgress(message: "")
            CVehicleRequest.sharedInstance().addSubscriptionId(subscription.itemId as! UInt, toDeviceId: subscription.selectedDevice.deviceId) { (responseData, isSuccess, error) in
                
                if isSuccess {
                    self.view.dismissProgress()

                    self.subscriptionArray.removeAll()
                    self.tableView.reloadData()
                    
                    self.getAllSubscriptions()
                    
                }
                else {
                    self.view.dismissProgress()
                    if responseData != nil {
                        self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseData as! NSDictionary))
                    }
                    else {
                        self.view.showIndicatorWithError(error?.localizedDescription)
                        
                    }
                    
                }
            }
        }
    }
    @IBAction func buySubscriptionTapped(_ sender: Any) {
        
        //Generate token and open payment url
        AppUtility.sharedUtility.sendGAEvent(withEventName: "BuySubscriptionTapped", category: kEventCategoryDeviceManagement, andScreenName: "Device Management Screen", eventLabel: nil, eventValue: nil)
        CVehicleRequest.sharedInstance().getToBuyProductResponse { (responseData, isSuccess, error) in
            
            if isSuccess {
                self.performSegue(withIdentifier: kManageDeviceToSebViewSegue, sender: responseData)
            }
            else {
                if (responseData != nil){
                    self.view.showIndicatorWithError(AppUtility.sharedUtility.getErrorDescription(FromDictionary: responseData as! NSDictionary))
                    
                }else{
                    self.view.showIndicatorWithError((error?.localizedDescription)!)
                }
                print(error?.localizedDescription ?? String())
            }
        }
    }
    
    @objc func hideIndicator() -> Void {
        self.view.dismissProgress()
    }
}
