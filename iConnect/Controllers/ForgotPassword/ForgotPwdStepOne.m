//
//  ForgotPwdStepOne.m
//  iConnect
//
//  Created by Amit Priyadarshi on 29/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "ForgotPwdStepOne.h"
#import "ForgotPwdStepTwo.h"

@interface ForgotPwdStepOne ()

@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

@implementation ForgotPwdStepOne

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[AppUtility sharedUtility] trackScreenNameWith:@"Forgot Password First Screen"];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = @"";
    //    [Flurry logEvent:@"ForgotPassword-1"];
    //    if (kDevLogger) {
    //        [SIV setScreenName:@"ForgetPassword-1"];
    //        [SIV setDataSize:@"0"];
    //    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)emailSubmitButtonTapped:(id)sender {
    
    txtFieldInValid = nil;
    
    if ([[kAppUtility trimWhiteSpacesInString:emailTextField.text] length] < 1) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"User Id is Required", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = emailTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidEmail" category:kEventCategoryLogin andScreenName:@"Forgot Password First Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if (![[AppUtility sharedUtility] isValidEmailWithEmailToBeValidated:emailTextField.text]) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Please enter a valid email address", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = emailTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidEmail" category:kEventCategoryLogin andScreenName:@"Forgot Password First Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    [self.view showIndicatorWithProgressWithMessage:@""];
    
    [[AppUtility sharedUtility] sendGAEventWithEventName:@"EmailSubmitTapped" category:kEventCategoryLogin andScreenName:@"Forgot Password First Screen" eventLabel:NULL eventValue:NULL];
    [self.submitButton setUserInteractionEnabled:NO];
    
    [SICUserRequest sendOtpForEmail:[kAppUtility trimWhiteSpacesInString:emailTextField.text]
                         forPurpose:COTPTypeForgotPassword
                           response:^(id responseObject, BOOL requestSuccess, NSError *error) {
                               [self.submitButton setUserInteractionEnabled:YES];

                               if (!error && requestSuccess) {
                                   [self.view dismissProgress];
                                   [self performSegueWithIdentifier:kPasswordStepOneToTwoSegue sender:nil];
                               }
                               else if(responseObject && error){
                                   [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];
                                   [FTProgressIndicator showInfoWithMessage:[kAppUtility getErrorDescriptionFromDictionary:responseObject]];
                               }else{
                                   [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];

                                   [FTIndicator showErrorWithMessage:NSLocalizedString(error.localizedDescription, nil)];
                               }
                               
                           }];
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:kPasswordStepOneToTwoSegue]) {
        ForgotPwdStepTwo *stepTwoScreen = segue.destinationViewController;
        [stepTwoScreen setEmailAddressToBeFilled:[kAppUtility trimWhiteSpacesInString:emailTextField.text]];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(txtFieldInValid != nil){
        if(string.length >= 1){
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
    }
    }
    return YES;

}
@end
