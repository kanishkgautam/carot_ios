//
//  ForgotPwdStepTwo.h
//  iConnect
//
//  Created by Amit Priyadarshi on 29/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "MIBaseViewController.h"

@interface ForgotPwdStepTwo : UIViewController{
    
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *otpTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *confirmPasswordTextField;
    UITextField *txtFieldInValid;

}

@property(nonatomic, strong) NSString *emailAddressToBeFilled;

- (IBAction)submitButtonTapped:(id)sender;
- (IBAction)resendButtonTapped:(id)sender;
- (IBAction)backButtonTapped:(id)sender;

- (void)resetPasswordForUserWithEmail:(NSString*)emailId;

@end
