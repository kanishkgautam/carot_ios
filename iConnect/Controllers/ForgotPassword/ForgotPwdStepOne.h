//
//  ForgotPwdStepOne.h
//  iConnect
//
//  Created by Amit Priyadarshi on 29/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "MIBaseViewController.h"

@interface ForgotPwdStepOne : UIViewController{
    
    __weak IBOutlet UITextField *emailTextField;
    UITextField *txtFieldInValid;

}
- (IBAction)backButtonTapped:(id)sender;
- (IBAction)emailSubmitButtonTapped:(id)sender;


@end
