//
//  ForgotPwdStepTwo.m
//  iConnect
//
//  Created by Amit Priyadarshi on 29/04/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "ForgotPwdStepTwo.h"

@implementation ForgotPwdStepTwo

- (void)viewDidLoad {
    [super viewDidLoad];
    [[AppUtility sharedUtility] trackScreenNameWith:@"Forgot Password Second Screen"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if ([kAppUtility trimWhiteSpacesInString:_emailAddressToBeFilled].length >0) {
        emailTextField.text = [kAppUtility trimWhiteSpacesInString:_emailAddressToBeFilled];
    }
}


- (IBAction)submitButtonTapped:(id)sender {
    
    txtFieldInValid = nil;

    if ([[kAppUtility trimWhiteSpacesInString:emailTextField.text] length] < 1) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"User Id is Required", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = emailTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidEmail" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if (![[AppUtility sharedUtility] isValidEmailWithEmailToBeValidated:emailTextField.text]) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Please enter a valid email address", nil)];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = emailTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidEmail" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:otpTextField.text].length < 6) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Please enter valid OTP Code", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kOTPLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = otpTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidOTP" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:passwordTextField.text].length < 6) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Password should contain minimum of 6 digits", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = [UIColor redColor];
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidPassword" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];

        return;
    }
    
    if ([[AppUtility sharedUtility] trimWhiteSpacesInString:confirmPasswordTextField.text].length < 1) {
        
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Confirm Password cannot be left blank", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kConfirmPasswordLabelTag].backgroundColor = [UIColor redColor];
        txtFieldInValid = confirmPasswordTextField;
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidConfirmPassword" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
        return;
    }

    
    if(![passwordTextField.text isEqualToString:confirmPasswordTextField.text])
    {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Password and confirm password should be same", @"")];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kConfirmPasswordLabelTag].backgroundColor = [UIColor redColor];
        [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = [UIColor redColor];
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"InvalidConfirmPassword" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    
    [[AppUtility sharedUtility] sendGAEventWithEventName:@"ForgotPasswordOTPSubmitTapped" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
    [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];
   // [FTIndicator showInfoWithMessage:NSLocalizedString(@"Please wait...", nil)];
    [FTIndicator showProgressWithMessage:NSLocalizedString(@"Please wait...", nil)];
    [SICUserRequest resetPasswordForEmail:[kAppUtility trimWhiteSpacesInString:emailTextField.text]
                                 password:[kAppUtility trimWhiteSpacesInString:passwordTextField.text]
                                      otp:[kAppUtility trimWhiteSpacesInString:otpTextField.text]
                                 response:^(id responseObject, BOOL requestSuccess, NSError *error) {
                                     
                                     if (requestSuccess && !error) {
                                         [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];

                                         [FTIndicator showInfoWithMessage:NSLocalizedString(@"Password changed successfully", nil)];
                                         [self.navigationController popToRootViewControllerAnimated:YES];
                                         
                                     }else if (error){
                                         [FTIndicator dismissProgress];
                                         if (responseObject) {
                                             [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];

                                             [FTIndicator showErrorWithMessage:NSLocalizedString([kAppUtility getErrorDescriptionFromDictionary:responseObject], nil)];
                                         }else{
                                             [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];

                                             [FTIndicator showErrorWithMessage:NSLocalizedString(error.localizedDescription, nil)];
                                         }
                                     }
                                     //[FTIndicator dismissProgress];
                                 }];
    
}

- (IBAction)resendButtonTapped:(id)sender {
    
    if (![[AppUtility sharedUtility] isValidEmailWithEmailToBeValidated:emailTextField.text]) {
        [[MICSnackbar sharedSnackBar] showWithTextWithStringToBeDisplayed:NSLocalizedString(@"Please enter a valid email address.", nil)];
        [[AppUtility sharedUtility] sendGAEventWithEventName:@"ResendInvalidEmail" category:kEventCategoryLogin andScreenName:@"Forgot Password Second Screen" eventLabel:NULL eventValue:NULL];
        return;
    }
    [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];
    [FTProgressIndicator showProgressWithMessage:NSLocalizedString(@"Resending OTP, Please check your email for the OTP.", nil)];
    
    [SICUserRequest sendOtpForEmail:emailTextField.text
                         forPurpose:COTPTypeForgotPassword
                           response:^(id responseObject, BOOL requestSuccess, NSError *error) {
                               
                               if (requestSuccess && !error) {
                                   [FTProgressIndicator showInfoWithMessage:NSLocalizedString(@"OTP sent, Please check your email for the OTP.", nil)];
                               }else if (error){
                                   [FTIndicator dismissProgress];
                                   if (responseObject) {
                                       [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];

                                       [FTIndicator showErrorWithMessage:NSLocalizedString([kAppUtility getErrorDescriptionFromDictionary:responseObject], nil)];
                                   }else{
                                       [FTIndicator setIndicatorStyle:UIBlurEffectStyleDark];

                                       [FTIndicator showErrorWithMessage:NSLocalizedString(error.localizedDescription, nil)];
                                   }
                               }
                               
                           }];
    
    
}

- (IBAction)backButtonTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resetPasswordForUserWithEmail:(NSString*)emailId {
    emailTextField.text = emailId;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(txtFieldInValid != nil){
        if(string.length >= 1){
            [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kemailIDLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
    [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kOTPLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;

    [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kPasswordLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;
    [[self.view viewWithTag:kBaseViewofLabels] viewWithTag:kConfirmPasswordLabelTag].backgroundColor = kTextUnderlineGrayColorObjectivceC;

    }
    }
    return YES;

}

@end
