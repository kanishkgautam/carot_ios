//
//  CDriveScore.h
//  Carot
//
//  Created by Vivek Joshi on 23/03/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDriveScore : NSObject

@property (nonatomic, retain) NSNumber *pScore;
@property (nonatomic, retain) NSNumber * pTripScore;
@property (nonatomic, retain) NSNumber * pFuelScore;
@property (nonatomic, retain) NSNumber * pSafetyScore;
@property (nonatomic, retain) NSNumber * distance;
@property (nonatomic, retain) NSNumber *runTime;
@property (nonatomic, retain) NSNumber * idleTime;
@property (nonatomic, retain) NSNumber * batchUpdated;
@property (nonatomic, retain) NSNumber *day;
@property (nonatomic, retain) NSNumber * week;
@property (nonatomic, retain) NSNumber * month;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSNumber * createdOn;
@property (nonatomic, copy) NSNumber * scoreID;


@property(nonatomic, assign) bool isScorable;


- (void)setDataFromDictionary:(NSDictionary*)dict forType:(NSString*)type;
- (void)setDataFromDictionaryv2:(NSDictionary*)dict forType:(NSString*)type;
@end
