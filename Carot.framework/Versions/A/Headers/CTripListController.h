//
//  CTripListController.h
//  Carot
//
//  Created by Amit Priyadarshi on 24/11/15.
//  Copyright © 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Carot/Carot.h>


@class CTripListController;
@protocol CTripListControllerDelegate <NSObject>
@required
- (void)configureCellAtIndexPath:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath withInfo:(CTrip*)info;
@end

@interface CTripListController : NSObject
@property(nonatomic, weak) UITableView* tableView;
@property(nonatomic, weak) id<CTripListControllerDelegate> delegate;
@property(nonatomic) CGSize defaultImageSize;
@property(nonatomic) BOOL tableIsMovingFast;
- (id)initWithTableView:(UITableView*)table;
- (NSUInteger)numberOfRowsInTableForSection:(NSUInteger)section;
- (CTrip*)tripInfoForTripAtIndexPath:(NSIndexPath*)indexPath;
- (void)loadImageForTripAtIndex:(NSIndexPath*)indexPath;
@end
