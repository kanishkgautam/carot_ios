//
//  SharedClass.h
//  Carot
//
//  Created by Amit Priyadarshi on 01/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define SIV [SharedClass sharedInstance]
#define MOC [SIV managedObjectContext]
#define TrimmedText(text) [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]
#define ErrorDescrption(key) NSLocalizedString(key, nil)

#define NonNullObject(dictionary, key) (![dictionary objectForKey:key]||[[dictionary objectForKey:key] isKindOfClass:[NSNull class]])?@"":[dictionary objectForKey:key]
#define NullObjectReplace(dictionary, key, replaceBy) (![dictionary objectForKey:key]||[[dictionary objectForKey:key] isKindOfClass:[NSNull class]])?replaceBy:[dictionary objectForKey:key]

#define rData NullObjectReplace(responseDict, @"data", @{})

//#define SDKClientHonda          101
//#define SDKClientMinda          102
//#define SDKClient kClient

extern NSString * const CUserAuthenticationException;
extern NSString * const CInvalidRequestException;
extern NSString * const CInputValueRequiredException;
extern NSString * const CVehicleSubscriptionExpiredException;
extern NSString * const CNoVehicleSelectedException;
extern NSString * const CVehicleSubscriptionPendingException;

extern NSString * const kSDKErrorDomain;

extern NSString * const CFieldCanNotBeBlankError;
extern NSString * const CFieldCanNotBeNullError;
extern NSString * const CNotValidFieldEntryError;
extern NSString * const CDuplicateEntryError;
extern NSString * const CInvalidRequestError;
extern NSString * const CNoNetworkConnectionError;
extern NSString * const CJSONParsingError;


extern NSString * const kErrorCause;
extern NSString * const kErrorTitle;
extern NSString * const kErrorDescription;
extern NSString * const kErrorField;
extern NSString * const kErrorUnknown;

extern NSString * const kVehicleFuelTypePetrol;
extern NSString * const kVehicleFuelTypeDiesel;
extern NSString * const kVehicleFuelTypeOther;

extern NSString * const kVehicleGearTypeAutomatic;
extern NSString * const kVehicleGearTypeManual;


extern NSString * const RequestEventTypeLogin;
extern NSString * const RequestEventTypeSignup;
extern NSString * const RequestEventTypeSendOtp;
extern NSString * const RequestEventTypeResetPassword;
extern NSString * const RequestEventTypeChangePassword;
extern NSString * const RequestEventTypeEditProfile;
extern NSString * const RequestEventTypeUserDetail;
extern NSString * const RequestEventTypeLogout;
extern NSString * const RequestEventTypeVehicleRefrance;
extern NSString * const RequestEventTypeUserDevices;
extern NSString * const RequestEventTypeUserDevicesForVehicle;
extern NSString * const RequestEventTypeUserVehicles;
extern NSString * const RequestEventTypeAddNewDevice;
extern NSString * const RequestEventTypeUserSubscriptions;
extern NSString * const RequestEventTypeAddSubscription;
extern NSString * const RequestEventTypeCurrentState;
extern NSString * const RequestEventTypeHealthStatus;
extern NSString * const RequestEventTypeDtcDetail;
extern NSString * const RequestEventTypeBuyProduct;
extern NSString * const RequestEventTypeAddressForLocation;
extern NSString * const RequestEventTypeSelectedVehicle;
extern NSString * const RequestEventTypeTrips;
extern NSString * const RequestEventTypeAddressForTrip;
extern NSString * const RequestEventTypeAddressForEvent;

extern NSString * const RequestEventTypeTripDetails;
extern NSString * const RequestEventTypeLoadMoreTrips;
extern NSString * const RequestEventTypeTripImage;
extern NSString * const RequestEventTypeTripLocations;
extern NSString * const RequestEventTypeEventsCount;
extern NSString * const RequestEventTypeEvents;
extern NSString * const RequestEventTypeMoreEvents;
extern NSString * const RequestEventTypeMarkEventRead;
extern NSString * const RequestEventTypeEventDetails;
extern NSString * const RequestEventTypeEventPreference;
extern NSString * const RequestEventTypeUpdateEventPreference;
extern NSString * const RequestEventTypeEmergencyContacts;
extern NSString * const RequestEventTypeNewEmergencyContact;
extern NSString * const RequestEventTypeUpdateEmergencyContact;
extern NSString * const RequestEventTypeActiveSession;
extern NSString * const RequestEventTypeRevokeSession;
extern NSString * const RequestEventTypeCreateSeesion;
extern NSString * const RequestEventTypeMessageForSos;

extern NSString * const RequestEventTypeGetGeoFence;
extern NSString * const RequestEventTypeDeleteFence;
extern NSString * const RequestEventTypeActORDectFence;
extern NSString * const RequestEventTypeSaveFence;
extern NSString * const RequestEventTypeUpdateFence;

extern NSString * const RequestEventTypeSafetyAlarmGet;
extern NSString * const RequestEventTypeDeleteSafetyAlarm;
extern NSString * const RequestEventTypeSaveSafetyAlarm;
extern NSString * const RequestEventTypeUpdateSafetyAlarm;


typedef enum {
    TextFieldTypeEmail,
    TextFieldTypePassword,
    TextFieldTypePhonenumber,
    TextFieldTypeZIP,
    TextFieldTypeName,
}TextFieldType;

typedef NS_ENUM(NSUInteger, CarotSDKError){
    CarotSDKErrorSDKNotInitialisedProperly = 999,
    CarotSDKErrorNoVehicleSelected = 1000,
    CarotSDKErrorUserNotLoggedIn,
    CarotSDKErrorUserEmailNotAvailable,
    CarotSDKErrorUserPasswordNotAvailable,
    CarotSDKErrorPasswordFormatIncorrect,
    CarotSDKErrorNoDevicetoken,
    CarotSDKErrorUserFirstNameNotAvailable,
    CarotSDKErrorUserLastNameNotAvailable,
    CarotSDKErrorPhoneNumberNotAvailable,
    CarotSDKErrorEmailNotMatching,
    CarotSDKErrorInvalidAddress,
    CarotSDKErrorInvalidPhone,
    CarotSDKErrorInvalidCity,
    CarotSDKErrorInvalidState,
    CarotSDKErrorInvalidZipcode,
    CarotSDKErrorInvalidRequest,
    CarotSDKErrorFirstNameNotEditable,
    CarotSDKErrorLastNameNotEditable,
    CarotSDKErrorEmailNotEditable,
    CarotSDKErrorInvalidTripID,
    CarotSDKErrorIncorrectImageDimension,
    CarotSDKErrorInvalidEventID,
    CarotSDKErrorIncorrectContactPreference,
    CarotSDKErrorIncorrectContactID,
    CarotSDKErrorInvalidSessionID,
    CarotSDKErrorInvalidSessionDuration,
    CarotSDKErrorInvalidLocation,
    CarotSDKErrorSubscriptionPending,
    CarotSDKErrorVehicleSubscriptionExpired,
    CarotSDKErrorInvalidAccessToken,
    CarotSDKErrorInvalidDeviceId,
    CarotSDKErrorInavlidSerialLength,
    CarotSDKErrorInvalidNameLength,
    CarotSDKErrorDeviceSerialNull,
    CarotSDKErrorDeviceNameNull,
    CarotSDKErrorDTCNull,
    CarotSDKErrorInvalidVehiclID,
    CarotSDKImageUploadParsingError,
     CarotSDKErrorInvalidGeofenceID,
};


@interface SharedClass : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSOperationQueue* networkQueue;
@property (nonatomic) NSUInteger selectedVehicleId;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (NSURL *)applicationCachesDirectory;

+(SharedClass*)sharedInstance;

- (NSString*)md5Hash:(NSString*)input;
- (NSString *) base64StringFromData: (NSData *)data length: (int)length;
- (NSData *)base64DataFromString: (NSString *)string;
- (BOOL)validateText:(NSString*)text forFieldType:(TextFieldType)type;
- (NSError*)unknownErrorInDomail:(NSString*)domain;
- (NSString*)makeHashForId:(NSUInteger)someId;
- (NSError*)errorForErrorCode:(NSError *)errorObj;
- (void)logEventOfType:(NSString*)requestType;
- (void)logRequestSuccess:(BOOL)success forRequestType:(NSString*)requestType withUserInfo:(id)userInfo;
@end




