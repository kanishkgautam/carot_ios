//
//  CDocumentTypeDetails.h
//  Carot
//
//  Created by Aditya Srivastava on 18/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CVehicle;
@class CUserDocuments;

@interface CDocumentTypeDetails : NSObject




@property (nonatomic, retain) NSString *docType;
@property (nonatomic, retain) NSString *validFrom;
@property (nonatomic, retain) NSString *expiryDate;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic) int docId;
@property ( nonatomic, copy) NSString *localPath;
@property ( nonatomic, retain) CVehicle *vehicle;
@property ( nonatomic, retain) CUserDocuments *userDocs;

//@property (nonatomic, retain) NSString *vehicleId;
//@property (nonatomic, retain) NSString *vehicleName;
@end
