//
//  CVehicleReference.h
//  Carot
//
//  Created by Vaibhav Gautam on 10/03/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CVehicleMake.h"
#import "CFuelType.h"

@interface CVehicleReference : NSObject

@property(nonatomic, retain) NSArray<CVehicleMake *> *vehicleMakers;
@property(nonatomic, retain) NSArray<CFuelType *> *fuelType;

@end
