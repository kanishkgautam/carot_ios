//
//  CServiceHistory.h
//  Carot
//
//  Created by Aditya Srivastava on 18/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CVehicle;

@interface CServiceHistory : NSObject



//@property (nonatomic, retain) NSString *vehicleName;
//@property (nonatomic, retain) NSString *vehicleId;
@property (nonatomic, retain) NSString *odometer;
@property (nonatomic, retain) NSString *model;
@property (nonatomic, retain) NSString *message;
@property (nonatomic,retain)  NSString *dueDate;
@property (nonatomic,retain)  NSString *documentID;
@property (nonatomic, retain) CVehicle *vehicle;

@end
