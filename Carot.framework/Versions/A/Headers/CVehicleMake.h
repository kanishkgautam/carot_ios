//
//  CVehicleMake.h
//  Carot
//
//  Created by Amit Priyadarshi on 09/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVehicleMake : NSObject
@property(nonatomic, strong) NSString* makerName;
@property(nonatomic, strong) NSArray* models;
@end
