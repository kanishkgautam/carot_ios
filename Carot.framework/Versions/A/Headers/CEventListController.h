//
//  CEventListController.h
//  Carot
//
//  Created by Amit Priyadarshi on 20/01/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Carot/Carot.h>

@class CEventListController;
@protocol CEventListControllerDelegate <NSObject>
@required
- (void)configureCellAtIndexPath:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath withInfo:(CEvent*)info;
@end
@interface CEventListController : NSObject
@property(nonatomic, weak) UITableView* tableView;
@property(nonatomic, weak) id<CEventListControllerDelegate> delegate;
@property(nonatomic) BOOL tableIsMovingFast;

- (id)initWithTableView:(UITableView*)table;
- (NSUInteger)numberOfRowsInTableForSection:(NSUInteger)section;
- (CEvent*)eventInfoAtIndexPath:(NSIndexPath*)indexPath;

@end
