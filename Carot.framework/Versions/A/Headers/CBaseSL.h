//
//  CBaseSL.h
//  Carot
//
//  Created by Amit Priyadarshi on 02/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharedClass.h"


typedef enum {
    RequestTypeGet,
    RequestTypePost,
    RequestTypePut,
    RequestTypeDelete,
}RequestType;

typedef enum {
    TokenTypeBasic,
    TokenTypeBaring,
    None,
}TokenType;

typedef enum {
    ServiceEdit,
    ServiceDelete,
    ServiceAdd,
}DocumentServiceType;

@class CTrip;
@class CVehicleHealthStatus;
@class CEvent;

typedef void (^ServerResponse) (id response, BOOL isSuccessful, NSError *error);
typedef void (^ServerResponseInternal) (NSDictionary* responseDict, NSUInteger statusCode, NSError *error);
typedef void (^TokenResponseInternal) (NSString* urlStr, RequestType rType, TokenType tType, NSDictionary* param, ServerResponseInternal response);
typedef void (^OnTripRefresh) (NSArray* trips, NSError* error);
typedef void (^OnEventRefresh) (NSArray* trips, NSError* error);
typedef void (^OnProfileRefresh) (NSArray* trips, NSError* error);
typedef void (^OnTripAddressComplition) (CTrip* trip, id index);
typedef void (^OnTripScoreCompletion) (CTrip* trip);
typedef void (^OnHealthRefresh) (CVehicleHealthStatus* health, NSError* error);
typedef void (^TagHandler) (NSNumber *tag, NSArray* delta);
typedef void (^OnEventAddressComplition) (CEvent* event, id index);

@interface CBaseSL : NSObject {

    NSString* baseUrl;
    NSString* baseUrlv2;
    
}
@property(nonatomic, strong) NSString* accessToken;

- (NSString*)uniqueStringFromUrl:(NSString*)string andParameters:(NSDictionary*)param;
- (NSString*)urlParametersFromDictionary:(NSDictionary*)dict;
- (NSData*)httpBodyForParamsDictionary:(NSDictionary*)paramDictionary;
- (NSString*)percentEscapeString:(NSString*)string;
- (NSError*)processedErrorForServerResponse:(NSDictionary*)resp statusCode:(NSUInteger)statusCode;
- (void)httpRequestUrl:(NSString*)url
           requestType:(RequestType)rType
             tokenType:(TokenType)tType
            parameters:(NSDictionary*)param
              response:(ServerResponseInternal)response;
- (void)httpRequestUrl:(NSString*)urlStr requestType:(RequestType)rType tokenType:(TokenType)tType parameters:(NSDictionary*)param  synchronous:(BOOL)sync response:(ServerResponseInternal)response;
- (void)httpRequestUrlForMarkEventRead:(NSString*)urlStr requestType:(RequestType)rType tokenType:(TokenType)tType parameters:(NSArray*)param  response:(ServerResponseInternal)response;
@end
