//
//  CMailBoxRequest.h
//  Carot
//
//  Created by Aditya Srivastava on 18/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Carot/Carot.h>
#import "CBaseSL.h"
//#import "CIUserInboxItem.h"
@interface CMailBoxRequest : CBaseSL
+(CMailBoxRequest*)sharedInstance;
- (void)getCurrentActivePromotionsResponse:(ServerResponse)response;
- (void)getPromotionsForInboxOptions:(NSDictionary*)param Response:(ServerResponse)response;
- (void)markReadForPromotionWithId:(NSUInteger)pid response:(ServerResponse)response;
- (void)markDeleteForPromotionWithId:(NSUInteger)pid response:(ServerResponse)response;
- (void)getCountTotalInboxMessagesResponse:(ServerResponse)response;
//- (void)setPropertiesFromDictionary:(NSDictionary*)dict forUserInboxItem:(CIUserInboxItem*)inboxObj;
-(void)deleteAllItems;
@end
