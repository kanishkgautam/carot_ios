//
//  Config.h
//  Carot
//
//  Created by Vaibhav Gautam on 17/01/17.
//  Copyright © 2017 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Config : NSObject{
    
    NSString *clientPlatformKey;
    NSString *clientRawKey;
    NSString *clientHereMapsappID;
    NSString *clientHereMapsAppCode;
    NSString *clientHereMapsRestAppID;
    NSString *clientHereMapsRestAppCode;
    NSString *clientSourceKey;
}

+ (Config *)sharedInstance;
- (BOOL )initialiseSDKWithPlatformKey:(NSString *)platformKey clientRawKey:(NSString *)rawKey signUpSource:(NSString*)source clientHereMapsID:(NSString *)hereMapsAppID clientHereMapsAppCode:(NSString *)hereMapsAppCode clientHereMapsRestAppID:(NSString *)hereRestAppID clientHereMapsRestAppCode:(NSString *)hereRestAppCode;

- (NSString *)getplatformKey;
- (NSString *)getRawKey;
- (NSString *)getHereMapsAppID;
- (NSString *)getHereMapsAppCode;
- (NSString *)getHereMapsRestAppID;
- (NSString *)getHereMapsRestAppCode;
-   (NSString *)getSourceKey;
@end
