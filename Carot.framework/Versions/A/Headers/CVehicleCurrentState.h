//
//  CVehicleCurrentState.h
//  Carot
//
//  Created by Amit Priyadarshi on 29/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLocation.h"
#import "CTrip.h"

@interface CVehicleCurrentState : NSObject
@property(nonatomic, strong) NSDate* updateTime;
@property(nonatomic, strong) NSString* tripId;
@property(nonatomic, strong) NSNumber* vehicleId;
@property(nonatomic, strong) CLocation* location;
@property (nonatomic, retain) CTrip *trip;
@property (nonatomic, retain) NSString *deviceMode;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *uin;
@property(nonatomic, strong) CLocation* startLocation;
@property(nonatomic, strong) CLocation* endLocation;

//@property (nonatomic, retain) VehicleHealth *health;


- (void)setPropertiesFromDictionary:(NSDictionary*)dict;
- (void)setPropertiesFromDictionaryv2:(NSDictionary*)dict ;
@end
