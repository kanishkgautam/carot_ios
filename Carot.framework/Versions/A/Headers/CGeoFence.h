//
//  CGeoFence.h
//  Carot
//
//  Created by Amit Priyadarshi on 07/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CGeoFence : NSObject

@property (nonatomic) NSUInteger geoFenceId;
@property (nonatomic) BOOL active;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) NSInteger radius; // in meters
@property (nonatomic, retain) NSDate *creationTime;
@property (nonatomic) double destLat;
@property (nonatomic) double destLon;
//@property (nonatomic) NSUInteger *vehicleId;
@property (nonatomic, retain) NSString *address;


- (NSDictionary*) dictionaryValue;

@end
