//
//  CUserDocuments.h
//  Carot
//
//  Created by Aditya Srivastava on 17/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDocumentTypeDetails;

@interface CUserDocuments : NSObject

@property (nonatomic, retain) NSString *docType;
@property (nonatomic, retain) NSString *numberOfDocs;
@property (nonatomic) BOOL recentlyExpiring;
@property (nonatomic, retain) CDocumentTypeDetails *expiringDocDetail;
@end
