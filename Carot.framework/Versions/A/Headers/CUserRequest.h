//
//  CUserRequest.h
//  Carot
//
//  Created by Amit Priyadarshi on 02/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Carot/Carot.h>
#import "CUser.h"



#define SICUserRequest [CUserRequest sharedInstance]

typedef enum {
    COTPTypeForgotPassword,
    COTPTypeConfirmEmail,
}COTPType;

@interface CUserRequest : CBaseSL


+(CUserRequest*)sharedInstance;

//typedef void (^ServerResponse) (id responseObject, BOOL requestSuccess, NSError* error);


/**
 This method will be used for logging into the app and generating access token which will be used by SDK internally for managing user.

 @param userEmailAddress user email address
 @param password user password
 @param deviceToken unique device token
 @param response response block which will return three values(responseObject, isSuccessful and pointer to error object)
 */
- (void)loginUserWithId:(NSString*)userEmailAddress password:(NSString*)password deviceToken:(NSString*)deviceToken response:(ServerResponse)response;

/**
 This method will be used for sending device token and getting User history from server.
 
 @param token This is the device token used for Push Notifications
 @param response response block which will return three values(responseObject, isSuccessful and pointer to error object)
 */
-(void)getLoginHistoryDetailsWithToken:(NSString*)token response:(ServerResponse)response;


/**
 *  Call this method to create a new Carot user.
 *  @brief For signing up a new user into Carot.
 *  @param emailId  A valid email string of user. If user already exists on server an error is returned.
 *  @param fName    A valid string which only contains alphabets and is first name of user.
 *  @param lName    A valid string which only contains alphabets and is last name of user.
 *  @param pwd      A valid string which only contains any ANSI char and length is more than 5. Only pass plain text to method, MD5 is created inside SDK.
 *  @param response A code block of type ServerResponse which excepts three values (See result section). A valid object of type CUser on successfull request, a BOOL for request success/failure, an Error object on request failure.
 *  @result responseObject - A CUser Object / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)signupUserWithEmail:(NSString*)emailId firstName:(NSString*)fName lastName:(NSString*)lName password:(NSString*)pwd response:(ServerResponse)response;

- (void)signupUserWithEmail:(NSString*)emailId firstName:(NSString*)fName lastName:(NSString*)lName phoneNumber:(NSString*)number password:(NSString*)pwd response:(ServerResponse)response;


/**
 *  Call this method to confirm an email.
 *  @brief Once user register with any email, that mail needs to be confirmed. A 6 digit pin is sent to users email, which needs to be varified and is done on server.
 *
 *  @param email    Email to be confirmed
 *  @param otpType  Purpose of generating the OTP. There are two places where OTP is generated - once when user forgets the password and want to reset it, and second when you want to confirm mail.
 *  @param response A code block of type ServerResponse which excepts three values (See result section).
 *  @result responseObject - OTP Sent / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)sendOtpForEmail:(NSString*)email forPurpose:(COTPType)otpType response:(ServerResponse)response;


/**
 *  Call this method to complete the final step in reseting user password.
 *  @brief Use this method after generating OTP for user of type COTPTypeForgotPassword. This will final step in changing user password if he/she forgets password.
 *  @param email    Email for user
 *  @param password New password of user
 *  @param otp      OTP that was generated in first step and was sent over user email.
 *  @param response A code block of type ServerResponse which excepts three values (See result section).
 *  @result responseObject - Success / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)resetPasswordForEmail:(NSString*)email password:(NSString*)password otp:(NSString*)otp response:(ServerResponse)response;


/**
 *  Call this method to change the password of existing confirmed user. To change the password both old and new password need to be passed.
 *
 *  @param oldPwd   Old password in plain text.
 *  @param newPwd   New password in plain text.
 *  @param response A code block of type ServerResponse which excepts three values (See result section).
 *  @result responseObject - Password changed / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)changePasswordFromOldPassword:(NSString*)oldPwd toNewPassword:(NSString*)newPwd response:(ServerResponse)response;


/**
 *  Call this method to update user profile. Please not that first name, last name and email is not editable.
 *  @brief Use this method to update user profile.
 *  @param user     A CUser object, don't set nil to unavailable values. Set blank string if value is not available
 *  @param response A code block of type ServerResponse which excepts three values (See result section).
 *  @result responseObject - CUser object / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)editUserProfileWithUserInfo:(CUser*)user response:(ServerResponse)response;


/**
 *  Call this method to get details for current loggedin user.
 *  @brief Use this method to get details for current loggedin user.
 *  @param response A code block of type ServerResponse which excepts three values (See result section).
 *  @result responseObject - CUser object / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)getUserDetailResponse:(ServerResponse)response;


/**
 This method can be used for confirming the email address which has been provided by user at the time of sign up

 @param email email address of user which needs to be verified
 @param otp OTP entered by user
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Ignore this unless success flag return false
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)confirmEmail:(NSString*)email withOtp:(NSString*)otp response:(ServerResponse)response;


/**
 This method can be used to check if user is logged in to SDK or not

 @return boolean value which will tell if user is logged in or not.
 */
-(BOOL)isUserLoggedIn;

/**
 This method will return string which can be used in app for making error messages, prompts and texts more readable for the user.

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = key value pair in JSON
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
-(void)getUserReadableStrings:(ServerResponse)response;


/**
 This method will delete local database and entries and will logout user
 */
- (void)logoutUser;


/**
 This method can be used to add a new contact to emergency contacts. These emergency contacts will be the one whom the customer care will call in case of vehicle crash.

 @param contact contact object with name and number to be saved as emergency contact for a user.
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = CUserContact object in case user is
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)addNewEmergencyContact:(CUserContacts*)contact response:(ServerResponse)response;


/**
 This method will fetch all the emergency contacts from the server and will return them in array of type CUserContacts.

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = CUserContact objects in array.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 */
- (void)getAllEmergencyContactsResponse:(ServerResponse)response;
//- (void)updateContactWith:(NSDictionary*)param contactId:(NSNumber*)cid response:(ServerResponse)response;


/**
 This method can be used for updating a single emergency contact on server

 @param contact updated contact object
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = CUserContact object which is updated
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)updateEmergencyContact:(CUserContacts*)contact response:(ServerResponse)response;


//Get state-city for MyProfile
/**
 This method can be used for fetching mapping of different cities of india with state

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = JSON object with mapping of city and states
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getStateCityMapResponse:(ServerResponse)response;

/**
 This method can be used for fetching user readable error strings from server

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = JSON object with mapping error key with error description.
 isSuccessful = boolean parameter which will tell if the request was successful or not.
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getErrorStringMapResponse:(ServerResponse)response;

@end
