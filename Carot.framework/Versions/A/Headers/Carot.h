//
//  Carot.h
//  Carot
//
//  Created by Amit Priyadarshi on 01/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "Config.h"
//#import "User.h"
#import "CUser.h"
#import "CDevice.h"
#import "CVehicle.h"
#import "CTripBreak.h"
#import "CTrip.h"
#import "CEvent.h"
#import "CUserContacts.h"
#import "Location+CoreDataProperties.h"
#import "CVehicleHealthStatus.h"
#import "CLocation.h"
#import "CVehicleCurrentState.h"
#import "CVehicleReference.h"
#import "CFuelType.h"


#import "CVehicleMake.h"
#import "CVehicleModel.h"
#import "CUserInboxItem.h"
#import "CUnusedSubscription.h"
#import "CServiceHistory.h"
#import "CUserDocuments.h"
#import "CDocumentTypeDetails.h"



#import "CVehicleRequest.h"
#import "CUserRequest.h"
#import "CEventRequest.h"
#import "CTripRequest.h"
#import "CProfileRequest.h"
#import "CSessionRequest.h"
#import "CTripListController.h"
#import "CUtilityRequest.h"
#import "CMailBoxRequest.h"
#import "CDocumentWalletRequest.h"
#import "CVehicleGeofence.h"

#import "CDriveScore.h"
#import "CSafetyAlarm.h"
#import "CSafetyAlarmRequest.h"

@interface Carot : NSObject{
    
}

+ (Carot *)sharedInstance;

/**
 This method must be called with shared instance to make sure that SDK is properly initialised else every call will return error in the success callback

 @param platformKey iConnect platform key
 @param rawKey iConnect raw key
 @param hereMapsAppID Here maps App ID. if you don't have one, get it from https://developer.here.com/develop/mobile-sdks
 @param hereMapsAppCode Here maps App code, If you don't have one, get it from https://developer.here.com/develop/mobile-sdks
 @param restAppID This is the App ID which will be used for making rest calls to developer platform
 @param restAppCode This is the App code which will be used for making rest calls to developer platform
 
 @return true in case all the values are provided, else false
 */
- (BOOL )initialiseSDKWithPlatformKey:(NSString *)platformKey clientRawKey:(NSString *)rawKey signUpSource:(NSString*)source clientHereMapsID:(NSString *)hereMapsAppID clientHereMapsAppCode:(NSString *)hereMapsAppCode hereMapsRestAppID:(NSString *)restAppID hereMapsRestAppCode:(NSString *)restAppCode;

@end


//#import "CIInvalidArgumentException.h"
