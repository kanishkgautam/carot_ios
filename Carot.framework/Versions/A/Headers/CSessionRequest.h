//
//  CSessionRquest.h
//  Carot
//
//  Created by Amit Priyadarshi on 22/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "CBaseSL.h"
#import <Carot/Carot.h>
#import <CoreLocation/CoreLocation.h>

#define SICSessionRequest [CSessionRequest sharedInstance]

@interface CSessionRequest : CBaseSL
+(CSessionRequest*)sharedInstance;

- (void)getAllActiveSessionResponse:(ServerResponse)response;

- (void)revokeSessionWithSessionId:(NSString*)sid response:(ServerResponse)response;

- (void)createSessionForDuration:(int)minutes response:(ServerResponse)response;

- (void)getSOSMessageForLocation:(CLLocation *)locationObj andServerResponse:(ServerResponse )response;

@end
