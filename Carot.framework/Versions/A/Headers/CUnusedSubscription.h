//
//  CUnusedSubscription.h
//  Carot
//
//  Created by Aditya Srivastava on 21/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDevice.h"

@interface CUnusedSubscription : NSObject
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSNumber * itemId;
@property (nonatomic, retain) NSNumber * validityInDays;
@property (nonatomic, retain) CDevice * selectedDevice;

@end
