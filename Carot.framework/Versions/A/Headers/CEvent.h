//
//  CEvent.h
//  Carot
//
//  Created by Amit Priyadarshi on 24/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location+CoreDataProperties.h"
#import "CVehicle.h"

@interface CEvent : NSObject

@property (nonatomic, retain) NSNumber * currentValue;
@property (nonatomic, retain) NSString * dtcCode;
@property (nonatomic, retain) NSString * eventDescription;
@property (nonatomic, retain) NSString * eventId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic        ) BOOL       read;
@property (nonatomic, retain) NSNumber * showCurrentAndThresholdValue;
@property (nonatomic, retain) NSNumber * thresholdValue;
@property (nonatomic, retain) NSDate   * timeStamp;
@property (nonatomic, retain) NSString * vehicleAlarmType;
@property (nonatomic, retain) NSString * vehicleEventType;
@property (nonatomic, retain) NSString * vehicleHealthId;
@property (nonatomic, retain) NSString * vehicleLocationId;
@property (nonatomic, retain) NSString * vehicleTripId;
@property (nonatomic, retain) NSString * vehicleTripStatusId;
@property (nonatomic, retain) Location * onLocation;
@property (nonatomic, retain) CITripStaticData *forTripStaticData;
@property (nonatomic, retain) CVehicle *forVehicle;
@property (nonatomic, retain) NSString * eventUnit;
//@property (nonatomic,retain) NSString *thresholdUnit;

@property (nonatomic, retain) NSNumber * eventSeverity;
//@property (nonatomic, retain) NSNumber * isForHealthStatus;
@property (nonatomic, retain) NSNumber * vehicleId;

- (void)setPropertiesFromDictionary:(NSDictionary*)dict;
- (void)setPropertiesFromDictionaryv2:(NSDictionary*)dict ;
@end
