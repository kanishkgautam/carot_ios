//
//  CUserInboxItem.h
//  Carot
//
//  Created by Aditya Srivastava on 21/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CUserInboxItem : NSObject
@property (nonatomic, retain) NSDate *expiryDate;
@property (nonatomic, retain) NSNumber *isRead;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSNumber *messageId;
@property (nonatomic, retain) NSNumber *showBuyButton;
@property (nonatomic, retain) NSString *title;
@end
