//
//  CSafetyAlarm.h
//  Carot
//
//  Created by Vivek Joshi on 22/12/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSafetyAlarm : NSObject

@property (nonatomic) BOOL isActiveOnSunday;
@property (nonatomic) BOOL isActiveOnMonday;
@property (nonatomic) BOOL isActiveOnTuesday;
@property (nonatomic) BOOL isActiveOnThursday;
@property (nonatomic) BOOL isActiveOnWednesday;
@property (nonatomic) BOOL isActiveOnFriday;
@property (nonatomic) BOOL isActiveOnSaturday;

@property(nonatomic, retain) NSNumber *vehicleId;
@property(nonatomic, retain) NSNumber *endTime;
@property(nonatomic, retain) NSNumber *startTime;
@property(nonatomic, retain) NSNumber *alarmId;

@property (nonatomic) BOOL isEnabled;
@property (nullable, nonatomic, copy) NSString *alarmRepeatType;

@end
