//
//  CTripBreak.h
//  Carot
//
//  Created by Amit Priyadarshi on 24/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location+CoreDataProperties.h"

@class CTrip;
@interface CTripBreak : NSObject

@property (nonatomic, retain) NSNumber * breakTime;
@property (nonatomic, retain) NSDate   * endedAt;
@property (nonatomic, retain) NSDate   * startedAt;
@property (nonatomic, retain) Location * location;
@property (nonatomic, retain) CTrip *forTrip;

@end
