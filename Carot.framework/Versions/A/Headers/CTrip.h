//
//  CTrip.h
//  Carot
//
//  Created by Amit Priyadarshi on 24/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Carot/Carot.h>
#import "CDriveScore.h"

@class CVehicle;

@interface CTrip : NSObject

@property (nonatomic, retain) NSNumber * averageSpeed;
@property (nonatomic, retain) NSDate   * endedOn;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate   * startedOn;
@property (nonatomic, retain) NSNumber * totalBreakTime;
@property (nonatomic, retain) NSNumber * totalIdleTime;
@property (nonatomic, retain) NSNumber * totalMileage;
@property (nonatomic, retain) NSString * tripId;
@property (nonatomic, retain) Location * endLocation;
@property (nonatomic, retain) Location * startLocation;
@property (nonatomic, retain) NSString * imagePath;

@property (nonatomic, retain) NSNumber  * updatedOn;
@property (nonatomic, retain) NSNumber  * vehicleId;
@property (nonatomic, retain) CVehicle * forVehicle;
@property (nonatomic, retain) NSArray     * locations;
@property (nonatomic, retain) NSArray     * tripBreaks;
@property (nonatomic, retain) NSNumber *tripScore;
@property (nonatomic, retain) CDriveScore  * driveScore;
@property (nonatomic, retain) NSString * startLocationStr;
@property (nonatomic, retain) NSString * endLocationStr;
//@property (nonatomic, retain) NSArray  * tripBreaks;
//@property (nonatomic, retain) NSArray  * locations;

@end
