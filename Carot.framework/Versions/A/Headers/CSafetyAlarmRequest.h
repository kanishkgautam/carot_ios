//
//  CSafetyAlarmRequest.h
//  Carot
//
//  Created by Vivek Joshi on 22/12/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBaseSL.h"

@class CSafetyAlarm;

@interface CSafetyAlarmRequest : CBaseSL

+(CSafetyAlarmRequest*)sharedInstance;

- (void)getAllSafetyAlarmsForVehicleWithId:(int)vehicleId response:(ServerResponse)response;
- (void)addSafetyAlarmForSelectedVehicle:(CSafetyAlarm*)alarm reponse:(ServerResponse)response;
- (void)deleteSafetyAlarmForSelectedVehicle:(CSafetyAlarm*)alarm reponse:(ServerResponse)response;
- (void)updateSafetyAlarmForSelectedVehicle:(CSafetyAlarm*)alarm reponse:(ServerResponse)response;

@end
