//
//  CUtilityRequest.h
//  Carot
//
//  Created by Administrator on 11/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBaseSL.h"
#import <CoreLocation/CoreLocation.h>
#import <Carot/Carot.h>
@interface CUtilityRequest : CBaseSL
+(CUtilityRequest*)sharedInstance;
-(void)getNearByFuelPumps:(CLLocation*)location response:(ServerResponse)response;
-(void)getNearByServiceStations:(CLLocation *)location response:(ServerResponse)response;
@end

