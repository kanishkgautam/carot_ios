//
//  CVehicleGeofence.h
//  Carot
//
//  Created by Administrator on 1/16/17.
//  Copyright © 2017 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBaseSL.h"
#import <Carot/Carot.h>
#import <CoreLocation/CoreLocation.h>
#import "CGeoFence.h"

@interface CVehicleGeofence : CBaseSL

+(CVehicleGeofence*)sharedInstance;

/**
 This method will return all the geofence for a particular user.

 @param response : This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server
 
 response = list of all the geofences for a particular vehicle.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 */
- (void) getAllFence:(ServerResponse)response;



/**
 This method will delete a specific geofence from the server as well as locally stored databse

 @param geofenceid Integer geofence ID which has to be deleted
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server
 
 For this API only the success flag needs to be checked to make sure that fence is deleted successfully, if success flag is false then check error object for error details and response object for more information(if available).
 */
- (void) deleteFenceWithID:(NSUInteger)geofenceid
                  response:(ServerResponse)response;


/**
 This method can be used to save a geofence to server so that it can be fetched later or server can raise alerts for fence breach or entry

 @param fence This is the object ot type CGeoFence which will have all the details of any particular geofence.
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server

 response = This object will have fence details of the newly created fence.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void) saveFence:(CGeoFence*) fence
          response:(ServerResponse)response;


/**
 This method can be used to activate a deactivated geofence.

 @param geofenceid Integer based geofence ID which has to be activated
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server
 
 response = Will be empty, check only the status of success flag.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void) activateFenceWithID:(NSUInteger)geofenceid
                    response:(ServerResponse)response;


/**
 This method can be used for updating an existing fence except its activation status. This method can update location, radius and name of the geofence.

 @param fence Geofence object which contains details of the fence to be edited
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server
 
 response = geofence object which is updated.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void) updateFence:(CGeoFence*) fence
            response:(ServerResponse)response;


/**
 This method can be used for searching lattitude and longitude of a particular location on the basis of a search text

 @param location this parameter will have longitude and latitude of the searched location.
 @param searchText This is the name of place for which user want the location object.
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server
 
 response = Array of suggested locations.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
-(void)getPlaceSuggestionFromLocation:(CLLocation*)location
                           searchText:(NSString*)searchText
                             response:(ServerResponse)response;


- (void) createFenceWithLat:(double) destLat withLong:(double)destLong withRadius:(double)radius withName:(NSString*)aName response:(ServerResponse)response;
@end
