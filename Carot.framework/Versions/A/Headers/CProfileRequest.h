//
//  CProfileRequest.h
//  Carot
//
//  Created by Amit Priyadarshi on 22/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "CBaseSL.h"
#import <Carot/Carot.h>
#define SICProfileRequest [CProfileRequest sharedInstance]

@interface CProfileRequest : CBaseSL
+(CProfileRequest*)sharedInstance;

- (void)getAllEmergencyContactsResponse:(ServerResponse)response;
- (void)getAllEmergencyContactsResponse:(ServerResponse)response onRefresh:(OnProfileRefresh)refreshResponse;

- (void)addNewEmergencyContactWithFirstName:(NSString*)fName
                                   lastName:(NSString*)lName
                                phoneNumber:(NSString*)pNumber
                                 preference:(int)preference
                                   response:(ServerResponse)response;

- (void)updateContactWithId:(NSNumber*)cid
              withFirstName:(NSString*)fName
                   lastName:(NSString*)lName
                phoneNumber:(NSString*)pNumber
                   response:(ServerResponse)response;

@end
