//
//  CUserContacts.h
//  Carot
//
//  Created by Amit Priyadarshi on 24/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CUserContacts : NSObject
@property (nonatomic, retain) NSNumber * contactId;
@property (nonatomic, retain) NSString * contactNumber;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * preference;


- (void)setPropertiesFromDictionary:(NSDictionary*)dict;

+ (CUserContacts*)userContactWithPreferenceId:(int)pid;
+ (CUserContacts*)userContactWithContactId:(NSNumber*)cid;
+ (CUserContacts*)newSafeCotactObjectWithPreferenceId:(int)pid isNew:(BOOL*)isNew;
+ (NSArray*)allEmergencyContacts;


@end
