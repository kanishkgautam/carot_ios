//
//  CDevice.h
//  Carot
//
//  Created by Amit Priyadarshi on 09/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDevice : NSObject

@property (nonatomic, retain) NSString   * makeYear;
@property (nonatomic, retain) NSString   * name;
@property (nonatomic, retain) NSString   * serialNumber;
@property (nonatomic, retain) NSString   * status;
@property (nonatomic, retain) NSString   * vehicleFuelType;
@property (nonatomic, retain) NSString   * vehicleGearType;
@property (nonatomic        ) NSUInteger deviceId;
@property (nonatomic        ) BOOL       onlyDefaultSubscription;
@property (nonatomic, retain) NSNumber   * vehicleModelId;

@property (nonatomic, retain) NSNumber *registrationDate;
@property (nonatomic, retain) NSString *vinID;
@property (nullable, nonatomic, copy) NSString *vinLinkageStatus;
@property (nonatomic, retain) NSString   * deviceMode;
@end
