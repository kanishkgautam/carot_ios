//
//  CDocumentWalletRequest.h
//  Carot
//
//  Created by Aditya Srivastava on 16/11/16.
//  Copyright © 2016 Amit Priyadarshi. All rights reserved.
//

#import <Carot/Carot.h>
#import "CBaseSL.h"
@interface CDocumentWalletRequest : CBaseSL

+(CDocumentWalletRequest*)sharedInstance;

- (void)getUserDocumentsforVehicleWithId:(int)vid response:(ServerResponse)response;
- (void)uploadDataforVehicleWithId:(int)vid file:(NSData*)fileData fileName:(NSString*)fileName validFrom:(NSString*)validFrom validTill:(NSString*)validTill docType:(NSString*)docType responseData:(ServerResponse)responseData;
-(void)getDocumentDetailForDocType:(NSString*)docType forVehicleId:(int)vehicleId response:(ServerResponse)response;
- (void)deleteDocumentForDocumentId:(int)documentId fileName:(NSString*)fileName response:(ServerResponse)response;
-(void)downloadDocumentImageForVehicleId:(int)vid docType:(NSString*)docType fileName:(NSString*)fileName responseData:(ServerResponse)responseData;
- (void)transferDocumentToVehicleFromOldVehicleId:(int)oldVid docType:(NSString*)docType fileName:(NSString*)fileName newVehicleId:(int)vid response:(ServerResponse)response;


//service
-(void)uploadServiceInfoForVehicleId:(int)vid docType:(NSString*)docType model:(NSString*)modelName odometerReading:(NSString*)reading dueOn:(NSString*)dueDate edit:(int)editOrDelete documentID:(NSString*)docId carName:(NSString*)carname response:(ServerResponse)response;
- (void)getDueServicesForVehicleWithId:(int)vid response:(ServerResponse)response;
- (void)getPastServicesForVehicleWithId:(int)vid completeResponse:(ServerResponse)completeResponse;

- (void)updateDataforVehicleWithId:(int)vid docId:(NSString*)docId fileName:(NSString*)fileName validFrom:(NSString*)validFrom validTill:(NSString*)validTill docType:(NSString*)docType responseData:(ServerResponse)responseData;

@end
