//
//  Location+CoreDataProperties.h
//  Carot
//
//  Created by Vaibhav Gautam on 08/03/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import "Location+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Location (CoreDataProperties)

+ (NSFetchRequest<Location *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *direction;
@property (nullable, nonatomic, copy) NSString *geoCode;
@property (nullable, nonatomic, copy) NSNumber *latitude;
@property (nullable, nonatomic, copy) NSNumber *longitude;
@property (nullable, nonatomic, copy) NSNumber *rpm;
@property (nullable, nonatomic, copy) NSNumber *speed;
@property (nullable, nonatomic, copy) NSNumber *timeStamp;
@property (nullable, nonatomic, retain) CIEvent *forEvent;
@property (nullable, nonatomic, retain) CITripBreak *forTripBreak;
@property (nullable, nonatomic, retain) CITrip *forTripEndLocation;
@property (nullable, nonatomic, retain) CITrip *forTripLocation;
@property (nullable, nonatomic, retain) CITripStaticData *forTripStaticData;
@property (nullable, nonatomic, retain) CITrip *fotTripStartLocation;

@end

NS_ASSUME_NONNULL_END
