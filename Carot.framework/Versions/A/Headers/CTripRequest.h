//
//  CTripRequest.h
//  Carot
//
//  Created by Amit Priyadarshi on 21/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "CBaseSL.h"
#import <Carot/Carot.h>
#import <UIKit/UIKit.h>
#import "CTrip.h"

@class CTrip;
#define SICTripRequest [CTripRequest sharedInstance]

typedef NS_ENUM(NSInteger, TimeGrouping){
    TimeGroupingLastSevenDays,
    TimeGroupingDaily,
    TimeGroupingWeekly,
    TimeGroupingMonthly,
    TimeGroupingYearly
};


@interface CTripRequest : CBaseSL
+(CTripRequest*)sharedInstance;

//- (void)getTrips:(int)count response:(ServerResponse)response;

/**
 This method can be used to get a number of trips from server with the count specified as parameter.

 @param count Number of trips which we want to fetch, this cannot be less than 5, if it is less than 5 even then SDK will return minimum 5 trips if available
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). This object is from local database
 
 response = This is the array of CTrip objects
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.

 @param onRefresh Same as response parameter but is guaranteed to be from server.
 */
- (void)getTrips:(int)count response:(ServerResponse)response onRefresh:(OnTripRefresh)onRefresh;

- (void)resolveAddressForTrip:(CTrip*)trip atIndex:(id)index onComplete:(OnTripAddressComplition)onResult;


/**
 This method can be used for getting all the details for a specific trip such as all the latitudes, logitudes, events for a spcific trip.

 @param tid This is the trip id in String which we have to use for fetching details of a specific trip
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). Note that this object will always be fetched from server.
 
 response = object of type CTripStaticData which contains all the details of this trip.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.

 */
- (void)getTripDetailsForTripWithId:(NSString*)tid response:(ServerResponse)response;


/**
 This method can be used for fetching array of paginated data after a spcific trip

 @param tripId trip id in string after which we want to fetch data
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). This object is from local database
 
 response = This is the array of CTrip objects
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 @param onRefresh Same as response parameter but is guaranteed to be from server.
 
 */
- (void)loadMoreAfterTripWithId:(NSString*)tripId response:(ServerResponse)response onRefresh:(OnTripRefresh)onRefresh;



/**
 This method can be used to generating a trip snapshot in a simple bitmap.

 @param tid trip id in string
 @param size desired size of the image
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). Note that this object will always be fetched from server.
 
 response = path of generated trip snapshot.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getTripStaticImageForTripWithId:(NSString*)tid size:(CGSize)size response:(ServerResponse)response;


/**
 This method will only return list of locations for a particular trip, This method can be used for generating interactive trip previews.

 @param tid trip id in string for which locations are needed
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). Note that this object will always be fetched from server.
 
 response = array of locations
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getTripLocationForTripWithId:(NSString*)tid response:(ServerResponse)response;


/**
 This method can be used for fetching all the locations of a currently running trip.

 @param tid trip id in string for which locations are needed
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). Note that this object will always be fetched from server.
 
 response = array of locations
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 */
- (void)getTripLocationsForLiveTrackingOfTripId:(NSString*)tid response:(ServerResponse)response;


/**
 This method will remove all the trips from local database
 */
-(void)deleteAllTrips;


/**
 A trip can have multiple evets and breaks, this method can be used to merge all of them.

 @param locations array of locations
 @param events array of events
 @param breaks array of break points
 @return array which consist of all the events which has happened during a trip.
 */
-(NSArray*) mergeEventsBreaksAndLocationsforTripData:(NSArray *)locations eventsData:(NSArray*)events breaksData:(NSArray*)breaks;


/**
 This method can be used to filter trip for a specific period of time

 @param fromInterval epoch timestamp from which trips are needed
 @param toInterval epoch timestamp to which trips are needed
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error). This object is from local database
 
 response = This is the array of CTrip objects
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.

 @param onRefresh Same as response parameter but is guaranteed to be from server.
 */
- (void)filterTripsFrom:(NSTimeInterval)fromInterval toInterval:(NSTimeInterval)toInterval response:(ServerResponse)response onRefresh:(OnTripRefresh)onRefresh;


/**
 This method can be used or fetching driving score for a specific vehicle within a specific time range on the basis of a grouping type

 @param vehicleId vehicle id for which driving score is needed
 @param selectedGrouping it can be daily, weekly, monthly or of last 7 days only
 @param startDate epoch timestamp from which driving score is needed
 @param endDate epoch timestamp to which driving score is needed
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error).
 
 response = array of driving scores in array of type CDriveScore
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getTripScoreForVehicle:(int)vehicleId selectedGroupingType:(TimeGrouping )selectedGrouping start:(long long)startDate end:(long long)endDate response:(ServerResponse)response;


/**
 This object will return the overall trip score for a vehicle from the begining of time

 @param vehicle vehicle object for which trip score is needed
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error).
 
 response = A single object hafing driving score for a specific vehicle
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
-(void)getOverAllTripScoreForVehicle:(CVehicle*)vehicle response:(ServerResponse)response;


/**
This object will return the overall trip score for a vehicle from the begining of time

@param uin string coming in v2?vs api
@param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error).

response = A single object hafing driving score for a specific vehicle
isSuccessful = boolean parameter which will tell if the request was successful or not
error = error object which will be having error description if success flag is false.

*/
-(void)getOverAllTripScoreForVehiclev2:(NSString*)uin response:(ServerResponse)response;

/**
 This methid can be used for fetching driving score for a particular trip from the server.

 @param trip trip for which driving score is needed
 @param completion This will return trip object(CTrip) only but with trip score added to it.
 */
- (void)getTripScoreForTrip:(CTrip *)trip withCallback:(OnTripScoreCompletion )completion;
@end
