//
//  CLocation.h
//  Carot
//
//  Created by Amit Priyadarshi on 29/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CEvent, CTrip, CTripBreak, CTripStaticData;

@interface CLocation : NSObject
@property (nonatomic,       ) float     direction;
@property (nonatomic, retain) NSString  * geoCode;
@property (nonatomic        ) double    latitude;
@property (nonatomic        ) double    longitude;
@property (nonatomic        ) float     rpm;
@property (nonatomic        ) float     speed;
@property (nonatomic, retain) NSDate *  updateTime;
@property (nonatomic,        ) double   timeStamp;

@property (nonatomic, retain) CTrip           * forTripLocation;
@property (nonatomic, retain) CTrip           * forTripEndLocation;
@property (nonatomic, retain) CTrip           * fotTripStartLocation;
@property (nonatomic, retain) CEvent          * forEvent;
@property (nonatomic, retain) CTripBreak      * forTripBreak;
@property (nonatomic, retain) CTripStaticData * forTripStaticData;

@end
