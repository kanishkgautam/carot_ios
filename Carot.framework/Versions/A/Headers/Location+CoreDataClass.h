//
//  Location+CoreDataClass.h
//  Carot
//
//  Created by Vaibhav Gautam on 08/03/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CIEvent, CITrip, CITripBreak, CITripStaticData, Location,CLocation;

NS_ASSUME_NONNULL_BEGIN

typedef void (^GeoCoderResponse) (NSString *geoCode, Location *forLoc);

@interface Location : NSManagedObject

+(Location*)newLocationObjectFromDictionary:(NSDictionary*)dict;
- (void)setPropertiesFromDictionary:(NSDictionary*)dict;
- (void)findAndSetGeoCodeOnComplition:(GeoCoderResponse)response;
- (NSDictionary*)userModel;
- (NSString*)findAndSetGeoCode;
-(CLocation*)mappingToUserModel;

@end

NS_ASSUME_NONNULL_END

#import "Location+CoreDataProperties.h"
