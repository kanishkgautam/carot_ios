//
//  CVehicleHealthStatus.h
//  Carot
//
//  Created by Amit Priyadarshi on 28/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
//@class CVehicle;

@interface CVehicleHealthStatus : NSObject
@property (nonatomic, retain) NSDictionary   * batteryEvent;
@property (nonatomic, retain) NSNumber   * coolantTemperature;
@property (nonatomic, retain) NSNumber   * coolantTemperatureMax;
@property (nonatomic        ) int   crashCount;
@property (nonatomic        ) int   towCount;
@property (nonatomic        ) BOOL         recentCrashEventFound;
@property (nonatomic        ) BOOL         recentTowEventFound;
@property (nonatomic, retain) NSArray    * dtcEvents;
//@property (nonatomic, assign) CVehicle *forVehicle;
@property (nonatomic, retain) NSNumber * obdSupported;

+ (CVehicleHealthStatus*)newObjectFromDictionary:(NSDictionary*)dict;
@end
