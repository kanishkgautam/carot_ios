//
//  CEventRequest.h
//  Carot
//
//  Created by Amit Priyadarshi on 21/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "CBaseSL.h"
#import <Carot/Carot.h>
#define SICEventRequest [CEventRequest sharedInstance]

@interface CEventRequest : CBaseSL
+(CEventRequest*)sharedInstance;


/**
 This method can be used for fetching an array of events/alerts from server after a specific time
 
 @param lastSeenTime This method accepts last seen time(epoch) in form of string after which we want the data to be fetched
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), This response is always from server
 
 response = List of events/alerts in an array
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getNewEventsCountAfterTime:(NSString*)lastSeenTime response:(ServerResponse)response;


/**
 This method will return a latest events for a specific vehicle. If supplied count of events is less than 5 then it will still return minimum if 5 available

 @param count Number of events which needs to be fetched(minimum 5)
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be from local database if data is available
 
 response = List of events/alerts in an array
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 @param onRefresh This response will be same as the response object except this will be from server
 */
- (void)getTopEvents:(int)count response:(ServerResponse)response onRefresh:(OnEventRefresh)onRefresh;


/**
 This method will return events after a specific events sorted by time

 @param count number of evets to be fetched from server(minimum 5, if less than 5 even then it will return 5 if available)
 @param event Event after which we need to fetch new events
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be from local database if data is available
 
 response = List of events/alerts in an array
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 @param onRefresh This response will be same as the response object except this will be from server
 */
- (void)loadMoreEvents:(int)count afterEvent:(CEvent*)event response:(ServerResponse)response onRefresh:(OnEventRefresh)onRefresh;

- (void)loadMoreEventsWithOffsetData:(int)count withOffset:(int)offset response:(ServerResponse)response  onRefresh:(OnEventRefresh)onRefresh;
- (void)getTopEventsForHonda:(int)count response:(ServerResponse)response onRefresh:(OnEventRefresh)onRefresh;
- (void)loadMoreEventsForHonda:(int)count afterEvent:(CEvent*)event response:(ServerResponse)response onRefresh:(OnEventRefresh)onRefresh;


/**
 If and events is seen by user then it can be marked as read on server using this API.

 @param eventId A single event ID in form of string which has to be marked as read.
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = no response, check only success flag and error parameter.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)markAsReadForEventId:(NSString*)eventId response:(ServerResponse)response;


/**
 This method will return details of events within a specific trip.

 @param eventId This object will be event ID in string for which we needs details to be displayed on UI
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = user will get an object of CIEvent class which will have all the details for the event
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getDetailsForEventWithId:(NSString*)eventId response:(ServerResponse)response;


/**
 This method will return list of available preference for a user and the value which is stored for the same on server.

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Array of all the available events for the user with their value in an array
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getUserEventPreferenceResponse:(ServerResponse)response;


/**
 This method can be used to update event preference for a single event on server so that user can start/stop receiving alerts for events.

 @param pid ID of preference which needs to be updated on the server
 @param isOn flag that can be true or false for a particular event
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = no response, check only success flag and error parameter.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)updateUserEventPreferenceWithId:(int)pid userPreferenceIsOn:(BOOL)isOn response:(ServerResponse)response;


/**
 This method will return user's push preferences from server

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = array of all the events available to user with their push preferences.
 isSuccessful = boolean parameter which will tell if the request was successful or not.
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getUserPushPreferenceResponse:(ServerResponse)response;


/**
 This method can be used to update push preference for a single type of event on server so that user can start/stop receiving push for that type of event.
 
 @param pid ID of preference which needs to be updated on the server
 @param isOn flag that can be true or false for a particular event
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = no response, check only success flag and error parameter.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)updateUserPushPreferenceWithId:(int)pid userPreferenceIsOn:(BOOL)isOn response:(ServerResponse)response;


/** FOR HONDA ONLY
 This method can be used to update push preference for a single type of event on server so that user can start/stop receiving push for that type of event.
 
 @param pid ID of preference which needs to be updated on the server
 @param isOn flag that can be true or false for a particular event
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = no response, check only success flag and error parameter.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)updateUserEventPreferenceForHonda:(int)pid userPreferenceIsOn:(BOOL)isOn minValue:(int)minimumValue response:(ServerResponse)response;

/**
 This method can be used for getting historical events for a specific type of alarm which us being raised in the app.

 @param alarmType alarm type fot which data needs to be fetched
 @param startDate date in epoch from which events need to be fetched
 @param endDate date in epoch till which events need to be fetched
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Array of type CEvents
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 

 */
- (void)getEventsForAlarmType:(NSString*)alarmType startDate:(NSString*)startDate endDate:(NSString*)endDate response:(OnEventRefresh)response;


/**
 This method can be used to reverse geocode latitude and longitude of a specific event.

 @param event Event which has to be reverse geocoded
 @param index index at which this item exists.
 @param onResult This object contain response in a callback block with two variables i.e. (CEvent* event, id index),

 event(in callback) = this is the updated event object with address reverse geocoded.
 index(in callback) = This is the index which was initially passed in this method.
 */
- (void)resolveAddressForEvent:(CEvent*)event atIndex:(id)index onComplete:(OnEventAddressComplition)onResult;

/**
 This method can be used to mark multiple events as read

 @param events array of CEvent which need to be marked as read on server
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = no response, check only success flag and error parameter.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)markAsReadForEvents:(NSArray *)events response:(ServerResponse)response;


/**
 This method will return only one type of alert and that for car tow away.

 @param pageNumber the response is paginated therefore it is used for getting reponse from server on the basis of page number.
 @param onResult This object contain response in a callback block with two variables i.e. (NSArray* trips, NSError* error),
 
 events = This is the array of tow alert
 error = error object which will be used for checking if the reponse was correct or not.
 
 */
- (void)getTowAlertWithPageNumber:(int )pageNumber andResponse:(OnEventRefresh )response;

@end
