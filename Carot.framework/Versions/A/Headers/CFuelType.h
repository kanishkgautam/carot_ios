//
//  CFuelType.h
//  Carot
//
//  Created by Vaibhav Gautam on 09/03/17.
//  Copyright © 2017 Minda iConnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFuelType : NSObject

@property(nonatomic, retain) NSString *fuelType;
@property(nonatomic) int fuelID;
@end
