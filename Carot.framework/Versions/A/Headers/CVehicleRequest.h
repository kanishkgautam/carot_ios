//
//  CVehicleRequest.h
//  Carot
//
//  Created by Amit Priyadarshi on 09/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import "CBaseSL.h"
#import <Carot/Carot.h>
#import <CoreLocation/CoreLocation.h>

@class CVehicleHealthStatus;

typedef void (^LocationResponse) (CLLocationCoordinate2D location);

#define SICVehicleRequest [CVehicleRequest sharedInstance]

@interface CVehicleRequest : CBaseSL
+(CVehicleRequest*)sharedInstance;


/**
 This method will return current selected vehicle.

 @return CVehicle details of selected vehicle object
 */
-(CVehicle *)getSelectedVehicle;


/**
 This method will check and return which device is currently associated with a specific vehicle

 @param vehicleObj vehicle for which device needs to be checked.
 @return Device which is associated with vehicle.
 */
-(CDevice *)getDeviceForVehicle:(CVehicle *)vehicleObj;



/**
 This method will check and return vehicle which is currently associated with a device

 @param deviceObj device for which vehicle needs to be checked.
 @return vehicle which is currently associated with device
 */
-(CVehicle *)getVehicleDetailsForDevice:(CDevice *)deviceObj;


/**
 *  Call this method to get list of all vehicle makers and their available models
 *  @brief A method to get list of all vehicle makers with their models
 *  @param response A code block of type ServerResponse which excepts three values (See result section).
 *  @result responseObject - NSArray of makers / nil
 *  @result requestSuccess - YES / NO
 *  @result error - nil / Some Error
 */
- (void)getVehicleReferenceDataResponse:(ServerResponse)response;


/**
 This method can be used to fetch all the connected car devices for a user.

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Array of type CDevice
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getUserDevicesResponse:(ServerResponse)response;

/**
 This method can be used to fetch vehicles for a user.
 
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Array of type CDevice
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getUserVehiclesResponse:(ServerResponse)response;


/**
 This device will be used at the time of adding a device for user to the system

 @param serial device Serial number
 @param name name for the device
 @param vinID vinID associated with this device
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Ignore the response in case if success flag is true.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 */
- (void)addNewDeviceWithSerial:(NSString*)serial
                    deviceName:(NSString*)name  vinID:(NSString*)vinID response:(ServerResponse)response;



/**
 This method can be used to add device with more details.

 @param serial serial number of device
 @param name name for your device
 @param modelId model ID of vehicle
 @param makeYear make year of vehicle
 @param fuelType fuel type for vehicle
 @param gearType gear type for vehicle
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Ignore the response in case if success flag is true.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)addNewDeviceWithSerial:(NSString*)serial
                    deviceName:(NSString*)name
                       modelId:(NSUInteger)modelId
                      makeYear:(NSUInteger)makeYear
                      fuelType:(NSString*)fuelType
                      gearType:(NSString*)gearType
                      response:(ServerResponse)response;


/**
 This medhod will return vehicle make and model for vehicle id

 @param vehicleID vehicle ID for which model needs to be searched
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = An object of Dictionary with model and make of vehicle
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
-(void)vehicleMakeAndModelForId:(NSUInteger)vehicleID response:(ServerResponseInternal)responseObj;


/**
 This method will return current status of user's subscription for all his devices

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = List of all the devices and their subscription statuses
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)getUserSubscriptionsResponse:(ServerResponse)response;


/**
 This method can be used to allocate a subscription to a device.

 @param subscriptionId subscription which has to be allocated.
 @param deviceId device ID to which the subscription has to be allocated.
 @param response @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Ignore the response in case if success flag is true.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)addSubscriptionId:(NSUInteger)subscriptionId toDeviceId:(NSUInteger)deviceId response:(ServerResponse)response;


/**
 This method is responsible for telling the current state of the vehicle. ***This method is depricated, the one with tag should be used***

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Object of type CVehicleCurrentState which will tell if vehicle is stopped, running or in some other state
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 */
- (void)getCurrentStateOfVehicleRresponse:(ServerResponse)response;





/**
 This method is responsible for telling the current state of the vehicle. In this method tags must be used for handling missed packets


 @param tag tag number of the packet.
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Object of type CVehicleCurrentState which will tell if vehicle is stopped, running or in some other state
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.

 @param handler This object will return a callback block with two variabled i.e. (NSNumber *tag, NSArray* delta)
 tag = tag returned for the packet
 delta = missed packets
 
 
 */
- (void)getCurrentStateOfVehicleWithTag:(NSNumber*)tag response:(ServerResponse)response tagHandler:(TagHandler)handler;


/**
 This method can be called when user want to renew subscription, This method will call server to generate URL for payment gateway from where the subscription has to be purchased.

 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Object of type NSMutableURLRequest which can be used for making subscription purchase.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.

 */
- (void)getRequestToBuyProductResponse:(ServerResponse)response;


/**
 This method will reverse geocode the provided coordinates into human readable address.

 @param location coordinates of the location to be reverse geocoded ?
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = this will be either nil or the reverse geocoded address.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.

 */
- (void)getAddressForLocation:(CLLocationCoordinate2D)location response:(ServerResponse)response;

/**
 This method will be used to selecting a vehicle to perform different operations.

 @param vid vehicle ID
 @param error if error object is nil then check it else ignore
 */
- (void)setSelectedForVehicleWithId:(NSUInteger)vid withError:(NSError **)error;


- (void)getUserVehiclesWhileAddingDeviceWithResponse:(ServerResponse)response;


/**
 This method can be used to fetch path from user's device(iPhone) location to vehicle's current location

 @param userLocation coordinate of user's location
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = array of coordinates from user's current location to vehicle's location.
 isSuccessful = boolean parameter which will tell if the request was successful or not.
 error = error object which will be having error description if success flag is false.

 */
-(void)locateMyCar:(CLLocation*)userLocation response:(ServerResponse)response;

//Health
/**
 This method will return the health status for the current selected vehicle.
 
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = This will return the object of type CVehicleStatus which will have different values related to a vehicle such as battery status, coolant temp, crash count and so on....
 isSuccessful = boolean parameter which will tell if the request was successful or not.
 error = error object which will be having error description if success flag is false.

 */
- (void)getHealthStatusForVehicleResponse:(ServerResponse)response;


/**
 This method will return the health status for the current selected vehicle.
 
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), To improve the performance this will come from local database in case it is available.
 
 response = This will return the object of type CVehicleStatus which will have different values related to a vehicle such as battery status, coolant temp, crash count and so on....
 isSuccessful = boolean parameter which will tell if the request was successful or not.
 error = error object which will be having error description if success flag is false.
 
@param onRefresh Same as respomse except this is guaranteed to be updated status from server.
 */
- (void)getHealthStatusForVehicleResponse:(ServerResponse)response onRefresh:(OnHealthRefresh)onRefresh;


/**
 On case there is some issue with vehicle's engine it will be reported as a DTC(Diagnostic Trouble Code) code. To get more detauls about

 @param code DTC code to be searched for
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.

 response = JSON object having details of the DTC code
 isSuccessful = boolean parameter which will tell if the request was successful or not.
 error = error object which will be having error description if success flag is false.
 */
- (void)getDtcDetailForDtcCode:(NSString*)code response:(ServerResponse)response;

- (void)updateDeviceInformationWithInfo:(NSDictionary*)param forVehicleWithId:(int)vid response:(ServerResponse)response;

- (void)linkDeviceWithDeviceID:(int)deviceID forVehicleWithId:(NSString*)vid response:(ServerResponse)response;

- (void)removeVinFromDeviceID:(int)deviceID forVehicleWithId:(NSString*)vid response:(ServerResponse)response;

/**
 This method can be used to update the details of a device

 @param serial serial number of device
 @param name name for your device
 @param vehicleID vehicleID to be updated
 @param modelId model ID of vehicle
 @param makeYear make year of vehicle
 @param fuelType fuel type for vehicle
 @param gearType gear type for vehicle
 @param response This object will contain user response in a callback block with three variables i.e. (id response, BOOL isSuccessful, NSError *error), Note that this response will be ALWAYS be come from server only.
 
 response = Ignore the response in case if success flag is true.
 isSuccessful = boolean parameter which will tell if the request was successful or not
 error = error object which will be having error description if success flag is false.
 
 */
- (void)updateDeviceWithSerial:(NSString*)serial deviceName:(NSString*)name vehicleID:(int )vehicleID modelId:(NSUInteger)modelId makeYear:(NSUInteger)makeYear fuelType:(NSString*)fuelType gearType:(NSString*)gearType response:(ServerResponse)response;
@end
