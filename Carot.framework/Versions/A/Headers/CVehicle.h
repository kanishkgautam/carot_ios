//
//  CVehicle.h
//  Carot
//
//  Created by Amit Priyadarshi on 09/09/15.
//  Copyright (c) 2015 Amit Priyadarshi. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "CEvent.h"
@class CVehicleHealthStatus;
@class CEvent;
@interface CVehicle : NSObject

@property (nonatomic, retain) NSDate     * createdOn;
@property (nonatomic        ) NSUInteger daysBeforeSubscriptionExpiry;
@property (nonatomic, retain) NSString   * name;
@property (nonatomic, retain) NSNumber   * userId;
@property (nonatomic        ) NSUInteger vehicleId;
@property (nonatomic        ) NSUInteger deviceId;

@property (nonatomic, ) NSUInteger  makeYear;
@property (nonatomic, retain) NSString * vehicleFuelType;
@property (nonatomic, retain) NSString * vehicleGearType;
@property (nonatomic, ) NSUInteger vehicleModelId;
@property (nonatomic, retain) NSString * vehicleMake;
@property (nonatomic, retain) NSString * vehicleModel;
@property (nonatomic, strong) CVehicleHealthStatus *healthStatus;
@property (nonatomic, retain) NSSet *trips;
@property (nonatomic, retain) CEvent *events;

@property (nonatomic) BOOL isValid;



@end
