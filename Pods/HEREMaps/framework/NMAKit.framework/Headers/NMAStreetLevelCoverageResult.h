/*
 * Copyright (c) 2011-2019 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAGeoCoordinates;

#pragma mark - DEPRECATED


/**
 * Defines the possible outcomes of a street level coverage check
 *
 * DEPRECATED Street Level APIs are deprecated as of release 3.7.
 */
DEPRECATED_ATTRIBUTE
typedef NS_ENUM(NSUInteger, NMAStreetLevelCoverage) {
    /** Street level coverage available */
    NMAStreetLevelCoverageAvailable = 0,
    /** Street level coverage not available */
    NMAStreetLevelCoverageUnavailable,
    /** Street level coverage could not be determined due to an error. See NMAStreetLevelCoverageError */
    NMAStreetLevelCoverageFailed
} DEPRECATED_ATTRIBUTE;

/**
 * Defines the Street level coverage error domain name
 *
 * DEPRECATED Street Level APIs are deprecated as of release 3.7.
 */
DEPRECATED_ATTRIBUTE
FOUNDATION_EXPORT NSString *const _Nonnull NMAStreetLevelCoverageErrorDomain;

/**
 * Defines the errors possible when performing a street level coverage check
 *
 * DEPRECATED Street Level APIs are deprecated as of release 3.7.
 */
DEPRECATED_ATTRIBUTE
typedef NS_ENUM(NSUInteger, NMAStreetLevelCoverageError) {
    /** No error */
    NMAStreetLevelCoverageErrorNone = 0,
    /** Invalid NMAGeoCoordinates object passed to request */
    NMAStreetLevelCoverageErrorInvalidGeoCoordinates,
    /** Failed due to network error */
    NMAStreetLevelCoverageErrorNetworkFailure,
    /** Access to this operation is denied. Contact your HERE representative for more information */
    NMAStreetLevelCoverageErrorOperationNotAllowed
} DEPRECATED_ATTRIBUTE;


/**
 * The result of a street level coverage check
 *
 * DEPRECATED Street Level APIs are deprecated as of release 3.7.
 */
DEPRECATED_ATTRIBUTE
@interface NMAStreetLevelCoverageResult : NSObject

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Street level coverage.
 *
 * @note A Result of NMAStreetLevelCoverageFailed means coverage could not be determined. It does not necessarily mean
 *       coverage is not available. See error property for failure reasons.
 */
@property (nonatomic, readonly) NMAStreetLevelCoverage coverage;

/**
 * Street level coverage request error.
 *
 * Contains nil if coverage was checked successfully, otherwise contains an error code describing the reason
 * for the failure. The most common failure scenario is a bad, or non existent, network connection.
 *
 * See also `NMAStreetLevelCoverageError` for error codes.
 *
 * @note No error (nil) does not mean coverage is available. See coverage property for actual coverage result.
 */
@property (nonatomic, readonly, nullable) NSError *error;

/**
 * Original coordinates with which the coverage request was made.
 */
@property (nonatomic, readonly, nullable) NMAGeoCoordinates *requestCoordinates;

/**
 * Original search radius with which the request was made.
 */
@property (nonatomic, readonly) float requestRadius;

@end
