/*
 * Copyright (c) 2011-2019 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMAStreetLevelObject.h"
#import "NMATypes.h"
#import <UIKit/UIKit.h>

#pragma mark - DEPRECATED


@class NMARoute;


/**
 *
 * NMAStreetLevelRoute provides a visual representation of an NMARoute
 * inside an NMAStreetLevelView.
 *
 * DEPRECATED Street Level APIs are deprecated as of release 3.7.
 */
DEPRECATED_ATTRIBUTE
@interface NMAStreetLevelRoute : NMAStreetLevelObject

/**
 * A convenience method to construct an NMAStreetLevelRoute from an NMARoute.
 *
 * @param route The NMARoute from which to create the NMAStreetLevelRoute.
 */
+ (nullable instancetype)streetLevelRouteWithRoute:(nonnull NMARoute *)route
NS_SWIFT_UNAVAILABLE("Use corresponding instance initializer");

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Initializes an `NMAStreetLevelRoute` instance with the
 * specified `NMARoute`.
 *
 * @param route The `NMARoute` to be displayed in a `NMAStreetLevelView`
 * @return The `NMAStreetLevelRoute`
 */
- (nullable instancetype)initWithRoute:(nonnull NMARoute *)route
NS_SWIFT_NAME(init(route:));

/**
 * The NMARoute object represented by the NMAStreetLevelRoute.
 */
@property (nonatomic, nullable) NMARoute *route;

/**
 * The NMARouteDisplayType of the NMAStreetLevelRoute.
 */
@property (nonatomic) NMARouteDisplayType displayType;

/**
 * The color of the NMAStreetLevelRoute. If the color is changed, the
 * displayType of the route will be set to NMARouteDisplayTypeUserDefined.
 */
@property (nonatomic, nullable) UIColor *color;

@end
