/*
 * Copyright (c) 2011-2019 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMAStreetLevelIconBase.h"

#pragma mark - DEPRECATED



/**
 * NMAStreetLevelIcon is a concrete implementation of NMAStreetLevelIconBase used
 * to place images in an NMAStreetLevelView.
 *
 * DEPRECATED Street Level APIs are deprecated as of release 3.7.
 */
DEPRECATED_ATTRIBUTE
@interface NMAStreetLevelIcon : NMAStreetLevelIconBase

/**
 * A convenience method to construct an NMAStreetLevelIcon at the specified
 * coordinates and with an NMAImage icon.
 * @param coordinates The NMAGeoCoordinates at which to create the NMAStreetLevelIcon.
 * @param icon The NMAImage to be displayed by the icon.
 */
+ (nullable instancetype)streetLevelIconWithGeoCoordinates:(nonnull NMAGeoCoordinates *)coordinates
                                                      icon:(nonnull NMAImage *)icon
NS_SWIFT_UNAVAILABLE("Use corresponding instance initializer");

/**
 * A convenience method to construct an NMAStreetLevelIcon at the specified
 * coordinates and with a UIImage icon.
 * @param coordinates The NMAGeoCoordinates at which to create the NMAStreetLevelIcon.
 * @param image The UIImage to be displayed by the icon.
 */
+ (nullable instancetype)streetLevelIconWithGeoCoordinates:(nonnull NMAGeoCoordinates *)coordinates
                                                     image:(nonnull UIImage *)image
NS_SWIFT_UNAVAILABLE("Use corresponding instance initializer");

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Initializes a `NMAStreetLevelIcon` instance with the specified `NMAGeoCoordinates`
 * and `NMAImage` for the displayed icon.
 *
 * @param coordinates A `NMAGeoCoordinates` representing the map coordinates for displaying
 *               the `NMAStreetLevelIcon`
 * @param icon A `NMAImage` representing the icon for the `NMAStreetLevelIcon`
 * @return The `NMAStreetLevelIcon`. Nil if `NMAImage` for the displayed icon is nil.
 */
- (nullable instancetype)initWithGeoCoordinates:(nonnull NMAGeoCoordinates *)coordinates
                                           icon:(nonnull NMAImage *)icon
NS_SWIFT_NAME(init(geoCoordinates:icon:));

/**
 * Initializes a `NMAStreetLevelIcon` instance with the specified `NMAGeoCoordinates`
 * and UImage for the displayed icon.
 *
 * @param coordinates A `NMAGeoCoordinates` representing the map coordinates for displaying
 *               the `NMAStreetLevelIcon`
 * @param image A UIImage to use to create the NMAImage marker of the icon.
 * @return The `NMAStreetLevelIcon` Nil if UImage for the displayed icon is nil.
 */
- (nullable instancetype)initWithGeoCoordinates:(nonnull NMAGeoCoordinates *)coordinates
                                          image:(nonnull UIImage *)image
NS_SWIFT_NAME(init(geoCoordinates:image:));

@end
