/*
 * Copyright (c) 2011-2019 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMARequest.h"



#pragma mark - DEPRECATED

/**
 *
 * Represents a request to retrieve a list of search terms.
 *
 * DEPRECATED As of SDK 3.10. Use `NMAAutoSuggestionRequest` instead.
 */
DEPRECATED_ATTRIBUTE
@interface NMASuggestionRequest : NMARequest

/**
 * Instances of this class should not be initialized directly
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instances of this class should not be initialized directly
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * The text format of the request results.
 *
 * @note The default value is NMATextFormatHTML.
 */
@property (nonatomic) NMATextFormat textFormat;

@end
