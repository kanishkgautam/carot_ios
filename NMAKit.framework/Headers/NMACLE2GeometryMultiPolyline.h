/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>
#import "NMACLE2Geometry.h"

@class NMAGeoPolyline;

NS_ASSUME_NONNULL_BEGIN

/**
 * \addtogroup NMA_CLE2 NMA Custom Location Extension 2 Group
 * @{
 */

/**
 * Represents a MultiPolyline custom geometry created and accessed through the
 * Custom Location Extension 2.
 *
 */
@interface NMACLE2GeometryMultiPolyline : NMACLE2Geometry

/**
 * %NSArray containting the polylines of this geometry.
 */
@property (nonatomic, readonly) NSArray<NMAGeoPolyline *> *multiPolylineArray;

@end

/**  @}  */

NS_ASSUME_NONNULL_END