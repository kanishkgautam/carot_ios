/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */
#import <UIKit/UIKit.h>
#import "NMAUrbanMobility.h"
@class NMAUrbanMobilityOperator;
@class NMAUrbanMobilityLink;

#ifdef HERE_SDK_INTERNAL
/**
 * Type of engine used by vehicle, available for transport type: %NMAUrbanMobilityTransportType#NMAUrbanMobilityTransportTypeCarShare.
 */
typedef NS_ENUM(NSUInteger, NMAUrbanMobilityCarEngineType) {
    /** \brief Engine type: electric. */
    NMAUrbanMobilityCarEngineTypeElectric   = 0,
    /** \brief Engine type: combustion. */
    NMAUrbanMobilityCarEngineTypeCombustion = 1,
    /** \brief Unknown engine type. */
    NMAUrbanMobilityCarEngineTypeUnknown    = 2
};
#endif


/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */

/**
 * \class NMAUrbanMobilityTransport NMAUrbanMobilityTransport.h "NMAUrbanMobilityTransport.h"
 *
 * Represents a kind of transport (e.g. transit line or bike) with its properties like name,
 * color that can be used to display on the map, or operator that serves given transport.
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to change
 * without notice.
 *
 * \sa NMAUrbanMobilityOperator
 * \sa NMAUrbanMobilityLink
 */
@interface NMAUrbanMobilityTransport : NSObject

/**
 * The name of the transport.
 */
@property (nonatomic, readonly) NSString *name;

/**
 * Means of transport which servers the transport (Bus, Tram, Train, etc).
 */
@property (nonatomic, readonly) NMAUrbanMobilityTransportType transportType;

/**
 * Specific name of the transport type which serves the transport.
 * Name is localized according to current device's language.
 * If given language is not supported English version is returned.
 */
@property (nonatomic, readonly) NSString *transportTypeName;

/**
 * Direction which the transport is heading.
 */
@property (nonatomic, readonly) NSString *direction;

/**
 * Operator serving the transport (might be not available).
 */
@property (nonatomic, readonly) NMAUrbanMobilityOperator *transportOperator;

/**
 * Describes whether or not bikes are allowed on the transport.
 */
@property (nonatomic, readonly) NMAUrbanMobilityFeatureAvailability bikeAllowed;

/**
 * Describes whether or not the transport is barrier free.
 */
@property (nonatomic, readonly) NMAUrbanMobilityFeatureAvailability barrierFree;

/**
 * Transport color in \#RRGGBB format.
 */
@property (nonatomic, readonly) UIColor *color;

/**
 * Transport name color in \#RRGGBB format.
 */
@property (nonatomic, readonly) UIColor *textColor;

/**
 * Color of the border around the transport name in \#RRGGBB format.
 */
@property (nonatomic, readonly) UIColor *outlineColor;
#ifdef HERE_SDK_INTERNAL
/**
 * Vehicle model.
 */
@property (nonatomic, readonly) NSString *model;

/**
 * Vehicle license plate number.
 */
@property (nonatomic, readonly) NSString *licensePlate;

/**
 * Vehicle engine type.
 */
@property (nonatomic, readonly) NMAUrbanMobilityCarEngineType engine;

/**
 * How clean is the vehicle inside: 0 - very dirty, 100 - perfect conditions or NMAUrbanMobilityValueNotAvailable.
 */
@property (nonatomic, readonly) NSUInteger interior;

/**
 * How clean is the vehicle outside: 0 - very dirty, 100 - perfect conditions or NMAUrbanMobilityValueNotAvailable.
 */
@property (nonatomic, readonly) NSUInteger exterior;

/**
 * Level of the fuel tank: 0 - empty tank, 100 - full tank or NMAUrbanMobilityValueNotAvailable.
 */
@property (nonatomic, readonly) NSUInteger fuel;

/**
 * Number of free seats on the vehicle or NMAUrbanMobilityValueNotAvailable.
 */
@property (nonatomic, readonly) NSUInteger seats;

/**
 * Left prime time percentage that need to be shown to the user when present or NMAUrbanMobilityValueNotAvailable.
 * For example value of 52 means +52% prime time.
 */
@property (nonatomic, readonly) NSUInteger primeTime;
#endif
@end
/** @}  */
