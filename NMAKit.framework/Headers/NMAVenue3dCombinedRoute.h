/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAVenue3dBaseLocation;

/**
 * \addtogroup NMA_VenueMaps3d NMA Venue Maps 3D Group
 * @{
 */


/**
 * \class NMAVenue3dCombinedRoute "NMAVenue3dCombinedRoute.h"
 *
 * \brief Describes route from start to end point. The route is split into one or more
 * route sections.
 * \sa NMAVenue3dRoutingController
 */
@interface NMAVenue3dCombinedRoute : NSObject

/**
 * The array of NMAVenue3dRouteSection objects for this route.
 *
 * This information can be used to check type of turn-by-turn information available for each route
 * section of the combined route.
 *
 */
@property (nonatomic, readonly) NSArray *routeSections;

/**
 * Returns @c YES if the returned route does no use any accessors set to be avoided
 * in {@link NMAVenue3dRouteOptions}.
 * <p>
 * In certain cases no route can be created that satisfies all route options when it comes
 * to use of elevators, escalators, stairs and ramps. If any of these are set to be
 * avoided, this methdod can be used to check if the calculated route still uses
 * any of these accessors.
 */
- (BOOL)conformsConnectorOptions;

/**
 * Returns @c YES if the returned route for every outdoor section except to/from parking,
 * is the mode user has requested.
 * <p>
 * In certain cases pedestrian mode is preferred over drive mode when distance to/from street is
 * greater than the direct walking distance and drive distance is less than a certain threshold
 * (ex. 2m).
 * This method can be used to check if any segment is of pedestrian type in drive mode.
 */
- (BOOL)conformsModeOptions;

/**
 * Returns a NMAVenue3dBaseLocation* associated with start point of the route.
 */
- (NMAVenue3dBaseLocation*)getStart;

/**
 * Returns a NMAVenue3dBaseLocation* associated with end point of the route.
 */
- (NMAVenue3dBaseLocation*)getEnd;

/**
 * Returns the array of NMAVenue3dBaseLocation* objects associated with waypoints on the route
 * including start and end points.
 */
- (NSArray*)getWaypoints;

@end
/** @} */
