/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

@class NMAAddress;
@class NMAGeoCoordinates;


/**
 * \addtogroup NMA_CLE NMA Custom Location Group
 * @{
 */

/**
 * Represents a custom location created and accessed through the
 * Custom Location Extension.
 *
 * \note The information for some locations may not be complete.
 *
 * @product nlp-hybrid-plus nlp-plus
 * \deprecated This class is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
DEPRECATED_ATTRIBUTE
@interface NMACustomLocation : NSObject

/**
 * An ID for the location specified by its owner.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *customerLocationId DEPRECATED_ATTRIBUTE;

/**
 * The primary name of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *name1 DEPRECATED_ATTRIBUTE;

/**
 * The secondary name of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *name2 DEPRECATED_ATTRIBUTE;

/**
 * The tertiary name of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *name3 DEPRECATED_ATTRIBUTE;

/**
 * The address of the location.
 *
 * \note Not all address fields will be available for every location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NMAAddress *address DEPRECATED_ATTRIBUTE;

/**
 * The phone number of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *phone DEPRECATED_ATTRIBUTE;

/**
 * The fax number of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *fax DEPRECATED_ATTRIBUTE;

/**
 * The website of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *webURL DEPRECATED_ATTRIBUTE;

/**
 * A custom description of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSString *locationDescription DEPRECATED_ATTRIBUTE;

/**
 * A collection of attributes possessed by the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, attributes can represent any information in the form of key/value from an %NSDictionary.
 */
@property (nonatomic, readonly) NSArray *customAttributes DEPRECATED_ATTRIBUTE;

/**
 * The exact geo coordinates of the location.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 * In CLE2, a location's coordinates can be obtained from an %NMACLE2GeometryPoint.
 */
@property (nonatomic, readonly) NMAGeoCoordinates *coordinates DEPRECATED_ATTRIBUTE;

/**
 * The geo coordinates of the location to use for routing.
 *
 * The route coordinates of a location will typically be located at the
 * nearest accessible road to the location, and may differ from the basic
 * coordinates property.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead.
 */
@property (nonatomic, readonly) NMAGeoCoordinates *routeCoordinates DEPRECATED_ATTRIBUTE;

@end
/**  @}  */
