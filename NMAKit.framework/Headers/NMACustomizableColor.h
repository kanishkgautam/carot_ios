#import "NMAZoomRange.h"
#import "NMACustomizableVariable.h"

@class UIColor;

/**
 * \addtogroup NMA_Map NMA Mapping Group
 * @{
 */


/**
 * \class NMACustomizableColor NMACustomizableColor.h "NMACustomizableColor.h"
 *
 * Represents a map customization color property with red, green, blue components ranging
 * from 0 to 255.
 */
@interface NMACustomizableColor : NMACustomizableVariable

@property (nonatomic) NSInteger red;
@property (nonatomic) NSInteger green;
@property (nonatomic) NSInteger blue;

- (id)initWithColorProperty:(NMASchemeColorProperty)property
               andZoomLevel:(float)zoomLevel
               andZoomRange:(NMAZoomRange *)zoomRange;

/*!
 * Utility method to configure the color from an UIColor object.
 *
 * \note This property is only relevant when SETTING a custom property
 * The alpha component of UIColor is not considered
 */
- (void)setColor:(UIColor *)color;

/*!
 * Utility method to configure the color from an UIColor object.
 *
 * \note This property is only relevant when READING a custom property
 * The alpha component of the returned UIColor is always 1.
 */
- (UIColor *)color;

@end
/** @}  */
