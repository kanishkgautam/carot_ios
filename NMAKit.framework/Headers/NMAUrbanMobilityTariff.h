/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */


/**
 * \class NMAUrbanMobilityTariff NMAUrbanMobilityTariff.h "NMAUrbanMobilityTariff.h"
 *
 * Group of fares which cover the whole journey.
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 */
@interface NMAUrbanMobilityTariff : NSObject

/**
 * Array of %NMAUrbanMobilityFare that are required for one journey.
 */
@property (nonatomic, readonly) NSArray *fares;

@end
/** @}  */
