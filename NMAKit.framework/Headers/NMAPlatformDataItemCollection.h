/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAPlatformDataItem;

/**
 * \addtogroup NMA_PDE NMA Platform Data Extensions Group
 * @{
 */


/**
 * \brief Array of NMAPlatformDataItem objects.
 */
@interface NMAPlatformDataItemCollection : NSObject <NSFastEnumeration>

/**
 * Do not directly instantiate an instance of this class.
 */
- (instancetype)init NS_UNAVAILABLE;

/**
 * Returns the object at the specified index. If the index is beyond the end of
 * the collection (that is, if index is greater than or equal to the value returned
 * by count), an NSRangeException is raised.
 *
 * \return The object located at index.
 */
- (NMAPlatformDataItem *)objectAtIndexedSubscript:(NSUInteger)index;

/**
 * Returns an enumerator object that lets you access each object in the collection.
 *
 * \return The enumerator object.
 */
- (NSEnumerator *)objectEnumerator;

/**
 * Extracts the data contained into an array of dictionaries. Note that for each
 * NMAPlatformDataItem contained, a dictionary is created.
 *
 * \return An array of dictionaries.
 *
 * \sa \link NMAPlatformDataItem::extract\endlink
 */
- (NSArray<NSDictionary<NSString *, NSString *>*> *)extract;

/**
 * The number of entries in the collection.
 */
@property (readonly) NSUInteger count;

@end

/**  @}  */
