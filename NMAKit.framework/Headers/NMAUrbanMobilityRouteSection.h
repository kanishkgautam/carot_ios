/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMAUrbanMobilityDeparture.h"
#import "NMAUrbanMobilityArrival.h"

/**
 * \addtogroup NMA_Route NMA Routing Group
 * @{
 */

/**
 * \class NMAUrbanMobilityRouteSection NMAUrbanMobilityRouteSection.h "NMAUrbanMobilityRouteSection.h"
 * Represents information about a single section/segment of a %NMAUrbanMobilityRoute, which is covered
 * by a single %NMAUrbanMobilityRouteSectionTransit section or is a pedestrian %NMAUrbanMobilityRouteSectionWalk section.
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 */

@interface NMAUrbanMobilityRouteSection : NSObject

/**
 * A unique id for this section.
 */
@property (nonatomic, readonly) NSString *sectionId;

/**
 * Array of%NMAGeoCoordinates representing the geometry of this section.
 */
@property (nonatomic, readonly) NSArray *geometry;

/**
 * Array of %NMAUrbanMobilityDeparture containing relevant departure object.
 */
@property (nonatomic, readonly) NMAUrbanMobilityDeparture *departure;

/**
 * Array of %NMAUrbanMobilityArrival containing relevant departure object.
 */
@property (nonatomic, readonly) NMAUrbanMobilityArrival *arrival;

/**
 * Distance in meters to cover by this section.
 */
@property (nonatomic, readonly) NSUInteger distance;

/**
 * Expected duration in seconds to cover that distance.
 */
@property (nonatomic, readonly) NSTimeInterval duration;

/**
 * Indicate if departure/arrival times are uncertain. It is true for estimated values and
 * false if it comes from real time or time table data.
 *
 * \return YES if departure/arrival time is uncertain (i.e. value is estimated),
 * NO otherwise (i.e. value is from time table or real time)
 */
@property (nonatomic, readonly, getter = isTimeUncertain) BOOL timeUncertain;

/**
 * Array of %NMAUrbanMobilityManeuver for this section.
 */
@property (nonatomic, readonly) NSArray *maneuvers;

/**
 * Array of %NMAUrbanMobilityLink related to this section.
 */
@property (nonatomic, readonly) NSArray *operatorDisclaimers;

/**
 * Array of associated %NMAUrbanMobilityAlert.
 */
@property (nonatomic, readonly) NSArray *alerts;

/**
 * The array of %NMAUrbanMobilityFare required for this particular segment.
 */
@property (nonatomic, readonly) NSArray *fares;

/**
 * The list of %NMAUrbanMobilityIntermediateStop that are part of this transit stop.
 */
@property (nonatomic, readonly) NSArray *intermediateStops;

/**
 * Type of transport which serves the departure (Bus, Tram, Train, etc).
 */
@property (nonatomic, readonly) NMAUrbanMobilityTransportType transportType;

@end
