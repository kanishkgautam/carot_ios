/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAUrbanMobilityTransport;
@class NMAUrbanMobilityRealTimeInfo;

/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */


/**
 * \class NMAUrbanMobilityAlternativeDeparture NMAUrbanMobilityAlternativeDeparture.h "NMAUrbanMobilityAlternativeDeparture.h"
 *
 * Defines an alternative departure.
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 */
@interface NMAUrbanMobilityAlternativeDeparture : NSObject

/**
 * Transport which serves the departure.
 */
@property (nonatomic, readonly) NMAUrbanMobilityTransport *transport;

/**
 * Departure time.
 */
@property (nonatomic, readonly) NSDate *time;

/**
 * Real-time data (time, platform).
 */
@property (nonatomic, readonly) NMAUrbanMobilityRealTimeInfo *realTimeInfo;

@end
/** @}  */
