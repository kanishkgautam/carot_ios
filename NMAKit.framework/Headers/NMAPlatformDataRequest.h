/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

@protocol NMAPlatformDataRequestListener;
@class NMAPlatformDataRequest;
@class NMAPlatformDataResult;
@class NMAGeoBoundingBox;

/**
 * \addtogroup NMA_PDE NMA Platform Data Extensions Group
 * @{
 */

/**
 * The error domain for NMAPlatformDataRequest errors.
 */
FOUNDATION_EXPORT NSString *const kNMAPlatformDataRequestErrorDomain;

/**
 * Defines the possible errors when performing a PDE request.
 */
typedef NS_ENUM(NSUInteger, NMAPlatformDataRequestError) {
    /** \brief Parameters passed to the PDE API are not valid. */
    NMAPlatformDataRequestErrorInvalidParameters = 0,
    /** \brief Access to the PDE API is denied. Contact your HERE
     *         representative for more information. */
    NMAPlatformDataRequestErrorNoPermission = 1,
    /** \brief Connection problem occurred. */
    NMAPlatformDataRequestErrorConnectionError = 2,
    /** \brief Server reported error. */
    NMAPlatformDataRequestErrorServerError = 3
};

/**
 * \brief A completion block used to handle the result of a platform data request.
 *
 * \param request The request object which started the data retrieval.
 * \param data The data retrieved.
 * \param error If an error occurs, the NSError object contains data on it and otherwise
 *              it is nil.
 */
typedef void (^NMAPlatformDataRequestCompletionBlock)(NMAPlatformDataRequest *request,
                                                      NMAPlatformDataResult  *result,
                                                      NSError                *error);

/**
 * Represents an event listener that reports information about query
 * progress changes and query completion.
 *
 * Methods of this protocol are called on the main queue.
 */
@protocol NMAPlatformDataRequestListener<NSObject>

@required

/**
 * \brief A callback method used to pass the result of a platform data request.
 *
 * \param request The request object which started the data retrieval.
 * \param result The result returned.
 * \param error If an error occurs, the NSError object contains data on it and otherwise
 *              it is nil.
 */
      - (void)request:(NMAPlatformDataRequest *)request
didCompleteWithResult:(NMAPlatformDataResult *)result
                error:(NSError *)error;

@end


/**
 * Runs the data requests with the specified
 * parameters.
 */
@interface NMAPlatformDataRequest : NSObject

/**
 * Do not use.
 */
- (instancetype)init NS_UNAVAILABLE;

/**
 * Creates a data request with the specified layers and GeoBounding box.
 *
 * \param layers The layers to search for inside the specified GeoBoundingBox.
 * \param geoBoundingBox The GeoBoundingBox to be searched for the layers specifed.
 *
 * \return If the parameters are valid, an NMAPlatformDataRequest object configured with
 *         the parameters and nil otherwise.
 */
- (instancetype)initWithLayers:(NSSet<NSString *> *)layers geoBoundingBox:(NMAGeoBoundingBox *)geoBoundingBox;

/**
 * Invokes the data request with the specified listener.
 *
 * \param requestListener A listener to listen for the data results.
 *
 * \sa \link startWithBlock:\endlink
 */
- (void)startWithListener:(id<NMAPlatformDataRequestListener>)requestListener;

/**
 * Invokes the data request with the block.
 *
 * \param block A block to run when the data received.
 *
 * \sa NMAPlatformDataRequestCompletionBlock
 * \sa \link startWithListener:\endlink
 */
- (void)startWithBlock:(NMAPlatformDataRequestCompletionBlock)block;

@end

/**  @}  */
