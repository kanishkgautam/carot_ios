/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class UIColor;
@class NMAImage;


/**
 * \addtogroup NMA_VenueMaps3d NMA Venue Maps 3D Group
 * @{
 */

/**
 * \class NMAVenue3dStyleSettings "NMAVenue3dStyleSettings.h"
 *
 * \brief Defines style settings for venue and space objects.
 *
 * \sa NMAVenue3dController
 */
@interface NMAVenue3dStyleSettings : NSObject

/**
* The fill color of the object.
*/
@property (nonatomic, readwrite) UIColor *fillColor;

/**
 * The outline color of the object.
 */
@property (nonatomic, readwrite) UIColor *outlineColor;

/**
 * The fill color of the object in the selected state.
 */
@property (nonatomic, readwrite) UIColor *selectedFillColor;

/**
 * The outline color of the object in the selected state.
 */
@property (nonatomic, readwrite) UIColor *selectedOutlineColor;

/**
 * The string which is shown in the label of the object.
 */
@property (nonatomic, readwrite) NSString *labelName;

/**
 * The image which is shown with the label of the object.
 */
@property (nonatomic, readwrite) NMAImage *labelImage;

/**
 * The fill color of the label text of the object.
 */
@property (nonatomic, readwrite) UIColor *labelFillColor;

/**
 * The outline color of the label text of the object.
 */
@property (nonatomic, readwrite) UIColor *labelOutlineColor;

@end
/** @} */
