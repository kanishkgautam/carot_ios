/*
 * Copyright © 2015-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMAUrbanMobility.h"

@class NMAGeoCoordinates;
@class NMAUrbanMobilityAddress;


/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */

/**
 * \class NMAUrbanMobilityPlace NMAUrbanMobilityPlace.h "NMAUrbanMobilityPlace.h"
 *
 * Represents a physical place like transit station, airport etc..
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to change
 * without notice.
 *
 * \sa NMAUrbanMobilityTransport
 */
@interface NMAUrbanMobilityPlace : NSObject

/**
 * Address of this place.
 */
@property (nonatomic, readonly) NMAUrbanMobilityAddress *address;

/**
 * Describes whether or not the blind guidance is available at the place.
 */
@property (nonatomic, readonly) NMAUrbanMobilityFeatureAvailability blindGuideAvailability;

/**
 * Describes whether or not the elevator is available at the place.
 */
@property (nonatomic, readonly) NMAUrbanMobilityFeatureAvailability elevatorAvailability;

/**
 * Describes whether or not the escalator is available at the place.
 */
@property (nonatomic, readonly) NMAUrbanMobilityFeatureAvailability escalatorAvailability;

/**
 * Transports which run through this place (an array of %NMAUrbanMobilityTransport).
 *
 * \sa NMAUrbanMobilityTransport
 */
@property (nonatomic, readonly) NSArray *transports;

/**
 * Distance (in meters) between the place and current location or
 * NMAUrbanMobilityValueNotAvailable if information is not available.
 *
 * \note It is available only when this NMAUrbanMobilityPlace comes from %NMAUrbanMobilityStationSearchCompletionBlock
 * when you search %NMAUrbanMobilityStation around you. It implied that your current location
 * was specified when searching nearby stations with %NMAUrbanMobilityStationSearchRequest.
 */
@property (nonatomic, readonly) NSUInteger distanceFromCurrentLocation;

/**
 * Expected time, in seconds, to reach the station from your current location or 
 * NMAUrbanMobilityValueNotAvailable if information is not available.
 *
 * \note  It is available only when this NMAUrbanMobilityPlace comes from %NMAUrbanMobilityStationSearchCompletionBlock
 * when you search %NMAUrbanMobilityStation around you. It implied that your current location
 * was specified when searching nearby stations with %NMAUrbanMobilityStationSearchRequest.
 */
@property (nonatomic, readonly) NSTimeInterval durationFromCurrentLocation;

/**
 * Additional info about place.
 */
@property (nonatomic, readonly) NSString *info;

@end
/** @}  */
