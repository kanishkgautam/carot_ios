/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMAUrbanMobility.h"

@class NMAGeoCoordinates;
@class NMAUrbanMobilityAccessPoint;
@class NMAUrbanMobilityAddress;
@class NMAUrbanMobilityDepartureFrequency;
@class NMAUrbanMobilityOperator;
@class NMAUrbanMobilityPlace;
@class NMAUrbanMobilityStation;
@class NMAUrbanMobilityTransport;
@class NMAUrbanMobilityRealTimeInfo;

/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */


/**
 * \class NMAUrbanMobilityDeparture NMAUrbanMobilityDeparture.h "NMAUrbanMobilityDeparture.h"
 *
 * Defines a departure from a transit station.
 *
 * \sa NMAUrbanMobilityStation
 */
@interface NMAUrbanMobilityDeparture : NSObject

/**
 * Transport which serves the departure.
 */
@property (nonatomic, readonly) NMAUrbanMobilityTransport *transport;

/**
 * Departure time.
 */
@property (nonatomic, readonly) NSDate *time;

/**
 * Departure platform.
 */
@property (nonatomic, readonly) NSString *platform;

/**
 * Real-time data (time, platform).
 */
@property (nonatomic, readonly) NMAUrbanMobilityRealTimeInfo *realTimeInfo;

/**
 * Frequency of the departure (nil if not available).
 */
@property (nonatomic, readonly) NMAUrbanMobilityDepartureFrequency *frequency;

/**
 * %NMAUrbanMobilityStation information from which this departure occurs.
 * NOTE: This property is available only if %NMAUrbanMobilityPlace of the departure is also a %NMAUrbanMobilityStation.
 *
 * \return  %NMAUrbanMobilityStation information or nil if not available.
 */
@property (nonatomic, readonly) NMAUrbanMobilityStation *station;

/**
 * %NMAUrbanMobilityPlace information from which this departure occurs.
 *
 * \return %NMAUrbanMobilityPlace from which this departure occurs or nil if not available.
 */
@property (nonatomic, readonly) NMAUrbanMobilityPlace *place;

/**
 * Access point which can be used to access the transit station which hosts this departure.
 * Information about a station access point (entrance/exit).
 */
@property (nonatomic, readonly) NMAUrbanMobilityAccessPoint *accessPoint;

/**
 * Array of %NMAUrbanMobilityAlternativeDeparture alternative departures.
 */
@property (nonatomic, readonly) NSArray *alternativeDepartures;
#ifdef HERE_SDK_INTERNAL
/**
 * Array of %NMAUrbanMobilityActivity required before departure.
 * \return NSArray of %NMAUrbanMobilityActivity, or nil if none.
 */
@property (nonatomic, readonly) NSArray *activities;
#endif
@end
/** @}  */
