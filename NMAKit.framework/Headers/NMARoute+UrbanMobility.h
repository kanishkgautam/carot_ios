/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMARoute.h"
#import "NMAUrbanMobilityArrival.h"
#import "NMAUrbanMobilityDeparture.h"

/**
 * \addtogroup NMA_Route NMA Routing Group
 * @{
 */

/**
 * Represents a wrapper of type NMARoute over the NMAUrbanMobilityRoute object.
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 */
@interface NMARoute (NMAUrbanMobilityRoute)

/**
 * A unique id for this route.
 */
@property (nonatomic, readonly) NSString *routeId;

/**
 * The number of times a public transport change is done on this route.
 */
@property (nonatomic, readonly) NSUInteger changes;

/**
 * The duration in seconds of the route.
 */
@property (nonatomic, readonly) NSTimeInterval duration;

/**
 * Information about arrival, see %NMAUrbanMobilityArrival class for details.
 */
@property (nonatomic, readonly) NMAUrbanMobilityArrival *arrival;

/**
 * Information about departure, see %NMAUrbanMobilityDeparture class for details.
 */
@property (nonatomic, readonly) NMAUrbanMobilityDeparture *departure;

/**
 * Array of %NMAUrbanMobilityRouteSection of the route. Can include different types %NMAUrbanMobilityTransportType of transport, like walk or subway.
 */
@property (nonatomic, readonly) NSArray *sections;

/**
 * Array of %NMAUrbanMobilityTariff representing different options you can purchase for just one route.
 */
@property (nonatomic, readonly) NSArray *tariffOptions;

@end
/** @}  */
