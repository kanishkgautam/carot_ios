/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAGeoCoordinates;
@class NMAAddress;


/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */

/**
 * \class NMAUrbanMobilityAddress NMAUrbanMobilityAddress.h "NMAUrbanMobilityAddress.h"
 *
 * Geo coordinates and address information of a geographic location.
 *
 * \note Address information might not be available.
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to
 * change without notice.
 *
 * \sa NMAAddress
 */

@interface NMAUrbanMobilityAddress : NSObject

/**
 * Geo coordinates of the geographic location.
 */
@property (nonatomic, readonly) NMAGeoCoordinates *location;

/**
 * The name of the place.
 */
@property (nonatomic, strong) NSString *name;

/**
 * The country name.
 */
@property (nonatomic, strong) NSString *country;

/**
 * The country code.
 *
 * \note The country code defined in "ISO 3166-1 alpha-3" three letter format.
 */
@property (nonatomic, strong) NSString *countryCode;

/**
 * The state name.
 */
@property (nonatomic, strong) NSString *state;

/**
 * The postal code.
 */
@property (nonatomic, strong) NSString *postalCode;

/**
 * The district name.
 */
@property (nonatomic, strong) NSString *district;

/**
 * The street name.
 */
@property (nonatomic, strong) NSString *street;

/**
 * The house number.
 */
@property (nonatomic, strong) NSString *houseNumber;

/**
 * The city name.
 */
@property (nonatomic, strong) NSString *city;

@end
/** @}  */
