/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */


/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */


/**
 * \brief Represents departure information for a public transport at a given stop/station.
 *
 * IMPORTANT: Urban Mobility Departure Board is a Beta feature. The related classes are subject to
 * change without notice.
 *
 */
@interface NMAUrbanMobilityDepartureBoard : NSObject

/**
 * Returns an array of %NMAUrbanMobilityDeparture.
 */
@property (nonatomic, readonly) NSArray *departures;

/**
 * Returns an array of %NMAUrbanMobilityTransport, transports serving departures shown in current departure list .
 */
@property (nonatomic, readonly) NSArray *transports;

/**
 * Returns an array of %NMAUrbanMobilityOperator, operators operating on this stop/station.
 */
@property (nonatomic, readonly) NSArray *operators;

/**
 * Returns an array of associated operators disclaimers (%NMAUrbanMobilityLink).
 */
@property (nonatomic, readonly) NSArray *operatorDisclaimers;

@end
/** @}  */
