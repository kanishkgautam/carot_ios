/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMARoutingMode.h"
#import "NMATransitRoutingOption.h"

/**
 * \addtogroup NMA_Route NMA Routing Group
 * @{
 */

/**
 * Contains options used when calculating a public transit route.
 *
 * @product nlp-hybrid-plus nlp-plus
 */
@interface NMARoutingMode (NMATransitRoutingMode)

/**
 * The OR-ed #NMATransitRoutingOption values for the NMARoutingMode.
 *
 * \note The default value is 0 (no options selected).
 *
 * @product nlp-hybrid-plus nlp-plus
 */
@property (nonatomic) NSUInteger transitRoutingOptions;

/**
 * The maximum number of vehicle changes allowed during the trip.
 *
 * @product nlp-hybrid-plus nlp-plus
 */
@property (nonatomic) NSUInteger maximumChanges;

@end
/** @}  */
