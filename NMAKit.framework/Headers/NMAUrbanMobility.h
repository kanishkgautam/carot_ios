/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 *
 * The Urban Mobility group provides classes, callbacks, and enumerations
 * that can be used to enable Urban Mobility functionality such as 
 * searching for nearby transit stations and requesting departure boards.
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to change
 * without notice.
 *
 * Key classes in this group are:
 * {@link NMAUrbanMobilityRequestManager},
 * {@link NMAUrbanMobilityStationSearchRequest},
 * {@link NMAUrbanMobilityDepartureBoardRequest},
 * {@link NMAUrbanMobilityMultiBoardRequest},
 * {@link NMAUrbanMobilityCitySearchRequest},
 * {@link NMAUrbanMobilityCityCoverageRequest},
 * {@link NMAUrbanMobilityNearbyCoverageRequest}
 *
 * @{
 */

/**
 * \brief This constant value is set for Urban Mobility property that is not available from backend server.
 */
FOUNDATION_EXPORT NSUInteger const NMAUrbanMobilityValueNotAvailable;


/**
 * \brief This enum defines Urban Mobility transport type (means of transport).
 */
typedef NS_ENUM(NSUInteger, NMAUrbanMobilityTransportType) {
    /** \brief Highspeed train */
    NMAUrbanMobilityTransportTypeHighspeedTrain      = 0,

    /** \brief Intercity train */
    NMAUrbanMobilityTransportTypeIntercityTrain      = 1,

    /** \brief Interregional train */
    NMAUrbanMobilityTransportTypeInterregionalTrain  = 2,

    /** \brief Regional train */
    NMAUrbanMobilityTransportTypeRegionalTrain       = 3,

    /** \brief City train */
    NMAUrbanMobilityTransportTypeCityTrain           = 4,

    /** \brief Bus */
    NMAUrbanMobilityTransportTypeBus                 = 5,

    /** \brief Ferry */
    NMAUrbanMobilityTransportTypeFerry               = 6,

    /** \brief Subway */
    NMAUrbanMobilityTransportTypeSubway              = 7,

    /** \brief Tram */
    NMAUrbanMobilityTransportTypeTram                = 8,

    /** \brief Ordered services/Private buses */
    NMAUrbanMobilityTransportTypePrivateBus          = 9,

    /** \brief Inclined */
    NMAUrbanMobilityTransportTypeInclined            = 10,

    /** \brief Aerial */
    NMAUrbanMobilityTransportTypeAerial              = 11,

    /** \brief Rapid bus */
    NMAUrbanMobilityTransportTypeRapidBus            = 12,

    /** \brief Monorail */
    NMAUrbanMobilityTransportTypeMonorail            = 13,

    /** \brief Flight */
    NMAUrbanMobilityTransportTypeFlight              = 14,

    /** \brief Unknown */
    NMAUrbanMobilityTransportTypeUnknown             = 15,

    /** \brief Use bike */
    NMAUrbanMobilityTransportTypeBike                = 17,

    /** \brief Use bike share */
    NMAUrbanMobilityTransportTypeBikeShare           = 18,

    /** \brief Use Park+Ride */
    NMAUrbanMobilityTransportTypeParkAndRide         = 19,

    /** \brief Use walk */
    NMAUrbanMobilityTransportTypeWalk                = 20,

    /** \brief Use car */
    NMAUrbanMobilityTransportTypeCar                 = 21,

    /** \brief Use car share */
    NMAUrbanMobilityTransportTypeCarShare            = 22,

    /** \brief Use taxi */
    NMAUrbanMobilityTransportTypeTaxi                = 23,

    /** \brief Undefined transport type */
    NMAUrbanMobilityTransportTypeUndefined           = 24

};

/**
 * \brief This enum defines Urban Mobility operation errors.
 */
typedef NS_ENUM(NSUInteger, NMAUrbanMobilityError) {
    /** \brief No error. */
    NMAUrbanMobilityErrorNone                        = 0,

    /** \brief Application is currently offline. */
    NMAUrbanMobilityErrorOffline                     = 1,

    /** \brief Backend returned bad HTTP response. */
    NMAUrbanMobilityErrorBadResponseCode             = 2,

    /** \brief Backend returned malformed response. */
    NMAUrbanMobilityErrorMalformedResponse           = 3,

    /** \brief Unknown error occurred. */
    NMAUrbanMobilityErrorUnknown                     = 4,

    /** \brief Credentials which were sent to the backend server were not recognised as valid. */
    NMAUrbanMobilityErrorUnauthorized                = 5,

    /** \brief No coverage in this region/area. */
    NMAUrbanMobilityErrorNoCoverage                  = 6,

    /** \brief Backend did not return any response. */
    NMAUrbanMobilityErrorNoResponse                  = 7,

    /** \brief No search matches were found. */
    NMAUrbanMobilityErrorNotFound                    = 8,

    /** \brief Invalid request parameters were provided. */
    NMAUrbanMobilityErrorInvalidParameters           = 9,

    /** \brief Unexpected error occurred. */
    NMAUrbanMobilityErrorUnexpected                  = 10,

    /** \brief API is not available in this region/area. */
    NMAUrbanMobilityErrorUnavailableAPI              = 11,

    /** \brief Given period is invalid. */
    NMAUrbanMobilityErrorInvalidPeriod               = 12,

    /** \brief Routing was not possible. */
    NMAUrbanMobilityErrorRoutingNotPossible          = 13,

    /** \brief The start and destination locations are too close to each other. */
    NMAUrbanMobilityErrorStartDestinationTooClose    = 14,

    /** \brief No stations were found near the given address. */
    NMAUrbanMobilityErrorNoStationNearby             = 15,

    /** \brief There was an error due to another request already being processed. */
    NMAUrbanMobilityErrorInvalidOperation            = 16,

    /** \brief There was insufficient memory to complete the request. */
    NMAUrbanMobilityErrorOutOfMemory                 = 17,

    /** \brief The request was cancelled. */
    NMAUrbanMobilityErrorRoutingCancelled            = 18,

    /** \brief A route was found but is invalid because it makes use of roads that were disabled by #NMARoutingOption */
    NMAUrbanMobilityErrorViolatesOptions             = 19,

    /** \brief The route cannot be calculated because there is not enough local map data to perform route calculation. Client can re-download map data and calculate route again. */
    NMAUrbanMobilityErrorInsufficientMapData         = 20,

    /** \brief The backend service was unavailable. Try again later. */
    NMAUrbanMobilityErrorServiceUnavailable          = 21,

    /** \brief There was a network communications error. */
    NMAUrbanMobilityErrorNetworkCommunication        = 22,

    /** \brief Access to this operation is denied. Contact your HERE representative for more information. */
    NMAUrbanMobilityErrorOperationNotAllowed         = 23

};

/**
 * \brief This enum defines availability options of a transit feature.
 */
typedef NS_ENUM(NSUInteger, NMAUrbanMobilityFeatureAvailability) {
    /** \brief Feature availability is unknown. */
    NMAUrbanMobilityFeatureAvailabilityUnknown      = 0,

    /** \brief Feature is available. */
    NMAUrbanMobilityFeatureAvailable                = 1,

    /** \brief Feature is not available. */
    NMAUrbanMobilityFeatureNotAvailable             = 2
};

/** @}  */
