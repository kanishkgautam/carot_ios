/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */


/**
 * \class NMAUrbanMobilityFare NMAUrbanMobilityFare.h "NMAUrbanMobilityFare.h"
 *
 * A single fare which covers a portion of a journey (e.g. ticket).
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 */

@interface NMAUrbanMobilityFare : NSObject

/**
 * Name of the fare.
 */
@property (nonatomic, readonly) NSString *name;

/**
 * Price of the fare.
 */
@property (nonatomic, readonly) double price;

/**
 * Gets ISO 4217 code of currency in which the price is given, e.g. "EUR".
 * @see <a href="https://en.wikipedia.org/wiki/ISO_4217">ISO 4217 code</a>
 */
@property (nonatomic, readonly) NSString *currency;

/**
 * Gets the array of %NMAUrbanMobilityLink associated with this fare e.g. to booking page.
 * \return array of %NMAUrbanMobilityLink associated with this fare, or nil.
 */
@property (nonatomic, readonly) NSArray *links;

@end
