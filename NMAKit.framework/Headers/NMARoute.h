/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>
#import "NMARoutingMode.h"
#import "NMARouteTta.h"
#import "NMADynamicPenalty.h"

@class NMAGeoBoundingBox;
@class NMAWaypoint;
@class NMAManeuver;
@class NMAMapPolyline;

/**
 * \addtogroup NMA_Route NMA Routing Group
 * @{
 */

/**
* \brief A constant used to indicate the whole route should be used in route leg selection.
*/
 FOUNDATION_EXPORT const NSInteger NMARouteSublegWhole;


/**
 * Route Serialization Errors
 *
 * IMPORTANT: NMARoute serialization is a Beta feature. The related classes are subject to
 * change without notice.
 */
typedef NS_ENUM(NSUInteger, NMARouteSerializationError) {
    /** \brief Parameter provided is nil, uninitialized or empty */
    NMARouteSerializationErrorInvalidParameter,
    /** \brief Map version from serialized route does not match current map version */
    NMARouteSerializationErrorMapVersionMismatch,
    /** \brief Data provided for deserialization is corrupted */
    NMARouteSerializationErrorDataCorrupted,
    /** \brief Transport mode not supported for current operation */
    NMARouteSerializationErrorTransportModeNotSupported,
    /** \brief Generic error */
    NMARouteSerializationErrorUnknown
};

/**
 * \class NMARoute NMARoute.h "NMARoute.h"
 *
 * \brief Represents a path (a collection of maneuvers) connecting two or more waypoints.
 *
 * Waypoints may be thought of as the input to a route calculation whereas maneuvers are
 * the results of calculating a route.
 */
@interface NMARoute : NSObject

/**
 * \brief Create an NMARoute from a previously serialized route
 *
 * This method returns nil if the deserialization fails. The deserialization fails when
 * the map version from which the NMARoute was serialized does not match current map version,
 * the SDK version from which the NMARoute was serialized is not compatible with current SDK
 * version, or when the input data is invalid or corrupted.
 * When deserializing a route with many waypoints, post to GCD to avoid blockage on the main
 * thread.
 *
 * IMPORTANT: NMARoute serialization is a Beta feature. The related classes are subject to
 * change without notice.
 *
 * \param data NSData to be deserialized into a NMARoute.
 * \param error nil if deserialization completed successfully,
 *              otherwise an NSError object with an %NMARouteSerializationError indicating
 *              the failure reason.
 */
+ (NMARoute *)routeFromSerializedRoute:(NSData *)data error:(NSError **)error;

/**
 * NMARoute is generated from route calculation or route deserialization. Please see
 * {@link NMACoreRouter::calculateRouteWithStops:routingMode:completionBlock:}
 * and {@link NMARoute::routeFromSerializedRoute:error:}.
 */
- (instancetype)init NS_UNAVAILABLE;

/**
 * \brief The starting NMAWaypoint for the %NMARoute.
 */
@property (nonatomic, readonly, weak) NMAWaypoint *start;

/**
 *  The destination NMAWaypoint for the %NMARoute.
 */
@property (nonatomic, readonly, weak) NMAWaypoint *destination;

/**
 *  Array of NMAWaypoint for all waypoints of the %NMARoute.
 */
@property (nonatomic, readonly, strong) NSArray *waypoints;

/**
 * The length of the %NMARoute, in meters. This is the actual distance
 * covered if you were to travel the route.
 */
@property (nonatomic, readonly) NSUInteger length;

/**
 *  Returns the number of sub-legs the route has
 * a subleg is the part of a route between two stop waypoints.
 */
@property (nonatomic, readonly) NSUInteger sublegCount;

/**
 * The NMARouteTta object of traveling the whole NMARoute.
 *
 * \note The object returned will not include any delays due to traffic.
 */
@property (nonatomic, readonly) NMARouteTta *tta;


/**
 * \brief The NMARouteTta object of traveling the specified subLeg of the NMARoute
 *
 * \param subleg The index of the subleg on the NMARoute to be used for this calculation. If
 * NMARouteSublegWhole is provided, it is the same as tta property.
 *
 * \note The object returned will not include any delays due to traffic.
 */
- (NMARouteTta *)ttaForSubleg:(NSUInteger)subleg;


/**
 * The smallest NMAGeoBoundingBox that contains the entire %NMARoute.
 */
@property (nonatomic, readonly, strong) NMAGeoBoundingBox *boundingBox;

/**
 * Array of NMAManeuver to represent all the maneuvers that travelers will
 * encounter along the %NMARoute.
 * In case of calculation %NMAUrbanMobilityRoute, this returns nil.
 * Instead, you can find array of %NMAUrbanMobilityManeuver in every %NMAUrbanMobilityRoute::NMAUrbanMobilityRouteSectionWalk.
 */
@property (nonatomic, readonly, strong) NSArray *maneuvers;

/**
 * The NSArray of %NMAGeoCoordinates representing, in order, the
 * polyline of the route.
 *
 * No elevation profile of the route is available. The %NMAGeoCoordinates altitude property will
 * return -FLT_MAX (representing an unknown altitude).
 */
@property (nonatomic, readonly, strong) NSArray *geometry;

/**
 * The NSArray of %NMAGeoCoordinates (with elevation data, if available) representing, in order, the
 * polyline of the route.
 *
 * An elevation profile of the route can be determined if the
 * altitude property of the %NMAGeoCoordinates does not return
 * -FLT_MAX (representing an unknown altitude).
 */
@property (nonatomic, readonly, strong) NSArray *geometryWithElevationData;

/**
 * The NMARoutingMode for the %NMARoute.
 */
@property (nonatomic, readonly, strong) NMARoutingMode *routingMode;

/**
 * A user-defined tag to identify the %NMARoute
 *
 * \note The default value for userTag is empty string.
 */
@property (nonatomic, strong) NSString *userTag;

/**
 * \brief The NMARouteTta object of traveling the the whole NMARoute with traffic consideration if
 * traffic information is available.
 *
 * This method returns a nil object if access to this operation is denied.
 * Contact your HERE representative for more information.
 *
 * To ensure ttaWithTraffic considers traffic for the ENTIRE route do either of the following:
 *
 * (1) If you calculate your route with traffic, i.e. by specifying a traffic dynamic penalty via
 *     NMARoutingManager, then simply call ttaWithTraffic.
 *     IMPORTANT: In this case the returned NMARouteTta may indicate the route is not blocked even
 *     if it is. Always check NMARouteViolatedOption in the [NMARouteManagerDelegate didCalculateRoutes:]
 *     callback.
 *
 * (2) If you calculate your route without traffic, but you are still interested in duration and
 *     details for your route with traffic, then you need to:
 *      a. Request traffic for the entire route, see "requestTrafficOnRoute:" in NMATrafficManager
 *      b. Wait for the callback "trafficDataDidFinish" to ensure the traffic data has been downloaded
 *      c. Get the duration with traffic by calling ttaWithTraffic.
 *
 * In both of the above cases you must be online!
 *
 * Please note that enabling traffic on NMAMapView (using any of the traffic related methods) will cause
 * traffic data to be downloaded and hence considered by ttaWithTraffic. This can lead to situations
 * in which ttaWithTraffic will only consider traffic for the visible part of the route if (1) or (2)
 * above were not performed.
 *
 * \param mode NMATrafficPenaltyMode to be used for this calculation.
 */
- (NMARouteTta *)ttaWithTraffic:(NMATrafficPenaltyMode)mode;

/**
 * \brief The NMARouteTta object of traveling the the whole NMARoute with traffic consideration if
 * traffic information is available.
 *
 * This method returns a nil object if access to this operation is denied.
 * Contact your HERE representative for more information.
 *
 * To ensure ttaWithTraffic considers traffic for the ENTIRE route do either of the following:
 *
 * (1) If you calculate your route with traffic, i.e. by specifying a traffic dynamic penalty via
 *     NMARoutingManager, then simply call ttaWithTraffic.
 *     IMPORTANT: In this case the returned NMARouteTta may indicate the route is not blocked even
 *     if it is. Always check NMARouteViolatedOption in the [NMARouteManagerDelegate didCalculateRoutes:]
 *     callback.
 *
 * (2) If you calculate your route without traffic, but you are still interested in duration and
 *     details for your route with traffic, then you need to:
 *      a. Request traffic for the entire route, see "requestTrafficOnRoute:" in NMATrafficManager
 *      b. Wait for the callback "trafficDataDidFinish" to ensure the traffic data has been downloaded
 *      c. Get the duration with traffic by calling ttaWithTraffic.
 *
 * In both of the above cases you must be online!
 *
 * Please note that enabling traffic on NMAMapView (using any of the traffic related methods) will cause
 * traffic data to be downloaded and hence considered by ttaWithTraffic. This can lead to situations
 * in which ttaWithTraffic will only consider traffic for the visible part of the route if (1) or (2)
 * above were not performed.
 *
 * \param mode NMATrafficPenaltyMode to be used for this calculation.
 * \param subleg The index of the subleg on the NMARoute to be used for this calculation. If
 * NMARouteSublegWhole is provided, it is the same as ttaWithTraffic property.
 */
- (NMARouteTta *)ttaWithTraffic:(NMATrafficPenaltyMode)mode forSubleg:(NSUInteger)subleg;

/**
 * \brief Serialize an NMARoute object
 *
 * This method does not serialize invalid routes or routes calculated with
 * %NMATransportModePublicTransport or %NMATransportModeUrbanMobility.
 * Instead, nil is returned.
 * When serializing a route with many waypoints, post to GCD to avoid blockage on the main
 * thread.
 *
 * IMPORTANT: NMARoute serialization is a Beta feature. The related classes are subject to
 * change without notice.
 *
 * \param error nil if deserialization completed successfully,
 *              otherwise an NSError object with an %NMARouteSerializationError indicating
 *              the failure reason.
 */
- (NSData *)serializedRouteWithError:(NSError **)error;

#pragma mark - DEPRECATED
/**
 *  The %NMAMapPolyline representation of the route.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. This is replaced by geometry.
 * %NMAMapPolyline may be instantiated from geometry via "mapPolylineWithVertices:".
 */
@property (nonatomic, readonly, strong) NMAMapPolyline *mapPolyline DEPRECATED_ATTRIBUTE;

@end

/**  @}  */
