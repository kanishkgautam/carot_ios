/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * \addtogroup NMA_Common  NMA Common Group
 * @{
 * \class NMAGeoPolygon NMAGeoPolygon.h "NMAGeoPolygon.h"
 *
 * Object representing a Polygon, which consists of three or more points.
 * This class can be used to manipulate a polygon geometry along with %NMAMapPolygon for map
 * rendering. The minimum number of points in an NMAGeoPolygon must be three.
 *
 */
@interface NMAGeoPolygon : NSObject

/**
 * Initializes an %NMAGeoPolygon instance with the specified %NMAGeoCoordinates.
 *
 * \param coordinates An NSArray containing two or more %NMAGeoCoordinates.
 * \return The %NMAGeoPolygon instance, or nil if initialization failed
 */
- (nullable instancetype)initWithCoordinates:(NSArray<NMAGeoCoordinates *> *)coordinates;

/**
 * Add a new point to the polygon.
 *
 * \param coordinate point to be added given as %NMAGeoCoordinates
 */
- (void)addPoint:(NMAGeoCoordinates *)coordinate;

/**
 * Adds a list of points to the polygon.
 *
 * \param coordinates NSArray containing the list of %NMAGeoCoordinates
 */
- (void)addPoints:(NSArray<NMAGeoCoordinates *> *)coordinates;

/**
 * Insert a point to the polygon at the specified index
 *
 * \param coordinate point to be added given as %NMAGeoCoordinates
 * \param index position of the point along the polygon
 */
- (void)insertPoint:(NMAGeoCoordinates *)coordinate atIndex:(int)index;

/**
 * Remove a point from the polygon with the specified index
 *
 * \param index position of the point along the polygon
 */
- (void)removePointAtIndex:(int)index;

/**
 * Remove all points from the polygon
 */
- (void)clear;

/**
 * Get the total number of points that currently exists in the polygon
 */
- (int)numberOfPoints;

/**
 * Get the point coordinates from the specified index of this polygon.
 * \param index position of the point along the polygon
 * \return The %NMAGeoCoordinates instance, or nil if failed
 */
- (nullable NMAGeoCoordinates *)pointAtIndex:(int)index;

/**
 * Get an NSArray with all points of the polygon.
 */
- (NSArray<NMAGeoCoordinates *> *)allPoints;

/**
 * Return the geographical length of the polygon in meters.
 */
- (double)length;

/**
 * Gets the %NMAGeoCoordinates along the path of the polygon that is closest to the specified point.
 *
 * \param coordinate the origin point to search from given in %NMAGeoCoordinates.
 */
- (NMAGeoCoordinates *)nearestCoordinateFrom:(NMAGeoCoordinates *)coordinate;

/**
 * Gets the point index along the path of the polygon that is closest to the specified point.
 *
 * \param coordinate the origin point to search from given in %NMAGeoCoordinates.
 */
- (NSUInteger)nearestIndexFromCoordinate:(NMAGeoCoordinates *)coordinate;

@end
/** @}  */

NS_ASSUME_NONNULL_END
