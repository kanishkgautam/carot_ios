/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>


/**
 * \addtogroup NMA_CLE NMA Custom Location Group
 * @{
 */

/**
 * Represents an abstract custom geometry created and accessed through the
 * Custom Location Extension.
 *
 * \note See %NMACustomGeometryMultiPolygon, %NMACustomGeometryPolyline and %NMACustomGeometryPoint
 *
 * \deprecated This class is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
DEPRECATED_ATTRIBUTE
@interface NMACustomGeometry : NSObject

/**
 * An ID of this geometry specified by its owner.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
@property (nonatomic, readonly) NSUInteger geometryId DEPRECATED_ATTRIBUTE;

/**
 * The name of the geometry.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
@property (nonatomic, readonly) NSString *name DEPRECATED_ATTRIBUTE;

/**
 * Additional custom attributes of this geometry.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
@property (nonatomic, readonly) NSDictionary *customAttributes DEPRECATED_ATTRIBUTE;

/**
 * Feature ID of this geometry, usually this is a concatenation of geometry name and geometry ID.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
@property (nonatomic, readonly) NSString *featureId DEPRECATED_ATTRIBUTE;

@end

/**  @}  */
