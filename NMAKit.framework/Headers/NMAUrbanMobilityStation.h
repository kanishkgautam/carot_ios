/*
 * Copyright © 2015-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMAUrbanMobilityPlace.h"


/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */

/**
 * \class NMAUrbanMobilityStation NMAUrbanMobilityStation.h "NMAUrbanMobilityStation.h"
 *
 * Public transport station.
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to
 * change without notice.
 *
 * \sa NMAUrbanMobilityDepartureBoard
 * \sa NMAUrbanMobilityPlace
 */
@interface NMAUrbanMobilityStation : NMAUrbanMobilityPlace

/**
 * Unique ID of the station.
 */
@property (nonatomic, readonly) NSString *stationId;

/**
 * YES if the departure board is available for this station.
 *
 * \sa NMAUrbanMobilityDepartureBoard
 */
@property (nonatomic, readonly, getter = isDepartureBoardAvailable) BOOL departureBoardAvailable;

@end
/** @}  */
