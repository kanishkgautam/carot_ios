/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAUrbanMobilityOperator;
@class NMAUrbanMobilityProvider;
@class NMAUrbanMobilityLink;

/**
 * \addtogroup NMA_Route NMA Routing Group
 * @{
 */

/**
 * \class NMAUrbanMobilityAlert NMAUrbanMobilityAlert.h "NMAUrbanMobilityAlert.h"
 * All available information about one transit alert.
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 */
@interface NMAUrbanMobilityAlert : NSObject

/**
 * Gets unique Id for this alert. Might help to identify
 * if this alert was already processed on the client.
 *
 * \return alert id.
 */
@property (nonatomic, readonly) NSString *uniqueId;

/**
 * Gets the %NMAUrbanMobilityProvider whose services are affected by this alert.
 *
 * \return affected provider.
 */
@property (nonatomic, readonly) NMAUrbanMobilityProvider *provider;

/**
 * Gets array of %NMAUrbanMobilityTransport objects affected by this alert.
 *
 * \return array of transports.
 */
@property (nonatomic, readonly) NSArray *transports;

/**
 * Gets alert information text to display to the user.
 * Information text is localized to the current device's language.
 * If given language is not supported, English version is returned.
 *
 * \return alert information text.
 */
@property (nonatomic, readonly) NSString *info;

/**
 * Gets the NSDate from which this alert is valid.
 *
 * \return valid from date.
 */
@property (nonatomic, readonly) NSDate *validFrom;

/**
 * Gets the NSDate until which this alert is valid.
 *
 * \return valid until date.
 */
@property (nonatomic, readonly) NSDate *validUntil;

/**
 * Gets the %NMAUrbanMobilityLink link to a web resource.
 */
@property (nonatomic, readonly) NMAUrbanMobilityLink *source;

/**
 * Gets a link to the image which, if present, should be displayed along with this alert.
 *
 * \return url (as NSString) to the image, or nil if not available.
 */
@property (nonatomic, readonly) NSString *imageUrl;

/**
 * Gets display text which, if present, should be displayed
 * next to image obtained by #imageUrl.
 *
 * \return display text, or nil if not available.
 */
@property (nonatomic, readonly) NSString *imageCaption;

@end
/** @}  */
