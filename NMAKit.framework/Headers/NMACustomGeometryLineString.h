/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

#import "NMACustomGeometry.h"

@class NMAMapPolyline;

/**
 * \addtogroup NMA_CLE NMA Custom Location Group
 * @{
 */


/**
 * Represents a line string custom geometry created and accessed through the
 * Custom Location Extension.
 *
 * \deprecated This class is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
DEPRECATED_ATTRIBUTE
@interface NMACustomGeometryLineString : NMACustomGeometry

/**
 * %NMAMapPolyline of this geometry.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2Geometry).
 */
@property (nonatomic, readonly) NMAMapPolyline *mapPolyline DEPRECATED_ATTRIBUTE;

@end

/**  @}  */
