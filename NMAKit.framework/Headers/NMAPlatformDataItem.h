/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAGeoCoordinates;

/**
 * \enum NMAPlatformDataConditionType
 * \brief Type of the condition entity as described by the CONDITION_TYPE field.
 */
typedef NS_ENUM(NSUInteger, NMAPlatformDataConditionType) {
    /** \brief Indicates that the condition type is undefined. */
    NMAPlatformDataConditionTypeUndefined                            = 0,

    /** \brief Indicates toll structure. */
    NMAPlatformDataConditionTypeTollStructure                        = 1,

    /** \brief Indicates construction status closed. */
    NMAPlatformDataConditionTypeConstructionStatusClosed             = 3,

    /** \brief Indicates gates. */
    NMAPlatformDataConditionTypeGates                                = 4,

    /** \brief Indicates direction of travel. */
    NMAPlatformDataConditionTypeDirectionOfTravel                    = 5,

    /** \brief Indicates restricted driving manoeuvre. */
    NMAPlatformDataConditionTypeRestrictedDrivingManoeuvre           = 7,

    /** \brief Indicates access restriction. */
    NMAPlatformDataConditionTypeAccessRestriction                    = 8,

    /** \brief Indicates special explication. */
    NMAPlatformDataConditionTypeSpecialExplication                   = 9,

    /** \brief Indicates special speed situation. */
    NMAPlatformDataConditionTypeSpecialSpeedSituation                = 10,

    /** \brief Indicates variable speed sign. */
    NMAPlatformDataConditionTypeVariableSpeedSign                    = 11,

    /** \brief Indicates usage fee required. */
    NMAPlatformDataConditionTypeUsageFeeRequired                     = 12,

    /** \brief Indicates lane traversal. */
    NMAPlatformDataConditionTypeLaneTraversal                        = 13,

    /** \brief Indicates through route. */
    NMAPlatformDataConditionTypeThroughRoute                         = 14,

    /** \brief Indicates traffic signal. */
    NMAPlatformDataConditionTypeTrafficSignal                        = 16,

    /** \brief Indicates traffic sign. */
    NMAPlatformDataConditionTypeTrafficSign                          = 17,

    /** \brief Indicates railway crossing. */
    NMAPlatformDataConditionTypeRailwayCrossing                      = 18,

    /** \brief Indicates no overtaking. */
    NMAPlatformDataConditionTypeNoOvertaking                         = 19,

    /** \brief Indicates junction view. */
    NMAPlatformDataConditionTypeJunctionView                         = 20,

    /** \brief Indicates protected overtaking. */
    NMAPlatformDataConditionTypeProtectedOvertaking                  = 21,

    /** \brief Indicates evacuation route. */
    NMAPlatformDataConditionTypeEvacuationRoute                      = 22,

    /** \brief Indicates transport access restriction. */
    NMAPlatformDataConditionTypeTransportAccessRestriction           = 23,

    /** \brief Indicates transport special speed situation. */
    NMAPlatformDataConditionTypeTransportSpclSpdSituation            = 25,

    /** \brief Indicates transport restricted driving manoeuvre. */
    NMAPlatformDataConditionTypeTransportRdm                         = 26,

    /** \brief Indicates transport preferred route. */
    NMAPlatformDataConditionTypeTransportPreferredRoute              = 27,

    /** \brief Indicates calculated restricted driving manoeuvre. */
    NMAPlatformDataConditionTypeCalculatedRestrictedDrivingManoeuvre = 30,

    /** \brief Indicates parking information. */
    NMAPlatformDataConditionTypeParkingInformation                   = 31,

    /** \brief Indicates environmental zone. */
    NMAPlatformDataConditionTypeEnvironmentalZone                    = 34,

    /** \brief Indicates blackspot. */
    NMAPlatformDataConditionTypeBlackspot                            = 38,

    /** \brief Indicates permitted driving manoeuvre. */
    NMAPlatformDataConditionTypePermittedDrivingManoeuvre            = 39,

    /** \brief Indicates variable speed limit. */
    NMAPlatformDataConditionTypeVariableSpeedLimit                   = 40,

    /** \brief Indicates short construction warning. */
    NMAPlatformDataConditionTypeShortConstructionWarning             = 41
};

/**
 * \enum NMAPlatformDataVehicleType
 * \brief The possible vehicle types that are allowed on a link
 *        as described by the VEHICLE_TYPES field.
 */
typedef NS_OPTIONS(NSUInteger, NMAPlatformDataVehicleType) {
    /** \brief Indicates that the vehicle type is undefined. */
    NMAPlatformDataVehicleTypeUndefined         = 0,

    /** \brief Indicates automobiles. */
    NMAPlatformDataVehicleTypeAutomobiles       = 1 << 0,

    /** \brief Indicates buses. */
    NMAPlatformDataVehicleTypeBuses             = 1 << 1,

    /** \brief Indicates taxis. */
    NMAPlatformDataVehicleTypeTaxis             = 1 << 2,

    /** \brief Indicates car pools. */
    NMAPlatformDataVehicleTypeCarPools          = 1 << 3,

    /** \brief Indicates pedestrians. */
    NMAPlatformDataVehicleTypePedestrians       = 1 << 4,

    /** \brief Indicates trucks. */
    NMAPlatformDataVehicleTypeTrucks            = 1 << 5,

    /** \brief Indicates deliveries. */
    NMAPlatformDataVehicleTypeDeliveries        = 1 << 6,

    /** \brief Indicates emergency vehicles. */
    NMAPlatformDataVehicleTypeEmergencyVehicles = 1 << 7,

    /** \brief Indicates through traffic.*/
    NMAPlatformDataVehicleTypeThroughTraffic    = 1 << 8,

    /** \brief Indicates motorcycles. */
    NMAPlatformDataVehicleTypeMotorcycles       = 1 << 9,

    /** \brief Indicates road trains. */
    NMAPlatformDataVehicleTypeRoadTrains        = 1 << 10,

    /** \brief Indicates all the above types. */
    NMAPlatformDataVehicleTypeAll               = 0x7FF
};

/**
 * \addtogroup NMA_PDE NMA Platform Data Extensions Group
 * @{
 */


/**
 * After a Platform Data Request run, each layer data is returned with objects
 * of this class. The properties are just helper shortcuts for the most used fields.
 * That is for any data not accessed via the properties, the users can access them
 * directly. For example, assume item is an object of NMAPlatformDataItem type.
 * Then, item[@"COUNTRY_ID"] returns the COUNTRY_ID string and nil if the item has
 * no such data.
 */
@interface NMAPlatformDataItem : NSObject <NSCopying>

/**
 * Do not directly instantiate an instance of this class.
 */
- (instancetype)init NS_UNAVAILABLE;

#pragma mark - Shortcut properties

/**
 * LINK_ID data.
 * <p>Positive 64 bit Integer that globally identifies the road, carto or buildin footprint link,
 * also across map releases. Link IDs are never reused.</p>
 *
 * <p>However, it is returned as an NString object. It can be converted to numeric value via the
 * [NString longLongValue] method.</p>
 *
 * \note It is nil if not set.
 */
@property (nonatomic, weak, readonly) NSString *linkId;

/**
 * LAT & LON data.
 *
 * <p>Latitude & longitude coordinates [10^-5 degree WGS84] along the polyline or of the reference node
 * and the non reference node. Comma separated. Each value is relative to the previous.</p>
 *
 * <p>The LAT & LON strings are converted into an NSArray of NMAGeoCoordinates.</p>
 *
 * \note It is nil if not set.
 */
@property (nonatomic, weak, readonly) NSArray<NMAGeoCoordinates*> *coordinates;

/**
 * LINK_LENGTH data.
 *
 * <p>The link length in meter. This attribute publishes the whole link length, no matter whether the link
 * spans across several tiles. The link length is computed by straight lines between subsequent shape
 * points, no splines or other smoothing functions or geodesic computations are used.</p>
 *
 * \note It is -1.0f if not set.
 */
@property (nonatomic, readonly) float linkLengthMeter;

/**
 * LINK_IDS data.
 *
 * <p>Comma separated list of Permanent link IDs that describe a route path. A negative sign means that
 * this link was driven towards reference node. If the list contains only one link, then a 'B' prefix
 * tells that it applies for both driving directions. Each link ID is a positive 64 bit Integer that
 * globally identifies the road link, also across map releases. Link IDs are never reused.</p>
 *
 * \note It is nil if not set.
 */
@property (nonatomic, weak, readonly) NSArray <NSString*> *linkIds;

/**
 * CONDITION_TYPE data.
 *
 * <p>Type of the condition entity.</p>
 *
 * \note It is NMAPlatformDataConditionTypeUndefined if not set.
 *
 * \sa NMAPlatformDataConditionType
 */
@property (nonatomic, readonly) NMAPlatformDataConditionType conditionType;

/**
 * VEHICLE_TYPES data.
 *
 * <p>Access Characteristics identify the vehicle types that are allowed on a link, allowed on a lane or to which
 * condition applies.</p>
 * <p>16 bit bitmask of affected vehicle types or functions. Sum of: Automobiles (1), buses (2), taxis (4), car
 * pools (8), pedestrians (16), trucks (32), deliveries (64), emergency vehicles (128), through traffic (256),
 * motorcycles (512) and road trains (1024).</p>
 *
 * \note It is NMAPlatformDataVehicleTypeUndefined if not set.
 *
 * \sa NMAPlatformDataVehicleType
 */
@property (nonatomic, readonly) NSUInteger vehicleTypes;

/**
 * DTM_AVG_HEIGHT data.
 *
 * <p>The average height [cm above WGS84 ellipsoid] along the link.</p>
 *
 * \note It is NSIntegerMax if not set.
 */
@property (nonatomic, readonly) NSInteger averageHeightCm;

#pragma mark - Methods

/**
 * Returns the data associated with a given key.
 *
 * \param key The key for the data requested.
 *
 * \return The associated data or nil if the key isn't
 *         found in result.
 */
- (NSString *)objectForKeyedSubscript:(NSString *)key;

/**
 * Returns all the key strings.
 *
 * \return The array of all the keys.
 */
- (NSArray<NSString *> *)allKeys;

/**
 * Returns the all the value strings.
 *
 * \return All the value strings contained in the item.
 */
- (NSArray<NSString *> *)allValues;

/**
 * Applies a given block object to the entries in the item.
 */
- (void)enumerateKeysAndObjectsUsingBlock:(void (^)(NSString *key, NSString *obj, BOOL *stop))block;

/**
 * Extracts the data contained into a dictionary.
 *
 * \return A dictionary of the string key/value pairs.
 */
- (NSDictionary<NSString *, NSString *> *)extract;

/**
 * The number of entries in the item.
 */
@property (readonly) NSUInteger count;

@end

/**  @}  */
