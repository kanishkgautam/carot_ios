/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */

/**
 * Type of the link as enum for convinient use.
 * The allowed values are:
 */
typedef NS_ENUM(NSUInteger, NMAUrbanMobilityLinkType) {
    /** \brief Link type agency: Operator information URL. */
    NMAUrbanMobilityLinkTypeAgency      = 0,
    /** \brief Link type logo: Operator logo URL. */
    NMAUrbanMobilityLinkTypeAgencyLogo  = 1,
    /** \brief Link type tariff: Tariff information URL. */
    NMAUrbanMobilityLinkTypeTariff      = 2,
    /** \brief Link type website: Website URL. */
    NMAUrbanMobilityLinkTypeWebsite     = 3,
    /** \brief Link type booking: Booking URL. */
    NMAUrbanMobilityLinkTypeBooking     = 4,
    /** \brief Link type alert: Alert URL. */
    NMAUrbanMobilityLinkTypeAlert       = 5,
    /** \brief Link type unknown. */
    NMAUrbanMobilityLinkTypeUnknown     = 6
};


/**
 * \class NMAUrbanMobilityLink NMAUrbanMobilityLink.h "NMAUrbanMobilityLink.h"
 *
 * Represents a link which can be associated with operator, transit alert or a ticket.
 * For example, a link to the transit operator's website.
 * This information must be exposed in the client application.
 *
 * IMPORTANT: Urban Mobility routing is a Beta feature. The related classes are subject to change
 * without notice.
 *
 * \sa NMAUrbanMobilityOperator
 */

@interface NMAUrbanMobilityLink : NSObject
/**
 * Gets the NMAUrbanMobilityLink text.
 */
@property (nonatomic, readonly) NSString *text;

/**
 * Gets the NSString representation of the NMAUrbanMobilityLink url.
 */
@property (nonatomic, readonly) NSURL *url;

/**
 * Gets clickable part of text. If not available (empty string) the full text need to be
 * clickable.
 * For example if \property text returns "Lorem ipsum dolor sit amet,
 * consectetur adipiscing elit." and \property urlText returns "Lorem ipsum" then only
 * "Lorem ipsum" part should be clickable.
 * \return clickable part of text, or nil if not available.
 */
@property (nonatomic, readonly) NSString *urlText;

/**
 * Gets the NMAUrbanMobilityLink type as  %NMAUrbanMobilityLinkType , i.e. is it url to agency web-site or tariff information url.
 */
@property (nonatomic, readonly) NMAUrbanMobilityLinkType linkType;

@end
/** @}  */
