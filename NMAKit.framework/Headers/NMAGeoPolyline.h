/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>
@class NMAGeoCoordinates;

NS_ASSUME_NONNULL_BEGIN

/**
 * \addtogroup NMA_Common  NMA Common Group
 * @{
 * \class NMAGeoPolyline NMAGeoPolyline.h "NMAGeoPolyline.h"
 *
 * Object representing a Polyline geometry, which consists of two or more points.
 * This class can be used to manipulate a polyline geometry along with %NMAMapPolyline for map
 * rendering.
 *
 */
@interface NMAGeoPolyline : NSObject

/**
 * Initializes an %NMAGeoPolyline instance with the specified %NMAGeoCoordinates.
 *
 * \param coordinates An NSArray containing two or more %NMAGeoCoordinates.
 * \return The %NMAGeoPolyline instance, or nil if initialization failed
 */
- (nullable instancetype)initWithCoordinates:(NSArray<NMAGeoCoordinates *> *)coordinates;

/**
 * Add a new point to the polyline.
 *
 * \param coordinate point to be added given as %NMAGeoCoordinates
 */
- (void)addPoint:(NMAGeoCoordinates *)coordinate;

/**
 * Adds a list of points to the polyline.
 *
 * \param coordinates NSArray containing the list of %NMAGeoCoordinates
 */
- (void)addPoints:(NSArray<NMAGeoCoordinates *> *)coordinates;

/**
 * Insert a point to the polyline at the specified index
 *
 * \param coordinate point to be added given as %NMAGeoCoordinates
 * \param index position of the point along the polyline
 */
- (void)insertPoint:(NMAGeoCoordinates *)coordinate atIndex:(int)index;

/**
 * Remove a point from the polyline with the specified index
 *
 * \param index position of the point along the polyline
 */
- (void)removePointAtIndex:(int)index;

/**
 * Remove all points from the polyline
 */
- (void)clear;

/**
 * Get the total number of points that currently exists in the polyline
 */
- (int)getNumberOfPoints;

/**
 * Get the point from the specified index of this polyline.
 * \param index position of the point along the polyline
 * \return The %NMAGeoCoordinates instance, or nil if failed
 */
- (nullable NMAGeoCoordinates *)getPointAtIndex:(int)index;

/**
 * Get an NSArray with all points of the polyline.
 */
- (NSArray<NMAGeoCoordinates *> *)getAllPoints;

/**
 * Return the geographical length of the polyline
 */
- (double)getLength;

/**
 * Gets the %NMAGeoCoordinates (point) along the path of the polyline that is closest to the specified point.
     *
 * \param coordinate the origin point to search from given in %NMAGeoCoordinates.
 */
- (NMAGeoCoordinates *)getNearestCoordinateFrom:(NMAGeoCoordinates *)coordinate;

/**
 * Gets the point index along the path of the polyline that is closest to the specified point.
 *
 * \param coordinate the origin point to search from given in %NMAGeoCoordinates.
 */
- (NSUInteger)getNearestIndexFromCoordinate:(NMAGeoCoordinates *)coordinate;

@end
/** @}  */

NS_ASSUME_NONNULL_END
