/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

/**
 * \addtogroup NMA_CLE NMA Custom Location Group
 * @{
 */


/**
 * Encapsulates the response to a custom location request made using
 * the NMACustomLocationManager.
 *
 * \sa NMACustomLocationManager
 *
 * \deprecated This class is deprecated as of NMA SDK 3.3. Please use CLE2 %NMACLE2Result instead.
 */
DEPRECATED_ATTRIBUTE
@interface NMACustomLocationResponse : NSObject

/**
 * The name of the layer on which locations were requested.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use %NMACLEResult instead.
 */
@property (nonatomic, readonly) NSString *layerName DEPRECATED_ATTRIBUTE;

/**
 * The Id of the layer on which locations were requested.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use %NMACLEResult instead.
 */
@property (nonatomic, assign, readonly) unsigned int layerId DEPRECATED_ATTRIBUTE;

/**
 * The maximum number of location results which were requested.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use %NMACLEResult instead.
 */
@property (nonatomic, assign, readonly) unsigned int resultLimit DEPRECATED_ATTRIBUTE;

/**
 * The array of NMACustomLocation objects found by the request.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use %NMACLEResult instead.
 */
@property (nonatomic, readonly) NSArray *locations DEPRECATED_ATTRIBUTE;

/**
 * The array of NMACustomGeometry objects found by the request.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use %NMACLEResult instead.
 */
@property (nonatomic, readonly) NSArray *geometries DEPRECATED_ATTRIBUTE;

@end
/**  @}  */
