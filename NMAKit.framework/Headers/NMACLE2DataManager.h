/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMACLE2Request.h"
#import "NMACLE2ResultListener.h"

NS_ASSUME_NONNULL_BEGIN

@class NMAGeoBoundingBox;

/**
 * \addtogroup NMA_CLE2 NMA Custom Location Extension 2 Group
 * @{
 */

/**
 * A typedef of a block parameter signature used with \link NMACLE2DataManager::downloadLayer:completionHandler:\endlink.
 *
 * The block will be called on the main queue.
 *
 * \param layerId The id of a layer being downloaded.
 * \param error The error if the download failed, or nil if it was successful.
 */
typedef void (^NMACLE2LayerDownloadCompletionBlock)(NSString* layerId, NSError *__nullable error);

/**
 * \class NMACLE2DataManager NMACLE2DataManager.h "NMACLE2DataManager.h"
 *
 * This class is responsible for managing layer data for CLE2 offline use.
 *
 * To download a full layer, call downloadLayer:completionHandler: method.
 * To store a subset of geometries, create a {@link NMACLE2Request} and set its
 * cachingEnabled property to true.
 * Multiple layers are supported. To download another layer, simply call the
 * downloadLayer:completionHandler: method with the next layer to be downloaded.
 *
 * \note NMACLE2DataManager is a singleton which must be obtained using the
 * {@link sharedManager} method.
 */
@interface NMACLE2DataManager : NSObject

/*!
 * Returns the %NMACLE2DataManager singleton instance.
 *
 * This method returns a nil object if access to this operation is denied.
 * Contact your HERE representative for more information.
 *
 * \note Use this method to obtain a %NMACLE2DataManager instance. Do not call
 * init directly.
 *
 * \return shared %NMACLE2DataManager instance.
 */
+ (nullable instancetype)sharedManager;

/*!
 * Request the full download of one specific layer.
 *
 * \param layerId Name of the layer specifying the layer to download.
 * \param completionHandler Specify the block to be executed upon completion.
 *
 * \note NSError is nil if the operation succeeded. It is possible to start
 * simultaneous downloads as long as a different layer id is provided.
 *
 */
- (void)downloadLayer:(NSString *)layerId
    completionHandler:(NMACLE2LayerDownloadCompletionBlock)completionHandler;

/*!
 * Cancel all ongoing layer downloads that were started with
 * downloadLayer:completionHandler:
 * Completion handlers will return with %NMACLE2ErrorCancelled if the request was
 * cancelled.
 *
 * \note An ongoing download might still succeed depending on when cancel
 *  is requested, e.g.: if it was already near completion.
 */
-(void)cancelAllLayerDownloads;

/*!
 * Cancels a specific layer download that is still in progress. Outstanding
 * download completion handlers will be called.
 *
 * \param layerId Name of the layer specifying which download should stop.
 * \return NSError is nil if the operation succeeded. NMACLE2ErrorInvalidParameter
 * if no download for the layer is in progress.
 *
 * \note An ongoing download might still succeed depending on when cancel
 *  is requested, such as if it was already near completion.
 */
-(NSError* )cancelLayerDownload:(NSString*) layerId;

/*!
 * Delete one specific layer from the local storage.
 *
 * \param layerId ID of the layer specifying the layer to download.
 *
 * \note NSError is nil if the operation succeeded.
 *
 */
-(nullable NSError *)deleteLayer:(NSString *)layerId;

/*!
 * Removes all data from the local storage.
 *
 * \note NSError is nil if the operation succeeded.
 *
 */
-(nullable NSError *)deleteAll;

/*!
 * Retrieves the total number of geometries stored locally.
 *
 * Includes geometries that were cached when a request is made with cacheEnabled
 * plus all geometries downloaded with downloadLayer:completionHandler: method.
 *
 * \param error Reference error object to get the error if any.
 *
 * \note The return value is nil the operation failed; See %NMACLE2Request for possible
 * error codes (%NMACLE2Error).
 *
 */
-(nullable NSNumber *)numberOfStoredGeometriesOnError:(NSError * _Nullable * _Nullable)error;

/*!
 * Retrieves the total number of geometries stored locally in a specific layer.
 *
 * Includes geometries that were cached when a request is made with cacheEnabled
 * plus all geometries downloaded with downloadLayer:completionHandler: method.
 *
 * \param layerId The layer id to be queried.
 * \param error Reference error object to get the error if any.
 *
 * \note The return value is nil the operation failed; See %NMACLE2Request for possible
 * error codes (%NMACLE2Error).
 *
 */
-(NSNumber *)numberOfStoredGeometriesinLayer:(NSString*)layerId onError:(NSError * _Nullable * _Nullable)error;

@end

/**  @}  */

NS_ASSUME_NONNULL_END
