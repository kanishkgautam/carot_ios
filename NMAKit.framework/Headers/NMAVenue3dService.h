/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAGeoCoordinates;
@class NMAGeoBoundingBox;
@class NMAVenue3dService;
@class NMAVenue3dVenue;
@class NMAVenue3dVenueInfo;

/**
 * \addtogroup NMA_VenueMaps3d NMA Venue Maps 3D Group
 * @{
 */

/**
 * Represents values that describe the initialization status of the {@link NMAVenue3dService}
 *
 */
typedef NS_ENUM(NSUInteger, NMAVenue3dServiceVenueLoadStatus) {
    /**
     * The venue service successfully downloaded the venue.
     */
    NMAVenue3dServiceVenueLoadStatusOnlineSuccess,

    /**
     * The venue service successfully returned venue from cache.
     */
    NMAVenue3dServiceVenueLoadStatusOfflineSuccess,

    /**
     * The venue service failed to deliver the venue. The details why venue service failed is unknown.
     */
    NMAVenue3dServiceVenueLoadStatusFailed,
};

/**
 * Represents values that describe the initialization status of the {@link NMAVenue3dService}
 *
 */
typedef NS_ENUM(NSUInteger, NMAVenue3dServiceInitializationStatus) {
    /**
     * The venue service is ready to be used.
     */
    NMAVenue3dServiceInitializationStatusOnlineSuccess,

    /**
     * The NMAVenue3dService failed to authenticate on the server, but
     * successfully initialized previously cached data.
     */
    NMAVenue3dServiceInitializationStatusOfflineSuccess,

    /**
     * The NMAVenue3dService failed to authenticate on the server and
     * there is no previously cached data.
     */
    NMAVenue3dServiceInitializationStatusAuthenticationFailed,

    /**
     * The NMAVenue3dService failed to initialize styles.
     */
    NMAVenue3dServiceInitializationStatusStyleInitializationFailed,

    /**
     * The NMAVenue3dService failed to initialize icons.
     */
    NMAVenue3dServiceInitializationStatusIconInitializationFailed,

    /**
     * The NMAVenue3dService failed to initialize index file.
     */
    NMAVenue3dServiceInitializationStatusIndexInitializationFailed,

    /**
     * The NMAVenue3dService failed to authenticate on the server. If
     * NMAVenue3dServiceInitializationStatusOfflineSuccess has been previously
     * received, the previously cached data is in use. Otherwise NMAVenue3dService
     * failed to initialise.
     */
    NMAVenue3dServiceInitializationStatusOnlineFailed,

    /**
     * The initialization wasn't started.
     */
    NMAVenue3dServiceInitializationStatusNotStarted,

    /**
     * The initialization in progress.
     */
    NMAVenue3dServiceInitializationStatusInProgress,

    /**
     * The NMAVenue3dService is locked with the method
     * \link NMAVenue3dService::stopAndClearCacheWithCallback:\endlink .
     *
     * NMAVenue3dService can't be started until
     * status will be changed to NMAVenue3dServiceInitializationStatusNotStarted.
     */
    NMAVenue3dServiceInitializationStatusLocked
};

/**
 * \brief Represents a interface that offers listeners and callback methods related to
 * getting venue.
 *
 * \note Methods of this protocol are called on the main queue.
 *
 */
@protocol NMAVenue3dServiceListener<NSObject>

@optional

/**
 * On NMAVenue3dService initialization callback.
 *
 * @param venueService
 *            The {@link NMAVenue3dService} object initialized.
 * @param result
 *            The {@link NMAVenue3dServiceInitializationStatus} enum which represent current status of
 *            the service.
 */
- (void)venueServiceDidInitialize:(NMAVenue3dService *)venueService withResult:(NMAVenue3dServiceInitializationStatus)result;

/**
 * On NMAVenue3dVenue loading completed callback.
 *
 *
 * If the venue exists in the cache, and there is an updated version available online,
 * this callback function is called two times. You can compare object pointers or unique
 * venue Ids to find out if the venue is an updated version of an already cached (from before) venue.
 *
 * @param venueService
 *            The {@link NMAVenue3dService} object returns venue.
 * @param venue
 *            The loaded {@link NMAVenue3dVenue}.
 *
 * \deprecated This is deprecated since release 3.3.
 * Please use didGetVenue:withVenueInfo:withStatus:
 */
- (void)venueService:(NMAVenue3dService *)venueService didGetVenue:(NMAVenue3dVenue *)venue DEPRECATED_ATTRIBUTE;

/**
 * On NMAVenue3dVenue loading completed callback.
 *
 * If the venue exists in the cache, and there is an updated version available online,
 * this callback function is called two times. You can compare object pointers or unique
 * venue Ids to find out if the venue is an updated version of an already cached (from before) venue.
 *
 * @param venueService
 *            The {@link NMAVenue3dService} object returns venue.
 * @param venueInfo
 *            The {@link NMAVenue3dVenueInfo} object related to requested {@link NMAVenue3dVenue}.
 * @param venue
 *            The loaded {@link NMAVenue3dVenue} or nil if the venue loading fails.
 * @param status
 *            The {@link NMAVenue3dServiceVenueLoadStatus} of the {@link NMAVenue3dVenue}.
 */
- (void)venueService:(NMAVenue3dService *)venueService didGetVenue:(NMAVenue3dVenue *)venue
                                                     withVenueInfo:(NMAVenue3dVenueInfo *) venueInfo
                                                        withStatus:(NMAVenue3dServiceVenueLoadStatus) status;
@end

/**
 * \brief Represents a interface that offers listeners and callback methods related to
 * stopping venue service.
 *
 * \note Methods of this protocol are called on the main queue.
 *
 */
@protocol NMAVenue3dServiceStopListener<NSObject>

@required

/**
 * On NMAVenue3dService stopped completed callback.
 *
 * This callback is called when NMAVenue3dService has been stopped using
 * \link NMAVenue3dService::stopWithCallback:\endlink
 * method and no more venues are being loaded.
 */
- (void)didStopVenueService:(NMAVenue3dService *)venueService;

@end

/**
 * \brief Represents a interface that offers listeners and callback methods related to
 * clearing cache of venue service.
 *
 * \note Methods of this protocol are called on the main queue.
 *
 */
@protocol NMAVenue3dServiceClearCacheListener<NSObject>

@required

/**
 * On NMAVenue3dService clear cache completed callback.
 *
 * This callback is called when NMAVenue3dService has cleared its cache using
 * \link NMAVenue3dService::stopAndClearCacheWithCallback:\endlink method.
 */
- (void)didClearCache;

@end


/**
 * \class NMAVenue3dService NMAVenue3dService.h "NMAVenue3dService.h"
 *
 * \brief NMAVenue3dService offers methods to search for venues and to get NMAVenue3dVenue objects based on search.
 *
 * Use of this object does not necessitate {@link NMAMapView} involvement.
 * <p>
 * This class can not be instantiated directly. Use \link NMAVenue3dService::sharedVenueService\endlink method
 * to get the singeton instance of the class instead.
 * </p>
 */
@interface NMAVenue3dService : NSObject

/**
 * \brief Returns a new additional %NMAVenue3dService instance.
 *
 * \note Use this method to obtain a new additional %NMAVenue3dService instance.
 * \note The returned object isn't related to the shared singleton %NMAVenue3dService instance.
 */
- (instancetype)initAdditionalVenueService;

/**
 * \brief Returns the singleton %NMAVenue3dService instance.
 *
 * \note Use this method to obtain a shared singleton %NMAVenue3dService instance.
 */
+ (instancetype)sharedVenueService;

/**
 * The venue service initialization status.
 */
@property (nonatomic, readonly) NMAVenue3dServiceInitializationStatus initializationStatus;

/**
 * Sets a HERE account token. In case of valid token and if private content
 * is used, {@c VenueService} will be using private bucket of a HERE account
 * instead of private bucket of the app.
 *
 * @param token a HERE account token.
 */
- (void)setHereAccountToken:(NSString *)token;

/**
 * Add a listener to the venue service. The listener must implement the NMAVenue3dServiceListener protocol.
 * The listener monitors the venue service initialization status and a venue loaded completion event.
 *
 * @param listener the NMAVenue3dServiceListener object to be added
 *
 * \sa NMAVenue3dServiceListener
 */
- (void)addListener:(id<NMAVenue3dServiceListener>)listener;

/**
 * Remove a listener from the venue service. The listener must implement the NMAVenue3dServiceListener protocol.
 * The listener monitors the venue service initialization status and a venue loaded completion event.
 *
 * @param listener the NMAVenue3dServiceListener object to be removed.
 */
- (void)removeListener:(id<NMAVenue3dServiceListener>)listener;

/**
 * A value indicating whether the HERE SDK or the HERE private 3D venue content is
 * to be used. The HERE SDK content is used by default.
 * <p>
 * This property needs to be set before \link NMAVenue3dService::start\endlink.
 * If NMAVenue3dService is already running, stop it first using
 * \link NMAVenue3dService::stopAndClearCacheWithCallback:\endlink or \link NMAVenue3dService::stopWithCallback:\endlink,
 * and then after calling this function, start it again using
 * \link NMAVenue3dService::start\endlink.
 * </p>
 */
@property (nonatomic) BOOL privateContent;

/**
 * Defines whether a normal or a test backend is used. The normal backend is used by default.
 * <p>
 * This property needs to be set before \link NMAVenue3dService::start\endlink.
 * If NMAVenue3dService is already running, stop it first using
 * \link NMAVenue3dService::stopAndClearCacheWithCallback:\endlink or \link NMAVenue3dService::stopWithCallback:\endlink,
 * and then after calling this function, start it again using
 * \link NMAVenue3dService::start\endlink.
 * </p>
 */
@property (nonatomic) BOOL testEnv;

/**
 * A value indicating whether the HERE SDK and the HERE private 3D venue content will
 * be used together. By default combined content is not used.
 * <p>
 * If <code>NMAVenue3dService</code> is using private content, then data from the private content
 * will be used as primary source, and HERE SDK data as an alternative one. Otherwise the HERE SDK
 * content will be primary source of data and the private content will be an alternative one.
 * This property needs to be set before \link NMAVenue3dService::start NMAVenue3DService::start\endlink.
 * If NMAVenue3dService is already running, stop it first using
 * \link NMAVenue3dService::stopAndClearCacheWithCallback:\endlink or \link NMAVenue3dService::stopWithCallback:\endlink,
 * and then after calling this function, start it again using
 * \link NMAVenue3dService::start\endlink.
 * </p>
 */
@property (nonatomic) BOOL combinedContent;

/**
 * Starts NMAVenue3dService asynchronously. The method will do nothing if NMAVenue3dService is already
 * initialized with status {@link NMAVenue3dServiceInitializationStatusOnlineSuccess}.
 * <p>
 * An initialization status is returned to objects registered as {@link NMAVenue3dServiceListener} to NMAVenue3dService.
 * </p>
 */
- (void)start;

/**
 * Asynchronously gets the {@link NMAVenue3dVenue} object that is specified by the given {@link NMAVenue3dVenueInfo} object.
 * <p>
 * Venue service listeners will be notified when the venue is loaded via the [NMAVenue3dServiceListener venueService:didGetVenue] method.
 * </p>
 *
 * \param venueInfo The %NMAVenue3dVenueInfo object specifying venue to be get.
 * \sa NMAVenue3dVenueInfo:
 */
- (void)getVenueWithInfo:(NMAVenue3dVenueInfo *)venueInfo;

/**
 * Asynchronously gets the {@link NMAVenue3dVenue} objects that are specified by the given list of {@link NMAVenue3dVenueInfo} objects.
 * <p>
 * Venue service listeners will be notified when a venue is loaded via the [NMAVenue3dServiceListener venueService:didGetVenue] method.
 * </p>
 *
 * \param venueInfoArray An array of %NMAVenue3dVenueInfo objects specifying venues to get.
 * \sa NMAVenue3dVenueInfo:
 */
- (void)getVenuesWithInfoArray:(NSArray *)venueInfoArray;

/**
 * Gets venue info object for the venue identifier.
 *
 * \param venueId venue id
 * \return Venue info object for the venue identifier
 *
 * \sa \link getVenueWithInfo:\endlink
 */
- (NMAVenue3dVenueInfo *)venueWithId:(NSString *)venueId;

/**
 * Searches for venues inside the given <code>NMAGeoBoundingBox</code>.
 *
 * \param boundingBox The area in which to search for venues.
 * \return An array of NMAVenue3dVenueInfo objects representing the venues found inside the specified area.
 *
 * \sa NMAVenue3dVenueInfo
 */
- (NSArray *)venuesInGeoBoundingBox:(NMAGeoBoundingBox *)boundingBox;

/**
 * Searches for venues at the given <code>NMAGeoCoordinates</code>.
 *
 * \param coordinates The location around which to search for venues.
 * \return An array of NMAVenue3dVenueInfo objects representing the venues found near the specified location.
 *
 * \sa NMAVenue3dVenueInfo
 */
- (NSArray *)venuesAtGeoCoordinates:(NMAGeoCoordinates *)coordinates;

/**
 * Searches for a venue closest to the given <code>NMAGeoCoordinates</code>.
 *
 * \param coordinates The location around which to search for venues.
 *
 * \return The closet NMAVenue3dVenueInfo object to the specified point or nil if there is no any.
 *
 * \sa NMAVenue3dVenueInfo
 */
- (NMAVenue3dVenueInfo *)venueAtGeoCoordinates:(NMAGeoCoordinates *)coordinates;

/**
 * Searches for venues near the given <code>NMAGeoCoordinates</code>.
 *
 * \param coordinates The location around which to search for venues.
 * @param radius     The radius in meters where to search venues.
 * \return The array of NMAVenue3dVenueInfo objects found near the specified point
 *
 * \sa NMAVenue3dVenueInfo
 */
- (NSArray *)venuesAtGeoCoordinates:(NMAGeoCoordinates *)coordinates radius:(float)radius;

/**
 * Searches for a venue closest to the given <code>NMAGeoCoordinates</code> within a radius.
 *
 * \param coordinates The location around which to search for venues.
 * \param radius      The radius in meters where to search venues.
 *
 * \return The closet NMAVenue3dVenueInfo object to the specified point or nil if there is no any
 *
 * \sa NMAVenue3dVenueInfo
 */
- (NMAVenue3dVenueInfo *)venueAtGeoCoordinates:(NMAGeoCoordinates *)coordinates radius:(float)radius;

/**
 * Stops NMAVenue3dService asynchronously. \link NMAVenue3dServiceStopListener::didStopVenueService:\endlink
 * callback will be called and this callback also indicates that no more Venue loading
 * is happening anymore.
 * Use \link NMAVenue3dService::start\endlink to start the service again.
 *
 * \param stopListener Notification callback.
 */
- (void)stopWithCallback:(id<NMAVenue3dServiceStopListener>)stopListener;

/**
 * Clears cached data including all downloaded venues. All running instances of
 * venue service will be stopped asynchronously and locked ( \link initializationStatus\endlink
 * will return NMAVenue3dServiceInitializationStatusLocked during the operation).
 * After the cache is cleared, NMAVenue3dService needs to be started again using the
 * \link NMAVenue3dService::start\endlink method.
 *
 * \param listener Notification callback.
 */
+ (void)stopAndClearCacheWithCallback:(id<NMAVenue3dServiceClearCacheListener>)listener;

@end
/** @} */
