/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

@class NMAUrbanMobilityStation;
@class NMAUrbanMobilityAddress;
@class NMAUrbanMobilityAccessPoint;
@class NMAGeoCoordinates;
@class NMAUrbanMobilityPlace;
@class NMAUrbanMobilityRealTimeInfo;

/**
 * \addtogroup NMA_UrbanMobility NMA Urban Mobility Group
 * @{
 */


/**
 * \class NMAUrbanMobilityArrival NMAUrbanMobilityArrival.h "NMAUrbanMobilityArrival.h"
 *
 * Defines an arrival place.
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to
 * change without notice.
 *
 * \sa NMAUrbanMobilityStation
 * \sa NMAUrbanMobilityPlace
 * \sa NMAUrbanMobilityAccessPoint
 */
@interface NMAUrbanMobilityArrival : NSObject

/**
 * Arrival time.
 */
@property (nonatomic, readonly) NSDate *time;

/**
 * Arrival platform.
 */
@property (nonatomic, readonly) NSString *platform;

/**
 * Real-time data (time, platform).
 */
@property (nonatomic, readonly) NMAUrbanMobilityRealTimeInfo *realTimeInfo;

/**
 * Arrival %NMAUrbanMobilityStation information.
 * NOTE: This property is available only if arrival %NMAUrbanMobilityPlace is also a %NMAUrbanMobilityStation.
 *
 * \return  %NMAUrbanMobilityStation information or nil if not available.
 */
@property (nonatomic, readonly) NMAUrbanMobilityStation *station;

/**
 * Arrival %NMAUrbanMobilityPlace information.
 *
 * \return arrival %NMAUrbanMobilityPlace information or nil if not available.
 */
@property (nonatomic, readonly) NMAUrbanMobilityPlace *place;

/**
 * Access point which can be used to access the transit station which hosts this arrival.
 */
@property (nonatomic, readonly) NMAUrbanMobilityAccessPoint *accessPoint;
#ifdef HERE_SDK_INTERNAL
/**
 * Array of %NMAUrbanMobilityActivity required after arrival.
 * \return NSArray of %NMAUrbanMobilityActivity, or nil if none.
 */
@property (nonatomic, readonly) NSArray *activities;
#endif
@end
/** @}  */
