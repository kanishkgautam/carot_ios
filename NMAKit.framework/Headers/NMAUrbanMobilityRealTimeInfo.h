/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

typedef NS_ENUM(NSUInteger, NMAUrbanMobilityRealTimeInfoStatus) {
    /** \brief Is scheduled at the original arrival/departure time and at the given platform. */
    NMAUrbanMobilityRealTimeInfoStatusOk,

    /** \brief The line is not following the normal stops sequence. */
    NMAUrbanMobilityRealTimeInfoStatusRedirected,

    /** \brief User should take a replacement transport for this departure/arrival. */
    NMAUrbanMobilityRealTimeInfoStatusReplaced,

    /** \brief The service has a permanent failure and will not arrive and depart. */
    NMAUrbanMobilityRealTimeInfoStatusCancelled,

    /** \brief This is an additional not planned service. */
    NMAUrbanMobilityRealTimeInfoStatusAdditional
};


/**
 * \class NMAUrbanMobilityRealTimeInfo NMAUrbanMobilityRealTimeInfo.h "NMAUrbanMobilityRealTimeInfo.h"
 *
 * IMPORTANT: Urban Mobility is a Beta feature. The related classes are subject to change
 * without notice.
 */

@interface NMAUrbanMobilityRealTimeInfo : NSObject

/**
 * Gets the real-time departure time if available, nil otherwise.
 *
 * \return real-time departure time or nil if not available
 */
@property (nonatomic, readonly) NSDate *departureTime;

/**
 * Gets the real-time arrival time if available, nil otherwise.
 *
 * \return real-time arrival time or nil if not available.
 */
@property (nonatomic, readonly) NSDate *arrivalTime;

/**
 * Gets the real-time departure/arrival platform if available, nil otherwise.
 *
 * \return real-time departure/arrival platform or empty string if not available.
 */
@property (nonatomic, readonly) NSString *platform;

/**
 * An indicator for some exceptional event happened to this departure/arrival.
 * Possible values are members of %NMAUrbanMobilityRealTimeInfoStatus.
 *
 * \return Exception event, default is %NMAUrbanMobilityRealTimeInfoStatusOk.
 */
@property (nonatomic, readonly) NMAUrbanMobilityRealTimeInfoStatus status;

@end
