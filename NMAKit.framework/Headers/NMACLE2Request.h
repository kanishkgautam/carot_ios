/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import "NMACLE2ResultListener.h"

@class NMACLE2Request;
@class NMACLE2Result;

NS_ASSUME_NONNULL_BEGIN

/**
 * \addtogroup NMA_CLE2 NMA Custom Location Extension 2 Group
 *
 * The Custom Location Extensions 2 group provides classes and protocols
 * that support advanced custom location searches.
 *
 * @product nlp-hybrid-plus nlp-plus
 *
 * @{
 */

/**
 * The error domain for NMACLE2Request.
 */
FOUNDATION_EXPORT NSString * const kNMACLE2ErrorDomain;

/**
 * The possible NMACLE2Request error codes.
 */
typedef NS_ENUM(NSUInteger, NMACLE2Error) {
    NMACLE2ErrorNone,                           //Not an error
    NMACLE2ErrorInvalidParameter,               //Invalid parameter(s) provided
    NMACLE2ErrorInProgress,                     //Request already in progress
    NMACLE2ErrorCancelled,                      //The request was aborted
    NMACLE2ErrorNetworkCommunication,           //Network connection error
    NMACLE2ErrorOperationNotAllowed,            //Missing permission or license
    NMACLE2ErrorDataManagerFailed,              //Local storage request failed
    NMACLE2ErrorServerFailed,                   //Error from the server
    NMACLE2ErrorPartialSuccess,                 //Invalid geometries detected
    NMACLE2ErrorUnknown
};

/**
 * Specifies the geometry representation in the result.
 *
 * \note When a geometry is uploaded to CLE, they are tiled for fast look-up.
 * This property allows specifying whether the original uploaded geometry is
 * required (NMACLE2GeometryFull), or the if the geometry whithin the tile is
 * sufficient (NMACLE2GeometryLocal), or if no geometry information is needed
 * at all (NMACLE2GeometryNone). The later would be the use-case where it is only
 * interesting to know whether a geometry falls within the specified search, and
 * if results are found, what are its attributes (without retrieving the geometry
 *  information itself).
 */
typedef NS_ENUM(NSUInteger, NMACLE2GeometryType) {
    NMACLE2GeometryFull,
    NMACLE2GeometryLocal,
    NMACLE2GeometryNone
};

/**
 * Specifies the request mode
 *
 * NMACLE2ConnectivityModeOnline: Local storage will not be considered at all; Default behaviour.
 * NMACLE2ConnectivityModeOffline: Use local storage only
 * NMACLE2ConnectivityModeAutomatic: Attemps online and falls back to offline if failed.
 *
 * \note By default, the offline features are disabled therefore the local storage contains no data.
 * There are two ways to insert geometries in local storage to make them available for offline searches:
 * 1 - Enable caching when performing one or more requests (e.g., using the NMACLE2ProximityRequest).
 * 2 - Download one or more layers.
 */
typedef NS_ENUM(NSUInteger, NMACLE2ConnectivityMode) {
    NMACLE2ConnectivityModeOnline,
    NMACLE2ConnectivityModeOffline,
    NMACLE2ConnectivityModeAutomatic,
};

/**
 * A typedef of a block parameter signature used with \link NMARequest::startWithBlock:\endlink.
 *
 * \note The block will be called on the main queue.
 *
 * \param request The search request being completed.
 * \param result The search results. It can be nil if no results are found or an error
 * is encountered.
 * \param error The error if the request failed, or nil if the request was successful.
 */
typedef void (^NMACLE2RequestCompletionBlock)(NMACLE2Request *request, NMACLE2Result *result, NSError *__nullable error);

/**
 * \class NMACLE2Request NMACLE2Request.h "NMACLE2Request.h"
 *
 * Represents a base interface for a search request.
 */
@interface NMACLE2Request : NSObject

/*!
 * Specifies the geometry type given in the result.
 * If the request was executed in offline mode, this parameter will be ignored.
 * Caching (if enabled) is not performed if geometryType is "local".
 */
@property (readwrite, nonatomic) NMACLE2GeometryType geometryType;

/*!
 * Set or get how the query should be performed. See %NMACLE2ConnectivityMode.
 * If not set, the default is NMACLE2ConnectivityModeOnline
 */
@property (readwrite, nonatomic) NMACLE2ConnectivityMode connectivityMode;

/*!
 * Set if received geometries should be stored locally. Default is NO.
 */
@property (readwrite, nonatomic) BOOL cachingEnabled;

/*!
 * Specifies the filter(s) to be used for a custom location request. When specified, only
 * geometries where expression evaluates to true will be returned.
 *
 * \param query A JavaScript expression as a string being evaluated for each geometry.
 * \note An example query is:
 * <code>"RATING > 3 &amp;amp;&amp;amp; NAME != 'MyPlace23'"</code> where
 * <code> RATING </code> and <code> NAME</code> are column/field names.
 */
@property (readwrite, nonatomic) NSString* query;

/*!
 * Invokes an asynchronous query request with a specified listener.
 *
 * \param aSearchEventListener A listener to listen for search results
 * \return %NMACLE2ErrorInvalidParameter if no listener is passed, nil otherwise
 *
 * \sa \link startWithBlock:\endlink
 * \note NMARequestErrorOperationNotAllowed indicates access to this operation is denied. Contact
 * your HERE representative for more information.
 */
- (nullable NSError *)startWithListener:(id<NMACLE2ResultListener>)searchEventListener;

/*!
 * Invokes an asynchronous query request with a specified block.
 *
 * \param aBlock A block to be executed upon completion of the request (runs by default on main queue).
 * \return %NMACLE2ErrorInvalidParameter if no listener is passed, nil otherwise
 *
 * \sa NMACLE2RequestCompletionBlock
 * \sa \link startWithListener:\endlink
 * \note NMARequestErrorOperationNotAllowed indicates access to this operation is denied. Contact
 * your HERE representative for more information.
 *
 */
- (nullable NSError *)startWithBlock:(NMACLE2RequestCompletionBlock)block;

/*!
 * Cancels a previously triggered request execution. Cancelled requests will have their
 * completion handler or listener called with the respective NMACLE2Error.
 */
- (void)cancel;

@end

/**  @}  */

NS_ASSUME_NONNULL_END
