/*
 * Copyright © 2011-2016 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */

#import <Foundation/Foundation.h>

#import "NMACustomGeometry.h"

/**
 * \addtogroup NMA_CLE NMA Custom Location Group
 * @{
 */


/**
 * Represents a multiline string custom geometry created and accessed through the
 * Custom Location Extension.
 *
 * \deprecated This class is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2GeometryMultiPolyline).
 */
DEPRECATED_ATTRIBUTE
@interface NMACustomGeometryMultiLineString : NMACustomGeometry

/**
 * Array of %NMAMapPolyline in this geometry.
 *
 * \deprecated This property is deprecated as of NMA SDK 3.3. Please use CLE2 instead (%NMACLE2GeometryMultiPolyline).
 */
@property (nonatomic, readonly) NSArray *mapPolylines DEPRECATED_ATTRIBUTE;

@end

/**  @}  */
